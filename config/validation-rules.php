<?php

return [
    'customer' => [
        'title' => 'required|in:Mr,Miss,Mrs,Ms,Dr',
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'subscribe_to_newsletter' => 'boolean',
        'mobile' => 'required',
        'home_tel' => 'required',
        'work_tel' => '',
        'address_line_1' => 'required',
        'address_line_2' => '',
        'city' => 'required',
//        'county' => 'required',
        'country_id' => 'required|exists:countries,id',
        'post_code' => 'required',
        'hear_from' => 'string',
    ],

    'child' => [
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'email' => 'required|email|confirmed',
        'birthday' => 'required|date|min_age:' . config('restrictions.children.min_age'),
        'sex' => 'required|boolean',
        'relationship' => '',
        'school_code' => 'required|exists:schools,code',
        'year_in_school_id' => 'required|exists:years_in_school,id',
        'doctor_name' => 'required',
        'doctor_telephone_number' => 'required',
        'mobile' => 'required',
        'home_postcode' => 'required',
        'home_address' => 'required',
    ],

    'orders' => [
        'orders.*.child_id' =>  'required|exists:children,id',
        'orders.*.course_id' => 'required|exists:course_parts,id',
        'orders.*.products.*.id' => 'exists:products,id,active,1',
        'orders.*.products.*.count' => 'required|integer|min:1'
    ]
];