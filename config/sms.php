<?php

return [
    /**
     * Base url for sms api
     */
    'base_url' => env('SMS_BASE_URL', 'https://www.bulksms.co.uk/'),

    /**
     * Credentials data for authorization in sms api
     */
    'username' => env('SMS_USERNAME'),
    'password' => env('SMS_PASSWORD'),

    /**
     * Mode for sms api - test by default for debug, live - for production
     */
    'api_mode' => env('SMS_API_MODE', 'test'),
];