<?php

use App\Entities\Venue;
use Illuminate\Database\Seeder;

class VenuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Venue::create([
            'name' => 'test',
            'address_line_1' => 'new-yourk',
            'address_line_2' => 'new-yourk',
            'country' => 'us',
            'city' => 'test',
            'post_code' => '12345',
            'description' => 'test'
        ]);
    }
}
