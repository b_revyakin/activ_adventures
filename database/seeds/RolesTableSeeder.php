<?php

use App\Entities\Role;

class RolesTableSeeder extends \Illuminate\Database\Seeder
{
    protected $roles = [
        ['Admin', 'admin'],
        ['Staff', 'staff'],
        ['Customer', 'customer'],
        ['Site Manager', 'site.manager']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
            Role::create([
                'name' => $role[0],
                'slug' => $role[1]
            ]);
        }
    }
}
