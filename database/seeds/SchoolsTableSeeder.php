<?php

use App\Entities\School;
use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    protected $schools = [
        'School1' => 'code1',
        'School2' => 'code2',
        'School3' => 'code3',
        'School4' => 'code4',
        'School5' => 'code5',
        'School6' => 'code6',
        'School7' => 'code7',
        'School8' => 'code8',
        'School9' => 'code9',
        'School10' => 'code10',
        'School11' => 'code11',
        'School12' => 'code12',
        'School13' => 'code13',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->schools as $name => $code) {
            School::create([
                'name' => $name,
                'code' => $code
            ]);
        }
    }
}
