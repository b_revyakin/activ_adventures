<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    protected $categories = [
        'Medical Conditions' => 'Have you suffered from, or received treatment for, any of the following?',
        'Allergies' => 'Have you suffered from, or received treatment for, any of the following?',
        'Wellbeing and treatment' => '',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->categories as $title => $description) {
            \App\Entities\Category::create([
                'title' => $title,
                'description' => $description
            ]);
        }
    }
}
