<?php

use Illuminate\Database\Seeder;

class ChildTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Entities\Child::class, 50)->create();

        $users = \App\Entities\User::all();
        foreach ($users as $user) {
            if (count($user->getPermissions()) == 0) {
                $user->attachRole(\App\Entities\Role::customer());
                $user->save();
            }
        }
    }
}
