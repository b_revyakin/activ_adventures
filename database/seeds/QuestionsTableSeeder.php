<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    protected $questions = [
        'Heart, blood or circulatory related conditions' => 1,
        'Diabetes' => 1,
        'Seizures' => 1,
        'Bone, joint or muscle problems' => 1,
        'Dizziness and / or headaches' => 1,
        'Disability (vision, hearing, other)' => 1,
        'Asthma or other respiratory condition' => 2,
        'Hay fever' => 2,
        'Food allergy' => 2,
        'Animal allergy' => 2,
        'Skin allergy' => 2,
        'Drug allergy' => 2,
        'Wasp and bee sting allergy' => 2,
        'Allergy other than that stated above' => 2,
        'Do you have a prescription for an auto-injector?' => 2,
        'Do you have any dietary requirements?' => 3,
        'Are you currently receiving or waiting for any medical treatment of any kind, including physiotherapy?' => 3,
        'Are you registered disabled?' => 3,
        ' Have you been admitted to hospital for any reason within the last two years?' => 3,
        'Have you suffered from any psychological illness or psychiatric disorder, including eating disorders, self harming, anxiety or depression?' => 3,
        'Are you currently prescribed or taking any medication other than those declared above? Note here that if the participant is to be bringing any painkillers or tablets to self-administer, however minor, you will state Yes here and give further details.' => 3,
        'Any other information in regard to any other wellbeing, medical or emotional conditions of any sort not declared above you feel will support us in ensuring you have a safe and enjoyable programme?' => 3,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->questions as $title => $categoryId) {
            $question = new \App\Entities\Question();

            $question->title = $title;
            $question->category()->associate(\App\Entities\Category::find($categoryId));
            $question->save();
        }
    }
}
