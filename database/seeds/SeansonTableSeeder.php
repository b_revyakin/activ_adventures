<?php

use App\Entities\Season;
use Illuminate\Database\Seeder;

class SeansonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Season::create([
            'name' => 'test',
            'description' => 'Test',
            'from' => '20-05-2016',
            'to' => '20-06-2016'
        ]);
    }
}
