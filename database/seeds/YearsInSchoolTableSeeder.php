<?php

use App\Entities\YearInSchool;
use Illuminate\Database\Seeder;

class YearsInSchoolTableSeeder extends Seeder
{
    protected $minYear = 7;

    protected $maxYear = 13;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = $this->minYear; $i <= $this->maxYear; $i++) {
            YearInSchool::create([
                'title' => 'Year ' . $i
            ]);
        }
    }
}
