<?php

use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Entities\Course::class, 14)->create();
        factory(\App\Entities\CoursePart::class, 14)->create();

        foreach (\App\Entities\Course::all() as $course) {
            $course->schools()->save(\App\Entities\School::findOrFail($course->school_id));
        }
    }
}
