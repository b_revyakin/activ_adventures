<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SwimOptionsTableSeeder::class);
        $this->call(YearsInSchoolTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(PermissionsToRolesSeeder::class);
        $this->call(PaymentSystemsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);

        if (App::environment() === 'local') {
            $this->call(SeansonTableSeeder::class);
            $this->call(VenuesTableSeeder::class);
            $this->call(SchoolsTableSeeder::class);
            $this->call(CustomersTableSeeder::class);
            $this->call(ChildTableSeeder::class);
            $this->call(CourseTableSeeder::class);
            $this->call(BookingsTableSeeder::class);
            $this->call(OrderTableSeeder::class);
        }

    }
}
