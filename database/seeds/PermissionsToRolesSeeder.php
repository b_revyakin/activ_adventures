<?php

use App\Entities\Role;
use Bican\Roles\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsToRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createCourse = Permission::create([
            'name' => 'Show an course',
            'slug' => 'course.create',
        ]);

        $showCourse = Permission::create([
            'name' => 'Show an course',
            'slug' => 'course.show',
        ]);

        $editCourse = Permission::create([
            'name' => 'Edit an course',
            'slug' => 'course.edit',
        ]);

        $capacitiesCourse = Permission::create([
            'name' => 'Capacities an course',
            'slug' => 'course.capacities',
        ]);

        $activationCourse = Permission::create([
            'name' => 'Activation an course',
            'slug' => 'course.activation',
            'description' => 'Activation and deactivation courses'
        ]);

        $cancelBooking = Permission::create([
            'name' => 'Cancel Booking',
            'slug' => 'booking.cancel'
        ]);

        $successBooking = Permission::create([
            'name' => 'Success Booking',
            'slug' => 'booking.success',
            'description' => 'Mark a booking as paid'
        ]);

        $permissions = [
            $createCourse,
            $showCourse,
            $editCourse,
            $capacitiesCourse,
            $activationCourse,
            $cancelBooking,
            $successBooking,
        ];

        $roles = [
            Role::admin(),
            Role::staff()
        ];

        foreach ($roles as $role) {
            foreach ($permissions as $permission) {
                $role->attachPermission($permission);
            }
        }

    }
}