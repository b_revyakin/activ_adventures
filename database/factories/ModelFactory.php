<?php

$factory->define(\App\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'password' => bcrypt('test123')
    ];
});

$factory->define(\App\Entities\Customer::class, function (Faker\Generator $faker) {
    return [
        'title' => 'Mr',
        'first_name' => $faker->unique()->name,
        'last_name' => $faker->unique()->name,
        'mobile' => $faker->phoneNumber,
        'home_tel' => $faker->phoneNumber,
        'work_tel' => $faker->phoneNumber,
        'address_line_1' => $faker->address,
        'address_line_2' => $faker->address,
        'address_line_3' => $faker->address,
        'city' => 'moscow',
        'county' => 'ru',
        'post_code' => '121234',
        'hear_from' => 'test',
        'user_id' => factory(\App\Entities\User::class)->create()->id,
        'country_id' => 1,
        'subscribe_to_newsletter' => 1
    ];
});

$factory->define(\App\Entities\Course::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'description' => 'test',
        'school_id' => rand(1, 13),
        'season_id' => 1,
        'venue_id' => 1,
        'staff_id' => 1
    ];
});

$factory->define(\App\Entities\CoursePart::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'course_id' => $faker->unique()->numberBetween(1, 14),
        'start_date' => $faker->dateTimeBetween($startDate = "-15 years", $endDate = "600 days")->format('Y-m-d'),
        'end_date' => $faker->dateTimeBetween($startDate = "-15 years", $endDate = "600 days")->format('Y-m-d'),
        'places' => 20,
        'min_age' => rand(3, 12),
        'max_age' => rand(13, 18),
        'cost_per_course' => 20,
        'free_places' => 20
    ];
});

$factory->define(\App\Entities\Order::class, function (Faker\Generator $faker) {
    return [
        'price' => rand(100, 300),
        'child_id' => rand(1, 30),
        'course_part_id' => rand(1, 14),
        'booking_id' => rand(1, 30),
    ];
});

$factory->define(\App\Entities\Booking::class, function (Faker\Generator $faker) {
    return [
        'customer_id' => rand(1, 30),
        'price' => rand(100, 300),
        'status' => 1
    ];
});

$factory->define(\App\Entities\Child::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->email,
        'birthday' => $faker->dateTimeBetween($startDate = "-15 years", $endDate = "600 days")->format('Y-m-d'),
        'sex' => rand(0, 1),
        'relationship' => 'test',
        'doctor_name' => $faker->name,
        'doctor_telephone_number' => $faker->phoneNumber,
        'medical_advice_and_treatment' => 'test',
        'medication' => 'test',
        'special_requirements' => 'test',


        'school_id' => rand(1, 13),
        'parent_id' => rand(1, 30),
        'year_in_school_id' => rand(1, 6)
    ];
});
