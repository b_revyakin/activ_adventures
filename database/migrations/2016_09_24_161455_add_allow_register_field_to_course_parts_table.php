<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAllowRegisterFieldToCoursePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_parts', function (Blueprint $table) {
            /** @noinspection PhpUndefinedMethodInspection */
            $table->boolean('allow_register')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_parts', function (Blueprint $table) {
            $table->dropColumn('allow_register');
        });
    }
}
