<?php

use Illuminate\Database\Migrations\Migration;

class AddSchoolToChildren extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function ($table) {
            $table->integer('school_id')->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function ($table) {
            $table->dropForeign('children_school_id_foreign');
            $table->dropColumn('school_id');
        });
    }
}
