<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveCoursePartForeignKeyFromMessageGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message_groups', function (Blueprint $table) {
            $table->dropForeign(['course_id']);
            $table->dropColumn(['course_part_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_groups', function (Blueprint $table) {
            $table->integer('course_part_id')->unsigned()->nullable()->after('subject');
            $table->foreign('course_part_id')->references('id')->on('course_parts');
        });
    }
}
