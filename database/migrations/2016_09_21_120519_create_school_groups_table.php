<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_groups', function (Blueprint $table) {
            $table->increments('id');

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('school_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('school_id')->references('id')->on('schools');

            $table->string('name');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_groups');
    }
}
