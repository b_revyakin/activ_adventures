<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('school_id')->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools');

            //Own of event
            $table->integer('staff_id')->unsigned();
            $table->foreign('staff_id')->references('id')->on('staffs');

            $table->string('name')->unique();

            $table->integer('venue_id')->unsigned();
            $table->foreign('venue_id')->references('id')->on('venues');

            $table->integer('season_id')->unsigned();
            $table->foreign('season_id')->references('id')->on('seasons');

            $table->integer('places')->unsigned();//Max places on one event

            $table->integer('min_age')->unsigned();
            $table->integer('max_age')->unsigned();

            $table->text('description')->nullable();

            $table->boolean('active')->default(true);

            $table->float('cost_per_course')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
