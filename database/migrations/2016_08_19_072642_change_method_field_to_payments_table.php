<?php

use Illuminate\Database\Migrations\Migration;

class ChangeMethodFieldToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE payments CHANGE COLUMN method method ENUM('', 'stripe') DEFAULT ''");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE payments CHANGE COLUMN method method ENUM('sagepay', 'stripe') DEFAULT 'sagepay'");
    }
}
