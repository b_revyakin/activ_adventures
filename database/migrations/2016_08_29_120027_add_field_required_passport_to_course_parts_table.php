<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldRequiredPassportToCoursePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_parts', function (Blueprint $table) {
            $table->boolean('required_passport')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_parts', function (Blueprint $table) {
            $table->dropColumn('required_passport');
        });
    }
}
