<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SwitchForeignKeyToPassportDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('passport_details')->truncate();
        
        Schema::table('passport_details', function (Blueprint $table) {
            $table->dropForeign(['child_id']);
            $table->dropColumn('child_id');

            $table->integer('order_id')->unsigned()->after('id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('passport_details')->truncate();
        
        Schema::table('passport_details', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
            $table->dropColumn('order_id');

            $table->integer('child_id')->unsigned()->after('id');
            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
        });
    }
}
