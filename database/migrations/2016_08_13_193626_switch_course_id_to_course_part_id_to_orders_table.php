<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SwitchCourseIdToCoursePartIdToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['course_id']);
            $table->dropColumn('course_id');

            $table->integer('course_part_id')->unsigned()->nullable()->after('child_id');
            $table->foreign('course_part_id')->references('id')->on('course_parts');

            $table->unique(['booking_id', 'child_id', 'course_part_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropUnique(['booking_id', 'child_id', 'course_part_id']);

            $table->dropForeign(['course_part_id']);
            $table->dropColumn('course_part_id');

            $table->integer('course_id')->unsigned()->nullable();
            $table->foreign('course_id')->references('id')->on('courses');
        });
    }
}
