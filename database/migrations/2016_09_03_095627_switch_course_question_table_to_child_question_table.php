<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SwitchCourseQuestionTableToChildQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('course_question');

        Schema::create('child_question', function (Blueprint $table) {
            $table->primary(['child_id', 'question_id']);

            $table->integer('child_id')->unsigned();
            $table->foreign('child_id')->references('id')->on('children');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('child_question');

        Schema::create('course_question', function (Blueprint $table) {
            $table->primary(['course_id', 'question_id']);

            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions');
        });
    }
}
