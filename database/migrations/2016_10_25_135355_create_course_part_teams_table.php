<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursePartTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_part_teams', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('course_part_id')->unsigned();
            $table->foreign('course_part_id')->references('id')->on('course_parts')->onDelete('cascade');

            $table->string('name');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_part_teams');
    }
}
