<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveMedicalDetailsOfParticipantOnChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropColumn('medical_advice_and_treatment', 'special_requirements', 'medication');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->boolean('medical_advice_and_treatment')->nullable();
            $table->text('medication')->nullable();
            $table->text('special_requirements')->nullable();
        });
    }
}
