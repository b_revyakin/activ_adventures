<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseSchoolGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_school_group', function (Blueprint $table) {
            $table->primary(['course_id', 'school_group_id']);

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('course_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('course_id')->references('id')->on('courses');

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('school_group_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('school_group_id')->references('id')->on('school_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_school_group');
    }
}
