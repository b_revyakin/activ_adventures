<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->integer('can_swim_id')->unsigned()->nullable();
            $table->foreign('can_swim_id')->references('id')->on('swim_options')->onDelete('set null');

            $table->string('emergency_contact_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropForeign(['can_swim_id']);

            $table->dropColumn('can_swim_id');
            $table->dropColumn('emergency_contact_number');
        });
    }
}
