<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRecipientModeFieldRemoveFileFieldToMessageGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message_groups', function (Blueprint $table) {
            $table->dropColumn('file');

            $table->tinyInteger('recipient_mode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_groups', function (Blueprint $table) {
            $table->dropColumn('recipient_mode');

            $table->string('file')->nullable();
        });
    }
}
