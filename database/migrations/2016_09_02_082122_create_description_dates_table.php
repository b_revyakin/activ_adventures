<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDescriptionDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('description_dates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('course_part_id')->unsigned();
            $table->foreign('course_part_id')->references('id')->on('course_parts')->onDelete('cascade');

            $table->date('date')->nullable();
            $table->string('title')->nullable();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('description_dates');
    }
}
