<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursePartQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_part_questions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('course_part_id')->unsigned();
            $table->foreign('course_part_id')->references('id')->on('course_parts')->onDelete('cascade');

            $table->text('text')->nullable();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_part_questions');
    }
}
