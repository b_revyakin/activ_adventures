<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCoursePartTeamToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('course_part_team_id')->unsigned()->nullable()->after('course_part_id');
            $table->foreign('course_part_team_id')->references('id')->on('course_part_teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['course_part_team_id']);
            $table->dropColumn(['course_part_team_id']);
        });
    }
}
