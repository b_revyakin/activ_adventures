<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChildSchoolGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_school_group', function (Blueprint $table) {
            $table->primary(['child_id', 'school_group_id']);

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('child_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('child_id')->references('id')->on('children');

            /** @noinspection PhpUndefinedMethodInspection */
            $table->integer('school_group_id')->unsigned();
            /** @noinspection PhpUndefinedMethodInspection */
            $table->foreign('school_group_id')->references('id')->on('school_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('child_school_group');
    }
}
