<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveFieldsFromChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropColumn([
                'authorised_person_name_1',
                'authorised_person_number_1',
                'authorised_person_name_2',
                'authorised_person_number_2',
                'authorised_person_name_3',
                'authorised_person_number_3',
                'doctor_address',
                'epi_pen',
                'emergency_contact_number',
                'emergency_contact_name',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->string('authorised_person_name_1')->nullable();
            $table->string('authorised_person_number_1')->nullable();
            $table->string('authorised_person_name_2')->nullable();
            $table->string('authorised_person_number_2')->nullable();
            $table->string('authorised_person_name_3')->nullable();
            $table->string('authorised_person_number_3')->nullable();

            $table->string('emergency_contact_number')->nullable();
            $table->string('emergency_contact_name')->nullable();
            $table->string('doctor_address')->nullable();
            $table->boolean('epi_pen')->nullable();
        });
    }
}
