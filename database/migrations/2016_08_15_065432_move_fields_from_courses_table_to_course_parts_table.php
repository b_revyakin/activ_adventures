<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MoveFieldsFromCoursesTableToCoursePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn(['places', 'min_age', 'max_age', 'cost_per_course']);
        });

        Schema::table('course_parts', function (Blueprint $table) {
            $table->integer('places')->unsigned();
            $table->integer('min_age')->unsigned();
            $table->integer('max_age')->unsigned();
            $table->float('cost_per_course')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->integer('places')->unsigned();
            $table->integer('min_age')->unsigned();
            $table->integer('max_age')->unsigned();
            $table->float('cost_per_course')->unsigned();
        });

        Schema::table('course_parts', function (Blueprint $table) {
            $table->dropColumn(['places', 'min_age', 'max_age', 'cost_per_course']);
        });
    }
}
