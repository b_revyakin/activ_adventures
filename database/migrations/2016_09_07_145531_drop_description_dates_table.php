<?php

use Illuminate\Database\Migrations\Migration;

class DropDescriptionDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new CreateDescriptionDatesTable())->down();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        (new CreateDescriptionDatesTable())->up();
    }
}
