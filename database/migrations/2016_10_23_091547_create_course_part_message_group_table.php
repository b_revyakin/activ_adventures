<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursePartMessageGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_part_message_group', function (Blueprint $table) {
            $table->primary(['course_part_id', 'message_group_id'], 'course_message_primary_key');

            $table->integer('course_part_id')->unsigned();
            $table->foreign('course_part_id')->references('id')->on('course_parts');

            $table->integer('message_group_id')->unsigned();
            $table->foreign('message_group_id')->references('id')->on('message_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_part_message_group');
    }
}
