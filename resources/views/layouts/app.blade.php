<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="stripe-publish-key" content="{{ config('services.stripe.key') }}">
    <meta name="auth-id" content="{{ Auth::id() }}">

    <title>Adventures</title>

    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    {{--<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>--}}

            <!-- Styles -->
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/>
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/customer.css">

    <link rel="stylesheet" type="text/css" href="/components/slick-carousel/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/components/slick-carousel/slick/slick-theme.css"/>
    <link rel="stylesheet" href="/components/select2/dist/css/select2.min.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    @yield('css')

</head>
<body id="app-layout" class="background-grey">
<nav class="navbar navbar-customer-2">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="logo" href="{{ url('http://www.activadventures.com') }}">
                <img src="/image/activ-adventures-logo.png" height="90px" width="188px">
            </a>
        </div>

        <div class="navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav navbar-customer-2">
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right navbar-customer-2">
                <!-- Authentication Links -->
                @if (Auth::guest() || !Auth::user()->customer)
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else

                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span><i class="fa fa-custom-color fa-user"
                                     aria-hidden="true"></i></span> {{ Auth::user()->customer->name() }}
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('customer::dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('customer::bookings.create') }}">My Details</a></li>
                            <li><a href="{{ url('/bookings/create#/children/index') }}">Participant Details</a></li>
                            <li><a href="{{ route('customer::bookings.index') }}">My Bookings</a></li>
                            <li><a href="{{ route('customer::courses.index') }}">Events</a></li>
                            <li><a href="{{ route('customer::payments.index') }}">My Payments</a></li>
                            <li><a href="{{ route('customer::profile.change-password') }}">Change Password</a></li>
                          @if(Session::has('adminIdForAuthBack'))
                                <li>
                                    <form action="{{ route('admin::back-to-admin') }}" method="POST">
                                        {!! csrf_field() !!}
                                      <button type="submit">Login Back To Admin</button>
                                    </form>
                                </li>
                            @endif
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="header-department background-grey">
        <div class="activ-container">
            <h2 class="header-customer-2">@yield('page-header')</h2>
        </div>
    </div>

    <div class="activ-container">
        @include('partials.flash')
        @yield('content')
    </div>
</div>


<!-- JavaScripts -->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
<script src="/components/jquery/dist/jquery.min.js"></script>
<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/components/select2/dist/js/select2.js"></script>

@if(App::environment('production'))
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/components/slick-carousel/slick/slick.min.js"></script>
@else
    <script src="/js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/components/slick-carousel/slick/slick.min.js"></script>
@endif
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

@yield('js')
</body>
</html>
