@extends('layouts.admin')

@section('title')
    {{ $season->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::seasons.index') }}">School Years</a></li>  <i class="fa fa-circle"></i>
  <li><span>{{ $season->name }}</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Show School Year
      </div>
      <div class="actions">
        <a href="{{ route('admin::seasons.index') }}" class="btn btn-primary btn-sm">All School Years</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>{{ $season->name }}</dd>
          <dt>From</dt>
          <dd>{{ $season->from ? $season->from->format('d-m-Y') : '' }}</dd>
          <dt>To</dt>
          <dd>{{ $season->to ? $season->to->format('d-m-Y') : '' }}</dd>
          <dt>Description</dt>
          <dd><span class="text">{{ $season->description }}</span></dd>
          <dt>Status</dt>
          <dd>
            @include('partials.active-labels', ['model' => $season])
          </dd>
        </dl>
            </div>
        </div>
    </div>
@endsection
