@extends('layouts.admin')

@section('css')
  <link href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet"
        type="text/css"/>
  <style>
    .datepicker > div {
      display: block;
    }
  </style>
@endsection

@section('js')
  <script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="/js/admin/datepicker.js"></script>
@endsection

@section('title')
    Add New School Year
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::seasons.index') }}">School Years</a></li>  <i class="fa fa-circle"></i>
    <li><span>Add New School Year</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New School Year
      </div>
      <div class="actions">
        <a href="{{ route('admin::seasons.index') }}" class="btn btn-primary btn-sm">All School Years</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::seasons.store') }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name') }}">
          </div>

          <div class="form-group">
            <label for="from">School Year Start Date</label>
            <input type="text" class="form-control date-picker" id="from" name="from"
                   value="{{ old('from') }}">
          </div>

          <div class="form-group">
            <label for="to">School Year End Date</label>
            <input type="text" class="form-control date-picker" id="to" name="to"
                   value="{{ old('to') }}">
          </div>

          <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" cols="30"
                      rows="10">{{ old('description') }}</textarea>
          </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
