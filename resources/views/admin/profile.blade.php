@extends('layouts.admin')

@section('title')
  My Account
@endsection

@section('breadcrumbs')
  <li><a href="#" class="active">My Account</a></li>
@endsection

@section('css')
  <link href="/css/admin/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>

@endsection

@section('js')
  <script src="/js/vendor/bootstrap-fileinput.js"></script>
  <script src="/js/vendor/jquery.sparkline.min.js"></script>
  <script src="/js/admin/profile.js"></script>
  <script src="/js/admin/profile.min.js"></script>
@endsection

@section('content')
  <div class="page-content-inner">
    <div class="row">
      <div class="col-md-12">
        <div class="profile-sidebar">
          <div class="portlet light">
            <div class="profile-userpic">
              <img src="{{ $user->avatar() }}" class="img-responsive" alt="">
            </div>
            <div class="profile-usertitle">
              <div class="profile-usertitle-name"> {{$user->staff->first_name}} {{$user->staff->last_name}}</div>
              <div class="profile-desc-text"> {{$user->email}} </div>
            </div>
          </div>
          <div class="portlet light ">
            <div>
              <h4 class="profile-desc-title">About</h4>
              <span class="profile-desc-text"> {{$user->staff->about}}</span>
            </div>
          </div>
        </div>
        <div class="profile-content">
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                  <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                  </div>
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                    </li>
                    <li>
                      <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                    </li>
                    <li>
                      <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                    </li>
                  </ul>
                </div>
                <div class="portlet-body">
                  <div class="tab-content">
                    <!-- PERSONAL INFO TAB -->
                    <div class="tab-pane active" id="tab_1_1">
                      <form action="{{ route('admin::profile.update') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                          <label for="first-name">First Name</label>
                          <input type="text" class="form-control" id="first-name"
                                 placeholder="First Name" name="first_name"
                                 value="{{ old('first_name', $user->staff->first_name) }}">
                        </div>

                        <div class="form-group">
                          <label for="last-name">Last Name</label>
                          <input type="text" class="form-control" id="last-name"
                                 placeholder="Last Name" name="last_name"
                                 value="{{ old('last_name', $user->staff->last_name) }}">
                        </div>

                        <div class="form-group">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" id="email"
                                 placeholder="Email"
                                 name="email"
                                 value="{{ old('email', $user->email) }}">
                        </div>

                        <div class="form-group">
                          <label for="mobile">Mobile Number</label>
                          <input type="text" class="form-control" id="mobile"
                                 placeholder="Mobile Number" name="mobile"
                                 value="{{ old('mobile', $user->staff->mobile) }}">
                        </div>

                        <div class="form-group">
                          <label for="about">About</label>
                          <textarea class="form-control" name="about" id="about" cols="30"
                                    rows="5">{{ old('about', $user->staff->about) }}</textarea>
                        </div>

                        <div>
                          <button type="submit" class="btn btn-primary">Save Changes</button>
                          <button type="button" class="btn btn-default" id="reset-info">Cancel
                          </button>
                        </div>
                      </form>
                    </div>
                    <!-- END PERSONAL INFO TAB -->
                    <!-- CHANGE AVATAR TAB -->
                    <div class="tab-pane" id="tab_1_2">


                      <form action="{{ route('admin::profile.avatar') }}" method="POST"
                            enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                              <img src="{{ url('/img/default-avatar.png') }}" alt=""/></div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                                                                            <span
                                                                                                class="btn default btn-file">
                                                                                                <span
                                                                                                    class="fileinput-new"> Select image </span>
                                                                                                <span
                                                                                                    class="fileinput-exists"> Change </span>
                                                                                                <input type="file"
                                                                                                       name="avatar"> </span>
                              <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                Remove </a>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="margin-top-10">
                          <button type="submit" class="btn green"> Submit</button>
                        </div>
                      </form>

                    </div>
                    <!-- END CHANGE AVATAR TAB -->
                    <!-- CHANGE PASSWORD TAB -->
                    <div class="tab-pane" id="tab_1_3">
                      <form action="{{ route('admin::profile.password') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                          <label for="old-password">Old Password</label>
                          <input type="password" class="form-control" id="old-password"
                                 placeholder="Old Password" name="old_password">
                        </div>

                        <div class="form-group">
                          <label for="new-password">New Password</label>
                          <input type="password" class="form-control" id="new-password"
                                 placeholder="New Password" name="new_password">
                        </div>

                        <div class="form-group">
                          <label for="new-password-confirmation">New Password
                            Confirmation</label>
                          <input type="password" class="form-control"
                                 id="new-password-confirmation"
                                 placeholder="New Password Confirmation"
                                 name="new_password_confirmation">
                        </div>

                        <div>
                          <button type="submit" class="btn btn-primary">
                            Change Password
                          </button>
                          <button type="reset" id="reset-password" class="btn btn-default">
                            Reset
                          </button>
                        </div>

                      </form>
                    </div>
                    <!-- END CHANGE PASSWORD TAB -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection