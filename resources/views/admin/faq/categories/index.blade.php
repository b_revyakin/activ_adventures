@extends('layouts.admin')

@section('css')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
@endsection

@section('title')
  FAQ Categories
@endsection

@section('breadcrumbs')
  <li><span>FAQ Categories</span></li>
@endsection

@section('js')
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script src="/js/admin/faq/categories.js"></script>
  <script src="/js/switch-order.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        FAQ Categories
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered" id="categories-table">
          <thead>
          <tr>
            <th>Title</th>
            <th class="actions" style="width: 200px">Actions</th>
          </tr>
          </thead>
          <tbody id="sortable">
          @foreach($categories as $category)
            <tr id="{{ $category->id }}">
              <td>{{ $category->title }}</td>
              <td>
                <div class="btn-group-xs">
                  <a href="{{ route('admin::faq.categories.questions.index', [$category->id]) }}"
                     class="btn btn-default">
                    Questions
                  </a>
                  <a href="{{ route('admin::faq.course-parts.categories.edit', [$coursePart->id, $category->id]) }}"
                     class="btn btn-warning">
                    Edit
                  </a>
                  <form method="post"
                        action="{{ route('admin::faq.course-parts.categories.destroy', [$coursePart->id, $category->id]) }}"
                        style="display: inline">
                    <input type="hidden" name="_method" value="DELETE">
                    {!! csrf_field() !!}
                    <button class="btn btn-danger btn-xs">Delete</button>
                  </form>
                </div>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <br>
        <a class="btn btn-primary save-order hide" data-url="{{ route('admin::faq.categories.switch-order') }}">Save
          Order</a>
        <a class="btn btn-success pull-right"
           href="{{ route('admin::faq.course-parts.categories.create', [$coursePart->id]) }}">
          Add New Category
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
