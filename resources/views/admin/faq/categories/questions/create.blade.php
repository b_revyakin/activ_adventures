@extends('layouts.admin')

@section('js')
  <script src="/js/admin/text-editor.js"></script>
@endsection

@section('title')
  Add New Question For Category {{ $category->title }}
@endsection

@section('breadcrumbs')
  <li>{{ $category->title }}</li><i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::faq.categories.questions.index', $category->id) }}">Questions</a></li><i
          class="fa fa-circle"></i>
  <li><span>Add New Question</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Question For Category {{ $category->title }}
      </div>
      <div class="actions">
        <a href="{{ route('admin::faq.categories.questions.index', [$category->id]) }}" class="btn btn-primary btn-sm">All
          Questions Category</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::faq.categories.questions.store', [$category->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Title</label>
            <input type="text" class="form-control" id="name" placeholder="Title" name="title"
                   value="{{ old('title') }}">
          </div>

          <div class="form-group">
            <label for="text-editor">Answer</label>
            <textarea class="form-control" name="answer" id="text-editor" cols="30"
                      rows="3">{{ old('answer') }}</textarea>
          </div>

          <button type="submit" class="btn btn-default">Create</button>
        </form>
      </div>
    </div>
  </div>
@endsection
