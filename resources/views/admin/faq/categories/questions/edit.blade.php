@extends('layouts.admin')

@section('js')
  <script src="/js/admin/text-editor.js"></script>
@endsection

@section('title')
  Edit Question For Category {{ $category->title }}
@endsection

@section('breadcrumbs')
  <li>{{ $category->title }}</li><i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::faq.categories.questions.index', [$category->id]) }}">Questions</a></li><i
          class="fa fa-circle"></i>
  <li>{{ $question->title }}</li><i class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit Question For Category {{ $category->title }}
      </div>
      <div class="actions">
        <a href="{{ route('admin::faq.categories.questions.index', [$category->id]) }}" class="btn btn-primary btn-sm">All
          Questions For Category</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::faq.categories.questions.update', [$category->id, $question->id]) }}"
              method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="name">Title</label>
            <input type="text" class="form-control" id="name" placeholder="Title" name="title"
                   value="{{ old('title', $question->title) }}">
          </div>

          <div class="form-group">
            <label for="text-editor">Answer</label>
            <textarea class="form-control" name="answer" id="text-editor" cols="30"
                      rows="3">{{ old('answer', $question->answer) }}</textarea>
          </div>

          <button type="submit" class="btn btn-default">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection
