@extends('layouts.admin')

@section('title')
  Edit Category
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::faq.course-parts.categories.index', [$coursePart->id]) }}">FAQ Categories</a></li>  <i
          class="fa fa-circle"></i>
  <li>{{ $category->title }}</li><i class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit Category
      </div>
      <div class="actions">
        <a href="{{ route('admin::faq.course-parts.categories.index', [$coursePart->id]) }}"
           class="btn btn-primary btn-sm">All Categories</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::faq.course-parts.categories.update', [$coursePart->id, $category->id]) }}"
              method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="name">Title</label>
            <input type="text" class="form-control" id="name" placeholder="Title" name="title"
                   value="{{ old('title', $category->title) }}">
          </div>

          <button type="submit" class="btn btn-default">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection
