@extends('layouts.admin')

@section('css')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
@endsection

@section('title')
  Categories
@endsection

@section('breadcrumbs')
  <li><span>Categories</span></li>
@endsection

@section('js')
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script src="/js/admin/categories.js"></script>
  <script src="/js/switch-order.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Categories
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered" id="categories-table">
          <thead>
          <tr>
            <th>Title</th>
            <th>Description</th>
            <th class="actions">Actions</th>
          </tr>
          </thead>
          <tbody id="sortable">
          @foreach($categories as $category)
            <tr id="{{ $category->id }}">
              <td>{{ $category->title }}</td>
              <td>{{ $category->description }}</td>
              <td>
                <div class="btn-group-xs">
                  <a href="{{ route('admin::categories.questions.index', [$category->id]) }}"
                     class="btn btn-default">
                    Questions
                  </a>
                  <a href="{{ route('admin::categories.edit', [$category->id]) }}"
                     class="btn btn-warning">
                    Edit
                  </a>
                  <form method="post" action="{{ route('admin::categories.destroy', $category->id) }}"
                        style="display: inline">
                    <input type="hidden" name="_method" value="DELETE">
                    {!! csrf_field() !!}
                    <button class="btn btn-danger btn-xs">Delete</button>
                  </form>
                </div>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <br>
        <a class="btn btn-primary save-order hide" data-url="{{ route('admin::categories.switch-order') }}">Save
          Order</a>
        <a class="btn btn-success pull-right" href="{{ route('admin::categories.create') }}">
          Add New Category
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
