@extends('layouts.admin')

@section('title')
  Add New Question For Category {{ $categories->title }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::categories.index') }}">Categories</a></li>  <i class="fa fa-circle"></i>
  <li>{{ $categories->title }}</li><i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::categories.questions.index', $categories->id) }}">Questions</a></li><i
          class="fa fa-circle"></i>
  <li><span>Add New Question</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Question For Category {{ $categories->title }}
      </div>
      <div class="actions">
        <a href="{{ route('admin::categories.questions.index', $categories->id) }}" class="btn btn-primary btn-sm">All
          Questions Category</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::categories.questions.store', $categories->id) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Title</label>
            <input type="text" class="form-control" placeholder="Title" name="title"
                   value="{{ old('title') }}">
          </div>

          <div class="form-group">
            <label for="name">Short Form</label>
            <input type="text" class="form-control" placeholder="Short Form" name="short_form"
                   value="{{ old('short_form') }}">
          </div>

          <div class="form-group">
            <label for="name">Placeholder</label>
            <input type="text" class="form-control" placeholder="Placeholder" name="placeholder"
                   value="{{ old('placeholder') }}">
          </div>

          <button type="submit" class="btn btn-default">Create</button>
        </form>
      </div>
    </div>
  </div>
@endsection
