@extends('layouts.admin')

@section('title')
  Edit Question For Category {{ $categories->title }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::categories.index') }}">Categories</a></li>  <i class="fa fa-circle"></i>
  <li>{{ $categories->title }}</li><i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::categories.questions.index', $categories->id) }}">Questions</a></li><i
          class="fa fa-circle"></i>
  <li>{{ $questions->title }}</li><i class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit Question For Category {{ $categories->title }}
      </div>
      <div class="actions">
        <a href="{{ route('admin::categories.questions.index', $categories->id) }}" class="btn btn-primary btn-sm">All
          Questions For Category</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::categories.questions.update', [$categories->id, $questions->id]) }}"
              method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="name">Title</label>
            <input type="text" class="form-control" placeholder="Title" name="title"
                   value="{{ old('title', $questions->title) }}">
          </div>

          <div class="form-group">
            <label for="name">Short Form</label>
            <input type="text" class="form-control" placeholder="Short Form" name="short_form"
                   value="{{ old('short_form', $questions->short_form) }}">
          </div>

          <div class="form-group">
            <label for="name">Placeholder</label>
            <input type="text" class="form-control" placeholder="Placeholder" name="placeholder"
                   value="{{ old('placeholder', $questions->placeholder) }}">
          </div>

          <button type="submit" class="btn btn-default">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection
