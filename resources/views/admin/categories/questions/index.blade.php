@extends('layouts.admin')

@section('css')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
@endsection

@section('title')
  Questions For Category {{ $categories->title }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::categories.index') }}">Categories</a></li><i class="fa fa-circle"></i>
  <li>{{ $categories->title }}</li><i class="fa fa-circle"></i>
  <li><span>Questions</span></li>
@endsection

@section('js')
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script src="/js/admin/questions.js"></script>
  <script src="/js/switch-order.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Questions For Category {{ $categories->title }}
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered" id="questions-table">
          <thead>
          <tr>
            <th>Title</th>
            <th>Short Form</th>
            <th class="actions">Actions</th>
          </tr>
          </thead>
          <tbody id="sortable">
          @foreach($questions as $question)
            <tr id="{{ $question->id }}">
              <td>{{ $question->title }}</td>
              <td>{{ $question->short_form }}</td>
              <td>
                <div class="btn-group-xs">
                  <a href="{{ route('admin::categories.questions.edit', [$categories->id, $question->id]) }}"
                     class="btn btn-warning">
                    Edit
                  </a>
                  <form method="post"
                        action="{{ route('admin::categories.questions.destroy', [$categories->id, $question->id]) }}"
                        style="display: inline">
                    <input type="hidden" name="_method" value="DELETE">
                    {!! csrf_field() !!}
                    <button class="btn btn-danger btn-xs">Delete</button>
                  </form>
                </div>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <br>
        <a class="btn btn-primary save-order hide"
           data-url="{{ route('admin::categories.questions.switch-order', $categories->id) }}">Save Order</a>
        <a class="btn btn-success pull-right" href="{{ route('admin::categories.questions.create', $categories->id) }}">
          Add New Question
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
