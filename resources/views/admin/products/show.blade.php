@extends('layouts.admin')

@section('title')
    {{ $product->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::products.show', [$product->id]) }}" class="active">{{ $product->name }}</a></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Show Product
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>{{ $product->name }}</dd>
          <dt>Price</dt>
          <dd>£{{ $product->price }}</dd>
          <dt>Status</dt>
          <dd>
            @include('partials.active-labels', ['model' => $product])
            @include('partials.active-forms', ['model' => $product, 'route' => 'admin::products'])
          </dd>
        </dl>
            </div>
        </div>
    </div>
@endsection
