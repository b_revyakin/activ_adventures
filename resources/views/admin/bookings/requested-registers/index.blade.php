@extends('layouts.admin')

@section('title')
  Bookings
@endsection

@section('breadcrumbs')
  <li><span>Bookings</span></li>
@endsection

@section('js')
  <script src="/js/admin/booking/bookings.js"></script>
  <script src="/js/modal-window.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Latest Bookings
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        @include('partials.bookings.requested-registers', ['withEventName' => true])
        <a class="btn btn-success pull-right"
           href="{{ route('admin::parents.index') }}">
          Add New Booking
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  @include('partials.modal-windows.booking.action')
@endsection
