@extends('layouts.admin')

@section('js')
  <script src="/js/admin/payments/payments.js"></script>
@endsection
@section('title')
  Booking №{{ $bookings->id }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::bookings.index') }}"><span>Bookings</span></a></li> <i class="fa fa-circle"></i>
  <li><span>Payments</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Booking Payments
        <div class="col-md-1 pull-right">
          <a href="{{ route('admin::bookings.payments.create', $bookings->id) }}" class="btn btn-success">Add
            Payment</a>
        </div>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <div class="row">
          <div class="col-md-6">
            <h4>Payments</h4>
          </div>
          <div class="col-md-1 pull-right">

          </div>
        </div>
        <table class="table table-bordered" id="payments-data-table">
          <thead>
          <tr>
            <th>Payment ID</th>
            <th>Amount</th>
            <th>Type</th>
            <th>Status</th>
            <th>Date</th>
            <th class="actions">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach($payments as $payment)
            <tr>
              <td>{{ $payment->id }}</td>
              <td>{{ $payment->getAmount() }}</td>
              <td>{{ $payment->showType() }} ({{ $payment->method }})</td>
              <td><span class="label label-{{ $payment->getClassByStatus() }}">{{ $payment->showStatus() }}</span>
              </td>
              <td>{{ date('d/m/Y H:i:s', strtotime($payment->created_at)) }}</td>
              <td class="btn-group-xs">
                @if($payment->isProgressing())
                  <form action="{{ route('admin::bookings.payments.cancel', [$bookings->id, $payment->id]) }}"
                        method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <button class="btn btn-danger btn-xs">Cancel</button>
                  </form>
                  <form action="{{ route('admin::bookings.payments.confirm', [$bookings->id, $payment->id]) }}"
                        method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <button class="btn btn-success btn-xs">Confirm</button>
                  </form>
                @endif
                @if($payment->isCompleted())
                  <form action="{{ route('admin::bookings.payments.refund', [$bookings->id, $payment->id]) }}"
                        method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <button class="btn btn-primary btn-xs">Refund</button>
                  </form>
                @endif
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
