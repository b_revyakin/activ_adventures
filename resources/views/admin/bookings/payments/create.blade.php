@extends('layouts.admin')

@section('title')
  Booking {{ $bookings->id }} Add Payment
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::bookings.index') }}">Bookings</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::bookings.show', [$bookings->id]) }}">Booking №{{ $bookings->id }}</a></li> <i
          class="fa fa-circle"></i>
  <li><span>Add Payment</span></li>
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Add Payment
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::bookings.payments.store', [$bookings->id]) }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="type">Type</label>
            <select class="form-control" name="type">
              @foreach(['online', 'BACS', 'cheque'] as $type)
                <option {{ old('type') ? 'selected' : '' }} value="{{ $type }}">
                  {{ $type }}
                </option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="amount">Amount</label>
            <input type="number" min="0" step="0.01" max="{{ $bookings->getLeftPay(true) }}" class="form-control"
                   name="amount"
                   value="{{ old('amount') }}" placeholder="Amount">
          </div>
          <button type="submit" class="btn btn-success">Add</button>
        </form>
      </div>
    </div>
  </div>
@endsection
