@extends('layouts.admin')

@section('title')
    Edit '{{ $product->name }}'
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::products.index') }}" class="active">Products</a></li>  <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::products.show', [$product->id]) }}">{{ $product->name }}</a></li>  <i
      class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit Product
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::products.update', [$product->id]) }}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name', $product->name) }}">
          </div>


          <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control" id="price" placeholder="Price" name="price"
                   value="{{ old('price', $product->price) }}">
                    </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
        </form>
            </div>
        </div>
    </div>
@endsection
