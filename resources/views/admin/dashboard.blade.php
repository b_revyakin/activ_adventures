@extends('layouts.admin')

@section('css')
    <link rel="stylesheet" href="/css/admin/dashboard.css">
@endsection

@section('js')
    <script src="/js/admin/dashboard.js"></script>
@endsection

@section('title')
    Dashboard
@endsection

@section('breadcrumbs')
  <li><span>Dashboard</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Hello {{ Auth::user()->staff->name }}, welcome to the Activ booking system.
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">

        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
              <div class="col-md-4">
                <div class="well well-link">
                  <a href="{{ route('admin::courses.index') }}">
                    <h4 class="text-danger">
                                            <span class="label label-danger pull-right"><i
                                                  class="fa fa-btn fa-users"></i></span>
                      Events </h4>
                  </a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="well well-link">
                  <a href="{{ route('admin::bookings.index') }}">
                    <h4 class="text-success">
                                            <span class="label label-success pull-right"><i
                                                  class="fa fa-btn fa-ticket"></i></span>
                      Booking</h4>
                  </a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="well well-link">
                  <a href="{{ route('admin::parents.index') }}">
                    <h4 class="text-primary">
                                            <span class="label label-primary pull-right"><i
                                                  class="fa fa-btn fa-users"></i></span>
                      Customers
                    </h4>
                  </a>
                </div>
              </div>
            </div><!--/row-->
          </div><!--/col-12-->
        </div><!--/row-->
      </div>
    </div>
  </div>
@endsection