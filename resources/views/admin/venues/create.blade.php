@extends('layouts.admin')

@section('title')
    Add New Venue
@endsection

@section('breadcrumbs')
    <li><a href="{{ route('admin::venues.index') }}">Venues</a></li>
    <i class="fa fa-circle"></i>
    <li><span>Add New Venue</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/venues.js"></script>
    <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Venue
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">

        <form action="{{ route('admin::venues.store') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Venue Name</label>
            <input type="text" class="form-control" id="name" placeholder="Venue Name" name="name"
                   value="{{ old('name') }}">
          </div>

          <div class="form-group">
            <label for="address-line-1">Address Line 1</label>
            <input type="text" class="form-control" id="address-line-1" placeholder="Address Line 1"
                   name="address_line_1"
                   value="{{ old('address_line_1') }}">
          </div>

          <div class="form-group">
            <label for="address-line-2">Address Line 2</label>
            <input type="text" class="form-control" id="address-line-2" placeholder="Address Line 2"
                   name="address_line_2"
                   value="{{ old('address_line_2') }}">
          </div>

          <div class="form-group">
            <label for="country">Country</label>
            <input type="text" class="form-control" id="country" placeholder="Country" name="country"
                   value="{{ old('county') }}">
          </div>

          <div class="form-group">
            <label for="city">Town / City</label>
            <input type="text" class="form-control" id="city" placeholder="Town / City" name="city"
                   value="{{ old('city') }}">
          </div>

          <div class="form-group">
            <label for="post-code">Post Code</label>
            <input type="text" class="form-control" id="post-code" placeholder="Post Code" name="post_code"
                   value="{{ old('post_code') }}">
          </div>

          <div class="form-group">
            <label for="description">Information</label>
            <textarea name="description" id="description" cols="30" rows="10"
                      class="form-control">{{ old('description') }}</textarea>
          </div>

          <div class="form-group">
            <label for="input-1" class="control-label">Select Photos</label>
            <input id="input-1" type="file" class="file" name="files[]" multiple
                   data-show-upload="false"
                   data-allowed-file-extensions='["jpg", "png", "gif", "jpeg"]'>
                    </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
            </div>
        </div>
    </div>
@endsection
