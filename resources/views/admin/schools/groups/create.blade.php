@extends('layouts.admin')

@section('title')
  Create New Group
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}">Schools</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.show', [$school->id]) }}">{{ $school->name }}</a></li> <i
          class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.groups.index', [$school->id]) }}">Groups</a></li> <i class="fa fa-circle"></i>
  <li class="active">Create</li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Group
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::schools.groups.store', [$school->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name') }}">
          </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
