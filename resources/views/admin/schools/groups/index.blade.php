@extends('layouts.admin')

@section('title')
  {{ $school->name }} Groups
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}">Schools</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.show', [$school->id]) }}">{{ $school->name }}</a></li> <i
          class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.groups.index', [$school->id]) }}" class="active">Groups</a></li>
@endsection


@section('js')
  <script src="{{ asset('js/admin/school/groups.js') }}"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Groups
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered" id="groups-table">
          <thead>
          <tr>
            <th>Name</th>
            <th class="actions">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach($groups as $group)
            <tr>
              <td>{{ $group->name }}</td>
              <td class="btn-group-xs">
                <a href="{{ route('admin::schools.groups.edit', [$school->id, $group->id]) }}"
                   class="btn btn-warning">
                  Edit
                </a>

                <form action="{{ route('admin::schools.groups.destroy', [$school->id, $group->id]) }}" method="POST"
                      class="inline">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">

                  <button type="submit" class="btn btn-danger btn-xs">Remove</button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="btn btn-success pull-right"
           href="{{ route('admin::schools.groups.create', [$school->id]) }}">
          Add New Group
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
