@extends('layouts.admin')

@section('title')
  Edit '{{ $group->name }}'
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}">Schools</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.show', [$school->id]) }}">{{ $school->name }}</a></li> <i
          class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.groups.index', [$school->id]) }}">Groups</a></li> <i class="fa fa-circle"></i>
  <li>{{ $group->name }}</li> <i class="fa fa-circle"></i>
  <li class="active">Edit</li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit Group
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::schools.groups.update', [$school->id, $group->id]) }}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name', $group->name) }}">
          </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
