@extends('layouts.admin')

@section('title')
  Edit '{{ $school->name }}'
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}" class="active">schools</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.show', [$school->id]) }}">{{ $school->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.edit', [$school->id]) }}" class="active">Edit</a></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit School
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::schools.update', [$school->id]) }}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name', $school->name) }}">
          </div>

          <div class="form-group">
            <label for="code">Code</label>
            <input type="text" class="form-control" id="code" placeholder="Code" name="code"
                   value="{{ old('code', $school->code) }}">
          </div>


          <button type="submit" class="btn btn-default">
            Update
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
