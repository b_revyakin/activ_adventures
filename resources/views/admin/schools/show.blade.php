@extends('layouts.admin')

@section('title')
  {{ $school->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}" class="active">Schools</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.show', [$school->id]) }}" class="active">{{ $school->name }}</a></li>
@endsection



@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Show school
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>{{ $school->name }}</dd>
          <dt>Code</dt>
          <dd>{{ $school->code }}</dd>

        </dl>
      </div>
    </div>
  </div>
@endsection
