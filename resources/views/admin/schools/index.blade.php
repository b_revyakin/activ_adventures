@extends('layouts.admin')

@section('title')
  Schools
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}" class="active">Schools</a></li>
@endsection


@section('js')
  <script src="{{ asset('js/admin/school/schools.js') }}"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Schools
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered" id="schools-table">
          <thead>
          <tr>
            <th>Name</th>
            <th class="code">Code</th>
            <th class="actions">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach($schools as $school)
            <tr>
              <td>{{ $school->name }}</td>
              <td>{{ $school->code }}</td>
              <td class="btn-group-xs">
                <a href="{{ route('admin::schools.show', [$school->id]) }}"
                   class="btn btn-primary">
                  Show
                </a>
                <a href="{{ route('admin::schools.edit', [$school->id]) }}"
                   class="btn btn-warning">
                  Edit
                </a>
                <a href="{{ route('admin::schools.groups.index', [$school->id]) }}"
                   class="btn btn-default">
                  Groups
                </a>
                <a href="{{ route('admin::schools.children.index', [$school->id]) }}"
                   class="btn btn-default">
                  Participants
                </a>

                <form action="{{ route('admin::schools.destroy', [$school->id]) }}" method="POST" class="inline">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">

                  <button type="submit" class="btn btn-danger btn-xs">Remove</button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="btn btn-success pull-right"
           href="{{ route('admin::schools.create') }}">
          Add New School
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
