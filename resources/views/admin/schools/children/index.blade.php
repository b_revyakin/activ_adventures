@extends('layouts.admin')

@section('css')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="//rawgithub.com/akottr/dragtable/master/dragtable.css">
  <link rel="stylesheet"
        href="/components/drmonty-datatables-colvis/css/dataTables.colVis.min.css"/>
  <link rel="stylesheet"
        href="/components/drmonty-datatables-colvis/css/dataTables.colvis.jqueryui.css"/>
@endsection

@section('title')
  {{ $school->name }} Participants
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::schools.index') }}">Schools</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::schools.show', [$school->id]) }}">{{ $school->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li class="active">Participants</li>
@endsection


@section('js')
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script src="//rawgithub.com/akottr/dragtable/master/jquery.dragtable.js"></script>
  <script type="text/javascript"
          src="/components/drmonty-datatables-colvis/js/dataTables.colVis.js"></script>
  <script src="/js/admin/children-school-groups.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Participants
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered sortable" id="children-school-groups-table">
          <thead>
            <tr>
              <th>Participants</th>
              <th>Parent's Email</th>
              @foreach($groups as $group)
                <th class="accept">
                  <i class="fa fa-arrows-h dragtable-column" aria-hidden="true"></i>
                  {{ $group->name }}
                </th>
              @endforeach
            </tr>
          </thead>
          <tbody>
            @foreach($children as $child)
              <tr>
                <td>{{ $child->full_name }}</td>
                <td>
                  <a target="_blank" href="{{ route('admin::parents.show', [$child->parent->id]) }}">
                    {{ $child->parent->user->email }}
                  </a>
                </td>
                @foreach($groups as $group)
                  <td data-id="{{ $child->id }}">
                    <input class="checkbox-group" type="checkbox" name="group_id"
                           {{ in_array($group->id, $child->schoolGroups->pluck('id')->toArray()) ? 'checked' : '' }}
                           value="{{ $group->id }}">
                  </td>
                @endforeach
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <a class="btn btn-success" id="save-children-school-groups"
         data-url="{{ route('admin::schools.children.store', [$school->id]) }}">Save</a>
    </div>
  </div>
@endsection
