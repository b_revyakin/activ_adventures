@extends('layouts.admin')

@section('title')
    Add New Staff
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::staffs.index') }}">Staffs</a></li>  <i class="fa fa-circle"></i>
  <li><span>Add New Staff</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Staff
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::staffs.store') }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" id="first_name"
                   placeholder="First Name" name="first_name"
                   value="{{ old('first_name') }}">
                    </div>
          <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" id="last_name"
                   placeholder="Last Name" name="last_name"
                   value="{{ old('last_name') }}">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email"
                   placeholder="Email" name="email"
                   value="{{ old('email') }}">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password"
                   placeholder="Password" name="password">
          </div>
          <div class="form-group">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" class="form-control" id="password_confirmation"
                   placeholder="Confirm Password" name="password_confirmation">
          </div>
          <div class="form-group">
            <label for="mobile">Mobile Number</label>
            <input type="text" class="form-control" id="mobile"
                   placeholder="Mobile Number" name="mobile"
                   value="{{ old('mobile') }}">
          </div>
          <div class="form-group">
            <label for="qualifications">Qualifications</label>
            <input type="text" class="form-control" id="qualifications"
                   placeholder="Qualifications" name="qualifications"
                   value="{{ old('qualifications') }}">
          </div>
          <div class="form-group">
            <label for="about">About</label>
            <textarea class="form-control" name="about" id="about" cols="30"
                      rows="10">{{ old('about') }}</textarea>
          </div>

          <button type="submit" class="btn btn-default">Create</button>
        </form>
            </div>
        </div>
    </div>
@endsection
