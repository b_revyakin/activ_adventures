@extends('layouts.admin')

@section('title')
  Booking By Event: {{ $order->coursePart->name }}
@endsection

@section('breadcrumbs')
  <li><span>Event Bookings</span></li>
@endsection

@section('js')
  <script src="/js/admin/order/show.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Show Booking
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Booking Id</dt>
          <dd>{{ $order->booking->id }}</dd>
          <dt>Price</dt>
          <dd>{{ $order->booking->getPrice() }}</dd>
          <dt>Status</dt>
          <dd>{{ $order->booking->showStatus() }}</dd>
          <dt>Parent</dt>
          <dd>{{ $order->child->parent->first_name }}  {{ $order->child->parent->last_name }}</dd>
          <dt>Participant:</dt>
          <dd>
            Name: {{ $order->child->first_name }} {{ $order->child->last_name }}<br>
            Birthday: {{ $order->child->birthday }}
          </dd>
          <dt>Event</dt>
          <dd>{{ $order->coursePart->name }}</dd>
          <dt>Payments:</dt>
          @forelse($order->booking->payments as $payment)
            <dd>
              Type: {{ $payment->showType() }}<br>
              Status: {{ $payment->showStatus() }}<br>
            </dd>
          @empty
            <dd>Not Payments</dd>
          @endforelse
          @if(count($categories) > 0)
            <dd>
            <div class="margin-top-20">
              <button class="btn btn-primary questions-toggle">Show questions</button>
            </div>
          </dd>
          @endif
        </dl>
        @if(count($categories) > 0)
        <dl class="dl-horizontal margin-top-20 questions" style="display:none;">
          <dt>Questions:</dt>
          <dd>
            <table class="table table-bordered" id="questionnaire-table">
              <tbody>
              @foreach($categories as $category)
                <tr>
                  <td><strong>{{ $category['title'] }}</strong></td>
                </tr>
                @foreach($category['questions'] as $question)
                  <tr>
                    <td>{{ $question->question->title }}:

                      @if($question->answer == 0)
                        No
                      @else
                        Yes
                        <br><br>
                        <i>{{ $question->text }}</i>
                      @endif
                    </td>
                  </tr>
                @endforeach
              @endforeach
              </tbody>
            </table>
          </dd>
        </dl>
        @endif
      </div>
    </div>
  </div>
@endsection
