@extends('layouts.admin')

@section('title')
    Event Bookings
@endsection

@section('breadcrumbs')
  <li><span>Event Bookings</span></li>
@endsection

@section('js')
    <script src="/js/admin/booking/bookings.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Event Bookings
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        @include('partials.orders.index', compact('orders'))
        <div class="clearfix"></div>
      </div>
        </div>
    </div>
@endsection
