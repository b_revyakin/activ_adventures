@extends('layouts.admin')

@section('title')
    Coupons
@endsection

@section('breadcrumbs')
  <li><span>Coupons</span></li>
@endsection

@section('js')
    <script src="/js/admin/booking/coupons.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Coupons
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
                <table class="table table-bordered" id="coupons-table">
                    <thead>
                    <tr>
                        <th>Phrase</th>
                        <th class="percent">Percent</th>
                        <th class="status">Status</th>
                        <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($coupons as $coupon)
                        <tr>
                            <td>{{ $coupon->phrase }}</td>
                            <td>{{ $coupon->percent }}</td>
                            <td>
                                @include('partials.active-labels', ['model' => $coupon])
                            </td>
                            <td class="btn-group-xs">
                                {{--
                                <a href="{{ route('admin::coupons.show', [$coupon->id]) }}"
                                   class="btn btn-primary">
                                    Show
                                </a>
                                --}}
                                <a href="{{ route('admin::coupons.edit', [$coupon->id]) }}"
                                   class="btn btn-warning">
                                    Edit
                                </a>
                                @include('partials.active-forms', ['model' => $coupon, 'route' => 'admin::coupons'])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <a class="btn btn-success pull-right"
                   href="{{ route('admin::coupons.create') }}">
                    Add New Coupon
                </a>
        <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
