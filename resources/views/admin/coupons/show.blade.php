@extends('layouts.admin')

@section('title')
    {{ $coupon->phrase }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::coupons.index') }}">Coupons</a></li>  <i class="fa fa-circle"></i>
  <li><span>{{ $coupon->phrase }}</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Show Coupon

      </div>
      <div class="actions">
        <a href="{{ route('admin::coupons.index') }}" class="btn btn-primary btn-sm">All Coupons</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Phrase</dt>
          <dd>{{ $coupon->phrase }}</dd>
          <dt>Percent</dt>
          <dd>{{ $coupon->percent }}</dd>
          <dt>Status</dt>
          <dd>
            @include('partials.active-labels', ['model' => $coupon])
            @include('partials.active-forms', ['model' => $coupon, 'route' => 'admin::coupons'])
          </dd>
        </dl>
            </div>
        </div>
    </div>
@endsection
