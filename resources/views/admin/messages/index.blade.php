@extends('layouts.admin')

@section('title')
  Messages
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::messages.index') }}">Messages</a></li>
@endsection

@section('css')
  <link rel="stylesheet" href="/components/AngularJS-Toaster/toaster.min.css">
  <link rel="stylesheet" href="/components/angular-bootstrap/ui-bootstrap-csp.css">
  <link rel="stylesheet" href="/components/ui-select/dist/select.min.css">
  <link rel="stylesheet" href="/components/redactor/redactor/redactor.css">
@endsection

@section('js')
  <script src="/components/angular/angular.min.js"></script>
  <script src="/components/angularUtils-pagination/dirPagination.js"></script>
  <script src="/components/angular-ui-router/release/angular-ui-router.min.js"></script>
  <script src="/components/AngularJS-Toaster/toaster.min.js"></script>
  <script src="/components/ng-file-upload/ng-file-upload.min.js"></script>

  <script type="text/javascript" src="/components/angular-bootstrap/ui-bootstrap.min.js"></script>
  <script type="text/javascript" src="/components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
  <script src="/components/redactor/redactor/redactor.js"></script>
  <script src="/components/redactor/redactor/redactor-plugins/fontcolor.js"></script>
  <script src="/components/redactor/redactor/redactor-plugins/fontsize.js"></script>
  <script src="/components/angular-sanitize/angular-sanitize.min.js"></script>
  <script type="text/javascript" src="/components/angular-redactor/angular-redactor.js"></script>


  <script src="{{ elixir('js/messages-app.js') }}"></script>
@endsection

@section('content')
  <div data-ng-app="message">

    <toaster-container></toaster-container>
    <ui-view></ui-view>

  </div>
@endsection
