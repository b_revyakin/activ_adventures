@extends('layouts.admin')

@section('title')
  Create New Event
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::courses.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li><span>Create New Event</span></li>
@endsection

@section('css')
  <link href="/css/admin/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css">
@endsection

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js"></script>
  <script src="/js/admin/courses/edit.js"></script>
  <script src="/js/admin/text-editor.js"></script>
  <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
  <script src="/js/vendor/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Event
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::courses.store') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name') }}">
          </div>

          <div class="form-group">
            <label for="venue">Venue</label>
            <select class="form-control" name="venue_id" id="venue">
              @foreach($venues as $venue)
                <option value="">Select Venue</option>
                <option
                    {{ old('venue_id') && $venue->id == old('venue_id') ? 'selected' : '' }} value="{{ $venue->id }}">
                  {{ $venue->name }}
                </option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="season">School Year</label>
            <select class="form-control" name="season_id" id="season">
              @foreach($seasons as $season)
                <option
                    {{ old('season_id') && $season->id == old('season_id') ? 'selected' : '' }} value="{{ $season->id }}">
                  {{ $season->name }}
                </option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="school">Schools</label>
            <select class="form-control selectpicker" name="schools[]" id="school" multiple>
              @foreach($schools as $school)
                <option
                        {{ old('school_id') && $school->id == old('school_id') ? 'selected' : '' }} value="{{ $school->id }}">
                  {{ $school->name }}
                </option>
              @endforeach
            </select>
          </div>

          <div class="form-group school-groups-form">
            <label for="school">School Groups</label>
            <select class="form-control selectpicker" name="school_groups[]" id="school-groups" multiple></select>
          </div>

          <div class="form-group">
            <label for="venue">Type</label>
            <select class="form-control" name="type" id="type">
              @foreach(['Single', 'Multiple'] as $type)
                <option {{ old('type') && $type == old('type') ? 'selected' : '' }} value="{{ $type }}">
                  {{ $type }}
                </option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="description">Details</label>
            <textarea class="form-control text-editor" name="description" cols="30"
                      rows="10">{{ old('description') }}</textarea>
          </div>

          <div class="form-group">
            <label for="description">Equipment & Food</label>
            <textarea class="form-control text-editor" name="equipment_food" cols="30"
                      rows="10">{{ old('equipment_food') }}</textarea>
          </div>

          <div class="form-group">
            <label for="input-1" class="control-label">Select Photos</label>
            <input id="input-1" type="file" class="file" name="files[]" multiple
                   data-show-upload="false"
                   data-allowed-file-extensions='["jpg", "png", "gif", "jpeg"]'>
          </div>

          <div class="form-group">
            <label for="input-document" class="control-label">Select Document</label>
            <input id="input-document" type="file" class="file" name="document"
                   data-show-upload="false" data-allowed-file-extensions='["pdf"]'>
          </div>

          <div id="adding-fields">
            <div class="form-group">
              <label for="places">Maximum Places</label>
              <input type="text" class="form-control" id="places" name="places"
                     value="{{ old('places') }}">
            </div>

            <div class="form-group">
              <label for="cost_per_course">Cost Per event</label>
              <input type="text" class="form-control" id="cost-per-day" name="cost_per_course"
                     value="{{ old('cost_per_course') }}">
            </div>

            <div class="form-group">
              <label for="min-age">Minimum Age</label>
              <input type="number" class="form-control" id="min-age" name="min_age"
                     value="{{ old('min_age') }}">
            </div>

            <div class="form-group">
              <label for="max-age">Maximum Age</label>
              <input type="number" class="form-control" id="max-age" name="max_age"
                     value="{{ old('max_age') }}">
            </div>

            <div class="input_fields_wrap">
              <div class="input_date">
                <div class="form-group">
                  <label for="places">Start date</label>
                  <div class="input-group d date form_datetime">
                    <input type="text" class="form-control" name="start_date"
                           value="{{ old('start_date') }}">
                  <span class="input-group-btn">
                 <button class="btn default date-set" type="button">
                   <i class="fa fa-calendar"></i>
                 </button>
              </span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="places">End date</label>
                  <div class="input-group d date form_datetime">
                    <input type="text" class="form-control" name="end_date"
                           value="{{ old('end_date') }}">
                  <span class="input-group-btn">
                 <button class="btn default date-set" type="button">
                   <i class="fa fa-calendar"></i>
                 </button>
              </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="excerpt">Event Overview</label>
            <textarea class="form-control" name="excerpt" id="excerpt" cols="30"
                      rows="10">{{ old('excerpt') }}</textarea>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="allow-deposit">Allow Deposit</label>
              <select class="form-control" name="allow_deposit" id="allow-deposit">
                <option value="0">Pay in full</option>
                <option value="1">Pay deposit</option>
              </select>
            </div>

            <label><input class="margin-right-10" type="checkbox" name="required_passport" value="1">Required
              Participant
              Passport Details</label><br>
            <label><input class="margin-right-10" type="checkbox" name="required_swim_option" value="1">Required
              Participant
              Swim Option</label><br>
            <label><input class="margin-right-10" type="checkbox" name="allow_register" value="1">Allow registration
              without payment</label>
          </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
