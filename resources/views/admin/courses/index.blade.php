@extends('layouts.admin')

@section('title')
    Events
@endsection

@section('breadcrumbs')
    <li><span>Events</span></li>
@endsection

@section('js')
    <script src="/js/admin/courses/courses.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">
                Events
            </div>
        </div>
        <div class="portlet-body">
            @include('partials.courses.index', compact('courses'))

            @permission('course.create')
            <a class="btn btn-success pull-right"
               href="{{ route('admin::courses.create') }}">
                Add New Event
            </a>
            @endpermission
          <div class="clearfix"></div>
        </div>
    </div>
@endsection
