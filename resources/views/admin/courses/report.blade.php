@extends('layouts.admin')

@section('title')
  Event Reports
@endsection

@section('breadcrumbs')
  <li><span>Event Reports</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/courses-reports.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered table-text-center" cellspacing="0" id="course-reports">
            <thead>
            <tr>
              <th rowspan="2"></th>
              @foreach($courses as $course)
                <th colspan="{{ $course->parts->count() ?: 1 }}">{{ $course->name }}</th>
              @endforeach
              <th rowspan="2">Total Orders</th>
              @role('admin')
                <th rowspan="2">Total Revenue</th>
              @endrole
              <th></th>
            </tr>
            <tr>
              @foreach($courses as $course)
                @forelse($course->parts as $index => $part)
                  <th>{{ $part->name ?: 'Part ' . $index }}</th>
                @empty
                  <th></th>
                @endforelse
              @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($schools as $school)
              @php($totalOrders = 0)
              @php($totalRevenue = 0)
              <tr>
                <td>{{ $school->name }}</td>
                @foreach($courses as $course)
                  @forelse($course->parts as $part)
                    @php($ordersBySchool = $part->orders()->getBySchool($school))
                    @php($currentCount = $ordersBySchool->count())
                    @php($totalRevenue += $ordersBySchool->sum('price'))
                    @php($totalOrders += $currentCount)
                    <td>{{ $currentCount }}</td>
                  @empty
                    <td></td>
                  @endforelse
                @endforeach
                <td>{{ $totalOrders }}</td>
                @role('admin')
                <td>{{ price_currency($totalRevenue) }}</td>
                @endrole
                <td></td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
