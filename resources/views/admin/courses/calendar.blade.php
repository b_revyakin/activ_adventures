@extends('layouts.admin')

@section('title')
  Calendar Events
@endsection

@section('breadcrumbs')
  <li><span> Calendar Events</span></li>
@endsection

@section('css')
  <link href="/css/admin/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
  <script src="/components/moment/min/moment.min.js" type="text/javascript"></script>
  <script src="/components/fullcalendar/dist/fullcalendar.min.js" type="text/javascript"></script>
  <script src="/js/admin/courses/calendar.js" type="text/javascript"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit calendar">
    <div class="portlet-title">
      <div class="caption">
        Calendar Events
      </div>
      <div class="tools"></div>
    </div>
    <div class="portlet-body">
      <div>
        <div id="calendar" class="has-toolbar"></div>
      </div>
    </div>
  </div>
@endsection
