@extends('layouts.admin')

@section('title')
  Events Archive
@endsection

@section('breadcrumbs')
  <li><span>Events Archive</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/courses.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Events Archive
      </div>
      <div class="tools"></div>
    </div>
    <div class="portlet-body">
      <div>
        @include('partials.courses.index', compact('courses'))
      </div>
    </div>
  </div>
@endsection
