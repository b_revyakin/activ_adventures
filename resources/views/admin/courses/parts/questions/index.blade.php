@extends('layouts.admin')

@section('title')
    Event Part {{ $coursePart->name }} Questions
@endsection

@section('breadcrumbs')
    <li>
        @if($coursePart->course->isSimple())
            <a href="{{ route('admin::courses.index') }}">Events</a>
        @else
            <a href="{{ route('admin::courses.parts.index', [$coursePart->course->id]) }}">Event Parts</a>
        @endif
    </li> <i class="fa fa-circle"></i>
    <li>{{ $coursePart->name }}</li> <i class="fa fa-circle"></i>
    <li><span>Questions</span></li>
@endsection

@section('js')
    <script src="/js/admin/courses/checkboxes.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">
                Event Parts Questions
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-bordered" id="checkboxes-table">
                <thead>
                <tr>
                    <th>Text</th>
                    <th width="120px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($questions as $question)
                    <tr>
                        <td>
                            {{ $question->text }}
                        </td>
                        <td>
                            <a class="btn btn-success pull-right"
                               href="{{ route('admin::course-parts.questions.edit', [$coursePart->id, $question->id]) }}">
                                Edit
                            </a>

                            <form method="post"
                                  action="{{ route('admin::course-parts.questions.destroy', [$coursePart->id, $question->id]) }}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger pull-right">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <a class="btn btn-success pull-right"
               href="{{ route('admin::course-parts.questions.create', [$coursePart->id]) }}">
                Add Question
            </a>

            <div class="clearfix"></div>
        </div>
    </div>
@endsection
