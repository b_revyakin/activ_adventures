@extends('layouts.admin')

@section('title')
    Create Question
@endsection

@section('breadcrumbs')
    <li>
        @if($coursePart->course->isSimple())
            <a href="{{ route('admin::courses.index') }}">Events</a>
        @else
            <a href="{{ route('admin::courses.parts.index', [$coursePart->course->id]) }}">Event Parts</a>
        @endif
    </li> <i class="fa fa-circle"></i>
    <li>{{ $coursePart->name }}</li> <i class="fa fa-circle"></i>
    <li><a href="{{ route('admin::course-parts.questions.index', [$coursePart->id]) }}">Questions</a></li>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">
                Create Question
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <form action="{{ route('admin::course-parts.questions.store', [$coursePart->id]) }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="text">Text</label>
                        <textarea class="form-control" name="text" id="text" cols="30"
                                  rows="3">{{ old('text') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-default">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
