@extends('layouts.admin')

@section('title')
  Create New Event part
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::courses.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::courses.parts.index', [$course->id]) }}">Parts</a></li> <i class="fa fa-circle"></i>
  <li><span>Edit Event Part</span></li>
@endsection

@section('css')
  <link href="/css/admin/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
  <script src="/js/admin/courses/edit.js"></script>
  <script src="/js/vendor/bootstrap-datetimepicker.min.js"></script>
  <script src="{{ asset('js/admin/fileinput/js/fileinput.js') }}"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create Event Part
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::courses.parts.store', [$course->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
          </div>

          <div class="form-group">
            <label for="places">Maximum Places</label>
            <input type="text" class="form-control" id="places" name="places"
                   value="{{ old('places') }}">
          </div>

          <div class="form-group">
            <label for="cost_per_course">Cost Per Event</label>
            <input type="text" class="form-control" id="cost-per-day" name="cost_per_course"
                   value="{{ old('cost_per_course') }}">
          </div>


          <div class="form-group">
            <label for="min-age">Minimum Age</label>
            <input type="number" class="form-control" id="min-age" name="min_age"
                   value="{{ old('min_age') }}">
          </div>

          <div class="form-group">
            <label for="max-age">Maximum Age</label>
            <input type="number" class="form-control" id="max-age" name="max_age"
                   value="{{ old('max_age') }}">
          </div>

          <div class="form-group">
            <label for="places">Start date</label>
            <div class="input-group d date form_datetime">
              <input type="text" class="form-control" name="start_date"
                     value="{{ old('start_date') }}">
              <span class="input-group-btn">
                 <button class="btn default date-set" type="button">
                   <i class="fa fa-calendar"></i>
                 </button>
              </span>
            </div>
          </div>

          <div class="form-group">
            <label for="places">End date</label>
            <div class="input-group d date form_datetime">
              <input type="text" class="form-control" name="end_date"
                     value="{{ old('end_date') }}">
              <span class="input-group-btn">
                 <button class="btn default date-set" type="button">
                   <i class="fa fa-calendar"></i>
                 </button>
              </span>
            </div>
          </div>

          <div class="form-group">
            <label for="excerpt">Event Overview</label>
            <textarea class="form-control" name="excerpt" id="excerpt" cols="30"
                      rows="10">{{ old('excerpt') }}</textarea>
          </div>

          <div class="form-group">
            <label for="allow-deposit">Allow Deposit</label>
            <select class="form-control" name="allow_deposit" id="allow-deposit">
              <option value="0">Pay in full</option>
              <option value="1">Pay deposit</option>
            </select>
          </div>

          <div>
            <label><input class="margin-right-10" type="checkbox" name="required_passport" value="1">Required
              Participant
              Passport Details</label><br>
            <label><input class="margin-right-10" type="checkbox" name="required_swim_option" value="1">Required
              Participant
              Swim Option</label><br>
            <label><input class="margin-right-10" type="checkbox" name="allow_register" value="1">Allow registration
              without payment</label>
          </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
