<a id="send-message" class="btn btn-success" data-toggle="modal" data-target="#send-message-modal">Send message</a>

<div class="modal fade" role="dialog" aria-labelledby="sendMessageModal" id="send-message-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Message</h4>
      </div>
      <form id="send-message-form" action="{{ route('admin::api::messages.create') }}" enctype="multipart/form-data"
            method="POST">
        {!! csrf_field() !!}
        <div class="modal-body">
          <div class="alert alert-danger hidden" id="errors"></div>
          <div>
            <label class="control-label">Recipients</label>
          </div>
          <div class="mt-radio-inline">
            <label class="mt-radio mt-radio-outline">
              <input type="radio" class="recipient" name="recipient" value="1" checked="checked">Parents
              <span></span>
            </label>
            <label class="mt-radio mt-radio-outline">
              <input type="radio" class="recipient" name="recipient" value="2">Participants
              <span></span>
            </label>
            <label class="mt-radio mt-radio-outline">
              <input type="radio" class="recipient" name="recipient" value="3">Parents and Participants
              <span></span>
            </label>
          </div>
          <div>
            <label class="control-label">Mode</label>
          </div>
          <div class="mt-radio-inline">
            <label class="mt-radio mt-radio-outline">
              <input type="radio" class="mode" name="mode" value="1" checked="checked">Email
              <span></span>
            </label>
            <label class="mt-radio mt-radio-outline">
              <input type="radio" class="mode" name="mode" value="2">SMS
              <span></span>
            </label>
          </div>

          <div class="form-group email-fields">
            <label class="control-label">Subject</label>
            <input class="form-control" type="text" id="subject" name="subject" placeholder="Subject">
          </div>

          <div class="form-group show-editor">
            <label class="control-label">Message</label>
            <textarea id="text-message" class="form-control text-editor-modal" id="text" name="text"
                      placeholder="Message"></textarea>
          </div>

          <div class="form-group email-fields">
            <label for="file">Attachment</label>
            <input type="file" id="file" name="file[]" multiple="multiple">
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-modal" data-dismiss="modal">Cancel</button>
          <button type="button" id="send-message-modal-submit" class="btn btn-success">Send
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
