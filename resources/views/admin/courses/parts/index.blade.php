@extends('layouts.admin')

@section('title')
  Event {{ $course->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::courses.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li>{{ $course->name }}</li> <i class="fa fa-circle"></i>
  <li><span>Parts</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/courses.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Event Parts
      </div>
    </div>
    <div class="portlet-body">

      <table class="table table-bordered datatable-with-filters">
        <thead>
        <tr>
          <td>Name</td>
          <td>Start Date</td>
          <td>End Date</td>
          <td>Actions</td>
        </tr>
        @foreach($parts as $part)
          <tr>
            <td>{{$part->name}}</td>
            <td>{{$part->start_date}}</td>
            <td>{{$part->end_date}}</td>
            <td>
              <a class="btn btn-success pull-right"
                 href="{{ route('admin::courses.parts.edit', [$course->id, $part->id]) }}">
                Edit
              </a>

              <a class="btn btn-danger pull-right"
                 href="{{ route('admin::courses.parts.destroy', [$course->id, $part->id]) }}">
                Delete
              </a>

              <a class="btn btn-default pull-right"
                 href="{{ route('admin::course-parts.checkboxes.index', [$part->id]) }}">
                Checkboxes
              </a>

              <a class="btn btn-default pull-right"
                 href="{{ route('admin::faq.course-parts.categories.index', [$part->id]) }}">
                FAQs
              </a>

              <a class="btn btn-default pull-right"
                 href="{{ route('admin::course-parts.questions.index', [$part->id]) }}">
                Questions
              </a>
            </td>
          </tr>
        @endforeach
        </thead>
      </table>

      @permission('course.questionnaire')
      <a href="{{ route('admin::courses.questions.index', [$course->id]) }}"
         class="btn btn-primary">
        Medical Questions
      </a>
      @endpermission

      @permission('course.create')
      <a class="btn btn-success pull-right"
         href="{{ route('admin::courses.parts.create', [$course->id]) }}">
        Add New Event Part
      </a>
      @endpermission

      <div class="clearfix"></div>
    </div>
  </div>
@endsection
