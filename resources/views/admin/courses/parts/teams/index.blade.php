@extends('layouts.admin')

@section('title')
  {{ $coursePart->full_name }} Teams
@endsection

@section('breadcrumbs')
  <li>
    @if($coursePart->course->isSimple())
      <a href="{{ route('admin::courses.index') }}">Events</a>
    @else
      <a href="{{ route('admin::courses.parts.index', [$coursePart->course->id]) }}">Event Parts</a>
    @endif
  </li><i class="fa fa-circle"></i>
  <li>{{ $coursePart->full_name }}</li><i class="fa fa-circle"></i>
  <li><span>Teams</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/teams.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Teams
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <table class="table table-bordered" id="teams-table">
          <thead>
          <tr>
            <th>Name</th>
            <th>Count Orders</th>
            <th class="actions">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach($teams as $team)
            <tr>
              <td>{{ $team->name }}</td>
              <td>{{ $team->orders->count() }}</td>
              <td class="btn-group-xs">
                <a href="{{ route('admin::course-parts.teams.edit', [$coursePart->id, $team->id]) }}"
                   class="btn btn-warning">
                  Edit
                </a>
                <form class="inline" method="POST"
                      action="{{ route('admin::course-parts.teams.destroy', [$coursePart->id, $team->id]) }}">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="btn btn-success pull-right"
           href="{{ route('admin::course-parts.teams.create', [$coursePart->id]) }}">
          Add New Team
        </a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
