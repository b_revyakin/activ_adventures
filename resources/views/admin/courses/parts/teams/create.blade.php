@extends('layouts.admin')

@section('title')
  Create Team
@endsection

@section('breadcrumbs')
  <li>
    @if($coursePart->course->isSimple())
      <a href="{{ route('admin::courses.index') }}">Events</a>
    @else
      <a href="{{ route('admin::courses.parts.index', [$coursePart->course->id]) }}">Event Parts</a>
    @endif
  </li><i class="fa fa-circle"></i>
  <li>{{ $coursePart->full_name }}</li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::course-parts.teams.index', [$coursePart->id]) }}">Teams</a></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Create Team
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::course-parts.teams.store', [$coursePart->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
          </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection