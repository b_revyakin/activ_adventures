@extends('layouts.admin')

@section('title')
  Edit Team {{ $team->name }}
@endsection

@section('breadcrumbs')
  <li>
    @if($coursePart->course->isSimple())
      <a href="{{ route('admin::courses.index') }}">Events</a>
    @else
      <a href="{{ route('admin::courses.parts.index', [$coursePart->course->id]) }}">Event Parts</a>
    @endif
  </li><i class="fa fa-circle"></i>
  <li>{{ $coursePart->full_name }}</li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::course-parts.teams.index', [$coursePart->id]) }}">Teams</a></li><i
          class="fa fa-circle"></i>
  <li>{{ $team->name }}</li><i class="fa fa-circle"></i>
  <li>Edit</li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Edit Team {{ $team->name }}
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::course-parts.teams.update', [$coursePart->id, $team->id]) }}"
              method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{ old('name', $team->name) }}">
          </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
