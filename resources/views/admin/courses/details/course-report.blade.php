@extends('admin.courses.details.layout')

@section('content-table')
  <div id="event-report-table">
    @include('partials.courses.report', compact('courses'))
  </div>
@endsection
