@extends('layouts.admin')

@section('title')
  {{ $course->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::courses.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li><span>{{ $course->name }}</span></li>
@endsection

@section('js')
  <script src="/js/plugins/jquery.form.min.js"></script>
  <script src="/js/admin/courses/datatable.js"></script>
  <script src="/js/admin/courses/reports.js"></script>
  <script src="/js/admin/text-editor.js"></script>
  <script src="/js/modal-window.js"></script>
@endsection

@section('content')
  <div id="overlay">
    <div>
      <div class="loading-message">
        <div class="block-spinner-bar">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="ajax-messages"></div>
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="btn-group event-buttons">
        <a id="event-register-b"
           class="btn btn-default  {{ $page === 'register-b' ? 'active disable' : '' }}"
           href="{{ route('admin::courses.details.register-b', [$course->id]) }}">Event Register</a>
        <a id="bookings" class="btn btn-default {{ $page === 'register-bookings' ? 'active disable' : '' }}"
           href="{{ route('admin::courses.details.register-bookings', [$course->id]) }}">Registered Only</a>
        <a id="event-register" class="btn btn-default  {{ $page === 'register' ? 'active disable' : '' }}"
           href="{{ route('admin::courses.details.register', [$course->id]) }}">Medical Register</a>
        <a id="event-register"
           class="btn btn-default  {{ $page === 'register-with-emergency-contacts' ? 'active disable' : '' }}"
           href="{{ route('admin::courses.details.register', [$course->id, true]) }}">Medical & Contacts Register</a>
        <a class="btn btn-default  {{ $page === 'payment-register' ? 'active disable' : '' }}"
           href="{{ route('admin::courses.details.payment-register', [$course->id]) }}">Payment Register</a>
      </div>
      <div id="buttons"></div>
      <div class="btn-group event-buttons pull-left">
        @yield('button-export')
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">

        @yield('content-table')
      </div>
    </div>
  </div>

@endsection
