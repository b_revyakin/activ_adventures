@extends('admin.courses.details.layout')

@section('button-export')
  @include('partials.export-buttons', ['url' => 'admin::courses.details.register-b.export', 'parameter' => false])
@endsection

@section('content-table')
  <div id="events-register-table">
    <table data-select-from="children" class="table table-bordered datatable-events-register-b">
      <thead>
      <tr>
        <td colspan="3">{{ $course->name }}</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <th><input id="select-all" type="checkbox"></th>
        <th>Surname</th>
        <th>Forename</th>
        <th>m/f</th>
        <th>D.O.B.</th>
        <th>School Name</th>
        <th>Team Name</th>
      </tr>
      </thead>
      <tbody>
      @foreach($orders as $order)
        <tr>
          <td><input class="select-id"
                     data-order-id="{{ $order->id }}"
                     data-course-part-id="{{ $order->coursePart->id }}"
                     type="checkbox" name="id"
                     value="{{ $order->child->id }}">
          </td>
          <td>{{ $order->child->last_name }}</td>
          <td>{{ $order->child->first_name }}</td>
          <td>{{ $order->child->sex_string }}</td>
          <td>{{ $order->child->birthday->format('d/m/Y') }}</td>
          <td>{{ $order->child->school->name }}</td>
          <td>{{ $order->coursePartTeam ? $order->coursePartTeam->name : '' }}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>

  @include('admin.courses.parts.send-message-modal-version')
  <input type="hidden" id="course-part-id" value="{{ $course->parts->first()->id }}">
  <a id="add-team" class="btn btn-success" data-toggle="modal" data-target="#add-team-modal">Add Team</a>

  <div class="modal fade" role="dialog" aria-labelledby="selectTeamsModal" id="add-team-modal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Please select a team</h4>
        </div>
        <form>
          <div class="modal-body">
            <select id="selected-team" class="form-control" name="team_id">
              <option value="" disabled selected>Select a team</option>
              @foreach($teams as $team)
                <option value="{{ $team->id }}">{{ $team->name }}</option>
              @endforeach
            </select>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" id="add-team-submit"
                    data-url="{{ route('admin::team.add-orders', [$course->parts->first()->id]) }}"
                    data-dismiss="modal" class="btn btn-success">Add
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
