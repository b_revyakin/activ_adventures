@extends('admin.courses.details.layout')

@section('content-table')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Latest Bookings
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        @include('partials.bookings.requested-registers', ['withEventName' => false])
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  @include('admin.courses.parts.send-message-modal-version')
  @include('partials.modal-windows.booking.action')
@endsection
