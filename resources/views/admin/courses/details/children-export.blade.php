<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Activ</title>

    <style>
        .page-break {
            page-break-after: always;
        }
        .page-break:last-child {
            display: none;
            page-break-after: always;
        }

        .pull-right {
            float: right !important;
        }

        .sub-header {
            font-weight: bold;
            font-size: large;
            display: block;
        }

        .bold {
            font-weight: bold;
        }

        .section {
            margin-top: 0;
            margin-bottom: 0;
        }
    </style>
</head>

<body>
@foreach($orders as $order)

    {{ \Carbon\Carbon::now()->format('m-d-Y') }}<span style="margin-left: 250px"> Activ Adventures</span>

    <h1>{{ $order->coursePart->course->name }}</h1>

    <p class="section">
        <span class="sub-header">Participants Details</span>
        <span class="bold">Surname: </span>{{ $order->child->last_name }}<br>
        <span class="bold">Forename: </span>{{ $order->child->first_name }}<br>
        <span class="bold">Age: </span>{{ $order->child->age }}<br>
        <span class="bold">Date of birth: </span>{{ $order->child->getBirthday()->format('m-d-Y') }}<br>
        <span class="bold">Gender: </span>{{ $order->child->sex_string }}<br>
        <span class="bold">Year In School: </span>: {{ $order->child->yearInSchool->title }}<br>
        <span class="bold">School Name: </span>{{ $order->child->school->name }}<br>
        <span class="bold">Doctor Name: </span>{{ $order->child->doctor_name }}<br>
        <span class="bold">Doctor Contact No.: </span>{{ $order->child->doctor_telephone_number }}<br>
        @php($emergencyContact = $order->child->emergencyContacts()->first())
        <span class="bold">Emergency Contact: </span>{{ $emergencyContact ? $emergencyContact->full_name : '' }}<br>
        <span class="bold">Emergency Contact No: </span>{{ $emergencyContact? $emergencyContact->mobile : '' }}<br>
        {{--Notes: <br>--}}
        @if($order->coursePart->required_swim_option)
            <span class="bold">Swim Ability: </span>{{ $order->orderDetail->canSwim ? $order->orderDetail->canSwim->option : '' }}
            <br>
        @endif
    </p>

    <p class="section">
        <span class="sub-header">Parent details</span>
        Parent Name: {{ $order->child->parent->first_name . ' ' . $order->child->parent->last_name }}<br>
        E-mail Address: {{ $order->child->parent->user->email }}<br>
        Mobile number: {{ $order->child->parent->mobile }}<br>
        Home number: {{ $order->child->parent->home_tel }}<br>
        @if($order->child->parent->work_tel)
            Work number: {{ $order->child->parent->work_tel }}<br>
        @endif
        @if($order->child->parent->address_line_1)
            Address 1: {{ $order->child->parent->address_line_1 }}<br>
        @endif
        @if($order->child->parent->address_line_2)
            Address 2: {{ $order->child->parent->address_line_2 }}<br>
        @endif
        @if($order->child->parent->address_line_3)
            Address 3: {{ $order->child->parent->address_line_3 }}<br>
        @endif
        Postcode: {{ $order->child->parent->post_code }}<br>
    </p>

    <p class="section">
        <span class="sub-header">Venue And Course Details</span>
        @if($order->coursePart->course->venue)
            Venue: {{ $order->coursePart->course->venue->name }}
        @endif
        Course: {{ $order->coursePart->course->name }}<br>
        Dates: @datetime($order->coursePart->start_date) - @datetime($order->coursePart->end_date)
    </p>

    @if($order->child->authorised_person_name_1 && $order->child->authorised_person_number_1)
        <p class="section">
            <span class="sub-header">Emergency Contact Details For Pickup</span>
            Name: {{ $order->child->authorised_person_name_1 }}<br>
            Mobile: {{ $order->child->authorised_person_number_1 }}<br>
            @if( $order->child->authorised_person_name_2 && $order->child->authorised_person_number_2 )
                Name: {{ $order->child->authorised_person_name_2 }}<br>
                Mobile: {{ $order->child->authorised_person_number_2 }}<br>
            @endif
            @if( $order->child->authorised_person_name_3 && $order->child->authorised_person_number_3 )
                Name: {{ $order->child->authorised_person_name_3 }}<br>
                Mobile: {{ $order->child->authorised_person_number_3 }}<br>
            @endif
        </p>
    @endif


    <div class="page-break"></div>
@endforeach

</body>
</html>