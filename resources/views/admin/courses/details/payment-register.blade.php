@extends('admin.courses.details.layout')

@section('button-export')
  @include('partials.export-buttons', ['url' => 'admin::courses.details.payment-register.export', 'parameter' => false])
@endsection

@section('content-table')
  <div id="events-register-table">
    <table data-select-from="children" class="table table-bordered datatable-payment-register">
      <thead>
      <tr>
        <td colspan="3">{{ $course->name }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <th><input id="select-all" type="checkbox"></th>
        <th>Surname</th>
        <th>Forename</th>
        <th>m/f</th>
        <th>D.O.B.</th>
        <th>Booking Date</th>
        <th>Price</th>
        <th>Paid</th>
        <th>Left to Pay</th>
        <th>Payment Status</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      @foreach($orders as $order)
        <tr>
          <td><input class="select-id" data-course-part-id="{{ $order->coursePart->id }}"
                     type="checkbox" name="id" value="{{ $order->child->id }}">
          </td>
          <td>{{ $order->child->last_name }}</td>
          <td>{{ $order->child->first_name }}</td>
          <td>{{ $order->child->sex_string }}</td>
          <td>{{ $order->child->birthday->format('d/m/Y') }}</td>
          <td>{{ $order->booking->created_at->format('d/m/Y H:i:s') }}</td>
          <td>{{ $order->booking->getPrice() }}</td>
          <td>{{ $order->booking->getPaidSum() }}</td>
          <td>{{ $order->booking->getLeftPay() }}</td>
          <td>
            <span class="label label-status
              {{ $order->booking->isProcessing() ? 'label-warning' :'' }}
            {{ $order->booking->isPaid() ? 'label-success' :'' }}
            {{ $order->booking->isCancelled() ? 'label-danger' :'' }}">
              {{ $order->booking->showStatus() }}</span>
          </td>
          <td>
            <a href="{{ route('admin::parents.show', $order->booking->customer->id) }}"
               class="btn btn-default btn-xs">Account Details</a>
            <a href="{{ route('admin::bookings.payments.index', $order->booking->id) }}"
               class="btn btn-default btn-xs">Payments</a>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  @include('admin.courses.parts.send-message-modal-version')
@endsection
