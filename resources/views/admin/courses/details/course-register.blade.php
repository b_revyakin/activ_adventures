@extends('admin.courses.details.layout')

@section('button-export')
    @include('partials.export-buttons', ['url' => 'admin::courses.details.register.export', 'parameter' => $withEmergencyContacts])
@endsection

@section('content-table')
    <div id="events-register-table">
      <table data-select-from="children"
             class="table table-bordered datatable-events{{ $withEmergencyContacts ? '-with-emergency-contacts' : ''  }}">
            <thead>
            <tr>
                <td colspan="7">{{ $course->name }}</td>
                <td class="report-medical-field">Medical</td>
                @if($withEmergencyContacts)
                    <td>Parent's comment</td>
                    <td colspan="5">Main Contact</td>
                    <td colspan="5">Second Contact</td>
                @endif
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th><input id="select-all" type="checkbox"></th>
                <th>Surname</th>
                <th>Forename</th>
                <th>m/f</th>
                <th>D.O.B.</th>
                <th>Participant mobile</th>
                <th>Participant email</th>
                <th class="report-medical-field">Medical Details</th>
                @if($withEmergencyContacts)
                    <th>Parent's comment</th>
                    <th>Surname</th>
                    <th>Forename</th>
                    <th>Relationship</th>
                    <th>Mobile</th>
                    <th>Alt number</th>
                    <th>Surname</th>
                    <th>Forename</th>
                    <th>Relationship</th>
                    <th>Mobile</th>
                    <th>Alt number</th>
                @endif
                <th>Status</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($course->getOrders() as $order)
                <tr data-id="{{ $order->id }}">
                    <td>
                        <input class="select-id" data-course-part-id="{{ $order->coursePart->id }}"
                               type="checkbox" name="id" value="{{ $order->child->id }}">
                    </td>
                    <td>{{ $order->child->last_name }}</td>
                    <td>{{ $order->child->first_name }}</td>
                    <td>{{ $order->child->sex_string }}</td>
                    <td>{{ $order->child->birthday->format('d/m/Y') }}</td>
                    <td>{{ $order->child->mobile }}</td>
                    <td>{{ $order->child->email }}</td>
                    <td class="report-medical-field">
                        <ul>
                            @foreach($order->child->questions as $question)
                                @if($question->pivot->answer)
                                    <li>
                                        <p>{{ $question->title }}<br>
                                            Answer: {{ $question->pivot->text }}</p>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </td>
                    @if($withEmergencyContacts)
                        <td>{{ $order->orderDetail->comment }}</td>
                        @if($order->child->mainContact())
                            <td>{{ $order->child->mainContact()->surname }}</td>
                            <td>{{ $order->child->mainContact()->forename }}</td>
                            <td>{{ $order->child->mainContact()->relationship }}</td>
                            <td>{{ $order->child->mainContact()->mobile }}</td>
                            <td>{{ $order->child->mainContact()->alternative_number }}</td>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                        @if($order->child->secondContact())
                            <td>{{ $order->child->secondContact()->surname }}</td>
                            <td>{{ $order->child->secondContact()->forename }}</td>
                            <td>{{ $order->child->secondContact()->relationship }}</td>
                            <td>{{ $order->child->secondContact()->mobile }}</td>
                            <td>{{ $order->child->secondContact()->alternative_number }}</td>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                    @endif
                    <td>
                        <span class="label label-status
                        {{ $order->booking->isProcessing() ? 'label-warning' :'' }}
                        {{ $order->booking->isPaid() ? 'label-success' :'' }}
                        {{ $order->booking->isCancelled() ? 'label-danger' :'' }}">
                            {{ $order->booking->showStatus() }}</span>
                    </td>
                    <td class="btn-group-xs">
                        <a href="{{ route('admin::bookings.show', [$order->booking->id]) }}"
                           class="btn btn-primary">
                            Show
                        </a>
                        <a href="{{ route('admin::courses.details.child.export', [$course->id, $order->child->id]) }}"
                           class="btn btn-primary">
                            Export Participant
                        </a>

                        @permission('booking.cancel')
                            <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                                    data-target=".bs-modal-sm"
                                    data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                                    data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                                    data-url="{{ route('admin::api::orders.cancel', [$order->id]) }}"
                                    data-ajax="true"
                                    data-action="cancel-order"
                                    data-id="{{ $order->id }}"
                                    data-method="POST">Cancel
                            </button>
                        @endpermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @include('admin.courses.parts.send-message-modal-version')
    @include('partials.modal-windows.booking.action')
@endsection
