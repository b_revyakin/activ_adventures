@extends('layouts.admin')

@section('title')
  {{ $course->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::courses.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li><span>{{ $course->name }}</span></li>
@endsection

@section('js')
  <script src="/js/admin/carousel.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Show Event
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>{{ $course->name }}</dd>
          <dt>Type</dt>
          <dd>{{ $course->type }}</dd>
          <dt>Status</dt>
          <dd>
            @include('partials.active-labels', ['model' => $course])
          </dd>
          <dt>Venue</dt>
          <dd>{{ $course->venue ? $course->venue->name : '' }}</dd>
          <dt>School Year</dt>
          <dd>{{ $course->season->name }}</dd>
          <dt>Schools</dt>
          <dd>
            @forelse($course->schools as $school)
              {{ $school->name }}<br>
            @empty
              Not Selected
            @endforelse
          </dd>
          <dt>Details</dt>
          <dd>{!! $course->description !!}</dd>
          @foreach($course->parts as $index =>$part)
            <br>
            @if(!$course->isSimple())
              <dt><b>Part</b></dt>
              <dd>{{$index + 1}}</dd>
            @endif
            <dt>Start Date</dt>
            <dd>{{ $part->start_date->format('d-m-Y') }}</dd>
            <dt>Start Time</dt>
            <dd>{{ $part->start_date->format('H:m:s') }}</dd>
            <dt>End Date</dt>
            <dd>{{ $part->end_date->format('d-m-Y') }}</dd>
            <dt>End Time</dt>
            <dd>{{ $part->end_date->format('H:m:s') }}</dd>
            <dt>Places</dt>
            <dd>{{ $part->places }}</dd>
            <dt>Cost Per Event</dt>
            <dd>{{ $part->cost_per_course }}</dd>
            <dt>Minimum Age</dt>
            <dd>{{ $part->min_age }}</dd>
            <dt>Maximum Age</dt>
            <dd>{{ $part->max_age }}</dd>
            <dt>Event Overview</dt>
            <dd>{!! $part->excerpt !!}</dd>
            <dt>Allow registration without payment</dt>
            <dd>{{ $part->allow_register ? 'Yes' : 'No' }}</dd>
          @endforeach
        </dl>
        @if($course->document)
          <dl class="dl-horizontal">
            <dt>Document:</dt>
            <dd><a href="{{ route('admin::course.document', [$course->id]) }}"><i class="fa fa-file-pdf-o"
                                                                                  aria-hidden="true"></i></a></dd>
          </dl>
        @endif
        @include('partials.photos.show', [ 'model' => $course, 'routeName' => 'admin::course.photos' ])
      </div>
    </div>
  </div>
@endsection
