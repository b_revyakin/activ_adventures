@extends('layouts.admin')

@section('title')
  Copy '{{ $course->name }}'
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::courses.index') }}">Events</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::courses.show', [$course->id]) }}">{{ $course->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><span>Copy Events</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit">
    <div class="portlet-title">
      <div class="caption">
        Copy Event
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::courses.copy.store', [$course->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="name">Name new event</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name"
                   value="{{ old('name') }}">
          </div>

          <button type="submit" class="btn btn-default">
            Copy
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
