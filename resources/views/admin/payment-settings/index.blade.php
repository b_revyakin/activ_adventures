@extends('layouts.admin')

@section('title')
  Payment Systems
@endsection

@section('breadcrumbs')
  <li><span>Payment Systems</span></li>
@endsection

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
  <script>
    $('.switcher').bootstrapSwitch();
  </script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Payment Systems:
      </div>
    </div>
    <div class="portlet-body">

      <div class="row">

        <form action="{{ route('admin::payment-systems.save') }}" method="post">
          <div class="col-md-12">
            {!! csrf_field() !!}
            @foreach($paymentSystems as $paymentSystem)
              <div class="checkbox">
                <div class="col-md-2">
                  <label for="switcher">{{ $paymentSystem->title }}</label>
                  <input id="switcher" class="switcher pull-right" type="checkbox" name="{{ $paymentSystem->system }}"
                         value="{{ $paymentSystem->active }}" {{ $paymentSystem->active? 'checked' : '' }}>
                </div>
              </div>
            @endforeach
          </div>
          <div class="col-md-12">
              <div class="col-md-12">
                <button class="btn btn-success save-payment-systems" type="submit">Save</button>
              </div>
          </div>
        </form>

      </div>
    </div>
  </div>
@endsection
