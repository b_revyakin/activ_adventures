@extends('layouts.admin')

@section('css')
  <link href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet"
        type="text/css"/>
  <style>
    .datepicker > div {
      display: block;
    }
  </style>
@endsection

@section('js')
  <script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="/js/admin/datepicker.js"></script>
@endsection

@section('title')
  Add New Participant to {{ $parent->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::parents.index') }}">Parents</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.children.index', [$parent->id]) }}">Participants</a></li> <i
      class="fa fa-circle"></i>
  <li><span>Add Participant</span></li>
@endsection

@section('content')

  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Create New Participant to {{ $parent->name() }}
      </div>

    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::parents.children.store', [$parent->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" id="first_name" placeholder="First Name"
                   name="first_name"
                   value="{{ old('first_name') }}">
          </div>

          <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" id="last_name" placeholder="Last Name"
                   name="last_name"
                   value="{{ old('last_name') }}">
          </div>

          <div class="form-group">
            <label for="birthday">Date of Birth</label>
            <input type="text" class="form-control date-picker" id="birthday"
                   placeholder="Date of Birth"
                   name="birthday"
                   value="{{ old('birthday') }}">
          </div>

          <div class="form-group">
            <label for="sex">Male / Female</label>
            <select name="sex" id="sex" class="form-control">
              <option {{ old('sex') ? 'selected' : '' }} value="1">Male</option>
              <option {{ !old('sex') ? 'selected' : '' }} value="0">Female</option>
            </select>
          </div>

          <div class="form-group">
            <label for="home_address">Home Address</label>
            <input type="text" class="form-control" id="home_address" placeholder="Home Address"
                   name="home_address"
                   value="{{ old('home_address') }}">
          </div>

          <div class="form-group">
            <label for="home_postcode">Home Postcode</label>
            <input type="text" class="form-control" id="home_postcode" placeholder="Home Postcode"
                   name="home_postcode"
                   value="{{ old('home_postcode') }}">
          </div>

          <div class="form-group">
            <label for="relationship">Relationship to Participant</label>
            <input type="text" class="form-control" id="relationship" name="relationship"
                   placeholder="Relationship to Participant"
                   value="{{ old('relationship') }}">
          </div>

          <div class="form-group">
            <label for="school_id">School</label>
            <select class="form-control" id="school_id" name="school_code">
              @foreach($schools as $item)
                <option value="{{ $item->code }}">{{ $item->name }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="year_in_school">Year in School</label>
            <select class="form-control" id="year_in_school" name="year_in_school_id">
              @foreach($yearInSchool as $item)
                <option value="{{ $item->id }}">{{ $item->title }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email"
                   placeholder="Email"
                   name="email"
                   value="{{ old('email') }}">
          </div>

          <div class="form-group">
            <label for="email_confirmation">Confirm Email</label>
            <input type="email" class="form-control" id="email_confirmation"
                   placeholder="Confirm Email"
                   name="email_confirmation"
                   value="{{ old('email_confirmation') }}">
          </div>

          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input type="text" class="form-control" id="mobile"
                   placeholder="Mobile"
                   name="mobile"
                   value="{{ old('mobile') }}">
          </div>

          <div class="form-group">
            <label for="doctor_name">Doctor's Name</label>
            <input type="text" class="form-control" id="doctor_name" placeholder="Doctor's Name"
                   name="doctor_name"
                   value="{{ old('doctor_name') }}">
          </div>

          <div class="form-group">
            <label for="doctor_telephone_number">Doctor's Telephone Number</label>
            <input type="text" class="form-control" id="doctor_telephone_number"
                   placeholder="Doctor's Telephone Number" name="doctor_telephone_number"
                   value="{{ old('doctor_telephone_number') }}">
          </div>

          <div class="form-group">
            <label for="medical_advice_and_treatment">Medical Advice & Treatment</label>
            <select name="medical_advice_and_treatment" id="medical_advice_and_treatment"
                    class="form-control">
              <option {{ old('medical_advice_and_treatment') ? 'selected' : '' }} value="1">Yes
              </option>
              <option {{ !old('medical_advice_and_treatment') ? 'selected' : '' }} value="0">No
              </option>
            </select>
          </div>

          <div class="form-group">
            <label for="medication">Medication</label>
            <textarea name="medication" id="medication" cols="30" rows="4"
                      class="form-control">{{ old('medication') }}</textarea>
          </div>

          <div class="form-group">
            <label for="special_requirements">Special Requirements</label>
            <textarea name="special_requirements" id="special_requirements" cols="30" rows="4"
                      class="form-control">{{ old('special_requirements') }}</textarea>
          </div>

          <hr>

          <div class="form-group">
            <label>Emergency Contacts</label>
          </div>
          <p>Main Contact</p>
          <div class="form-group">
            <label for="main_contact_forename">Forename</label>
            <input type="text" class="form-control" id="main_contact_forename"
                   placeholder="Forename" name="main_contact[forename]"
                   value="{{ old('main_contact[forename]') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_surname">Surname</label>
            <input type="text" class="form-control" id="main_contact_surname"
                   placeholder="Surname" name="main_contact[surname]"
                   value="{{ old('main_contact[surname]') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_relationship">Relationship</label>
            <input type="text" class="form-control" id="main_contact_relationship"
                   placeholder="Relationship" name="main_contact[relationship]"
                   value="{{ old('main_contact[relationship]') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_mobile">Mobile</label>
            <input type="text" class="form-control" id="main_contact_mobile"
                   placeholder="Mobile" name="main_contact[mobile]"
                   value="{{ old('main_contact[mobile]') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_alternative_number">Alternative Number</label>
            <input type="text" class="form-control" id="main_contact_alternative_number"
                   placeholder="Alternative Number" name="main_contact[alternative_number]"
                   value="{{ old('main_contact[alternative_number]') }}">
          </div>

          <p>Second Contact</p>
          <div class="form-group">
            <label for="second_contact_forename">Forename</label>
            <input type="text" class="form-control" id="second_contact_forename"
                   placeholder="Forename" name="second_contact[forename]"
                   value="{{ old('second_contact[forename]') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_surname">Surname</label>
            <input type="text" class="form-control" id="second_contact_surname"
                   placeholder="Surname" name="second_contact[surname]"
                   value="{{ old('second_contact[surname]') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_relationship">Relationship</label>
            <input type="text" class="form-control" id="second_contact_relationship"
                   placeholder="Relationship" name="second_contact[relationship]"
                   value="{{ old('second_contact[relationship]') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_mobile">Mobile</label>
            <input type="text" class="form-control" id="second_contact_mobile"
                   placeholder="Mobile" name="second_contact[mobile]"
                   value="{{ old('second_contact[mobile]') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_alternative_number">Alternative Number</label>
            <input type="text" class="form-control" id="second_contact_alternative_number"
                   placeholder="Alternative Number" name="second_contact[alternative_number]"
                   value="{{ old('second_contact[alternative_number]') }}">
          </div>

          <hr>

          <div class="form-group">
            <label>Second Parent</label>
          </div>
          <div class="form-group">
            <label for="second_parent_first_name">First Name</label>
            <input type="text" class="form-control" id="second_parent_first_name"
                   placeholder="First Name" name="second_parent[first_name]"
                   value="{{ old('second_parent[first_name]') }}">
          </div>
          <div class="form-group">
            <label for="second_parent_last_name">Last Name</label>
            <input type="text" class="form-control" id="second_parent_last_name"
                   placeholder="Last Name" name="second_parent[last_name]"
                   value="{{ old('second_parent[last_name]') }}">
          </div>
          <div class="form-group">
            <label for="second_parent_email">Email</label>
            <input type="text" class="form-control" id="second_parent_email"
                   placeholder="Email" name="second_parent[email]"
                   value="{{ old('second_parent[email]') }}">
          </div>
          <div class="form-group">
            <label for="second_parent_phone">Phone</label>
            <input type="text" class="form-control" id="second_parent_phone"
                   placeholder="Phone" name="second_parent[phone]"
                   value="{{ old('second_parent[phone]') }}">
          </div>

          <button type="submit" class="btn btn-default">
            Create
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
