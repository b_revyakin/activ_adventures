@extends('layouts.admin')

@section('title')
  Medical Questions For {{ $child->name }}
@endsection

@section('breadcrumbs')
  <li><a href="">Children</a></li> <i class="fa fa-circle"></i>
  <li><a href="">{{ $child->name }}</a></li> <i
          class="fa fa-circle"></i>
  <li><span>Medical Questions</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/questionnaires.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Medical Questions For {{ $child->name }}
      </div>
      <a class="btn btn-success pull-right" id="add-question"
         href="{{ route('admin::children.questions.create', [$child->id]) }}">
        Edit Question
      </a>
    </div>
    <div class="portlet-body">
      <table class="table table-bordered" id="questionnaire-table">
        <thead>
        <tr>
          <th>Questions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($questionsByCategories as $category => $questions)
          <tr>
            <td><h3>{{ $categories[$category]['title'] }}</h3>
              <br>
              <i>{{ $categories[$category]['description'] }}</i>
            </td>
          </tr>
          @foreach($questions as $question)
            <tr>
              <td>{{ $question->title }}</td>
            </tr>
          @endforeach
        @endforeach
        </tbody>
      </table>
      <div class="clearfix"></div>
    </div>
  </div>
@endsection
