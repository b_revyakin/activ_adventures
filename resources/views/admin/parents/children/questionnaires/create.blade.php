@extends('layouts.admin')

@section('title')
  Medical Questions For {{ $child->name }}
@endsection

@section('breadcrumbs')
  <li><a href="">Events</a></li> <i class="fa fa-circle"></i>
  <li><a href="">{{ $child->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><span>Edit Medical Questions</span></li>
@endsection

@section('js')
  <script src="/js/admin/courses/questional-edit.js"></script>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-title">
      <div class="caption">
        Edit Medical Questions To {{ $child->name }}
      </div>
    </div>
    <div class="portlet-body">
      <form id="add-form" action="{{ route('admin::children.questions.store', $child->id) }}" method="POST">
        {{ csrf_field() }}
        <table class="table table-bordered" id="questionnaire-table">
          <tbody>
          @foreach($categories as $category)
            <tr>
              <td><h3> {{ $category->title }} </h3> <br>
                <div>{{ $category->description }}</div>
              </td>
              <td>
                <div class="mt-checkbox-list">
                  <label class="mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" class="select_category" data-category_id="{{ $category->id }}">
                    <span></span>
                  </label>
                </div>
              </td>
            </tr>
            @foreach($category->questions as $question)
              <tr>
                <td>{{ $question->title }}</td>
                <td>
                  <div class="mt-checkbox-list">
                    <label class="mt-checkbox mt-checkbox-outline">
                      <input data-category_id="{{ $category->id }}" type="checkbox" class="question"
                             value="{{ $question->id }}"
                             @if(in_array($question->id, $selectQuestionIds)) checked @endif name="question_ids[]">
                      <span></span>
                    </label>
                  </div>
                </td>
              </tr>
            @endforeach
          @endforeach
          </tbody>
        </table>
        <button type="submit" class="btn btn-primary pull-right">Save</button>
      </form>
      <div class="clearfix"></div>
    </div>
  </div>
@endsection