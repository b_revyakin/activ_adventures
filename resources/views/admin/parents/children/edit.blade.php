@extends('layouts.admin')

@section('css')
  <link href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet"
        type="text/css"/>
  <style>
    .datepicker > div {
      display: block;
    }
  </style>
@endsection

@section('js')
  <script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="/js/admin/datepicker.js"></script>
@endsection

@section('title')
  Edit {{ $parent->name }}'s Participant {{ $child->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::parents.index') }}">Parents</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.children.index', [$parent->id]) }}">Participants</a></li> <i
      class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.children.show', [$parent->id, $child->id]) }}">{{ $child->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')

  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Update New Participant to {{ $parent->name() }}
      </div>

    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::parents.children.update', [$parent->id, $child->id]) }}"
              method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" id="first_name" placeholder="First Name"
                   name="first_name"
                   value="{{ old('first_name', $child->first_name) }}">
          </div>

          <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" id="last_name" placeholder="Last Name"
                   name="last_name"
                   value="{{ old('last_name', $child->last_name) }}">
          </div>

          <div class="form-group">
            <label for="birthday">Date of Birth</label>
            <input type="text" class="form-control date-picker" id="birthday"
                   placeholder="Date of Birth"
                   name="birthday"
                   value="{{ old('birthday', $child->birthday->format('d-m-Y')) }}">
          </div>

          <div class="form-group">
            <label for="sex">Male / Female</label>
            <select name="sex" id="sex" class="form-control">
              <option {{ old('sex', $child->sex) ? 'selected' : '' }} value="1">Male</option>
              <option {{ !old('sex', $child->sex) ? 'selected' : '' }} value="0">Female</option>
            </select>
          </div>

          <div class="form-group">
            <label for="home_address">Home Address</label>
            <input type="text" class="form-control" id="home_address" placeholder="Home Address"
                   name="home_address"
                   value="{{ old('home_address', $child->home_address) }}">
          </div>

          <div class="form-group">
            <label for="home_postcode">Home Postcode</label>
            <input type="text" class="form-control" id="home_postcode" placeholder="Home Postcode"
                   name="home_postcode"
                   value="{{ old('home_postcode', $child->home_postcode) }}">
          </div>

          <div class="form-group">
            <label for="relationship">Relationship to Participant</label>
            <input type="text" class="form-control" id="relationship" name="relationship"
                   value="{{ old('relationship', $child->relationship) }}">
          </div>

          <div class="form-group">
            <label for="school_id">School</label>
            <select class="form-control" id="school_id" name="school_code">
              @foreach($schools as $item)
                <option
                        {{ $child->school_id === $item->id ? 'selected' : '' }} value="{{ $item->code }}">{{ $item->name }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="year_in_school">Year in School</label>
            <select class="form-control" id="year_in_school" name="year_in_school_id">
              @foreach($yearInSchool as $item)
                <option
                    {{ $child->year_in_school_id === $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->title }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email"
                   placeholder="Email"
                   name="email"
                   value="{{ old('email', $child->email) }}">
          </div>

          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input type="text" class="form-control" id="mobile"
                   placeholder="Mobile"
                   name="mobile"
                   value="{{ old('mobile', $child->mobile) }}">
          </div>

          <div class="form-group">
            <label for="doctor_name">Doctor's Name</label>
            <input type="text" class="form-control" id="doctor_name" placeholder="Doctor's Name"
                   name="doctor_name"
                   value="{{ old('doctor_name', $child->doctor_name) }}">
          </div>

          <div class="form-group">
            <label for="doctor_telephone_number">Doctor's Telephone Number</label>
            <input type="text" class="form-control" id="doctor_telephone_number"
                   placeholder="Doctor's Telephone Number" name="doctor_telephone_number"
                   value="{{ old('doctor_telephone_number', $child->doctor_telephone_number) }}">
          </div>

          <div class="form-group">
            <label for="medical_advice_and_treatment">Medical Advice & Treatment</label>
            <select name="medical_advice_and_treatment" id="medical_advice_and_treatment"
                    class="form-control">
              <option
                  {{ old('medical_advice_and_treatment', $child->medical_advice_and_treatment) ? 'selected' : '' }} value="1">
                Yes
              </option>
              <option
                  {{ !old('medical_advice_and_treatment', $child->medical_advice_and_treatment) ? 'selected' : '' }} value="0">
                No
              </option>
            </select>
          </div>

          <div class="form-group">
            <label for="medication">Medication</label>
            <textarea name="medication" id="medication" cols="30" rows="4"
                      class="form-control">{{ old('medication', $child->medication) }}</textarea>
          </div>

          <div class="form-group">
            <label for="special_requirements">Special Requirements</label>
            <textarea name="special_requirements" id="special_requirements" cols="30" rows="4"
                      class="form-control">{{ old('special_requirements', $child->special_requirements) }}</textarea>
          </div>

          <hr>

          <div class="form-group">
            <label>Emergency Contacts</label>
          </div>
          <p>Main Contact</p>
          <div class="form-group">
            <label for="main_contact_forename">Forename</label>
            <input type="text" class="form-control" id="main_contact_forename"
                   placeholder="Forename" name="emergency_contacts[0][forename]"
                   value="{{ old('emergency_contacts[0][forename]', $child->mainContact() ? $child->mainContact()->forename : '') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_surname">Surname</label>
            <input type="text" class="form-control" id="main_contact_surname"
                   placeholder="Surname" name="emergency_contacts[0][surname]"
                   value="{{ old('emergency_contacts[0][surname]', $child->mainContact() ? $child->mainContact()->surname : '') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_relationship">Relationship</label>
            <input type="text" class="form-control" id="main_contact_relationship"
                   placeholder="Relationship" name="emergency_contacts[0][relationship]"
                   value="{{ old('emergency_contacts[0][relationship]', $child->mainContact() ? $child->mainContact()->relationship : '') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_mobile">Mobile</label>
            <input type="text" class="form-control" id="main_contact_mobile"
                   placeholder="Mobile" name="emergency_contacts[0][mobile]"
                   value="{{ old('emergency_contacts[0][mobile]', $child->mainContact() ? $child->mainContact()->mobile : '') }}">
          </div>
          <div class="form-group">
            <label for="main_contact_alternative_number">Alternative Number</label>
            <input type="text" class="form-control" id="main_contact_alternative_number"
                   placeholder="Alternative Number" name="emergency_contacts[0][alternative_number]"
                   value="{{ old('emergency_contacts[0][alternative_number]', $child->mainContact() ? $child->mainContact()->alternative_number : '') }}">
          </div>

          <p>Second Contact</p>
          <div class="form-group">
            <label for="second_contact_forename">Forename</label>
            <input type="text" class="form-control" id="second_contact_forename"
                   placeholder="Forename" name="emergency_contacts[1][forename]"
                   value="{{ old('emergency_contacts[1][forename]', $child->secondContact() ? $child->secondContact()->forename : '') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_surname">Surname</label>
            <input type="text" class="form-control" id="second_contact_surname"
                   placeholder="Surname" name="emergency_contacts[1][surname]"
                   value="{{ old('emergency_contacts[1][surname]', $child->secondContact() ? $child->secondContact()->surname : '') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_relationship">Relationship</label>
            <input type="text" class="form-control" id="second_contact_relationship"
                   placeholder="Relationship" name="emergency_contacts[1][relationship]"
                   value="{{ old('emergency_contacts[1][relationship]', $child->secondContact() ? $child->secondContact()->relationship : '') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_mobile">Mobile</label>
            <input type="text" class="form-control" id="second_contact_mobile"
                   placeholder="Mobile" name="emergency_contacts[1][mobile]"
                   value="{{ old('emergency_contacts[1][mobile]', $child->secondContact() ? $child->secondContact()->mobile : '') }}">
          </div>
          <div class="form-group">
            <label for="second_contact_alternative_number">Alternative Number</label>
            <input type="text" class="form-control" id="second_contact_alternative_number"
                   placeholder="Alternative Number" name="emergency_contacts[1][alternative_number]"
                   value="{{ old('emergency_contacts[1][alternative_number]', $child->secondContact() ? $child->secondContact()->alternative_number : '') }}">
          </div>

          <hr>

          <div class="form-group">
            <label>Second Parent</label>
          </div>
          <div class="form-group">
            <label for="second_parent_first_name">First Name</label>
            <input type="text" class="form-control" id="second_parent_first_name"
                   placeholder="First Name" name="second_parent[first_name]"
                   value="{{ old('second_parent[first_name]', $child->secondParent ? $child->secondParent->first_name : '') }}">
          </div>
          <div class="form-group">
            <label for="second_parent_last_name">Last Name</label>
            <input type="text" class="form-control" id="second_parent_last_name"
                   placeholder="Last Name" name="second_parent[last_name]"
                   value="{{ old('second_parent[last_name]', $child->secondParent ? $child->secondParent->last_name : '') }}">
          </div>
          <div class="form-group">
            <label for="second_parent_email">Email</label>
            <input type="text" class="form-control" id="second_parent_email"
                   placeholder="Email" name="second_parent[email]"
                   value="{{ old('second_parent[email]', $child->secondParent ? $child->secondParent->email : '') }}">
          </div>
          <div class="form-group">
            <label for="second_parent_phone">Phone</label>
            <input type="text" class="form-control" id="second_parent_phone"
                   placeholder="Phone" name="second_parent[phone]"
                   value="{{ old('second_parent[phone]', $child->secondParent ? $child->secondParent->phone : '') }}">
          </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection
