@extends('layouts.admin')

@section('title')
  {{ $child->name }} is participant of {{ $parent->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::parents.index') }}">Parents</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.children.index', [$parent->id]) }}">Participants</a></li> <i
      class="fa fa-circle"></i>
  <li><span>{{ $child->name }}</span></li>
@endsection

@section('content')

  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        {{ $parent->name }}'s Participant
      </div>
      <div class="actions">
        <a class="btn btn-primary"
           href="{{ route('admin::parents.children.index', [$parent->id]) }}">
          All Participants
        </a>
        <a href="{{ route('admin::parents.children.edit', [$parent->id, $child->id]) }}"
           class="btn btn-warning">Edit</a>
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <dl class="dl-horizontal">
          <dt>First Name</dt>
          <dd>{{ $child->first_name }}</dd>
          <dt>Last Name</dt>
          <dd>{{ $child->last_name }}</dd>
          <dt>Email Address</dt>
          <dd>{{ $child->email }}</dd>
          <dt>Mobile</dt>
          <dd>{{ $child->mobile }}</dd>
          <dt>Date of Birth</dt>
          <dd>{{ (new \Carbon\Carbon($child->birthday))->format('d-m-Y') }} ({{ $child->age }} years)</dd>
          <dt>Sex</dt>
          <dd>{{ $child->sex_string }}</dd>
          <dt>Relationship to Participant</dt>
          <dd>{{ $child->relationship }}</dd>
          <dt>School</dt>
          <dd>{{ $child->school->name }}</dd>
          <dt>Home Address</dt>
          <dd>{{ $child->home_address }}</dd>
          <dt>Home Postcode</dt>
          <dd>{{ $child->home_postcode }}</dd>
          <dt>Year in School</dt>
          <dd>{{ $child->yearInSchool->title }}</dd>
          <dt>Doctor's Name</dt>
          <dd>{{ $child->doctor_name }}</dd>
          <dt>Doctor's Telephone Number</dt>
          <dd>{{ $child->doctor_telephone_number }}</dd>
          <dt>Medical Advice & Treatment</dt>
          <dd>{{ $child->medical_advice_and_treatment ? 'Yes' : 'No' }}</dd>
          <dt>Medication</dt>
          <dd>
            <span class="text">{{ $child->medication }}</span>
          </dd>
          <dt>Special Requirements</dt>
          <dd>
            <span class="text">{{ $child->special_requirements }}</span>
          </dd>
        </dl>
      </div>
    </div>
  </div>
@endsection
