@extends('layouts.admin')

@section('title')
    Edit {{ $parent->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::parents.index') }}">Parents</a></li>  <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.show', [$parent->id]) }}">{{ $parent->name }}</a></li>  <i
      class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">

    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::parents.update', [$parent->id]) }}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="title">Title</label>
            <select class="form-control" name="title" id="title">
              <option {{ old('title', $parent->title) === 'Mr' ? 'selected' : '' }} value="Mr">
                Mr
              </option>
              <option {{ old('title', $parent->title) === 'Miss' ? 'selected' : '' }} value="Miss">
                Miss
              </option>
              <option {{ old('title', $parent->title) === 'Mrs' ? 'selected' : '' }} value="Mrs">
                Mrs
              </option>
              <option {{ old('title', $parent->title) === 'Ms' ? 'selected' : '' }} value="Ms">
                Ms
              </option>
              <option {{ old('title', $parent->title) === 'Dr' ? 'selected' : '' }} value="Dr">
                Dr
              </option>
            </select>
                    </div>
          <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" id="first_name"
                   placeholder="First Name" name="first_name"
                   value="{{ old('first_name', $parent->first_name) }}">
          </div>
          <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" id="last_name"
                   placeholder="Last Name" name="last_name"
                   value="{{ old('last_name', $parent->last_name) }}">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email"
                   placeholder="Email" name="email"
                   value="{{ old('email', $parent->user->email) }}">
          </div>
          <div class="form-group">
            <label for="subscribe_to_newsletter">Subscribe to Newsletter</label>
            <select class="form-control" name="subscribe_to_newsletter"
                    id="subscribe_to_newsletter">
              <option
                  {{ old('subscribe_to_newsletter', $parent->subscribe_to_newsletter) ? 'selected' : '' }} value="1">
                True
              </option>
              <option
                  {{ !old('subscribe_to_newsletter', $parent->subscribe_to_newsletter) ? 'selected' : '' }} value="0">
                False
              </option>
            </select>
          </div>
          <div class="form-group">
            <label for="mobile">Mobile Number</label>
            <input type="text" class="form-control" id="mobile"
                   placeholder="Mobile Number" name="mobile"
                   value="{{ old('mobile', $parent->mobile) }}">
          </div>
          <div class="form-group">
            <label for="home_tel">Home Telephone</label>
            <input type="text" class="form-control" id="home_tel"
                   placeholder="Home Telephone" name="home_tel"
                   value="{{ old('home_tel', $parent->home_tel) }}">
          </div>
          <div class="form-group">
            <label for="work_tel">Work Telephone</label>
            <input type="text" class="form-control" id="work_tel"
                   placeholder="Work Telephone" name="work_tel"
                   value="{{ old('work_tel', $parent->work_tel) }}">
          </div>
          <div class="form-group">
            <label for="address_line_1">Address Line 1</label>
            <input type="text" class="form-control" id="address_line_1"
                   placeholder="Address Line 1" name="address_line_1"
                   value="{{ old('address_line_1', $parent->address_line_1) }}">
          </div>
          <div class="form-group">
            <label for="address_line_2">Address Line 2</label>
            <input type="text" class="form-control" id="address_line_2"
                   placeholder="Address Line 2" name="address_line_2"
                   value="{{ old('address_line_2', $parent->address_line_2) }}">
          </div>
          <div class="form-group">
            <label for="city">Town / City</label>
            <input type="text" class="form-control" id="city"
                   placeholder="Town / City" name="city"
                   value="{{ old('city', $parent->city) }}">
          </div>
          <div class="form-group">
            <label for="country">Country</label>
            <select name="country_id" id="country" class="form-control countries">
              @foreach($countries as $country)
                <option
                    {{ $country->id === $parent->country->id ? 'selected' : '' }} value="{{ $country->id }}">{{ $country->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="post_code">Post Code</label>
            <input type="text" class="form-control" id="post_code"
                   placeholder="Post Code" name="post_code"
                   value="{{ old('post_code', $parent->post_code) }}">
          </div>
          <div class="form-group">
            <label for="hear_from">How did you hear about us?</label>
            <input type="text" class="form-control" id="hear_from"
                   placeholder="" name="hear_from"
                   value="{{ old('hear_from', $parent->hear_from) }}">
          </div>

          <button type="submit" class="btn btn-default">Update</button>
        </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/js/countries.js"></script>
@endsection
