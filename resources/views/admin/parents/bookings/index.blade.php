@extends('layouts.admin')

@section('title')
  {{ $customer->first_name }}
@endsection

@section('js')
  <script src="/js/customer/bookings.js"></script>
  <script src="/js/modal-window.js"></script>
@endsection

@section('breadcrumbs')
  <li><span>{{ $customer->first_name }} Bookings</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        {{ $customer->first_name }} Bookings
      </div>
      <div class="actions">
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        @include('partials.customer.bookings.index', compact('bookings'))
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@endsection
