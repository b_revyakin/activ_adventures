@extends('layouts.admin')

@section('title')
  Set Password For {{ $customer->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::parents.index') }}">Parents</a></li>  <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::parents.show', [$customer->id]) }}">{{ $customer->name }}</a></li>  <i
          class="fa fa-circle"></i>
  <li><span>Set Password</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable">
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::parents.set-password', [$customer->id]) }}" method="POST">
          {{ csrf_field() }}

          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password">
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" class="form-control" name="password_confirmation">
          </div>

          <button type="submit" class="btn btn-default">Set Password</button>
        </form>
      </div>
    </div>
  </div>
@endsection
