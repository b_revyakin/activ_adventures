@extends('layouts.admin')

@section('title')
    Edit '{{ $admin->staff->name }}'
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::admins.index') }}">Admins</a></li> <i class="fa fa-circle"></i>
  <li><a href="{{ route('admin::admins.show', [$admin->id]) }}">{{ $admin->staff->name }}</a></li> <i
      class="fa fa-circle"></i>
  <li><span>Edit</span></li>
@endsection

@section('content')
  <div class="portlet light portlet-fit portlet-datatable ">
    <div class="portlet-title">
      <div class="caption">
        Edit Admin
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-container">
        <form action="{{ route('admin::admins.update', [$admin->id]) }}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">

          <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" id="first_name"
                   placeholder="First Name" name="first_name"
                   value="{{ old('first_name', $admin->staff->first_name) }}">
                    </div>
          <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" class="form-control" id="last_name"
                   placeholder="Last Name" name="last_name"
                   value="{{ old('last_name', $admin->staff->last_name) }}">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email"
                   placeholder="Email" name="email"
                   value="{{ old('email', $admin->email) }}">
          </div>
          <div class="form-group">
            <label for="mobile">Mobile Number</label>
            <input type="text" class="form-control" id="mobile"
                   placeholder="Mobile Number" name="mobile"
                   value="{{ old('mobile', $admin->staff->mobile) }}">
          </div>
          <div class="form-group">
            <label for="qualifications">Qualifications</label>
            <input type="text" class="form-control" id="qualifications"
                   placeholder="Qualifications" name="qualifications"
                   value="{{ old('qualifications', $admin->staff->qualifications) }}">
          </div>
          <div class="form-group">
            <label for="about">About</label>
            <textarea class="form-control" name="about" id="about" cols="30"
                      rows="10">{{ old('about', $admin->staff->about) }}</textarea>
          </div>

          <button type="submit" class="btn btn-default">Update</button>
        </form>
            </div>
        </div>
    </div>
@endsection
