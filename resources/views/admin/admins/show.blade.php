@extends('layouts.admin')

@section('title')
    {{ $admin->staff->name }}
@endsection

@section('breadcrumbs')
  <li><a href="{{ route('admin::admins.index') }}">Admins</a></li>  <i class="fa fa-circle"></i>
  <li><span>{{ $admin->staff->name }}</span></li>
@endsection

@section('content')
  <div class="page-content-inner">
    <div class="row">
      <div class="col-md-12">
        <div class="profile-sidebar">
          <div class="portlet light">
            <div class="profile-userpic">
              <img src="{{ $admin->avatar() }}" class="img-responsive" alt="">
            </div>
            <div class="profile-usertitle">
              <div class="profile-usertitle-name"> {{ $admin->staff->first_name }} {{ $admin->staff->last_name }}</div>
              <div class="profile-desc-text"> {{ $admin->email }} </div>
            </div>
          </div>
          <div class="portlet light ">
            <div>
              <h4 class="profile-desc-title">About</h4>
              <span class="profile-desc-text">{{ $admin->staff->about }}</span>
            </div>
          </div>
        </div>
        <div class="profile-content">
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                  <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Show Admin</span>
                  </div>
                </div>
                <div class="portlet-body">
                  <dl class="dl-horizontal">
                    <dt>First Name</dt>
                    <dd>{{ $admin->staff->first_name }}</dd>
                    <dt>Last Name</dt>
                    <dd>{{ $admin->staff->last_name }}</dd>
                    <dt>Email</dt>
                    <dd>{{ $admin->email }}</dd>
                    <dt>Active</dt>
                    <dd>
                      @include('partials.active-labels', ['model' => $admin->staff])
                    </dd>
                    <dt>Mobile Number</dt>
                    <dd>{{ $admin->staff->mobile }}</dd>
                    <dt>Qualifications</dt>
                    <dd>{{ $admin->staff->qualifications }}</dd>
                    <dt>About</dt>
                    <dd>{{ $admin->staff->about }}</dd>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
