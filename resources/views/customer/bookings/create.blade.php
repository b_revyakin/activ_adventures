@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="/components/AngularJS-Toaster/toaster.min.css">
    <link rel="stylesheet" href="/components/angular-bootstrap/ui-bootstrap-csp.css">
    <link rel="stylesheet" href="/components/ui-select/dist/select.min.css">
    <link rel="stylesheet" href="/components/selectize/dist/css/selectize.default.css">
@endsection

@section('js')
    <script src="/components/moment/min/moment.min.js"></script>
    <script src="/components/angular/angular.min.js"></script>
    <script src="/components/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="/components/angular-resource/angular-resource.min.js"></script>
    <script src="/components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="/components/AngularJS-Toaster/toaster.min.js"></script>
    <script src="/components/a0-angular-storage/dist/angular-storage.min.js"></script>
    <script src="/components/ui-select/dist/select.min.js"></script>

    <script type="text/javascript" src="/components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script type="text/javascript" src="/components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>


    <script src="{{ elixir('js/app.js') }}"></script>
    <script src="{{ elixir('js/templates.js') }}"></script>
@endsection

@section('page-header')
@endsection

@section('content')
    <div data-ng-app="booking">
        <toaster-container></toaster-container>

        <ui-view></ui-view>
    </div>
@endsection