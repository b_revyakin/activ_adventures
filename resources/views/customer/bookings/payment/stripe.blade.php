@extends('layouts.app')

@section('page-header')
  Booking №{{ $bookings->id }} {{ $bookings->created_at->format('d-m-Y') }}
@endsection

@section('css')
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">
              <h3>Payment</h3>
            </div>
          </div>
          <div class="panel-body">
            <div class="well">
              <strong>Booking Payment Details</strong>
              <dl class="dl-horizontal">
                <dt>Price</dt>
                <dd>{{ $bookings->getPrice() }}</dd>
                <dt>Paid</dt>
                <dd>{{ $bookings->getPaidSum() }}</dd>
                <dt>Left to Pay</dt>
                <dd>{{ $bookings->getLeftPay() }}</dd>
                <dt>Payments</dt>
                @if($bookings->payments()->paid()->count())
                  <dd>
                    @foreach($bookings->payments()->paid()->get() as $payment)
                      <li><span>{{ $payment->getDate() }}</span> - <span>{{ $payment->getAmount() }}</span></li>
                    @endforeach
                  </dd>
                @else
                  <dd>
                    Not Payments
                  </dd>
                @endif
              </dl>
            </div>
            <form role="form" id="payment-form" method="post"
                  action="{{ route('customer::bookings.payment.stripe.pay', ['bookings' => $bookings->id]) }}"
                  autocomplete="off">
              {!! csrf_field() !!}
              <div class="row">
                <div class="col-xs-12">
                  <div class="payment-errors"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label for="cardNumber">CARD NUMBER</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="cardNumber" placeholder="Valid Card Number" required autofocus data-stripe="number" />
                      <span class="input-group-addon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-7 col-md-7">
                  <div class="form-group">
                    <label for="expMonth">EXPIRATION DATE</label>
                    <div class="row">
                      <div class="col-xs-6 col-lg-6 pl-ziro">
                        <input type="text" class="form-control" name="expMonth" placeholder="MM" required data-stripe="exp_month" />
                      </div>
                      <div class="col-xs-6 col-lg-6 pl-ziro">
                        <input type="text" class="form-control" autocomplete="off" name="expYear" placeholder="YY"
                               required data-stripe="exp_year"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-5 col-md-5 pull-right">
                  <div class="form-group">
                    <label for="cvCode">CV CODE</label>
                    <input type="password" class="form-control" autocomplete="off" name="cvCode" placeholder="CV"
                           required data-stripe="cvc"/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  @if($bookings->orders()->count() == 1 && !$bookings->payments()->paid()->count() && $bookings->orders->first()->coursePart->allow_deposit)
                    <div class="form-group">
                      <label for="allowDeposit">AMOUNT</label>
                      <div class="form-group">
                        <select class="form-control" name="amount" id="allowDeposit">
                          <option value="0">Full</option>
                          <option {{ isset($payOption) && $payOption ? 'selected' : '' }} value="1">50%</option>
                        </select>
                      </div>
                    </div>
                  @else
                    <input type="hidden" name="amount" value="0">
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-xs-6">
                  <a href="{{ route('customer::bookings.index') }}" class="btn btn-danger btn-lg btn-block">Cancel</a>
                </div>
                <div class="col-xs-6">
                  <input class="btn btn-success btn-lg btn-block" type="submit" value="Pay">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.min.js"></script>
  <script src="https://js.stripe.com/v2/"></script>
  <script src="/js/billing.js"></script>
@endsection
