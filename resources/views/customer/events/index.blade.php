@extends('layouts.app')

@section('page-header')
    Orders
@endsection

@section('content')
    @include('partials.events', ['events' => $events])
@endsection
