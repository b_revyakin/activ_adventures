@extends('layouts.app')

@section('js')

@endsection

@section('page-header')
  My Payments
@endsection

@section('content')
  @if($payments->count())
    <table class="table table-bordered table-responsive table-hover">
      <thead>
      <tr>
        <th>Id</th>
        <th>Date</th>
        <th>Type</th>
        <th>Method</th>
        <th>Amount</th>
        <th>Booking</th>
        <th>Status</th>
      </tr>
      </thead>
      <tbody>
      @foreach($payments as $payment)
        <tr>
          <td>{{ $payment->id }}</td>
          <td>{{ $payment->getDate() }}</td>
          <td>{{ $payment->showType() }}</td>
          <td>{{ $payment->showMethod() }}</td>
          <td>{{ $payment->getAmount() }}</td>
          <td>{{ $payment->booking->id }}</td>
          <td>
            <span class="label label-status label-{{ $payment->getClassByStatus() }}">{{ $payment->showStatus() }}</span>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  @else
    <div class="text-center">
      <h3>You don't have any payments</h3>
    </div>
  @endif
@endsection
