@extends('layouts.app')

@section('page-header')
    School Years
@endsection

@section('content')
    @foreach($seasons->chunk(3) as $seasons)
        <div class="row  subject-one">
            @foreach($seasons as $season)
                <div class="col-md-4 section">
                    <img class="subject-image" src="/image/mountains.jpg" alt="mountains">
                    <h3 class="subject-name">
                        {{ $season->name }}
                    </h3>
                    <div class="text subject-description">{{ $season->description }}</div>

                    <a class="btn btn-success button"
                       href="{{ route('customer::seasons.show', [$season->id]) }}">View School Year</a>
                </div>
            @endforeach
        </div>
    @endforeach
@endsection
