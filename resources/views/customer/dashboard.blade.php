@extends('layouts.app')

@section('page-header')
    Dashboard
@endsection

@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
@endsection

@section('js')
    <script src="/js/customer/clickable-well-links.js"></script>
@endsection

@section('content')
    <div class="portlet light portlet-fit portlet-datatable">
        <div class="portlet-title">
            <div class="caption">

            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card custom-card card-block text-xs-center">
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                            <h4 class="card-title">My Account</h4>
                            <p class="card-text"></p>
                            <a href="{{ route('customer::bookings.create') }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card custom-card card-block text-xs-center">
                            <span><i class="fa fa-users" aria-hidden="true"></i></span>
                            <h4 class="card-title">My Participants</h4>
                            <p class="card-text"></p>
                            <a href="{{ url('/bookings/create#/children/index') }}"
                               class="btn btn-primary">View/Edit</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card custom-card card-block text-xs-center">
                            <span><i class="fa fa-list" aria-hidden="true"></i></span>
                            <h4 class="card-title">Book an Event</h4>
                            <p class="card-text"></p>
                            <a href="{{ route('customer::courses.index') }}" class="btn btn-primary">View Events</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card custom-card card-block text-xs-center">
                            <span><i class="fa fa-shopping-basket" aria-hidden="true"></i></span>
                            <h4 class="card-title">My Bookings</h4>
                            <p class="card-text"></p>
                            <a href="{{ route('customer::bookings.index') }}" class="btn btn-primary">View Bookings</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection