@extends('layouts.app')

@section('page-header')
    Events
@endsection

@section('css')
    <link href="/css/admin/libs/datatables.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="/js/vendor/datatables.all.min.js"></script>
    <script src="/components/datatables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/js/customer/courses.js"></script>
    <script src="/js/extensions.js"></script>
@endsection

@section('content')
    @if($courses->first())
        <table class="table table-bordered" id="courses-table">
            <thead class="light-blue">
            <tr>
                <th>Name Of Event</th>
                <th>Group</th>
                <th>Start Date</th>
                {{--<th>End Date</th>--}}
                <th>Cost</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @forelse($courses as $course)
                @foreach($course->parts as $part)
                    <tr>
                        <td>{{ $course->name . ($part->name ? (' - ' . $part->name) : '') }}</td>
                        <td>
                            {{ $course->venue ? $course->venue->name : '' }}
                        </td>
                        <td>{{ date_format($part->start_date, 'd/m/Y') }}</td>
                        {{--<td>{{ date_format($part->end_date, 'd/m/Y') }}</td>--}}
                        <td>{{ $part->getCost() }}</td>
                        <td class="btn-group-xs" width="210px">
                            <a href="{{ route('customer::courses.show', [$course->id]) }}"
                               class="btn btn-primary">
                                Show
                            </a>
                        </td>
                    </tr>
                @endforeach
            @empty
                <tr>
                    <td colspan="6" class="text-center">
                        As your participants do not have schools events
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    @else
        <h3 class="text-center">
            This page is empty because you do not have participants in the system,
            or you have members but now they have no events available.
        </h3>
    @endif

    <div class="pull-right">
        <a class="btn btn-primary" href="{{ url('/bookings/create#/children/create') }}">Add Participant</a>
        <a class="btn btn-primary" href="{{ url('/bookings/create#/children/index') }}">Book Event</a>
    </div>
@endsection
