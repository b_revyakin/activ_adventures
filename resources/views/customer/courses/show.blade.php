@extends('layouts.app')

@section('page-header')
  <strong>{{ $course->name }}</strong><br>
@endsection

@section('js')
  <script src="/js/customer/carousel.js"></script>
  <script src="/js/tab.js"></script>
@endsection

@section('content')
  <div class="when-and-where">
    <div class="row">
      <div class="col-md-6">
        <div class="photos">
          @foreach($course->photos as $photo)
            <div class="photo-wrap">
              <a href="{{ route('customer::courses.photos', [$course->id, $photo->id]) }}">
                <img src="{{ route('customer::courses.photos', [$course->id, $photo->id]) }}" alt="" width="420px">
              </a>
            </div>
          @endforeach
        </div>
      </div>
      <div class="col-md-6">

        @foreach($course->parts as $index => $part)
          @if(!$course->isSimple())
            <div class="hr-to-dl"></div>
            <div><span class="span-dt">Part:</span><span class="span-dd">{{ $index + 1 }}</span></div>
          @endif
          <div class="hr-to-dl"></div>
          <div><span class="span-dt">Cost:</span><span class="span-dd">{{ $part->getCostPerCourse() }}</span></div>
          <div class="hr-to-dl"></div>
          <div>
            <span class="span-dt">Start:</span>
            <span class="span-dd"> {{ $part->start_date->format('d-m-Y') }}</span>
          </div>
          <div class="hr-to-dl"></div>
          <div>
            <span class="span-dt">Group:</span>
            <span class="span-dd">&nbsp;</span>
          </div>
          {{--<div class="hr-to-dl"></div>--}}
          {{--<div><span class="span-dt">End:</span><span
                    class="span-dd"> {{  $part->end_date->format('d-m-Y') }}</span>
          </div>--}}
          <div class="hr-to-dl"></div>{{--
          <div>
            <span class="span-dt">Ages:</span>
            <span class="span-dd"> {{ $part->min_age . ' - ' . $part->max_age }} years</span></div>
          <div class="hr-to-dl"></div>--}}
        @endforeach

        <a href="{{ route('customer::bookings.create') }}" class="btn button">Book Now</a>
      </div>
    </div>
    <div class="row" style="padding-top: 20px">
      <div class="col-md-12">
        <div class="row">
          <div data-toggle="tab" href="#information" class="col-md-3 tab tab-custom-active">
            <i class="fa fa-2x fa-info-circle" aria-hidden="true"></i>
            <p>Information</p>
          </div>
          @if($course->equipment_food)
            <div data-toggle="tab" href="#what-you-need" class="col-md-3 tab tab-custom">
              <i class="fa fa-2x fa-list" aria-hidden="true"></i>
              <p>What you need</p>
            </div>
          @endif
          @if($faqsParts->count())
            <div data-toggle="tab" href="#faqs" class="col-md-3 tab tab-custom">
              <i class="fa fa-2x fa-question-circle" aria-hidden="true"></i>
              <p>FAQs</p>
            </div>
          @endif
          @if($course->document)
            <div data-toggle="tab" href="#downloads" class="col-md-3 tab tab-custom">
              <i class="fa fa-2x fa-cloud-download" aria-hidden="true"></i>
              <p>Downloads</p>
            </div>
          @endif
        </div>
        <div class="tab-content">
          <div id="information" class="tab-pane fade in active">
            <div class="paragraph">{!! $course->description !!}</div>
          </div>
          @if($course->equipment_food)
            <div id="what-you-need" class="tab-pane fade">
              <div class="paragraph">{!! $course->equipment_food !!}</div>
            </div>
          @endif
          @if($faqsParts->count())
            <div id="faqs" class="tab-pane fade">
              @foreach($faqsParts as $index => $part)
                <div class="paragraph">
                  Part {{ $index + 1  }}<br>
                  @foreach($part as $category)
                    {{ $category->title }}<br>
                    @foreach($category->questions as $question)
                      <strong>{{ $question->title }}</strong>
                      <p>{!! $question->answer !!}</p>
                    @endforeach
                  @endforeach
                </div>
              @endforeach
            </div>
          @endif
          @if($course->document)
            <div id="downloads" class="tab-pane fade">
              <h4>Document</h4>
              <div>
                <a href="{{ route('customer::course.document', [$course->id]) }}"><i class="fa fa-file-pdf-o"
                                                                                     aria-hidden="true"></i></a>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection
