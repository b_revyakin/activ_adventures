<style>

    table.email-table-padding td {
        padding: 10px;
    }

</style>

<div class="col-md-12">

    <table class="email-content email-table-padding" cellspacing="10">
        @foreach($booking->orders as $order)
            <tr>
                <td colspan="2">
                    {{ $order->child->getNameAttribute() }} / Age {{ $order->child->getAgeAttribute() }} years <br>
                </td>
            </tr>
            <tr>
                <td width="350px">Event Name</td>
                <td class="right-column">{{ $order->coursePart->course->isSimple() ? $order->coursePart->course->name : $order->coursePart->name }}</td>
            </tr>
        @endforeach

        <tr>
            <td>Select Course:</td>
            <td>{{ $booking->getPriceWithoutDiscount() }}</td>
        </tr>

        <tr>
            <td>Foundation discount:</td>
            <td>{{ $booking->getDiscount() }}</td>
        </tr>

        {{--<tr>--}}
        {{--<td>Extras</td>--}}
        {{--<td>£0.00</td>--}}
        {{--</tr>--}}

        <tr>
            <td>Total:</td>
            <td>{{ $booking->getPrice() }}</td>
        </tr>

    </table>

</div>
