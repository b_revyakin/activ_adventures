<div class="col-md-8 col-md-offset-2">
    <p>Please go to <a href="{{ URL::to('') }}">{{ URL::to('') }}</a> or contact us for further information.</p>
</div>