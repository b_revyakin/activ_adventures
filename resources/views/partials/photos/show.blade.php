<div class="photos">
  @foreach($model->photos as $photo)
    <div class="photo-wrap">
      @role('admin')
      <form id="form-remove-photo" action="{{ route($routeName, [$model->id, $photo->id]) }}"
            method="POST">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <button type="submit" class="remove-photo"><i class="fa fa-times" aria-hidden="true"></i></button>
      </form>
      @endrole
      <a href="{{ route($routeName, [$model->id, $photo->id]) }}">
        <img class="min-photo img-thumbnail"
             src="{{ route($routeName, [$model->id, $photo->id]) }}"
             alt="">
      </a>
    </div>
  @endforeach
</div>