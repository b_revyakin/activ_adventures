<table class="table table-bordered datatable-empty">
    <thead>
    <tr>
        <th>Event ID</th>
        <th>Event Name</th>
        <th>Event Start Date</th>
        <th>Event End Date</th>
        @foreach($event->getArrayWorkDates() as $date)
            <th>{{ $date->day . '.' . $date->month }}</th>
        @endforeach
        <th>Total Units</th>
        <th>Total Bookings</th>
        @if(Auth::user()->isAdmin())
            <th>Total Revenue</th>
        @endif
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $event->id }}</td>
        <td>{{ $event->name }}</td>
        <td>{{ $event->start_date->format('d-m-Y') }}</td>
        <td>{{ $event->end_date->format('d-m-Y') }}</td>
        @foreach($event->getArrayWorkDates() as $date)
            <td>{{ $event->getCountOrdersInDate($date) }}</td>
        @endforeach
        <td>{{ $event->getBookedDatesCount() }}</td>
        <td>{{ $event->getActiveOrders()->count() }}</td>
        @if(Auth::user()->isAdmin())
            <td>£{{ $event->getActiveOrders()->sum('price') }}</td>
        @endif
    </tr>
    </tbody>
</table>