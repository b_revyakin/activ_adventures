<table class="table table-bordered" id="events-table">
  <thead>
  <tr>
    <th>Name</th>
    <th>Venue</th>
    <th class="date">Start Date</th>
    <th class="date">End Date</th>
    <th class="count">Total Booking</th>
    <th class="count">Type</th>
    <th class="actions">Actions</th>
  </tr>
  </thead>
  <tbody>
  @foreach($courses as $course)
    <tr>
      <td>{{ $course->name }}</td>
      <td>
        @if(! empty($course->venue))
          <a href="{{ route('admin::venues.show', [$course->venue->id]) }}">
            {{ $course->venue->name }}
          </a>
        @endif
      </td>
      <td>{{ $course->getStartDate() ? date('d/m/Y H:i', strtotime($course->getStartDate())) : '' }}</td>
      <td>{{ $course->getEndDate() ? date('d/m/Y H:i', strtotime($course->getEndDate())) : '' }}</td>
      <td>
        {{ $course->totalBookings() }}
      </td>
      <td>
        {{ $course->type }}
      </td>
      <td class="btn-group-xs" width="210px">
        @permission('course.show')
        <a href="{{ route('admin::courses.show', [$course->id]) }}"
           class="btn btn-primary">
          Show
        </a>
        <a href="{{ route('admin::courses.details.register-b', [$course->id]) }}"
           class="btn btn-default">
          Details
        </a>
        @endpermission
        @permission('course.edit')
        <a href="{{ route('admin::courses.edit', [$course->id]) }}"
           class="btn btn-warning">
          Edit
        </a>


        <a href="{{ route('admin::courses.copy', [$course->id]) }}"
           class="btn btn-warning">
          Copy
        </a>
        @endpermission

        @permission('course.edit')
        @if(! $course->isSimple())
        <a href="{{ route('admin::courses.parts.index', [$course->id]) }}"
           class="btn btn-warning">
          Edit parts
        </a>
        @endif
        @endpermission

        @if($course->isSimple())
          <a href="{{ route('admin::course-parts.checkboxes.index', [$course->parts->first()->id]) }}"
             class="btn btn-default">
            Checkboxes
          </a>
          <a href="{{ route('admin::course-parts.questions.index', [$course->parts->first()->id]) }}"
             class="btn btn-default">
            Questions
          </a>
          <a href="{{ route('admin::course-parts.teams.index', [$course->parts->first()->id]) }}"
             class="btn btn-default">
            Teams
          </a>
          <a href="{{ route('admin::faq.course-parts.categories.index', [$course->parts->first()->id]) }}"
             class="btn btn-default">
            FAQs
          </a>
        @endif

        @include('partials.active-forms', ['model' => $course, 'route' => 'admin::courses'])
      </td>
    </tr>
  @endforeach
  </tbody>
</table>