<table class="table table-bordered datatable-empty">
  <thead>
  <tr>
    <th>Course ID</th>
    <th>Course Name</th>
    <th>Course Start Date</th>
    <th>Course End Date</th>
    @foreach($course->parts as $index => $part)
      <th>{{ $part->name ?: 'Part ' . $index }}</th>
    @endforeach
    <th>Total Bookings</th>
    @role('admin')
      <th>Total Revenue</th>
    @endrole
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>{{ $course->id }}</td>
    <td>{{ $course->name }}</td>
    <td>{{ $course->getStartDate() }}</td>
    <td>{{ $course->getEndDate() }}</td>
    @foreach($course->parts as $part)
      <th>{{ $part->getActiveOrders()->count() }}</th>
    @endforeach
    <td>{{ $course->totalBookings() }}</td>
    @role('admin')
    <td>{{ price_currency($course->getOrders()->sum('price')) }}</td>
    @endrole
  </tr>
  </tbody>
</table>