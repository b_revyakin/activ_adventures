<table class="table table-bordered" id="requested-register-bookings-table">
  <thead>
  <tr>
    @if(!$withEventName)
      <th><input id="select-all" type="checkbox"></th>
    @endif
    <th class="{{ !$withEventName ? 'hidden' : '' }}">Event Name</th>
    <th>Parent Name</th>
    <th>Participant Name</th>
    <th>Status</th>
    <th class="actions">Actions</th>
  </tr>
  </thead>
  <tbody>
  @foreach($bookings as $booking)
    <tr data-id="{{ $booking->id }}">
      @if(!$withEventName)
        <td><input class="select-id" data-course-part-id="{{ $booking->orders->first()->coursePart->id }}"
                   type="checkbox" name="id" value="{{ $booking->customer->id }}">
        </td>
      @endif
      <td class="{{ !$withEventName ? 'hidden' : '' }}">{{ $booking->orders->first()->coursePart->full_name }}</td>
      <td>{{ $booking->customer->name }}</td>
      <td>{{ $booking->orders->first()->child->name }}</td>
      <td>
                <span class="label label-status
                {{ $booking->isProcessing() ? 'label-warning' :'' }}
                {{ $booking->isPaid() ? 'label-success' :'' }}
                {{ $booking->isCancelled() ? 'label-danger' :'' }}">
                {{ $booking->showStatus() }}</span>
      </td>
      <td class="btn-group-xs">
        @if($booking->isProcessing() && $booking->requested_register)
          <button type="button" class="btn btn-xs btn-success btn-confirm" data-toggle="modal"
                  data-target=".bs-modal-sm"
                  data-title="{{ trans('messages.admin.booking.confirm.title') }}"
                  data-content="{{ trans('messages.admin.booking.confirm.content') }}"
                  data-url="{{ route('admin::api::bookings.register.confirm', [$booking->id]) }}"
                  data-ajax="true"
                  data-action="confirm"
                  data-id="{{ $booking->id }}"
                  data-method="POST">Approve
          </button>
        @endif
        @if(! $booking->isCancelled())
          @permission('booking.cancel')
          <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                  data-target=".bs-modal-sm"
                  data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                  data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                  data-url="{{ route('admin::api::bookings.cancel', [$booking->id]) }}"
                  data-ajax="true"
                  data-action="cancel"
                  data-id="{{ $booking->id }}"
                  data-method="POST">{{ $booking->isProcessing() && $booking->requested_register ? 'Decline' : 'Cancel' }}
          </button>
          @endpermission
        @endif
        <a href="{{ route('admin::bookings.show', $booking->id) }}"
           class="btn btn-default">Show</a>
        <a href="{{ route('admin::parents.show', $booking->customer->id) }}"
           class="btn btn-default">Account Details</a>
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
