<div class="col-md-12">
    <table class="table table-bordered" id="orders-table">
        <thead>
        <tr>
            <th>Booking ID</th>
            <th>Participants First Name</th>
            <th>Participants Last Name</th>
            <th>Age</th>
            <th>Parent First Name</th>
            <th>Parent Last Name</th>
            <th>Event</th>
            <th>Status</th>
            <th class="actions">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->booking->id }}</td>
                <td>
                    <a href="{{ route('admin::children.show', $order->child->id) }}">{{ $order->child->first_name }}</a>
                </td>
                <td>
                    <a href="{{ route('admin::children.show', $order->child->id) }}">{{ $order->child->last_name }}</a>
                </td>
                <td>{{ $order->child->getAgeAttribute() }}</td>
                <td>{{ $order->child->parent->first_name }}</td>
                <td>{{ $order->child->parent->last_name }}</td>
                <td>{{ $order->coursePart ? $order->coursePart->name : '' }}</td>
                <td>{{ $order->booking->showStatus() }}</td>
                <td>
                    <a href="{{ route('admin::orders.show', $order->id) }}" class="btn btn-default btn-xs">Show</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>