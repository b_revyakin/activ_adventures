<div class="activ-container-list">
    
    <div class="date hide">
        <form>
            <input type="date">
        </form>
    </div>
    <ul class="list-breadcrumbs hide">
        <li><a href="#">List View</a></li>
        <li><a href="#">Grid View</a></li>
    </ul>
    <div class="clearfix"></div>
    
    
    @foreach($events as $event)
        <div class="list-subject">
            @if($event->photos()->first())
                <img src="{{ route('customer::events.photos.show', [$event->id, $event->photos()->first()->id]) }}"
                     alt="">
            @else
                <img src="/image/mountains.jpg" alt="mountains">
            @endif
            <div class="list-info">
                <h3>{{ $event->name }}</h3>
                <h4>{{ $event->getPeriod() }}</h4>
                <div>{!! $event->description !!}</div>
                <a class="btn btn-primary button" href="{{ route('customer::events.show', [$event->id]) }}">Details</a>
                <a class="btn btn-success button" href="{{ route('customer::bookings.create') }}">Book Now</a>
            </div>
        </div>
    @endforeach
</div>
