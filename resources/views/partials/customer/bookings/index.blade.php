<table class="table table-bordered table-responsive table-hover" id="customer-bookings">
  <thead>
    <tr>
      <th>Event Title</th>
      <th>Booking Id</th>
      <th>Participant Name</th>
      <th>Booking Date</th>
      <th>Price</th>
      <th>Paid</th>
      <th>Left to Pay</th>
      <th>Payment Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    @forelse($bookings as $booking)
      <tr data-id="{{ $booking->id }}">
        <td>{{ $booking->orders->count() === 1 ? $booking->order()->coursePart->full_name : '' }}</td>
        <td>{{ $booking->id }}</td>
        <td>{{ $booking->orders->count() === 1 ? $booking->order()->child()->withTrashed()->first()->fullname : '' }}</td>
        <td>{{ $booking->created_at->format('d-m-Y') }}</td>
        <td>{{ $booking->getPrice() }}</td>
        <td>{{ $booking->getPaidSum() }}</td>
        <td>{{ $booking->getLeftPay() }}</td>
        <td>
                    <span class="label label-status
                        {{ $booking->isProcessing() ? 'label-warning' :'' }}
                    {{ $booking->isPaid() ? 'label-success' :'' }}
                    {{ $booking->isCancelled() ? 'label-danger' :'' }}">
                        {{ $booking->showStatus() }}</span>
        </td>
        <td>
          <div class="btn-group">
            <button data-booking-id="{{ $booking->id }}" class="btn btn-primary btn-xs btn-view">Details</button>
            <button data-booking-id="{{ $booking->id }}" class="btn btn-primary btn-xs btn-hide"
                    style="display: none">Hide
            </button>
            @if($booking->isSimple())
              <a href="{{ route((Auth::user()->is('customer') ? 'customer' : 'admin') . '::courses.show', [$booking->orders->first()->coursePart->course->id]) }}"
                 class="btn btn-success btn-xs">View Event</a>
            @else
              <a href="{{ route((Auth::user()->is('customer') ? 'customer' : 'admin') . '::courses.index') }}"
                 class="btn btn-success btn-xs">View Events</a>
            @endif

          </div>
          @if(Auth::user()->is('customer'))
            @if($booking->isProcessing())
              <button type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                      data-target=".bs-modal-sm"
                      data-title="{{ trans('messages.customer.booking.cancel.title') }}"
                      data-content="{{ trans('messages.customer.booking.cancel.content') }}"
                      data-url="{{ route('customer::bookings.destroy', [$booking->id]) }}"
                      data-method="DELETE">Cancel
              </button>
              <a id="stripe-system" href="{{ route('customer::bookings.payment.stripe', $booking->id) }}"
                 class="btn btn-xs btn-default">Pay</a>
            @endif
          @else
            @if($booking->isProcessing())
              <button type="button" class="btn btn-xs btn-danger btn-cancel" data-toggle="modal"
                      data-target=".bs-modal-sm"
                      data-title="{{ trans('messages.admin.booking.cancel.title') }}"
                      data-content="{{ trans('messages.admin.booking.cancel.content') }}"
                      data-url="{{ route('admin::api::bookings.cancel', [$booking->id]) }}"
                      data-ajax="true"
                      data-action="cancel"
                      data-id="{{ $booking->id }}"
                      data-method="POST">Cancel
              </button>
            @endif
            <a href="{{ route('admin::bookings.show', $booking->id) }}"
               class="btn btn-default btn-xs">Show</a>
            <a href="{{ route('admin::bookings.payments.index', $booking->id) }}"
               class="btn btn-default btn-xs">Payments</a>
          @endif
        </td>
      </tr>
      <tr style="display: none" data-booking-id="{{ $booking->id }}">
        <td colspan="9">
          <div id="children">
            <div class="order">
              @foreach($booking->orders as $order)
                <div class="child">
                  {{ $order->child()->withTrashed()->first()->fullname }} /
                  Age {{ $order->child()->withTrashed()->first()->age }} years
                </div>
                <div>
                  <div>
                    <span>Event</span> <span>{{ $order->coursePart->full_name }}</span>
                  </div>
                  <div>
                    <span>Register</span> <span>{{ $order->coursePart->allow_register ? 'Yes' : 'No' }}</span>
                  </div>
                  <div>
                    @if($order->coursePart->course->venue)
                      <span>Venue</span> <span>{{ $order->coursePart->course->venue->name }}</span>
                    @endif
                  </div>
                  <div>
                    <span>Price</span> <span>{{ $order->price }}</span>
                  </div>
                  <div>
                    <span>Date booked</span>
                    <span>{{ $order->coursePart->start_date->format('d-m-Y') . ' - ' . $order->coursePart->end_date->format('d-m-Y')  }}</span>
                    <ul>
                    </ul>
                  </div>
                </div>
              @endforeach
              @if($booking->payments->count())
                Payments:
                @foreach($booking->payments as $payment)
                  <div><span>{{ $payment->getDate() }}</span> - <span>{{ $payment->getAmount() }}</span>
                    <span>({{ $payment->showType() }})</span>
                    @if(Auth::user()->isAdmin())
                      <span>{{ $payment->transaction_id ? "({$payment->transaction_id})" : '' }}</span>
                    @endif
                  </div>
                @endforeach
              @else
                <div>Not Payments</div>
              @endif
            </div>
          </div>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="9" class="text-center">
          You have no orders
        </td>
      </tr>
    @endforelse
  </tbody>
</table>

@include('partials.modal-windows.booking.action')
