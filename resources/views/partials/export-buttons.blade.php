@foreach(['xls' => 'Excel', 'pdf' => 'PDF', 'csv' => 'CSV'] as $type => $typeName)
  <a class="btn btn-default export-button"
     href="{{ route($url, [$course->id, $type, $parameter]) }}">{{ $typeName }}</a>
@endforeach