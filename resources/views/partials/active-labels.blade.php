@if($model->isActive())
    <span class="label label-success">
        active
    </span>
@else
    <span class="label label-danger">
        archive
    </span>
@endif
