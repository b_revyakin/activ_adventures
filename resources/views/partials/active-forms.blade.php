@permission('course.activation')
    @if($model->isActive())
        <form action="{{ route($route . '.deactivate', [$model->id]) }}" method="POST" class="inline">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">

            <button type="submit" class="btn btn-danger btn-xs">Archive</button>
        </form>
    @else
        <form action="{{ route($route . '.activate', [$model->id]) }}" method="POST" class="inline">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">

            <button type="submit" class="btn btn-success btn-xs">Activate</button>
        </form>
    @endif
@endpermission