<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
    <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Adventures - #SeriousFun.">

    <p>
        Dear {{ $customer->first_name }},
    </p>

    <p>
        Welcome to Activ Adventures. Your account has been successfully set up.
        You can now book your events with us by logging into your account using the following email address:
    </p>

    <p>
        {{ $customer->user->email }}
    </p>

    <p><strong>How to book an event</strong></p>

    <p>
        To book an event you must first add a participant to your account making sure you input your school's unique
        code.
        This code will link your account to your school's events.
        Please contact your school's trip organiser if you do not have this code.
    </p>

    <p>
        If you require any assistance with your account,or if you need any further information regarding
        the events you would like to book, please do not hesitate to <a
                href="http://www.activadventures.com/contact/">get in touch</a>.
    </p>


    <p>
        Thank you,
    </p>

    <p>
        Activ Adventures.
    </p>
</div>