<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
    <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Adventures - #SeriousFun.">

    <div>
        <strong>Many thanks for your application to one of our events. We cannot confirm your booking until payment has
            been received. If you have trouble completing your payment through Stripe or have not received a booking
            confirmation despite completing payment please do get in touch.
        </strong>
    </div>

    <div><strong>Thank you for booking on to one of our Activ Adventures! Your booking is now created.</strong></div>

    @include('partials.emails.booking.content', compact('booking'))

</div>
@include('partials.emails.booking.footer')