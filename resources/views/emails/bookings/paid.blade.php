<style>

  table.email-table-padding td {
    padding: 10px;
  }

</style>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
  <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Adventures - #SeriousFun.">

  <p>
    Dear {{ $customer->first_name }},
  </p>

  <p>
    Many thanks for your recent booking with Activ Adventures.
    @if($booking->price)
      Your payment has been received and we look forward to seeing you on the following programme:
    @endif
  </p>

  <p>
    @include('partials.emails.booking.content', compact('booking'))
  </p>

  <h4>IMPORTANT INFORMATION</h4>
  <p>
    Please click the following link to be taken to the <a
        href="http://www.activadventures.com/activ-guide/expedition-faqs/">FAQ section</a> on our website.
    If you have any questions that remain unanswered please do not hesitate to <a
        href="http://www.activadventures.com/contact/">contact us</a>.
  </p>

  <h4>MEDICATION</h4>
  <p>
    We will send you an email leading up to events to remind you to check the medical details of the participant and
    the emergency contact details you have supplied us for the event. You are responsible for updating these details
    at last 24 hours prior to the event.
  </p>

  <p>
    These details are printed on the morning before an event. We cannot accept changes that are made to the system
    after this point. If you need to update any medical details or emergency contact details immediately before an
    activity please email us.
  </p>

  <h4>CANCELLATIONS & CHANGING BOOKING DATES</h4>
  <p>
    If you need to change or cancel your booking with us then please let us know by email without delay. Please do
    have a look at our <a href="http://www.activadventures.com/btc/">Booking Terms and Conditions</a> for
    information on the charges due for any cancellations
    requested.
  </p>

  <p>
    Cancellation charges will also be due for any changes to dates you require once your original booking has been
    confirmed.
  </p>

  <h4>KIT AND EQUIPMENT</h4>
  <p>
    Go to our <a href="http://www.activadventures.com/shop/">online shop</a> for kit and equipment for your adventure.
    Our <a href="http://www.activadventures.com/activ-guide/clothing-equipment/">Activ Guide</a> contains comprehensive
    kit lists and kit guides.
  </p>

  <h4>INSURANCE</h4>

  <p>Activ Adventures is insured through Activities Industry Mutual for all activities you book with us. Our policy
    does not provide cover for you in the event of cancellation for any reason or personal accident for any
    participant, or for damage to, or loss of, equipment belonging to us caused by wilful, reckless or negligent
    behaviour. </p>

  <p>We strongly recommend that you take out personal and travel insurance as required to cover you in the event of
    cancellation of all or part of an event due to illness or injury, if injured in an accident on the event, and to
    cover personal kit damaged or lost during the event for whatever reason. </p>

  <p>Please do read our full <a href="http://www.activadventures.com/btc/">Booking Terms and Conditions</a> for more
    further details.</p>

  <h4>EVENT UPDATES</h4>

  <p>
    We will provide event updates on the event page on your account. Just log in to your account and click 'EVENTS'
    to show your event and all the details you need to ensure you get the most out of our adventure experience.
  </p>

  <p>We hope all our participants have a wonderful time on their adventures. We'd love to hear your stories so please
    do let us know how it all goes.</p>

  <p>If you require any assistance with your account, or if you need any further information regarding the events you
    would like to book, please do not hesitate to <a href="http://www.activadventures.com/contact/">get in touch</a>.
  </p>

  <p>
    Best wishes,
  </p>

  <p>
    Activ Adventures.
  </p>

</div>
@include('partials.emails.booking.footer')
