<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
      crossorigin="anonymous">
<div class="container col-md-8 col-md-offset-2">
  <img src="{{ asset('/image/email-logo.png') }}" alt="Activ Adventures - #SeriousFun.">

  <p>
    Dear {{ $customer->first_name }},
  </p>

  <p>
    The event of "{{ $order->coursePart->course->name . ' - ' . $order->coursePart->name }}" will begin after 6 days.
    You need to pay the order before the event starts.
  </p>

  <p>
    @include('partials.emails.booking.content', ['booking' => $order->booking])
  </p>

  <p>
    You will receive a booking confirmation email once full payment has been received.
  </p>

  <p>
    For full booking terms and conditions please <a href="http://www.activadventures.com/btc/">click here</a>.
  </p>

  <p>
    Many thanks,
  </p>
  <p>
    Activ Adventures.
  </p>
</div>
@include('partials.emails.booking.footer')
