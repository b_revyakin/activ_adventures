(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowChildController', ShowChildController);

  ShowChildController.$inject = ['child', 'questions'];

  /* @ngInject */
  function ShowChildController(child, questions) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;
    vm.questions_in_categories = questions;
    vm.answers = initAnswers();

    function isAnswer(questionId, answers) {
      var response = false;

      angular.forEach(answers, function (answer) {
        if (answer.id === questionId && answer.pivot.answer) {
          response = {
            answer: answer.pivot.answer,
            text: answer.pivot.text
          };
        }
      });

      return response;
    }

    function initAnswers() {
      var answers = [];

      angular.forEach(vm.questions_in_categories, function (category) {
        angular.forEach(category.questions, function (question) {
          var checkAnswer = isAnswer(question.id, child.questions);

          if (checkAnswer) {
            answers[question.id] = checkAnswer;
          }
        });
      });

      return answers;
    }
  }
})();
