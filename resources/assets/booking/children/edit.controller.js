(function () {
  'use strict';

  angular
    .module('booking')
    .controller('EditChildController', EditChildController);

  EditChildController.$inject = ['child', 'childService', '$state', 'alertService', 'swimOptions', 'yearsInSchool', 'schools', 'questions'];

  /* @ngInject */
  function EditChildController(child, childService, $state, alertService, swimOptions, yearsInSchool, schools, questions) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;
    vm.schools = schools;
    vm.questions_in_categories = questions;

    vm.form = child;
    vm.form.school_code = child.school.code;
    vm.form.answers = initAnswers();

    vm.medical_term_first = vm.medical_term_second = false;

    vm.update = update;

    vm.datePickerOpen = false;
    vm.datePickerForExpireDateOpen = false;
    vm.datePickerForDateOfIssueOpen = false;
    vm.datePickerForDateOfExpiryOpen = false;

    ////////////

    function update() {
      if (!vm.medical_term_first) {
        alertService.error('Please confirm that you give all medical details of your participant.');

        return;
      }
      if (!vm.medical_term_second) {
        alertService.error('Please confirm that you give all medical details of your participant.');

        return;
      }

      childService.update(vm.form).then(function () {
        alertService.success('The participant was updated!');

        $state.go('^.index');
      });
    }

    function initAnswers() {
      var answers = [];

      angular.forEach(vm.questions_in_categories, function (category) {
        angular.forEach(category.questions, function (question) {
          var checkAnswer = isAnswer(question.id, child.questions),
            answer = {
              answer: false,
              text: ''
            };

          if (checkAnswer) {
            answer = checkAnswer;
          }


          answers[question.id] = answer;
        });
      });

      return answers;
    }

    function isAnswer(questionId, answers) {
      var response = false;

      angular.forEach(answers, function (answer) {
        if (answer.id === questionId && answer.pivot.answer) {
          response = {
            answer: answer.pivot.answer,
            text: answer.pivot.text
          };
        }
      });

      return response;
    }
  }
})();
