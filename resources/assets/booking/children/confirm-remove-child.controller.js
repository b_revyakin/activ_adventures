(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ConfirmRemoveChildController', ConfirmRemoveChildController);

  ConfirmRemoveChildController.$inject = ['$uibModalInstance'];

  /* @ngInject */
  function ConfirmRemoveChildController($uibModalInstance) {
    /* jshint validthis: true */
    var vm = this;

    vm.ok = ok;
    vm.cancel = cancel;

    function ok() {
      $uibModalInstance.close();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
