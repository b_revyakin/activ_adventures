(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildrenController', ChildrenController);

  ChildrenController.$inject = ['$uibModal', 'children', 'childService', 'alertService', '$state'];

  /* @ngInject */
  function ChildrenController($uibModal, children, childService, alertService, $state) {
    /* jshint validthis: true */
    var vm = this;

    vm.children = children;
    vm.confirmedUpdated = false;
    vm.confirmedLegal = false;

    vm.remove = remove;
    vm.next = next;

    ///////////////////

    function next() {
      if (!vm.confirmedUpdated) {
        alertService.error('Please confirm that you have updated all of your participant\'s contact and medical information before booking.');

        return;
      }
      if (!vm.confirmedLegal) {
        alertService.error('Please confirm that you are the legal guardian of the participants above.');

        return;
      }

      $state.go('booking.orders.create');
    }

    function remove(child) {
      $uibModal.open({
        templateUrl: '/children/confirm-remove-modal.html',
        controller: 'ConfirmRemoveChildController',
        controllerAs: 'ConfirmRemoveChildCtrl',
        backdrop: 'static',
        keyboard: false,
        size: 'lg'
      }).result.then(function () {
        childService.remove(child.id).then(function () {
          alertService.success('The participant was removed!');

          vm.children.splice(vm.children.indexOf(child), 1);
        });
      });
    }
  }
})();
