(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateChildController', CreateChildController);

  CreateChildController.$inject = ['childService', '$state', 'alertService', 'swimOptions', 'yearsInSchool', 'schools', 'questions', 'orderService'];

  /* @ngInject */
  function CreateChildController(childService, $state, alertService, swimOptions, yearsInSchool, schools, questions, orderService) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;
    vm.schools = schools;
    vm.questions_in_categories = questions;

    vm.form = {
      first_name: undefined,
      last_name: undefined,
      email: undefined,
      email_confirmation: undefined,
      birthday: undefined,
      sex: undefined,
      relationship: undefined,
      school_code: undefined,
      year_in_school: undefined,
      doctor_name: undefined,
      doctor_telephone_number: undefined,
      home_address: undefined,
      home_postcode: undefined,
      mobile: undefined,
      passport_detail: {
        no: undefined,
        expiry_date: undefined,
        nationality: undefined,
        first: undefined,
        place_of_issue: undefined,
        date_of_issue: undefined,
        date_of_expiry: undefined
      },
      second_parent: {
        first_name: undefined,
        last_name: undefined,
        email: undefined,
        phone: undefined
      },
      main_contact: {
        forename: undefined,
        surname: undefined,
        relationship: undefined,
        mobile: undefined,
        alternative_number: undefined
      },
      second_contact: {
        forename: undefined,
        surname: undefined,
        relationship: undefined,
        mobile: undefined,
        alternative_number: undefined
      },
      answers: initAnswers(),
    };

    vm.create = create;

    vm.datePickerOpen = false;
    vm.datePickerForExpireDateOpen = false;
    vm.datePickerForDateOfIssueOpen = false;
    vm.datePickerForDateOfExpiryOpen = false;

    vm.medical_term_first = vm.medical_term_second = false;

    //////////////////////////////

    function create() {
      if (!vm.medical_term_first) {
        alertService.error('Please confirm that you give all medical details of your participant.');

        return;
      }
      if (!vm.medical_term_second) {
        alertService.error('Please confirm that you give all medical details of your participant.');

        return;
      }

      childService.store(vm.form).then(function () {
        alertService.success('The child was created!');

        orderService.refresh();

        $state.go('^.index');
      });
    }

    function initAnswers() {
      var answers = [];

      angular.forEach(vm.questions_in_categories, function (category) {
        angular.forEach(category.questions, function (question) {
          var answer = {
            answer: false,
            text: ''
          };

          answers[question.id] = answer;
        });
      });

      return answers;
    }

  }
})();
