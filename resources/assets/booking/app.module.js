(function () {
  'use strict';

  angular.module('booking', [
    'ngResource',
    'ui.router',
    'toaster',
    'angular-storage',
    'ui.bootstrap',
    'ui.select',
    'ngSanitize'
  ]);

})();
