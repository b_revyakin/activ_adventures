(function () {
    'use strict';

    angular
        .module('booking')
        .service('questionService', questionService);

    questionService.$inject = ['$http', 'helperService'];

    /* @ngInject */
    function questionService($http, helperService) {
        /* jshint validthis: true */
        var vm = this;

        vm.all = all;

        /////////////////////////

        function all() {
            return $http.get('/api/questions').then(helperService.fetchResponse);
        }
    }
})();
