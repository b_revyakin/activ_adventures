(function () {
  'use strict';

  angular
    .module('booking')
    .service('courseService', courseService);

  courseService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function courseService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.all = all;
    vm.questions = questions;

    ///////////////////////////

    function all() {
      return $http.get('/api/courses')
        .then(helperService.fetchResponse, helperService.rejectResponse)
        .then(function (courses) {
          /*for(var i = 0, course; (course = courses[i]); i++) {
           course.start_date = moment(course.start_date);
           course.end_date = moment(course.end_date);
           }*/

          return courses;
        });
    }

    function questions(courseId) {
      return $http.get('/api/course/' + courseId + '/questions').then(helperService.fetchResponse, helperService.rejectResponse);
    }
  }
})();
