(function () {
  'use strict';

  angular
    .module('booking')
    .service('helperService', helperService);

  helperService.$inject = ['$filter', 'alertService'];

  /* @ngInject */
  function helperService($filter, alertService) {
    /* jshint validthis: true */
    var vm = this;

    vm.fetchResponse = fetchResponse;
    vm.ShowValidationErrors = ShowValidationErrors;
    vm.redirect = redirect;
    vm.findCourse = findCourse;
    vm.prepareOrders = prepareOrders;
    vm.prepareDatesForRequest = prepareDatesForRequest;

    /////////////////////////

    function prepareOrders(orders)
    {
      var _orders = [];

      for (var i = 0, order; (order = orders[i]); i++) {

        _orders.push({
          _id: order._id,
          child_id: order.child_id,
          course_id: order.course_id,
          can_swim_id: order.can_swim_id,
          comment: order.comment,
          passport_detail: order.passport_detail,
          products: order.products,
          answers: order.answers
        });
      }

      return _orders;
    }

    function redirect(url, method, fields) {
      $('<form>', {
        method: method,
        action: url
      }).append(fields).appendTo('body').submit();
    }

    function fetchResponse(data) {
      return data.data;
    }

    function ShowValidationErrors(errors) {
      if (errors.length) {
        for (var i = 0, error; (error = errors[i]); i++) {
          alertService.error(error.message);
        }
      }

      return errors;
    }

    function findCourse(courses, courseId) {
      for (var i = 0, course; (course = courses[i]); i++) {
        if (course.id === courseId) {
          return angular.copy(course);
        }
      }
    }

    function prepareDatesForRequest(object, properties, needClone) {
      var localObject = needClone ? angular.copy(object) : object;

      properties.forEach(function (property) {
        localObject[property] = moment(localObject[property]).format('YYYY-MM-DD');
      });

      return localObject;
    }
  }
})();
