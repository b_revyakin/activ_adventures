(function () {
  'use strict';

  angular
    .module('booking')
    .service('orderService', orderService);

  orderService.$inject = ['$filter', '$q', 'localStore', 'childService', 'courseService', '$http', 'helperService'];

  /* @ngInject */
  function orderService($filter, $q, localStore, childService, courseService, $http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    refresh();

    vm.customerId = localStore.get('customer-id');

    if(vm.customerId) {
      localStore.remove('customer-id');
      localStore.remove('orders');
      localStore.remove('index');
    }

    vm.orders = localStore.get('orders');
    vm.index = localStore.get('index');

    if (!vm.orders) {
      vm.orders = [];
      vm.index = 1;
    }

    vm.validate = validate;
    vm.validateAdditionalFields = validateAdditionalFields;

    vm.count = count;
    vm.all = all;
    vm.add = add;
    vm.get = get;
    vm.update = update;
    vm.remove = remove;
    vm.calculate = calculate;
    vm.clear = clear;
    vm.products = products;
    vm.addQuestions = addQuestions;
    vm.refresh = refresh;

    ///////////////////////

    function refresh() {
      vm.promises = $q.all([childService.all(), courseService.all()]).then(function (data) {
        vm.children = data[0];
        vm.courses = data[1];
      });
    }

    function products() {
      return $http.get('/api/products').then(helperService.fetchResponse);
    }

    function count() {
      return vm.orders.length;
    }

    function clear() {
      var orders = angular.copy(vm.orders);

      var localPromise = $q.defer();

      vm.promises.then(function () {
        localStore.remove('orders');
        localStore.remove('index');

        localPromise.resolve(true);
      });

      return localPromise.promise;
    }

    function calculate(order) {
      var sum = 0;
      sum += order.course.cost_per_course;
      return sum;
    }

    function validate(order, children, courses) {
      var errors = [];

      var child = $filter('filter')(children, {id: order.child_id});
      if (!(order.child_id && child.length)) {
        errors.push({
          title: 'child',
          message: 'Necessary select child'
        });
      }

      var course = $filter('filter')(courses, {id: order.course_id});
      if (!(order.course_id && course.length)) {
        errors.push({
          title: 'course',
          message: 'Necessary select course'
        });
      }

      if (course[0].required_swim_option && !order.can_swim_id) {
        errors.push({
          title: 'swim option',
          message: 'Necessary select swim option'
        });
      }

      if (course[0].required_passport) {
        for (var key in order.passport_detail) {
          if (!order.passport_detail[key]) {
            errors.push({
              title: 'passport detail',
              message: 'Passport Detail Is Required For This Course'
            });

            break;
          }
        } 
      }

      var orders = angular.copy(vm.orders);
      var uniqueOrder = orders.every(function (savedOrder) {
        return !(order.child_id == savedOrder.child_id && order.course_id == savedOrder.course_id);
      });

      if (!uniqueOrder) {
        errors.push({
          title: 'order',
          message: 'You have bought it already'
        });
      }

      return errors.reverse();
    }

    function validateAdditionalFields(order, course) {
      var errors = [];

      if (course.required_swim_option && !order.can_swim_id) {
        errors.push({
          title: 'swim option',
          message: 'Necessary select swim option'
        });
      }

      if (course.required_passport) {
        for (var key in order.passport_detail) {
          if (!order.passport_detail[key]) {
            errors.push({
              title: 'passport detail',
              message: 'Passport Detail Is Required For This Course'
            });

            break;
          }
        }
      }

      return errors.reverse();
    }

    function all() {
      var orders = angular.copy(vm.orders);

      var localPromise = $q.defer();

      vm.promises.then(function () {
        for (var i = 0, order; (order = orders[i]); i++) {
          setChild(order);
          setCourse(order);
        }

        localPromise.resolve(orders);
      });

      return localPromise.promise;
    }

    function add(order) {
      if (localStore.get('orders') && localStore.get('orders').length) {
        if (localStore.get('lastVisit')) {
          if (24 <= moment().diff(moment(localStore.get('lastVisit')), 'hours')) {
            clear();
            vm.orders = [];
            vm.index = 1;
          }
        } else {
          clear();
          vm.orders = [];
          vm.index = 1;
        }
      }
      localStore.set('lastVisit', moment().format('YYYY-MM-DD HH:mm'));

      var localPromise = $q.defer();

      vm.promises.then(function () {
        order._id = vm.index++;
        vm.orders.push(order);
        saveOrders();

        localPromise.resolve(
          angular.copy(order)
        );
      });


      return localPromise.promise;
    }

    function addQuestions(questions) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        vm.orders[vm.orders.length - 1].questions = questions;
        saveOrders();

        localPromise.resolve(
          angular.copy(questions)
        );
      });


      return localPromise.promise;
    }


    function get(id) {
      var localPromise = $q.defer();

      vm.promises.then(function () {
        var order = angular.copy($filter('filter')(vm.orders, {_id: id})[0]);

        setChild(order);
        setCourse(order);

        localPromise.resolve(order);
      });


      return localPromise.promise;
    }

    function update(id, order) {
      var localPromise = $q.defer();

      var _order = $filter('filter')(vm.orders, {_id: id})[0];

      _order.child_id = order.child_id;
      _order.child = order.child;
      _order.course_id = order.course_id;
      _order.courses = order.courses;

      saveOrders();

      localPromise.resolve(_order);

      return localPromise.promise;
    }

    function remove(id) {
      var localPromise = $q.defer();

      var order = $filter('filter')(vm.orders, {_id: id})[0];

      vm.orders.splice(vm.orders.indexOf(order), 1);

      saveOrders();

      localPromise.resolve(true);

      return localPromise.promise;
    }

    function saveOrders() {
      localStore.set('orders', vm.orders);
      localStore.set('index', vm.index);
    }

    function setChild(order) {
      order.child = $filter('filter')(vm.children, {id: order.child_id})[0];
    }

    function setCourse(order) {
      order.course = $filter('filter')(vm.courses, {id: order.course_id})[0];
    }

  }
})();
