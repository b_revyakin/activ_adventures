(function () {
  'use strict';

  angular
    .module('booking')
    .service('childService', childService);

  childService.$inject = ['$resource', '$http', 'helperService'];

  /* @ngInject */
  function childService($resource, $http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    var Child = $resource('/api/children/:childId', {childId: '@id'}, {
      update: {method: 'PUT'}
    });

    vm.all = all;
    vm.get = get;
    vm.store = store;
    vm.update = update;
    vm.remove = remove;
    vm.swimOptions = swimOptions;
    vm.yearsInSchool = yearsInSchool;
    vm.schools = schools;

    ///////////////////////

    function yearsInSchool() {
      return $http.get('/api/years-in-school').then(helperService.fetchResponse);
    }

    function schools() {
      return $http.get('/api/schools').then(helperService.fetchResponse);
    }

    function swimOptions() {
      return $http.get('/api/swim-options').then(helperService.fetchResponse);
    }

    function all() {
      return Child.query()
          .$promise
          .then(function (children) {
            for(var i = 0, child; (child = children[i]); i++) {
              child.birthday = moment(child.birthday).toDate();
            }

            return children;
          });
    }

    function get(id) {
      return Child.get({childId: id})
        .$promise
        .then(function (child) {
          child.birthday = moment(child.birthday).toDate();
          
          if(child.passport_detail) {
            child.passport_detail.expiry_date = child.passport_detail.expiry_date ? moment(child.passport_detail.expiry_date).toDate() : undefined;
            child.passport_detail.date_of_issue = child.passport_detail.date_of_issue ? moment(child.passport_detail.date_of_issue).toDate() : undefined;
            child.passport_detail.date_of_expiry = child.passport_detail.date_of_expiry ? moment(child.passport_detail.date_of_expiry).toDate() : undefined; 
          } else {
            child.passport_detail = {};
          }

          if (!child.second_parent) {
            child.second_parent = {};
          }

          if (!child.emergency_contacts) {
            child.emergency_contacts[0] = {};
            child.emergency_contacts[1] = {};
          }

          return child;
        });
    }

    function store(data) {
      return Child.save(helperService.prepareDatesForRequest(data, ['birthday'], true)).$promise;
    }

    function update(data) {
      return Child.update(helperService.prepareDatesForRequest(data, ['birthday'], true)).$promise;
    }

    function remove(id) {
      return Child.delete({childId: id}).$promise;
    }
  }
})();
