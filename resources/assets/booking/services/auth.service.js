(function () {
  'use strict';

  angular
    .module('booking')
    .service('authService', authService);

  authService.$inject = [];

  /* @ngInject */
  function authService() {
    /* jshint validthis: true */
    var vm = this;

    vm.getUserId = getUserId;

    ////////////////////////////

    function getUserId() {
      return document.head.querySelector('meta[name="auth-id"]').content;
    }
  }
})();
