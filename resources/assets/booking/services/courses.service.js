(function () {
  'use strict';

  angular
    .module('booking')
    .service('courseService', courseService);

  courseService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function courseService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.all = all;
    vm.get = get;
    vm.questions = questions;

    ///////////////////////////

    function all() {
      return $http.get('/api/courses').then(helperService.fetchResponse);
    }

    function questions(courseId) {
      return $http.get('/api/course/' + courseId + '/questions').then(helperService.fetchResponse, helperService.rejectResponse);
    }

    function get(courseId, childId) {
      return $http.get('/api/children/' + childId + '/courses/' + courseId + '/get').then(helperService.fetchResponse, helperService.rejectResponse);
    }
  }
})();
