(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ShowOrderController', ShowOrderController);

  ShowOrderController.$inject = ['order'];

  /* @ngInject */
  function ShowOrderController(order) {
    /* jshint validthis: true */
    var vm = this;

    vm.order = order;
  }
})();
