(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CreateOrderController', CreateOrderController);

  CreateOrderController.$inject = ['$uibModal', 'children', 'swimOptions', 'orderService', '$state', 'alertService', 'helperService', '$filter', 'products', 'countOrders', 'courseService', 'questions'];

  /* @ngInject */
  function CreateOrderController($uibModal, children, swimOptions, orderService, $state, alertService, helperService, $filter, products, countOrders, courseService, questions) {
    /* jshint validthis: true */
    var vm = this;

    if (!children.length) {
      alertService.error('Please add your children!');
      $state.go('booking.children.create');
    }

    vm.countOrders = countOrders;
    vm.swimOptions = swimOptions;
    vm.children = children;
    vm.products = products;
    vm.courses = [];

    vm.request = true;
    vm.showPassportForm = false;
    vm.showSwimOptionForm = false;

    vm.form = {
      child_id: undefined,
      course_id: undefined,
      can_swim_id: undefined,
      comment: undefined,
      passport_detail: {
        no: undefined,
        nationality: undefined,
        first: undefined,
        place_of_issue: undefined,
        date_of_issue: undefined,
        date_of_expiry: undefined
      },
      products: [],
      answers: [],
    };
    vm.selectedProduct = undefined;
    vm.datePickerForDateOfIssueOpen = false;
    vm.datePickerForDateOfExpiryOpen = false;

    vm.create = create;
    vm.selectedChild = selectedChild;
    vm.disableNext = disableNext;
    vm.addProduct = addProduct;

    ///////////////////////

    function addProduct() {

    }

    function selectedChild() {
      var child = $filter('filter')(children, {id: vm.form.child_id})[0];

      $uibModal.open({
        templateUrl: '/orders/child-details-modal.html',
        controller: 'ChildDetailsModalController',
        controllerAs: 'ShowChildCtrl',
        size: 'lg',
        resolve: {
          child: function () {
            return child;
          },
          questions: function () {
            return questions;
          }
        }
      });

      courseService.all().then(function (courses) {
        vm.courses = $filter('filter')(courses, function (course) {
          var age = $filter('age')(child, course.start_date);
          var schoolIds = $filter('schoolIds')(course.course.schools);
          return course.min_age <= age && age <= course.max_age && 0 <= schoolIds.indexOf(child.school.id) && checkSchoolGroups(course.course.school_groups, child.school_groups);
        });
      });
    }

    function checkSchoolGroups(courseSchoolGroups, childSchoolGroups) {
      if (courseSchoolGroups.length && childSchoolGroups.length) {
        var courseSchoolGroupIds = $filter('ids')(courseSchoolGroups),
          childSchoolGroupIds = $filter('ids')(childSchoolGroups);

        return courseSchoolGroupIds.some(function (courseSchoolGroupId) {
          return childSchoolGroupIds.indexOf(courseSchoolGroupId) !== -1;
        });
      } else {
        return true;
      }
    }

    function create() {
      courseService.get(vm.form.course_id, vm.form.child_id).then(function (course) {
        if (course.length !== 0) {
          $uibModal.open({
            templateUrl: '/questions/questions.html',
            controller: 'QuestionsController',
            controllerAs: 'QuestionsCtrl',
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
              course: function () {
                return course;
              },
              swimOptions: function () {
                return swimOptions;
              }
            }
          }).result.then(function (additionalData) {
            vm.form.passport_detail = helperService.prepareDatesForRequest(additionalData.passport_detail, ['date_of_issue', 'date_of_expiry'], true);
            vm.form.can_swim_id = additionalData.can_swim_id;
            vm.form.answers = additionalData.answers;
            vm.form.comment = additionalData.comment;

            if (helperService.ShowValidationErrors(orderService.validate(vm.form, vm.children, vm.courses)).length) {
              vm.request = false;
              return;
            }

            orderService.add(vm.form).then(function () {
              alertService.success('Order was saved');
              $state.go('^.^.complete');
            });
          });
        }
        else {
          alertService.error('Selected event is invalid.');
          vm.request = false;
        }
      }).catch(function () {
        vm.request = false;
      });
    }

    function disableNext() {
      return vm.request && !(vm.form.child_id && vm.form.course_id);
    }
  }
})();
