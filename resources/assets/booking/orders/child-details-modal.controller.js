(function () {
  'use strict';

  angular
    .module('booking')
    .controller('ChildDetailsModalController', ChildDetailsModalController);

  ChildDetailsModalController.$inject = ['$uibModalInstance', 'child', 'questions'];

  /* @ngInject */
  function ChildDetailsModalController($uibModalInstance, child, questions) {
    /* jshint validthis: true */
    var vm = this;

    vm.child = child;
    vm.questions_in_categories = questions;
    vm.answers = initAnswers();

    vm.close = close;

    /////////////////////

    function close() {
      $uibModalInstance.close();
    }

    function initAnswers() {
      var answers = [];

      angular.forEach(vm.questions_in_categories, function (category) {
        angular.forEach(category.questions, function (question) {
          var checkAnswer = isAnswer(question.id, child.questions);

          if (checkAnswer) {
            answers[question.id] = checkAnswer;
          }
        });
      });

      return answers;
    }

    function isAnswer(questionId, answers) {
      var response = false;

      angular.forEach(answers, function (answer) {
        if (answer.id === questionId && answer.pivot.answer) {
          response = {
            answer: answer.pivot.answer,
            text: answer.pivot.text
          };
        }
      });

      return response;
    }
  }
})();
