(function() {
  'use strict';

  angular
    .module('booking')
    .config(config);

  config.$inject = ['$httpProvider'];

  function config($httpProvider) {

    $httpProvider.interceptors.push('slInterceptor');
  }

})();
