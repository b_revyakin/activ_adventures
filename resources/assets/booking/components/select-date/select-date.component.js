(function () {
  'use strict';

  angular
    .module('booking')
    .component('selectDate', {
      bindings: {
        date: '=ngModel'
      },
      templateUrl: '/components/select-date/select-date.html',
      controllerAs: 'SelectDateCtrl',
      controller: 'SelectDateController'
    });
})();