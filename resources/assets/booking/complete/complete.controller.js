(function () {
  'use strict';

  angular
    .module('booking')
    .controller('CompleteController', CompleteController);

  CompleteController.$inject = ['paymentSystems', 'payOptions', 'ordersWithPrices', 'bookingService', 'paymentTypes', 'alertService', 'orderService', 'helperService', '$filter', '$state', '$uibModal'];

  /* @ngInject */
  function CompleteController(paymentSystems, payOptions, ordersWithPrices, bookingService, paymentTypes, alertService, orderService, helperService, $filter, $state, $uibModal) {
    /* jshint validthis: true */
    var vm = this;

    vm.orders = ordersWithPrices;
    vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
    vm.orders.coupon = 0;

    vm.paymentTypes = paymentSystems.length === 0 ? paymentTypes.splice(1,1) : paymentTypes;
    vm.paymentSystems = paymentSystems;
    vm.payOptions = payOptions;
    vm.couponApplied = false;

    vm.form = {
      payment_type: undefined,

      //Comment for many count systems. By default selected Stripe.
      // payment_system: paymentSystems.length === 1 ? paymentSystems[0].system : undefined,
      payment_system: paymentSystems[0].system,
      orders: [],
      coupon: undefined,
      pay_option: payOptions[1].value, // Set full payment option by default
    };
    vm.agreed = false;
    vm.request = false;

    calculateTotalPrice();
    setOrdersToBookingForm();

    vm.book = book;
    vm.remove = remove;
    vm.applyCoupon = applyCoupon;
    vm.cancelCoupon = cancelCoupon;
    vm.allowRequestRegister = allowRequestRegister;
    vm.allowRequestDeposit = allowRequestDeposit;

    ////////////////////////////////

    function applyCoupon() {
      if (!vm.form.coupon) {
        return;
      }

      vm.request = true;

      bookingService.checkCoupon(vm.form.coupon).then(function (persent) {
        vm.orders.couponPersent = persent;
        calculateTotalPrice();
        alertService.success('Your coupon is activated.');
        vm.couponApplied = true;
      }, function () {
        alertService.error('Coupon not exists or expired.');
      }).finally(function () {
        vm.request = false;
      });
    }

    function cancelCoupon() {
      //Clear data about previous coupon
      vm.form.coupon = '';
      vm.orders.couponPersent = undefined;
      vm.orders.coupon = 0;
      vm.couponApplied = false;

      calculateTotalPrice();

      alertService.success('You coupon was deactivated.');
    }

    function remove(order) {
      var modalInstance = $uibModal.open({
        templateUrl: '/complete/confirm-remove-modal.html',
        controller: 'ConfirmRemoveOrderController',
        controllerAs: 'ConfirmRemoveCtrl',
        size: 'md'
      });

      modalInstance.result.then(function () {
        orderService.remove(order._id).then(function () {
          //Refresh data
          orderService.all().then(function (orders) {
            alertService.success('The order was removed');

            if(! orders.length) {
              $state.go('booking.orders.create');
            }

            var _orders = helperService.prepareOrders(orders);
            bookingService.calculate(_orders).then(function (prices) {
              var ordersWithPrice = [];

              for (var i = 0, order; (order = orders[i]); i++) {
                order.sum = 0;

                var price = $filter('filter')(prices, {_id: order._id}, true);
                if (price.length && price[0].amount) {
                  order.sum = price[0].amount;
                }

                ordersWithPrice.push(order);
              }


              vm.orders = ordersWithPrice;
              vm.orders.sum = $filter('sumByKey')(vm.orders, 'sum');
              vm.orders.coupon = 0;
              calculateTotalPrice();
            });

            
          });
        });
      });
    }

    function book() {
      if(! vm.agreed) {
        alertService.error('You should with booking\'s terms and conditions');
        return ;
      }

      vm.request = true;

      return bookingService.book(vm.form).then(function (data) {
        alertService.success('You booked!');

        //Clear current orders
        orderService.clear();

        helperService.redirect(data.redirect, data.method, data.fields);
      }, function () {
        vm.request = false;
      });
    }

    function setOrdersToBookingForm() {
      vm.form.orders = helperService.prepareOrders(vm.orders);
    }

    function calculateTotalPrice() {
      if(vm.form.coupon && vm.orders.couponPersent) {
        vm.orders.coupon = vm.orders.sum * vm.orders.couponPersent / 100;
        vm.orders.total = vm.orders.sum - vm.orders.coupon;
      } else {
        vm.orders.total = vm.orders.sum;
      }
    }

    function allowRequestRegister() {
      return (vm.orders.length === 1) && vm.orders[0].course.allow_register;
    }

    function allowRequestDeposit() {
      return (vm.orders.length === 1) && vm.orders[0].course.allow_deposit;
    }

  }
})();
