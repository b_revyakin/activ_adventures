(function () {
  'use strict';

  angular
    .module('booking')
    .filter('fullDate', fullDate);

  fullDate.$inject = [];

  function fullDate() {
    return main;

    function main(date) {
      return moment(date).format('DD-MM-YYYY HH:mm');
    }
  }
})();
