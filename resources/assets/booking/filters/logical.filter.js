(function(){
  'use strict';

  angular
    .module('booking')
    .filter('logical', logical);

  logical.$inject = [];

  function logical() {
    return main;

    function main(value) {
      return value ? 'Yes' : 'No';
    }
  }
})();