(function () {
  'use strict';

  angular
    .module('booking')
    .filter('ids', ids);

  ids.$inject = [];

  function ids() {
    return main;

    function main(arrayObjects) {
      var arrayIds = [];

      for (var i = 0; i < arrayObjects.length; i++) {
        arrayIds.push(arrayObjects[i].id);
      }

      return arrayIds;
    }
  }
})();
