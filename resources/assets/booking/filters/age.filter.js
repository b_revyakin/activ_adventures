(function(){
  'use strict';

  angular
    .module('booking')
    .filter('age', age);

  age.$inject = [];

  function age() {
    return main;

    function main(person, date) {
      date = date ? moment(date) : moment();

      var birthday = moment(person.birthday);
      return date.diff(birthday, 'years');
    }
  }
})();