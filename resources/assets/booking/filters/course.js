(function () {
  'use strict';

  angular
    .module('booking')
    .filter('course', course);

  course.$inject = [];

  function course() {
    return main;

    function main(course) {
      return (course.course ? course.course.name : '') + (course.name ? ' - ' + course.name : '');
    }
  }
})();