(function () {
    'use strict';

    angular
        .module('booking')
        .filter('schoolIds', schoolIds);

    schoolIds.$inject = [];

    function schoolIds() {
        return main;

        function main(schools) {
            var arrayIds = [];

            for (var i = 0; i < schools.length; i++) {
                arrayIds.push(schools[i].id);
            }

            return arrayIds;
        }
    }
})();
