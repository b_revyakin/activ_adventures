(function () {
  'use strict';

  angular
    .module('booking')
    .filter('range', range);

  range.$inject = [];

  function range() {
    return main;

    function main(input, min, max, order) {
      min = parseInt(min);
      max = parseInt(max);

      if (order === 'desc') {
        for (var i = max; i >= min; i--) {
          input.push(i);
        }
      } else {
        for (var j = min; j <= max; j++) {
          input.push(j);
        }
      }

      return input;
    }
  }
})();