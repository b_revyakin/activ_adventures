(function () {
  'use strict';

  angular
    .module('booking')
    .controller('QuestionsController', QuestionsController);

  QuestionsController.$inject = ['$uibModalInstance', 'helperService', 'orderService', 'course', 'swimOptions'];

  /* @ngInject */
  function QuestionsController($uibModalInstance, helperService, orderService, course, swimOptions) {
    /* jshint validthis: true */
    var vm = this;

    vm.swimOptions = swimOptions;
    vm.course = course;
    vm.datePickerForDateOfIssueOpen = false;
    vm.datePickerForDateOfExpiryOpen = false;

    vm.form = {
      passport_detail: {
        no: undefined,
        nationality: undefined,
        first: undefined,
        place_of_issue: undefined,
        date_of_issue: undefined,
        date_of_expiry: undefined
      },
      answers: initAnswers(),
    };

    vm.confirm = initConfirm();

    vm.send = send;
    vm.close = close;
    vm.check = check;

    //////////

    function send() {
      if (helperService.ShowValidationErrors(orderService.validateAdditionalFields(vm.form, course)).length) {
        return;
      }

      $uibModalInstance.close(vm.form);
    }

    function close() {
      $uibModalInstance.dismiss();
    }

    function check() {
      var confirmed = 1;

      angular.forEach(vm.confirm, function (value) {
        confirmed = confirmed * value;
      });

      angular.forEach(vm.form.answers, function (answer) {
        confirmed = confirmed * (answer.answer && answer.text ? 1 : 0);
      });
      
      return confirmed;
    }

    function initConfirm() {
      var checkboxes = [];

      angular.forEach(course.checkboxes, function (value) {
        checkboxes.push(false);
      });

      return checkboxes;
    }

    function initAnswers() {
      var answers = [];

      angular.forEach(course.questions, function (question) {
        var answer = {
          answer: false,
          text: ''
        };

        answers[question.id] = answer;
      });

      return answers;
    }

  }
})();