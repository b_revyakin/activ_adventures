(function () {
  'use strict';

  angular
    .module('booking')
    .factory('localStore', localStore);

  localStore.$inject = ['store', 'authService'];

  /* @ngInject */
  function localStore(store, authService) {
    //Set prefix for local storage. It is used to separate orders for different users.
    return store.getNamespacedStore(authService.getUserId());
  }
})();
