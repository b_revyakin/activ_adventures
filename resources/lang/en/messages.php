<?php

return [
    'admin' => [
        'booking' => [
            'cancel' => [
                'title' => 'Confirmation Cancel',
                'content' => 'Are you sure that you want cancel the booking? You can\'t restore back cancelled bookings!'
            ],
            'confirm' => [
                'title' => 'Confirmation',
                'content' => 'Are you sure that you want confirm the booking? You can\'t restore back confirmed bookings!'
            ]
        ],
    ],
    'customer' => [
        'booking' => [
            'cancel' => [
                'title' => 'Please confirm',
                'content' => 'Are you sure that you want cancel the booking? You can\'t restore back cancelled bookings!'
            ]
        ],
    ]
];
