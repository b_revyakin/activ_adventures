(function () {
  'use strict';

  angular
    .module('message')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider) {

    var dependecies = {
      messageGroups: ['messageService', function (messageService) {
        return messageService.getGroups();
      }],
      schools: ['groupService', function (groupService) {
        return groupService.getSchools();
      }],
      courseParts: ['groupService', function (groupService) {
        return groupService.getCourseParts();
      }],
      messages: ['messageService', '$stateParams', function (messageService, $stateParams) {
        return messageService.getMessages($stateParams.groupId);
      }],
      courseId: ['$stateParams', function ($stateParams) {
        return $stateParams.courseId;
      }],
      customerIds: ['$stateParams', function ($stateParams) {
        return $stateParams.customerIds;
      }],
    };

    $urlRouterProvider.otherwise('/sent');

    var uiView = '<ui-view></ui-view>';

    $stateProvider
      .state('send', {
        abstract: true,
        template: uiView
      })
      .state('send.index', {
        url: '/send?courseId&customerIds',
        templateUrl: '/messages/send/send.html',
        controller: 'SendController',
        controllerAs: 'SendCtrl',
        resolve: {
          courseId: dependecies.courseId,
          customerIds: dependecies.customerIds,
          schools: dependecies.schools,
          courseParts: dependecies.courseParts
        }
      })
      .state('sent', {
        abstract: true,
        template: uiView
      })
      .state('sent.index', {
        url: '/sent',
        templateUrl: '/messages/sent/sent.html',
        controller: 'SentController',
        controllerAs: 'SentCtrl',
        resolve: {
          messageGroups: dependecies.messageGroups
        }
      })
      .state('sent.show', {
        url: '/sent/show/:groupId',
        templateUrl: '/messages/sent/detail.html',
        controller: 'DetailController',
        controllerAs: 'DetailCtrl',
        resolve: {
          messages: dependecies.messages
        }
      });
  }
})();
