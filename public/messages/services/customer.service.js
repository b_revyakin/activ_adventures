(function () {
  'use strict';

  angular
    .module('message')
    .service('customerService', customerService);

  customerService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function customerService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.get = get;
    vm.getInSchool = getInSchool;
    vm.getInCoursePart = getInCoursePart;

    ///////////////////////

    function get() {
      return $http.get('/admin/api/customers/all').then(helperService.fetchResponse);
    }

    function getInSchool(schoolId) {
      return $http.get('/admin/api/schools/' + schoolId + '/customers/').then(helperService.fetchResponse);
    }

    function getInCoursePart(coursePartId) {
      return $http.get('/admin/api/courses-parts/' + coursePartId + '/customers/').then(helperService.fetchResponse);
    }

  }
})();
