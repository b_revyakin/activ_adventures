(function () {
  'use strict';

  angular
    .module('message')
    .service('helperService', helperService);

  helperService.$inject = ['alertService', '$q'];

  /* @ngInject */
  function helperService(alertService) {
    /* jshint validthis: true */
    var vm = this;

    vm.fetchResponse = fetchResponse;
    vm.ShowValidationErrors = ShowValidationErrors;

    /////////////////////////


    function fetchResponse(data) {
      return data.data;
    }

    function ShowValidationErrors(errors) {
      if (errors.length) {
        for (var i = 0, error; (error = errors[i]); i++) {
          alertService.error(error.message);
        }
      }

      return errors;
    }


  }
})();
