(function () {
  'use strict';

  angular
    .module('message')
    .service('groupService', groupService);

  groupService.$inject = ['$http', 'helperService'];

  /* @ngInject */
  function groupService($http, helperService) {
    /* jshint validthis: true */
    var vm = this;

    vm.getSchools = getSchools;
    vm.getCourseParts = getCourseParts;

    ///////////////////////

    function getSchools() {
      return $http.get('/admin/api/schools').then(helperService.fetchResponse);
    }

    function getCourseParts() {
      return $http.get('/admin/api/courses-parts').then(helperService.fetchResponse);
    }


  }
})();
