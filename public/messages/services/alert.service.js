(function () {
  'use strict';

  angular
    .module('message')
    .service('alertService', alertService);

  alertService.$inject = ['toaster'];

  /* @ngInject */
  function alertService(toaster) {
    /* jshint validthis: true */
    var vm = this;

    vm.pop = pop;
    vm.success = success;
    vm.error = error;
    vm.info = info;
    vm.showResponseError = showResponseError;

    function pop(data) {
      toaster.pop(data);
    }

    function success(data) {
      data = prepareData(data, 'Success');
      toaster.success(data);
    }

    function info(data) {
      prepareData(data, 'Info');
      toaster.info(data);
    }

    function error(data) {
      prepareData(data, 'Error');
      toaster.error(data);
    }

    function showResponseError(response, title) {
      for (var key in response) {
        if (response.hasOwnProperty(key) && (response[key] instanceof Array)) {
          for (var i in response[key]) {
            if (response[key].hasOwnProperty(i)) {
              error({
                title: title ? title : 'Error',
                body: response[key][i]
              });
            }
          }
        } else {
          error({
            title: title,
            body: response[key]
          });
        }
      }
    }

    function prepareData(data, title) {
      if (!(data instanceof Object)) {
        data = {
          body: data
        };
      }

      data.title = data.title ? data.title : title;

      return data;
    }
  }


})();
