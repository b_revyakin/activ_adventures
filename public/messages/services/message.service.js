(function () {
  'use strict';

  angular
    .module('message')
    .service('messageService', messageService);

  messageService.$inject = ['$http', 'helperService', 'Upload'];

  /* @ngInject */
  function messageService($http, helperService, Upload) {
    /* jshint validthis: true */
    var vm = this;

    vm.store = store;
    vm.getGroups = getGroups;
    vm.getMessages = getMessages;

    ///////////////////////

    function store(data) {

      return Upload.upload({
        url: '/admin/api/messages/create/',
        data: data
      });

    }

    function getGroups() {
      return $http.get('/admin/api/messages/groups').then(helperService.fetchResponse);
    }

    function getMessages(data) {
      return $http.get('/admin/api/messages/' + data).then(helperService.fetchResponse);
    }

  }
})();
