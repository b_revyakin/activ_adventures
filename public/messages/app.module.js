(function () {
  'use strict';

  angular.module('message', [
    'ui.router',
    'toaster',
    'ui.bootstrap',
    'ngFileUpload',
    'angularUtils.directives.dirPagination',
    'angular-redactor',
    'ngSanitize'
  ]);

})();
