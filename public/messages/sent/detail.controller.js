(function () {
  'use strict';

  angular
    .module('message')
    .controller('DetailController', DetailController);

  DetailController.$inject = ['messages'];

  /* @ngInject */
  function DetailController(messages) {
    /* jshint validthis: true */
    var vm = this;

    vm.group = messages;

    vm.formatDate = formatDate;

    ////////

    function formatDate(date) {
      var dateOut = new Date(date);
      return dateOut;
    }

  }

})();