(function () {
  'use strict';

  angular
    .module('message')
    .controller('SentController', SentController);

  SentController.$inject = ['messageGroups'];

  /* @ngInject */
  function SentController(messageGroups) {
    /* jshint validthis: true */
    var vm = this;

    vm.search = '';

    vm.sortReverse = false;
    vm.sortType = '';

    vm.messageGroups = messageGroups.map(function (group) {
      if (group.course_names) {
        group.type = 'Course Part';
        group.type_name = group.course_names;
      } else if (group.school) {
        group.type = 'School';
        group.type_name = group.school.name;
      } else {
        group.type = 'Individuals';
        group.type_name = '-';
      }

      return group;
    });

    vm.formatDate = formatDate;

    ////////

    function formatDate(date) {
      var dateOut = new Date(date);
      return dateOut;
    }
  }

})();