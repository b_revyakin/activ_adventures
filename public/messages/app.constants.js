(function () {

  'use strict';

  angular
    .module('message')
    .constant('mode', ['Email', 'SMS'])
    .constant('recipients', [
      {
        value: 1,
        title: 'Parents'
      },
      {
        value: 2,
        title: 'Participants'
      },
      {
        value: 3,
        title: 'Parents and Participants'
      },
    ]);

})();
