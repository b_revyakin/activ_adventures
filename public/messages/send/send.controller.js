(function () {
  'use strict';

  angular
    .module('message')
    .controller('SendController', SendController);

  SendController.$inject = ['$scope', 'courseParts', 'schools', 'recipients', 'customerService', 'messageService', 'alertService', '$state', '$filter', 'courseId', 'customerIds'];

  /* @ngInject */
  function SendController($scope, courseParts, schools, recipients, customerService, messageService, alertService, $state, $filter, courseId, customerIds) {
    /* jshint validthis: true */
    var vm = this;

    vm.schools = schools;
    vm.recipients = recipients;
    vm.courseParts = courseParts;

    vm.orderBy = {
      column: undefined,
      reverse: false
    };

    vm.isSchools = true;
    vm.isCourses = false;
    vm.loading = false;

    vm.selectGroup = {course: undefined, school: undefined};
    vm.sortReverse = false;
    vm.sortType = '';

    vm.search = '';
    vm.searchCustomer = '';
    vm.customers = undefined;
    vm.customerIds = initArrayCustomerIds(customerIds);
    vm.courseId = +courseId;

    //filter customers by 1 - schools, 2 - course parts, 3 - all customers. Default by schools.
    vm.filterCustomers = 1;
    vm.disableFilterCustomers = false;

    vm.viewSchools = viewSchools;
    vm.viewCourse = viewCourse;
    vm.viewAllCustomers = viewAllCustomers;

    vm.getCustomersInSchool = getCustomersInSchool;
    vm.getCustomersInCoursePart = getCustomersInCoursePart;

    vm.sendMessage = sendMessage;
    vm.clearFile = clearFile;

    vm.toggleAllSelected = toggleAllSelected;
    vm.toggleSelected = toggleSelected;
    vm.sortByColumn = sortByColumn;

    vm.form = {
      text: undefined,
      mode: 1,
      recipient: 1,

      course_part_ids: [],
      school_id: undefined,

      customer_ids: []
    };

    vm.isAllSelected = true;

    initSelectedCustomers();

    $scope.$watch('SendCtrl.form.mode', function (newValue, oldValue) {
      vm.form.text = '';
    });
    //////////

    function toggleAllSelected() {
      angular.forEach(vm.customers, function (customer) {
        customer.selected = vm.isAllSelected;
      });
    }

    function toggleSelected() {
      vm.isAllSelected = vm.customers.every(function (customer) {
        return customer.selected;
      });
    }

    function viewSchools() {
      vm.isCourses = false;
      vm.isSchools = true;
    }

    function viewCourse() {
      vm.isCourses = true;
      vm.isSchools = false;
    }

    function viewAllCustomers() {
      vm.isAllSelected = false;

      vm.isCourses = false;
      vm.isSchools = false;

      vm.loading = true;

      customerService.get().then(function (customers) {
        vm.customers = customers;
        toggleAllSelected();
      }).finally(function () {
        vm.loading = false;
      });

      sortByColumn('first_name');
    }

    function getCustomersInSchool(schoolId) {
      vm.isAllSelected = true;
      vm.loading = true;
      vm.selectGroup.school = schoolId;

      vm.form.school_id = schoolId;
      vm.form.course_part_ids = undefined;

      customerService.getInSchool(schoolId).then(function (customers) {
        vm.customers = customers;
        toggleAllSelected();
        sortByColumn('first_name', false);
      }).finally(function () {
        vm.loading = false;
      });
    }

    function getCustomersInCoursePart(coursePartId) {
      vm.isAllSelected = true;
      vm.loading = true;
      vm.selectGroup.course = coursePartId;

      vm.form.course_part_ids = [coursePartId];
      vm.form.school_id = undefined;

      customerService.getInCoursePart(coursePartId).then(function (customers) {
        vm.customers = customers;
        toggleAllSelected();
        sortByColumn('first_name', false);
      }).finally(function () {
        vm.loading = false;
      });
    }

    function clearFile() {
      vm.form.file = undefined;
    }

    function sendMessage() {
      vm.loading = true;

      vm.form.customer_ids = getSelectedCustomerIds(vm.customers);

      messageService.store(vm.form).then(function () {
        alertService.success('The messages was send!');
        $state.go('sent.index');
      }).finally(function () {
        vm.loading = false;
      });
    }

    function initSelectedCustomers() {
      if (vm.customerIds.length && vm.courseId) {
        var selectedCourseParts = $filter('filter')(vm.courseParts, {course_id: vm.courseId});

        viewCourse();

        vm.customers = [];
        vm.filterCustomers = 2;
        vm.disableFilterCustomers = true;
        vm.courseParts = selectedCourseParts;

        vm.form.course_part_ids = selectedCourseParts.map(function (coursePart) {
          return coursePart.id;
        });

        angular.forEach(selectedCourseParts, function (part) {
          customerService.getInCoursePart(part.id).then(function (customers) {
            angular.forEach(customers, function (customer) {
              customer.selected = vm.customerIds.includes(customer.id);

              vm.customers.push(customer);
            });
          }).then(function () {
            sortByColumn('selected', true);
          });
        });

        vm.isAllSelected = false;
      }
    }

    function initArrayCustomerIds(customerIds) {
      var arrayCustomerIds = [];

      if (customerIds) {
        customerIds = customerIds.split(',');

        angular.forEach(customerIds, function (element) {
          return arrayCustomerIds.push(+element);
        });
      }

      return arrayCustomerIds;
    }

    function getSelectedCustomerIds(customers) {
      return $filter('filter')(customers, {selected: true}).map(function (customer) {
        return customer.id;
      });
    }

    function sortByColumn(column, reverse) {
      if (column === vm.orderBy.column) {
        vm.orderBy.reverse = !vm.orderBy.reverse;
      } else {
        vm.orderBy.column = column;
      }

      if (reverse !== undefined) {
        vm.orderBy.reverse = reverse;
      }

      vm.customers = $filter('orderBy')(vm.customers, vm.orderBy.column, vm.orderBy.reverse);
    }
  }
})();
