(function () {
  'use strict';

  angular
    .module('message')
    .config(config);

  config.$inject = ['$httpProvider', 'redactorOptions'];

  function config($httpProvider, redactorOptions) {

    $httpProvider.interceptors.push('slInterceptor');

    redactorOptions.plugins = ['fontsize', 'fontcolor'];
  }

})();
