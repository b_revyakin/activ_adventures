(function () {
  'use strict';

  angular
    .module('message')
    .filter('lntobr', lntobr);

  lntobr.$inject = ['$sce'];

  function lntobr($sce) {
    return function (text) {
      return $sce.trustAsHtml(text.replace(/\n/g, '<br/>'));
    };
  }
})();