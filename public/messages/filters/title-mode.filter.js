(function () {
  'use strict';

  angular
    .module('message')
    .filter('titleMode', titleMode);

  titleMode.$inject = ['mode'];

  function titleMode(mode) {
    return function (item) {
      return mode[item - 1];
    };
  }
})();