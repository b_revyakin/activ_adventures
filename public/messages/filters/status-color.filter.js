(function () {
  'use strict';

  angular
    .module('message')
    .filter('statusColor', statusColor);

  statusColor.$inject = [];

  function statusColor() {
    return function (status) {
      var color;

      if (status === 0) {
        color = 'blue';
      } else if (status === 1 || status === 10 || status === 12 || status === 102 || status === 103 || status === 200) {
        color = 'green';
      } else {
        color = 'red';
      }


      return 'color:' + color;
    };
  }
})();