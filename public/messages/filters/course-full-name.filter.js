(function () {
  'use strict';

  angular
    .module('message')
    .filter('courseFullName', courseFullName);

  courseFullName.$inject = [];

  function courseFullName() {
    return main;

    function main(coursePart) {
      return coursePart.course.name + (coursePart.name ? ' - ' + coursePart.name : '');
    }
  }
})();
