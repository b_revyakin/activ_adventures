(function () {
  'use strict';

  angular
    .module('message')
    .filter('trustAsHtml', trustAsHtml);

  trustAsHtml.$inject = ['$sce'];

  function trustAsHtml($sce) {
    return function (text) {
      return $sce.trustAsHtml(text);
    };
  }
})();