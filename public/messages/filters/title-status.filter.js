(function () {
  'use strict';

  angular
    .module('message')
    .filter('titleStatus', titleStatus);

  titleStatus.$inject = [];

  function titleStatus() {
    return function (typeStatus, mode) {
      var type;

      switch (mode){
        // Email
        case 1:
          switch (typeStatus) {
            case 0:
              type = 'In progress';
              break;
            case 1:
              type = 'Delivered';
              break;
            case 2:
              type = 'Dropped';
              break;
            case 101:
              type = 'Spam complaints';
              break;
            case 102:
              type = 'Clicks';
              break;
            case 103:
              type = 'Opens';
              break;
            default:
              type = 'Unknown status code of email message';
          }
          break;
        // Bulk SMS
        case 2:
          switch (typeStatus) {
            case 0:
              type = 'In progress';
              break;
            case 11:
              type = 'Delivered mobile';
              break;
            case 22:
              type = 'Internal fatal error';
              break;
            case 23:
              type = 'Authentication error';
              break;
            case 24:
              type = 'Data validation error';
              break;
            case 25:
              type = 'You do not have sufficient credits';
              break;
            case 26:
              type = 'Upstream credits not available';
              break;
            case 27:
              type = 'You have exceeded your daily quota';
              break;
            case 28:
              type = 'Upstream quota exceeded';
              break;
            case 29:
              type = 'Message sending cancelled';
              break;
            case 31:
              type = 'Unroutable';
              break;
            case 32:
              type = 'Blocked (probably because of a recipient’s complaint against you)';
              break;
            case 33:
              type = 'Failed: censored';
              break;
            case 50:
              type = 'Delivery failed - generic failure';
              break;
            case 51:
              type = 'Delivery to phone failed';
              break;
            case 52:
              type = 'Delivery to network failed';
              break;
            case 53:
              type = 'Message expired';
              break;
            case 54:
              type = 'Failed on remote network';
              break;
            case 55:
              type = 'Failed: remotely blocked (variety of reasons)';
              break;
            case 56:
              type = 'Failed: remotely censored (typically due to content of message)';
              break;
            case 57:
              type = 'Failed due to fault on handset (e.g. SIM full)';
              break;
            case 64:
              type = 'Queued for retry after temporary failure delivering, due to fault on handset (transient)';
              break;
            case 70:
              type = 'Unknown upstream status';
              break;
            case 201:
              type = 'Maximum batch size exceeded';
              break;
            case 200:
              type = 'In progress';
              break;
            default:
              type = 'Unknown status code of Bulk SMS message';
          }
          break;
        default:
          type = 'Unknown method of message';
          break;
      }

      return type;
    };
  }
})();