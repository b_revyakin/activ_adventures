(function () {
  'use strict';

  angular
    .module('message')
    .filter('titleLink', titleLink);

  titleLink.$inject = [];

  function titleLink() {
    return function (url) {
      if (url !== '') {
        return url.replace('/files/', '');
      }
    };
  }
})();