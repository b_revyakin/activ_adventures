(function () {
  'use strict';

  angular
    .module('message')
    .factory('slInterceptor', slInterceptor);

  slInterceptor.$inject = ['alertService', '$q'];

  function slInterceptor(alertService, $q) {
    return {
      responseError: function (res) {
        var status = res.status,
          fetchedResponse = res.data;

        if (fetchedResponse === 'Unauthorized.') {
          alertService.error('Your session was expired. Please login again.');

          window.location.href = '/login';
        } else if (fetchedResponse.length === undefined || fetchedResponse.length < 256) {
          alertService.showResponseError(fetchedResponse, titleByCodeStatus(status));
        } else {
          alertService.showResponseError(['Server Error'], titleByCodeStatus(status));
        }

        return $q.reject(fetchedResponse);
      }
    };

    function titleByCodeStatus(status) {
      var title;

      switch (status) {
        case 422:
          title = 'Validation Fail!';
          break;
        case 403:
          title = 'Access Denied!';
          break;
        case 500:
          title = 'Sorry. Server Error.';
          break;
      }

      return title;
    }
  }
})();
