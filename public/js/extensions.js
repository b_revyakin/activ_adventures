(function () {
  //Extension for datatables sort by date.
  $.extend($.fn.dataTableExt.oSort, {
    "date-uk-pre": function (a) {
      if (a == null || a == "") {
        return 0;
      }

      var date = a.split(' ')[0],
        time = a.split(' ')[1],
        ukDatea = date.split('/'),
        ukTime = time ? time.split(':') : undefined;

      return (ukDatea[2] + ukDatea[1] + ukDatea[0] + (ukTime ? ukTime[0] + ukTime[1] : 0)) * 1;
    },
    "date-uk-asc": function (a, b) {
      return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    "date-uk-desc": function (a, b) {
      return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
  });

})();