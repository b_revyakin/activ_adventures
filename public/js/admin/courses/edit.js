$(document).ready(function () {

  function deleteExceptionDate() {
    $('.exception-date-delete').on('click', function () {
      $(this).parent().remove();
    });
  }

  $('#exception-date-add').on('click', function () {
    var date = $('#exception-date').val();

    if (!date) {
      return;
    }

    $('#exception-date-example')
      .clone()
      .removeAttr('id')
      .find('span')
      .text(date)
      .parent()
      .find('input')
      .val(date)
      .parent()
      .appendTo('.exception-dates');

    deleteExceptionDate();
  });

  $('form').on('submit', function () {
    $('#exception-date-example').remove();
  });

  deleteExceptionDate();

  $(".form_datetime").datetimepicker({
    autoclose: !0,
    isRTL: App.isRTL(),
    format: "yyyy-mm-dd hh:ii:ss",
    pickerPosition: App.isRTL() ? "bottom-right" : "bottom-left"
  });


  $('#add_fields_date').click(function (e) {
    e.preventDefault();

    $('.input_fields_wrap').append('<div>' + $('.input_date').html() + '<button class="remove_field btn btn-danger margin-top-5 margin-bottom-10">Remove part</button></div>');

    $(".form_datetime").datetimepicker({
      autoclose: !0,
      isRTL: App.isRTL(),
      format: "yyyy-mm-dd hh:ii:ss",
      pickerPosition: App.isRTL() ? "bottom-right" : "bottom-left"
    });
  });

  $('.input_fields_wrap').on("click", ".remove_field", function (e) {
    e.preventDefault();

    $(this).parent('div').remove();
  });

  $('#type').change(function (event) {
    event.preventDefault();

    var type = $("#type option:selected").val();

    if (type == 'Single') {
      $('#adding-fields').show();
    } else {
      $('#adding-fields').hide();
    }
  }).change();

  $('#description').redactor();

  $('#excerpt').redactor();

  //Selected school group
  $('#school').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
    var arraySelectedSchools = $(e.currentTarget).val();

    if (arraySelectedSchools.length) {
      $('.school-groups-form').show();
    } else {
      $('.school-groups-form').hide();
    }

    $('#school-groups').prop('disabled', true);

    $.ajax({
      url: '/admin/api/schools/groups-by-schools',
      method: 'GET',
      data: {
        school_ids: arraySelectedSchools,
      },
      success: function (data) {
        $('#school-groups').find('option').remove();
        $.each(data, function (index, group) {
          $('#school-groups').append($('<option></option>').attr('value', group.id).text(group.name));
        });
        $('#school-groups').selectpicker('refresh');
      }
    });

    $('#school-groups').prop('disabled', false);
  });

});
