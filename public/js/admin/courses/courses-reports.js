$(document).ready(function () {
  $('#course-reports').dataTable({
    iDisplayLength: -1,
    "scrollX": true,
    "searching": false,
    "ordering": false,
    "paging": false,
    "info": false,
    "columnDefs": [{
      "visible": false,
      "targets": -1
    }]
  });
});