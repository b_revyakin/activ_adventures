$(document).ready(function () {
  var eventsRegisterBDatatable,
    select = $('.select-id'),
    sendMessageButton = $('#send-message'),
    sendMessageForm = $('#send-message-form'),
    addTeamButton = $('#add-team'),
    addTeamSubmitButton = $('#add-team-submit'),
    courseId = $('#course-id').val(),
    coursePartId = $('#course-part-id').val(),
    sendMessageModal = $('#send-message-modal'),
    selectFrom = $('table').data('select-from'),
    loader = $('#overlay'),
    selectedIds = [],
    selectedOrderIds = [],
    selectedCoursePartIds = [],
    arrayAllIds = [];

  sendMessageButton.hide();
  addTeamButton.hide();
  loader.hide();

  $(document).delegate('.select-id', 'change', function (event) {
    event.preventDefault();
    var selected = $(this);

    if (selected.prop('checked')) {
      selectedIds.push(selected.val());
      selectedOrderIds.push(selected.data('order-id'));

      if (selectedCoursePartIds.indexOf(selected.data('course-part-id')) === -1) {
        selectedCoursePartIds.push(selected.data('course-part-id'));
      }
    } else {
      var index = selectedIds.indexOf(selected.val()),
        orderIndex = selectedOrderIds.indexOf(selected.data('order-id')),
        coursePartIndex = selectedCoursePartIds.indexOf(selected.data('course-part-id'));

      if (index !== -1) {
        selectedIds.splice(index, 1);
      }

      if (orderIndex !== -1) {
        selectedOrderIds.splice(orderIndex, 1);
      }

      if (coursePartIndex !== -1) {
        selectedCoursePartIds.splice(orderIndex, 1);
      }
    }

    needCheckedSelectAllCheckbox();

    selectedIds.length ? sendMessageButton.show() : sendMessageButton.hide();
    selectedOrderIds.length ? addTeamButton.show() : addTeamButton.hide();
  });

  // sendMessageButton.on('click', function (event) {
  //   window.open('/admin/message#/send?courseId=' + courseId + '&customerIds=' + selectedIds, '_blank');
  // });

  addTeamSubmitButton.on('click', function (event) {
    var button = $(this),
      url = button.data('url');

    loader.show();

    $.ajax({
      url: url,
      method: 'POST',
      data: {
        _token: $('meta[name=csrf-token]').attr('content'),
        team_id: $('#selected-team').val(),
        order_ids: selectedOrderIds
      },
      success: function (data) {
        location.reload();
      },
      error: function () {
        $('#ajax-messages').prepend('<div class="alert alert-danger">Server Error. Team Was Not Added To Orders.</div>');
      },
      complete: function () {
        loader.hide();

        setTimeout(function () {
          $(".alert-danger").hide()
        }, 2500);
      }
    });
  });

  if (coursePartId) {
    $.get('/admin/api/course-parts/' + coursePartId + '/teams', {}, function (teams) {
      var buttons = [],
        exportButtons = $('.export-button');

      buttons.push({
        text: 'All',
        action: function (e, dt, node, config) {
          setExportUrl();

          dt.column(-1).search('').draw();
          eventsRegisterBDatatable.buttons().enable();
        }
      });

      buttons.push({
        text: 'Without',
        action: function (e, dt, node, config) {
          setExportUrl('null');

          dt.column(-1).search('^$', true, false).draw();
          eventsRegisterBDatatable.buttons().enable();
        }
      });

      for (var i = 0; i < teams.length; i++) {
        var button = {};

        button.text = teams[i].name;
        button.action = function (e, dt, node, config) {
          if (e.type === 'click') {
            setExportUrl(config.text);

            dt.column(-1).search(config.text).draw();
            eventsRegisterBDatatable.buttons().enable();

            this.disable();
          }
        };

        buttons.push(button);
      }

      eventsRegisterBDatatable = $('.datatable-events-register-b').DataTable({
        "columns": [
          {"searchable": false, orderable: false},
          null,
          null,
          null,
          {type: 'date-uk'},
          null,
          null,
        ],
        order: [[1, 'asc']],
        dom: "<'#buttons'B><'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        buttons: [
          {
            extend: 'collection',
            text: 'Team',
            className: 'btn purple btn-outline dt-button',
            buttons: buttons
          }
        ]
      });

      $('.dt-button').addClass('btn dt-button');
    }, 'json');
  }

  function setExportUrl(teamName) {
    $('.export-button').each(function () {
      var baseUrl = $(this).prop('href').split('?')[0],
        newUrl = teamName ? baseUrl + '?team=' + teamName : baseUrl;

      $(this).prop('href', newUrl);
    });
  }

  var selectedMode = 1, selectedRecipientMode = 1, textMessage = $('#text-message');

  sendMessageModal.on('change', '.mode', function () {
    var mode = $(this), emailFields = $('.email-fields');

    selectedMode = mode.val();

    if (mode.val() === '1') {
      emailFields.show();

      textMessage.redactor({
        plugins: [
          'fontsize',
          'fontcolor'
        ]
      });
      textMessage.val('');

    } else {
      emailFields.hide();
      emailFields.children('input').val('');

      textMessage.redactor('core.destroy');
      textMessage.val('');
    }
  });

  sendMessageModal.on('change', '.recipient', function () {
    selectedRecipientMode = $(this).val();
  });

  sendMessageModal.on('click', '#send-message-modal-submit', function () {
    loader.show();

    sendMessageForm.ajaxForm({
      data: {
        customer_ids: selectedIds,
        select_from: selectFrom,
        course_part_ids: selectedCoursePartIds
      },
      success: function () {
        $('#ajax-messages').prepend('<div class="alert alert-success">Message Sent.</div>');
        $('#close-modal').trigger({type: "click"});
      },
      error: function (response) {
        var errors = response.responseJSON;

        showErrorsInModal(errors);
      },
      complete: function (xhr) {
        loader.hide();

        setTimeout(function () {
          $("#ajax-messages").children().hide()
        }, 2500);
      }
    }).submit();
  });

  function clearDataModal() {
    $('#text').val('');
    $('#subject').val('');
    $('#file').val('');

    selectedMode = 1;
    selectedRecipientMode = 1;
    selectedIds = [];
    selectedCoursePartIds = [];
  }

  $('#select-all').change(function () {
    var cells = $('.table').DataTable().cells().nodes(),
      checkboxes = $(cells).find(':checkbox'),
      isSelected = $(this).is(':checked');

    checkboxes.prop('checked', isSelected);

    if (isSelected) {
      checkboxes.each(function (index) {
        var checkbox = $(this);

        selectedIds.push(checkbox.val());
        selectedOrderIds.push(checkbox.data('order-id'));

        if (selectedCoursePartIds.indexOf(checkbox.data('course-part-id')) === -1) {
          selectedCoursePartIds.push(checkbox.data('course-part-id'));
        }
      });
    } else {
      selectedIds = [];
      selectedOrderIds = [];
      selectedCoursePartIds = [];
    }
    selectedIds.length ? sendMessageButton.show() : sendMessageButton.hide();
    selectedOrderIds.length ? addTeamButton.show() : addTeamButton.hide();
  });

  function getAllSelectedIds() {
    var cells = $('.table').DataTable().cells().nodes(),
      checkboxes = $(cells).find(':checkbox'),
      allIds = [];

    checkboxes.each(function (index) {
      allIds.push($(this).val());
    });

    return allIds;
  }

  function needCheckedSelectAllCheckbox() {
    arrayAllIds = getAllSelectedIds();

    var isAll = (arrayAllIds.length == selectedIds.length) && arrayAllIds.every(el => selectedIds.includes(el));

    isAll ? $('#select-all').prop('checked', true) : $('#select-all').prop('checked', false);
  }

  function showErrorsInModal(errors) {
    var errorsLog = $('#errors');

    errorsLog.empty();

    errorsLog.append('<ul></ul>');

    for (var error in errors) {
      errorsLog.find('ul').append('<li>' + errors[error][0] + '</li>');
    }

    errorsLog.removeClass('hidden');
  }

  function hideErrorsInModal() {
    var errorsLog = $('#errors');

    errorsLog.addClass('hidden');
  }

  sendMessageModal.on('hidden.bs.modal', function () {
    hideErrorsInModal();
  })
});
