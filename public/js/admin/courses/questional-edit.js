$(function () {
  $(".select_category").change(function () {
    var categoryId = $(this).attr('data-category_id');

    if ($(this).is(':checked')) {
      $('.question[data-category_id=' + categoryId + ']').prop('checked', true);
    } else {
      $('.question[data-category_id=' + categoryId + ']').prop('checked', false);
    }

  });

  $(".question").change(function () {
    var categoryId = $(this).attr('data-category_id');
    if ($('.question[data-category_id=' + categoryId + ']').length === $('.question[data-category_id=' + categoryId + ']:checked').length) {
      $('.select_category[data-category_id=' + categoryId + ']').prop('checked', true);
    } else {
      $('.select_category[data-category_id=' + categoryId + ']').prop('checked', false);
    }
  });

  $('.select_category').each(function () {
    var categoryId = $(this).attr('data-category_id');

    if ($('.question[data-category_id=' + categoryId + ']').length === $('.question[data-category_id=' + categoryId + ']:checked').length) {
      $('.select_category[data-category_id=' + categoryId + ']').prop('checked', true);
    } else {
      $('.select_category[data-category_id=' + categoryId + ']').prop('checked', false);
    }
  });

});