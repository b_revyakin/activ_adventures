$(document).ready(function () {
    $('.datatable').dataTable({
        aLengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        iDisplayLength: 10,
        "scrollX": true
    });

  $('.datatable-events').dataTable({
    "autoWidth": false,
    "columns": [
      {"searchable": false, "orderable": false},
      null,
      null,
      null,
      {type: 'date-uk'},
      null,
      null,
      {'width': '50%'},
      null,
      {"searchable": false, "orderable": false}
    ],
    order: [[1, 'asc']],
    "scrollX": true
  });

  $('#requested-register-bookings-table').dataTable({
    "columns": [
      {"searchable": false, orderable: false},
      {orderable: false},
      null,
      null,
      null,
      {
        searchable: false,
        orderable: false
      }
    ],
    order: [[2, 'asc']]
  });

  // $('.datatable-events-register-b').dataTable({
  //   "columns": [
  //     {"searchable": false, orderable: false},
  //     null,
  //     null,
  //     null,
  //     null,
  //     null,
  //     null,
  //   ],
  //   order: [[1, 'asc']]
  // });

  $('.datatable-payment-register').dataTable({
    "columns": [
      {"searchable": false, "orderable": false},
      null,
      null,
      null,
      {type: 'date-uk'},
      {type: 'date-uk'},
      null,
      null,
      null,
      null,
      null,
    ],
    order: [[1, 'asc']],
  });

  $('.datatable-events-with-emergency-contacts').dataTable({
    "columns": [
      {"searchable": false, "orderable": false},
      null,
      null,
      null,
      {type: 'date-uk'},
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      {"searchable": false, "orderable": false}
    ],
    order: [[1, 'asc']],
    "scrollX": true
  });

    $('.datatable-empty').dataTable({
        iDisplayLength: -1,
        "scrollX": true,
        "searching": false,
        "ordering": false,
        "paging": false,
        "info": false
    });

    function enableAllButtonsAndDisableCurrent(button) {
        eventsDatatable.buttons().enable();
        button.disable();
    }

    var eventsDatatable = $('.datatable-with-filters').DataTable({
        aLengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        iDisplayLength: 10,
        "scrollX": true,
        dom: '<"pull-right"B>lfrtip',
        buttons: [
            {
                text: 'All',
                action: function (e, dt, node, config) {
                    dt.column(-2).search('').draw();
                    enableAllButtonsAndDisableCurrent(this);
                }
            },
            {
                text: 'Processing',
                action: function (e, dt, node, config) {
                    dt.column(-2).search('Processing').draw();
                    enableAllButtonsAndDisableCurrent(this);
                }
            },
            {
                text: 'Completed',
                action: function (e, dt, node, config) {
                    dt.column(-2).search('Completed').draw();
                    enableAllButtonsAndDisableCurrent(this);
                }
            }
        ]
    });
});
