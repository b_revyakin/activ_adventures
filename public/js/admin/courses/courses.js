$(document).ready(function () {

  var eventsDatatable;

  $.get('/admin/api/venues', {}, function (venues) {
    var buttons = [];
    for (var i = 0; i < venues.length; i++) {
      var button = {};

      button.text = venues[i].name;
      button.action = function (e, dt, node, config) {
        if (e.type === 'click') {
          dt.column(-6).search(config.text).draw();
          eventsDatatable.buttons().enable();
          this.disable();
        }
      };

      buttons.push(button);
    }

    eventsDatatable = $('#events-table').DataTable({
      "columns": [
        null,
        null,
        {type: 'date-uk'},
        {type: 'date-uk'},
        null,
        null,
        {"searchable": false, "orderable": false}
      ],
      dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

      buttons: [

        {
          extend: 'excel',
          className: 'btn green btn-outline dt-button'
        },
        {
          extend: 'pdf',
          className: 'btn yellow btn-outline dt-button'
        },
        {
          text: 'All',
          className: 'btn dark btn-outline dt-button',
          action: function (e, dt, node, config) {
            dt.column(-6).search('').draw();
            eventsDatatable.buttons().enable();
          }
        },
        {
          extend: 'collection',
          text: 'Venue',
          className: 'btn purple  btn-outline dt-button',
          buttons: buttons
        }
      ]
    });

    $('.dt-button').addClass('btn dt-button');
  }, 'json');

  $('#description').redactor();



});
