var AppCalendar = function () {
  return {
    init: function () {
      this.initCalendar()
    }, initCalendar: function () {
      if (jQuery().fullCalendar) {
        var e = new Date, t = e.getDate(), a = e.getMonth(), n = e.getFullYear(), r = {};
        App.isRTL() ? $("#calendar").parents(".portlet").width() <= 720 ? ($("#calendar").addClass("mobile"), r = {
          right: "title, prev, next",
          center: "",
          left: "agendaDay, agendaWeek, month, today"
        }) : ($("#calendar").removeClass("mobile"), r = {
          right: "title",
          center: "",
          left: "agendaDay, agendaWeek, month, today, prev,next"
        }) : $("#calendar").parents(".portlet").width() <= 720 ? ($("#calendar").addClass("mobile"), r = {
          left: "title, prev, next",
          center: "",
          right: "today,month,agendaWeek,agendaDay"
        }) : ($("#calendar").removeClass("mobile"), r = {
          left: "title",
          center: "",
          right: "prev,next,today,month,agendaWeek,agendaDay"
        });
        var o = function (e) {
          e = 0 === e.length ? "Untitled Event" : e;
          var t = $('<div class="external-event label label-default">' + e + "</div>");
          jQuery("#event_box").append(t);//l(t)
        };
        $("#external-events div.external-event").each(function () {
          //  l($(this))
        }), $("#event_add").unbind("click").click(function () {
          var e = $("#event_title").val();
          o(e)
        }), $("#event_box").html(""), o("My Event 1"), o("My Event 2"), o("My Event 3"), o("My Event 4"), o("My Event 5"), o("My Event 6"), $("#calendar").fullCalendar("destroy"), $("#calendar").fullCalendar({
          header: r,
          defaultView: "month",
          slotMinutes: 15,
          editable: false,
          events: [],
          defaultDate: getMonth(),
          viewRender: function (view, element) {
            history.pushState('', '', window.location.href.split("?")[0]
              + '?month=' + $('#calendar').fullCalendar('getDate').format('YYYY-MM-DD'));
          }

        })
      }
    }
  }
}();

jQuery(document).ready(function () {
  AppCalendar.init();

  $.get({
    url: '/admin/api/courses-parts/calendar',
    success: function (events) {
      $('#calendar').fullCalendar('removeEvents');
      $('#calendar').fullCalendar('addEventSource',
        events.map(function (event) {
          event['backgroundColor'] = color[event.id % 10];
          return event;
        })
      );

      $('#calendar').fullCalendar('rerenderEvents');
    }
  });
});

var color = [
  '#1bbc9b',
  '#F8CB00',
  '#56b9f3',
  '#95a5a6',
  '#9b59b6',
  '#69a4e0',
  '#56f371',
  '#597ab6',
  '#cc8e41',
  '#cc8e41'
];

function getMonth() {

  var query = location.search.substring(1);
  var vars = query.split("&");

  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");

    if (pair[0] == 'month') {
      return pair[1];
    }
  }

  return null;
}