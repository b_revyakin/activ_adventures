$(document).ready(function () {
  var childGroups = [],
    saveButton = $('#save-children-school-groups');

  $(".sortable").dragtable({maxMovingRows: 1, dragHandle: '.dragtable-column', dragaccept: '.accept'});

  $('.checkbox-group:checked').each(function () {
    var checkedChildGroup = {
      child_id: $(this).parent().data().id,
      group_id: $(this).attr('value')
    };
    childGroups.push(checkedChildGroup);
  });

  function hasElement(childGroup) {
    return childGroups.findIndex(function (element) {
      return element.group_id === childGroup.group_id && element.child_id === childGroup.child_id;
    });
  }

  /* Create an array with the values of all the checkboxes in a column */
  $.fn.dataTable.ext.order['dom-checkbox'] = function (settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
      return $('input', td).prop('checked') ? '1' : '0';
    });
  };

  var tableSchoolGroups = $('#children-school-groups-table').DataTable({
    "columnDefs": [
      {"orderDataType": "dom-checkbox", "targets": 'accept'}
    ],
    "ordering": true,
    dom: "<'row' <'col-md-12'C>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
  });
  saveButton.hide();

  tableSchoolGroups.on('click', '.checkbox-group', function () {
    var childGroup = {
      child_id: $(this).parent().data().id,
      group_id: $(this).val()
    };

    if ($(this).is(':checked')) {
      if (hasElement(childGroup) === -1) {
        childGroups.push(childGroup);
      }
    } else {
      var index = hasElement(childGroup);

      if (index !== -1) {
        childGroups[index].group_id = '';
      }
    }
    saveButton.show();
  });

  saveButton.click(function (event) {
    event.preventDefault();

    saveButton.prop('disabled', true);

    $.ajax({
      url: saveButton.data('url'),
      method: 'POST',
      data: {
        _token: $('meta[name="csrf-token"]').attr('content'),
        groups: childGroups
      },
      success: function () {
        $('.portlet-body').prepend('<div class="alert alert-success">School Groups Was Saved.</div>');
        saveButton.hide();
      },
      error: function () {
        $('.portlet-body').prepend('<div class="alert alert-danger">Server Error. School Groups Was Not Saved.</div>');
      },
      complete: function () {
        saveButton.prop('disabled', false);

        setTimeout(function () {
          $(".alert-success").fadeOut('fast');
        }, 2500);

        setTimeout(function () {
          $(".alert-danger").fadeOut('fast');
        }, 2500);
      }
    });
  });
});
