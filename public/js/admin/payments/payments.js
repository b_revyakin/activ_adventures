$(document).ready(function () {
    $('#payments-data-table').dataTable({
        "columns": [
            {
                searchable: false,
                orderable: true
            },
            null,
            null,
            null,
            {type: 'date-uk'},
            {
                searchable: false,
                orderable: false
            }
        ],
        "order": [[0, "desc"]]
    });
});