$(document).ready(function () {
  var table = $('#parents-table').dataTable({
    "columns": [
      null,
      null,
      null,
      null,
      null,
      {"searchable": false, "orderable": false},
      {"searchable": false, "orderable": false}
    ],
    dom: 'Blfrtip',
    buttons: [
      {
        extend: 'excel',
        className: 'btn green btn-outline dt-button'
      },
      {
        extend: 'pdf',
        className: 'btn yellow btn-outline dt-button'
      }
    ]
  });

  $('.dt-button').addClass('btn btn-default');
});
