$(function () {
  var infoInputs = {
    firstName: $('#first-name'),
    lastName: $('#last-name'),
    email: $('#email'),
    phone: $('#mobile'),
    about: $('#about'),
  };

  var info = {
    firstName: infoInputs.firstName.val(),
    lastName: infoInputs.lastName.val(),
    email: infoInputs.email.val(),
    phone: infoInputs.phone.val(),
    about: infoInputs.about.val()
  };

  $('#reset-info').on('click', function () {
    infoInputs.firstName.val(info.firstName);
    infoInputs.lastName.val(info.lastName);
    infoInputs.email.val(info.email);
    infoInputs.phone.val(info.phone);
    infoInputs.about.val(info.about);
  });

  $('#reset-password').on('click', function () {
    $('#old-password').val('');
    $('#new-password').val('');
    $('#new-password-confirmation').val('');
  });
});
