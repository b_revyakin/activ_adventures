$(document).ready(function () {
  $('#text-editor').redactor();

  $('.text-editor').redactor();

  $('.text-editor-modal').redactor({
    plugins: [
      'fontsize',
      'fontcolor'
    ]
  });
});