$(document).ready(function () {
  $('#categories-table').dataTable({
    'ordering': false,
    "columns": [
      null,
      {"searchable": false, "orderable": false}
    ]
  });
});