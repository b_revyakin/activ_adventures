$(document).ready(function () {
  $('#schools-table').dataTable({
    "columns": [
      null,
      null,
      {"searchable": false, "orderable": false}
    ],
    dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    buttons: [
      {
        extend: 'excel',
        className: 'btn green btn-outline dt-button'
      },
      {
        extend: 'pdf',
        className: 'btn yellow btn-outline dt-button'
      }
    ]
  });

  $('.dt-button').addClass('btn btn-default');
});
