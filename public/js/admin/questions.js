$(document).ready(function () {
  $('#questions-table').dataTable({
    'ordering': false,
    "columns": [
      null,
      null,
      {"searchable": false, "orderable": false}
    ]
  });
});
