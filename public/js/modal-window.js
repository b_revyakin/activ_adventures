$(function () {
    /*$.ajaxSetup({
     headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
     });*/

    var modal = $('#modal'),
      form = modal.find('#form');

    form.on('submit', function (event) {
        var ajax = form.data('ajax');
        var action = form.data('action');
        var entityId = form.data('id');
        var tableId = form.data('table-id');

        if (!ajax) {
            return;
        }

        event.preventDefault();
        //Disable buttons
        modal.find('button').prop('disabled', true);

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            type: form.attr('method'),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function () {
                if(action === 'cancel-order') {
                    var orderTr = $('#' + tableId).find('tr[data-id=\'' + entityId + '\']');
                    orderTr.hide();

                    return;
                }

                var bookingTr = $('#' + tableId).find('tr[data-id=\'' + entityId + '\']');
                if(action === "cancel") {
                    bookingTr.find('.btn-cancel').addClass('hide');
                }
                if (action === 'confirm') {
                    bookingTr.find('.btn-cancel').text('Cancel');
                }
                bookingTr.find('.btn-confirm').addClass('hide');
                bookingTr.find('.label-status')
                  .removeClass('label-warning')
                  .text(action === "cancel" ? 'Cancelled' : 'Completed')
                  .addClass(action === "cancel" ? 'label-danger' : 'label-success');
            },
            error: function () {
                alert('Error on action! Please refresh page and contact with sys admin.');
            },
            complete: function () {
                //Enable buttons
                modal.find('button').prop('disabled', false);

                $('#modal').modal('hide');
            }
        });
    });
});

$(document).ready(function () {
    $('#modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var url = button.data('url');
        var method = button.data('method');
        var title = button.data('title');
        var content = button.data('content');
        var ajax = button.data('ajax');
        var action = button.data('action');
        var id = button.data('id');
        var tableId = button.parents('table').attr('id');

        var modal = $(this);
        modal.find('.modal-title').text(title);
        modal.find('.modal-body p').text(content);
        modal.find('#form').attr('action', url);
        modal.find('#form').data('ajax', ajax);
        modal.find('#form').data('action', action);
        modal.find('#form').data('id', id);
        modal.find('#form').data('table-id', tableId);
        modal.find('#form input[name="_method"]').val(method);
    });

    $('#payment-systems').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var url = button.data('url');
        var code = button.data('code');

        var modal = $(this);
        modal.find('#stripe-system').attr('href', url);
        modal.find('#form input[name="Crypt"]').val(code);
    });
});