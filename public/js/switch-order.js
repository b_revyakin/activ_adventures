$(document).ready(function () {
  var data;

  $("#sortable").sortable({
    axis: 'y',
    update: function (event, ui) {
      data = $(this).sortable('toArray');
      $('.save-order').removeClass('hide');
    }
  });

  $('.save-order').click(function (event) {
    event.preventDefault();

    $('.save-order').prop('disabled', true);

    $.ajax({
      url: $('.save-order').data('url'),
      method: 'POST',
      data: {
        _token: $('meta[name="csrf-token"]').attr('content'),
        _method: 'PUT',
        items: data
      },
      success: function () {
        $('.portlet-body').prepend('<div class="alert alert-success">Order Was Saved.</div>');
      },
      error: function () {
        $('.portlet-body').prepend('<div class="alert alert-danger">Server Error. Order Was Not Saved.</div>');
      },
      complete: function () {
        $('.save-order').prop('disabled', false);

        setTimeout(function () {
          $(".alert-success").hide('blind', {}, 500)
        }, 2500);

        setTimeout(function () {
          $(".alert-danger").hide('blind', {}, 500)
        }, 2500);
      }
    });
  });
});
