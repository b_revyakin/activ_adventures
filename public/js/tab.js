$(document).ready(function () {
  $('.tab').click(function (event) {
    event.preventDefault();

    $('.tab').removeClass('tab-custom-active').addClass('tab-custom');
    $(this).removeClass('tab-custom').addClass('tab-custom-active');

  });
});