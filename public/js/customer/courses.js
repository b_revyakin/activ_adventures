$(document).ready(function () {
  $('#courses-table').dataTable({
    responsive: true,
    "columns": [
      null,
      null,
      {type: 'date-uk'},
      null,
      //null,
      {
        searchable: false,
        orderable: false
      }
    ]
  });
});