var elixir = require('laravel-elixir');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var templateCache = require('gulp-angular-templatecache');

require('laravel-elixir-angular');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir.extend('buildjstemplates', function (src, dist, settings) {
  return gulp.src(src)
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(templateCache(settings))
    .pipe(gulp.dest(dist));
});

elixir(function (mix) {
  mix.angular('./resources/assets/booking/', 'public/js/', 'app.js');

  mix.angular('public/messages/', 'public/js/', 'messages-app.js');

  mix.buildjstemplates(['./resources/assets/booking/**/*.html'], 'public/js/', {module: 'booking', root: '/'});

  mix.version([
    'js/app.js',
    'js/messages-app.js',
    'js/templates.js'
  ]);
});

