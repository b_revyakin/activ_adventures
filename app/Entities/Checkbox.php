<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Checkbox extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = ['text'];

    protected $dates = ['deleted_at'];

    public function coursePart()
    {
        return $this->belongsTo(CoursePart::class);
    }
}
