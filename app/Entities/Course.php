<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Course extends Model
{
    use ActiveTrait;

    protected $fillable = ['name', 'active', 'description', 'type', 'equipment_food'];

    /**
     * Get venue of event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    /**
     * Get season of event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * Get own(staff) of event who created event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    /**
     * @return mixed
     */
    public function school()
    {
        return $this->schools()->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schools()
    {
        return $this->belongsToMany(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parts()
    {
        return $this->hasMany(CoursePart::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function photos()
    {
        return $this->morphMany(Photo::class, 'imageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function document()
    {
        return $this->hasOne(Document::class);
    }

    /**
     * @param null $byTeam
     * @return Collection
     */
    public function getOrders($byTeam = null)
    {
        $orders = new Collection();

        /** @noinspection PhpUndefinedFieldInspection */
        foreach ($this->parts as $part) {
            /** @noinspection PhpUndefinedMethodInspection */
            foreach ($part->orders()->with('child')->active()->byTeam($byTeam)->get() as $order) {
                $orders->push($order);
            }
        }

        return $orders;
    }

    /**
     * @return int
     */
    public function totalBookings()
    {
        $count = 0;

        /** @noinspection PhpUndefinedFieldInspection */
        foreach ($this->parts as $part) {
            $count += $part->orders()->active()->count();
        }

        return $count;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $part = $this->parts()->orderBy('start_date')->first();

        return $part ? $part->start_date : '';
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $part = $this->parts()->orderBy('end_date', 'desc')->first();
        
        return $part ? $part->end_date : '';
    }

    /**
     * @return bool
     */
    public function isSimple()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->type === 'Single' ? true : false;
    }

    /**
     * @return string
     */
    public function getCost()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return '£ ' . $this->parts()->sum('cost_per_course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schoolGroups()
    {
        return $this->belongsToMany(SchoolGroup::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeByNotExistsFaqs($query)
    {
        return $query->whereHas('parts', function ($query) {
            return $query->has('categoryFaqs', '=', 0);
        });
    }

    /**
     * @return Collection
     */
    public function getRequestedRegisterBookings()
    {
        $bookings = new Collection();

        foreach ($this->getOrders() as $order) {
            if ($order->booking->requested_register) {
                $bookings->push($order->booking);
            }
        }

        return $bookings;
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        $children = new Collection();

        foreach ($this->getOrders() as $order) {
            $children->push($order->child);
        }

        return $children->unique();
    }
}
