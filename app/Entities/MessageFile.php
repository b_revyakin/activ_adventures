<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MessageFile extends Model
{
    protected $fillable = ['name', 'path', 'original_name'];

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->setPath();
        });
    }

    public function baseDir()
    {
        return '/files/';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function messageGroup()
    {
        return $this->belongsTo(MessageGroup::class);
    }

    /**
     * Set name attribute.
     * @param $extension
     */
    public function setName($extension)
    {
        $this->name = time() . str_random() . '.' . $extension;
    }

    /**
     * Set path for file.
     */
    public function setPath()
    {
        $this->path = $this->baseDir() . $this->name;
    }
}
