<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MessageStatus extends Model
{
    /**
     * Status message email delivered
     */
    const STATUS_DELIVERED = 1;

    /**
     * Status message email dropped
     */
    const STATUS_DROPPED = 2;

    /**
     * Status message email spam
     */
    const STATUS_SPAM = 101;

    /**
     * Status message email click
     */
    const STATUS_CLICKS = 102;

    /**
     * Status message email open
     */
    const STATUS_OPENS = 103;

    protected $fillable = ['service_id', 'status'];
}