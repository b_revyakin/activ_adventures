<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CoursePartTeam extends Model
{
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coursePart()
    {
        return $this->belongsTo(CoursePart::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
