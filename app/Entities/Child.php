<?php

namespace App\Entities;

use App\Traits\FirstAndLastNameToCapital;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Child extends Model
{
    use SoftDeletes, FirstAndLastNameToCapital;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'birthday',
        'sex',
        'relationship',
        'doctor_name',
        'doctor_telephone_number',
        'medical_advice_and_treatment',
        'medication',
        'special_requirements',
        'mobile',
        'home_address',
        'home_postcode',
    ];

    protected $dates = ['birthday'];

    public function parent()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getAgeAttribute()
    {
        return Carbon::now()->diff(new Carbon($this->birthday))->format('%y');
    }

    public function getSexStringAttribute()
    {
        return $this->sex ? 'Male' : 'Female';
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullnameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getBirthday()
    {
        return new Carbon($this->birthday);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function yearInSchool()
    {
        return $this->belongsTo(YearInSchool::class);
    }

    public function secondParent()
    {
        return $this->hasOne(SecondParent::class, 'child_id');
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = new Carbon($value);
    }

    public function emergencyContacts()
    {
        return $this->hasMany(EmergencyContact::class);
    }

    public function mainContact()
    {
        return $this->emergencyContacts->where('type', 'main')->first();
    }

    public function secondContact()
    {
        return $this->emergencyContacts->where('type', 'second')->first();
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class)->withPivot(['answer', 'text']);
    }

    public function getQuestionsByCategories()
    {
        return $this->questions->groupBy(function ($item, $key) {
            return $item['category_id'];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schoolGroups()
    {
        return $this->belongsToMany(SchoolGroup::class);
    }

    public function getQuestionsForExport()
    {
        $questionsToString = '';

        foreach ($this->questions as $question) {
            if ($question->pivot->answer) {
                if ($question->short_form) {
                    $title = $question->short_form;
                } else {
                    $cutTitle = explode(' ', $question->title, 3);
                    $title = $cutTitle[0] . ' ' . (key_exists(1, $cutTitle) ? $cutTitle[1] : '') . '...';
                }
                $questionsToString .= $title . "\nAnswer: " . $question->pivot->text . "\n";
            }
        }

        return $questionsToString;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable');
    }
}
