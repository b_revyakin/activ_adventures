<?php

namespace App\Entities;

use App\Traits\OrderTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryFaq extends Model
{
    use SoftDeletes, OrderTrait;

    public $timestamps = false;

    protected $fillable = ['title', 'level'];

    public function questions()
    {
        return $this->hasMany(FaqQuestion::class);
    }

    public function coursePart()
    {
        return $this->belongsTo(CoursePart::class);
    }
}
