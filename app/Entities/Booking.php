<?php

namespace App\Entities;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    const
        WAIT = 0,
        PAID = 1,
        CANCELLED = 2;

    //Statuses
    protected $fillable = ['price', 'discount', 'requested_register'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function order()
    {
        return $this->orders()->first();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * @return Payment
     */
    public function payment()
    {
        return $this->payments()->first();
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function showStatus()
    {
        switch ($this->status) {
            case self::WAIT:
                $type = 'Processing';
                break;
            case self::PAID:
                $type = 'Completed';
                break;
            case self::CANCELLED:
                $type = 'Cancelled';
                break;
            default:
                throw new Exception('Status not found', 400);
        }

        return $type;
    }

    public function isProcessing()
    {
        return $this->status === self::WAIT;
    }

    public function isPaid()
    {
        return $this->status === self::PAID;
    }

    public function isCancelled()
    {
        return $this->status === self::CANCELLED;
    }

    public function getPrice()
    {
        return '£' . number_format($this->price, 2);
    }

    public function getDiscount()
    {
        return '£' . number_format($this->discount, 2);
    }

    public function getPriceWithoutDiscount()
    {
        return '£' . number_format(($this->price + $this->discount), 2);
    }

    public function scopeActive($query)
    {
        return $query->where('status', '<>', self::CANCELLED);
    }

    public function scopeProcessing($query)
    {
        return $query->where('status', self::WAIT);
    }

    /**
     * Get amount that left pay to customer
     *
     * @return double
     */
    public function getLeftAmountAttribute()
    {
        return $this->price - $this->payments()->paid()->sum('amount');
    }

    public function getPaidSum()
    {
        return '£' . number_format($this->payments()->paid()->sum('amount'), 2);
    }

    /**
     * @param bool $simpleValue if true return only value without currency
     * @return string
     */
    public function getLeftPay($simpleValue = false)
    {
        $value = number_format($this->left_amount, 2);
        return $simpleValue ? $value : '£' . $value;
    }

    /**
     * @return $this
     */
    public function setPaid()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $this->status = self::PAID;
        $this->save();

        return $this;
    }

    public function isSimple()
    {
        return $this->orders()->count() === 1;
    }

    /**
     * @param $query
     * @param bool $include
     * @return mixed
     */
    public function scopeRequestedRegisters($query, $include = true)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $query->where('requested_register', $include);
    }
}
