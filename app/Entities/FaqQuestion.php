<?php

namespace App\Entities;

use App\Traits\OrderTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqQuestion extends Model
{
    use SoftDeletes, OrderTrait;

    public $timestamps = false;

    protected $fillable = ['level', 'title', 'answer'];

    public function category()
    {
        return $this->belongsTo(CategoryFaq::class, 'category_faq_id');
    }
}
