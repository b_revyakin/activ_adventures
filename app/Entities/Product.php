<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use ActiveTrait;

    protected $fillable = ['name', 'price'];

    //ToDo: need use in Downloadable Registers export as 'Merchandise ordered' column
}
