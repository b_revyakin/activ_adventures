<?php

namespace App\Entities;

use App\Traits\FirstAndLastNameToCapital;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecondParent extends Model
{
    use SoftDeletes, FirstAndLastNameToCapital;
    
    public $timestamps = false;
    
    protected $fillable = ['first_name', 'last_name', 'email', 'phone'];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
