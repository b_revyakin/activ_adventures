<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['price'];

    /**
     * @return mixed
     */
    public function child()
    {
        return $this->belongsTo(Child::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coursePart()
    {
        return $this->belongsTo(CoursePart::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coursePartTeam()
    {
        return $this->belongsTo(CoursePartTeam::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function orderDetail()
    {
        return $this->hasOne(OrderDetail::class);
    }

    /**
     * @param $query
     */
    public function scopeActive($query)
    {
        return $query->whereHas('booking', function ($query) {
            $query->where('status', '<>', Booking::CANCELLED);
        });
    }

    public function getPrice()
    {
        return '£' . number_format($this->price, 2);
    }

    public function questionOrder()
    {
        return $this->hasMany(QuestionOrder::class);
    }

    public function answers()
    {
        return $this->belongsToMany(CoursePartQuestion::class, 'order_question', 'order_id', 'question_id')->withPivot(['answer']);
    }

    public function scopeGetBySchool($query, School $school)
    {
        return $query->active()->with('coursePart.course.schools')->whereHas('coursePart.course.schools', function ($query) use ($school) {
            $query->where('id', $school->id);
        });
    }

    public function scopeByTeam($query, $teamName = null)
    {
        if ($teamName) {
            if ($teamName === 'null') {
                return $query->whereNull('course_part_team_id');
            }
            /** @noinspection PhpUndefinedMethodInspection */
            return $query->whereHas('coursePartTeam', function ($query) use ($teamName) {
                /** @noinspection PhpUndefinedMethodInspection */
                return $query->where('name', $teamName);
            });
        } else {
            return $query;
        }

    }
}
