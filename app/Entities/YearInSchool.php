<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class YearInSchool extends Model
{
    protected $table = 'years_in_school';

    protected $fillable = ['title'];

    public $timestamps = false;
}
