<?php

namespace App\Entities;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const
        ONLINE = 1,
        CHEQUE = 2,
        BACS = 3;

    // Payment Types
    const
        SAGEPAY = 'sagepay',
        STRIPE = 'stripe';

    // Payment Methods
    const
        WAIT = 0,
        PAID = 1,
        CANCELLED = 2,
        REFUND = 3;

    //Statuses
    protected $fillable = ['amount', 'type', 'method', 'response'];

    public static function getTypeByTitle($title)
    {
        switch ($title) {
            case 'online':
                $type = self::ONLINE;
                break;
            case 'BACS':
                $type = self::BACS;
                break;
            case 'cheque':
                $type = self::CHEQUE;
                break;
            default:
                throw new Exception('Payment type not found', 400);
        }

        return $type;
    }

    public function getType()
    {
        switch ($this->type) {
            case self::ONLINE:
                $type = 'online';
                break;
            case self::BACS:
                $type = 'BACS';
                break;
            case self::CHEQUE:
                $type = 'cheque';
                break;
            default:
                throw new Exception('Payment type not found', 400);
        }

        return $type;
    }

    public function showType()
    {
        switch ($this->type) {
            case self::ONLINE:
                $type = 'Online';
                break;
            case self::BACS:
                $type = 'BACS';
                break;
            case self::CHEQUE:
                $type = 'Cheque';
                break;
            default:
                throw new Exception('Type not found', 400);
        }

        return $type;
    }

    public function showMethod()
    {
        return ucfirst($this->method);
    }

    /**
     * Method or 'by admin' if done from admin's side
     *
     * @return string
     */
    public function getMethodAttribute()
    {
        return $this->attributes['method'] ?: 'by admin';
    }

    public function getClassByStatus()
    {
        switch ($this->status) {
            case self::WAIT:
                $status = 'warning';
                break;
            case self::PAID:
                $status = 'success';
                break;
            case self::CANCELLED:
                $status = 'danger';
                break;
            case self::REFUND:
                $status = 'danger';
                break;
            default:
                throw new Exception('Status not found', 400);
        }

        return $status;
    }

    public function showStatus()
    {
        switch ($this->status) {
            case self::WAIT:
                $status = 'Processing';
                break;
            case self::PAID:
                $status = 'Completed';
                break;
            case self::CANCELLED:
                $status = 'Cancel';
                break;
            case self::REFUND:
                $status = 'Refund';
                break;
            default:
                throw new Exception('Status not found', 400);
        }

        return $status;
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function isOnline()
    {
        return $this->type === self::ONLINE;
    }

    public function setPaid($transactionId = null)
    {
        $this->status = self::PAID;
        if($transactionId) {
            $this->transaction_id = $transactionId;
        }

        $this->save();
    }

    public function setMethodStripe()
    {
        $this->method = self::STRIPE;
        $this->save();
    }

    public function setCancelled()
    {
        $this->status = self::CANCELLED;

        return $this->save();
    }

    public function setRefund()
    {
        $this->status = self::REFUND;

        return $this->save();
    }

    public function scopePaid($query)
    {
        return $query->where('status', self::PAID);
    }

    public function getAmount()
    {
        return '£' . number_format($this->amount, 2);
    }

    public function getDate()
    {
        return $this->created_at->format('d-m-Y H:i:s');
    }

    public function isProgressing()
    {
        return $this->status === self::WAIT;
    }

    public function isCompleted()
    {
        return $this->status === self::PAID;
    }
}
