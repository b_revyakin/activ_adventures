<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class CoursePart extends Entity
{
    use SoftDeletes;

    const PAY_FULL = 0, PAY_DEPOSIT = 1;
    
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'places',
        'min_age',
        'max_age',
        'cost_per_course',
        'free_places',
        'required_passport',
        'required_swim_option',
        'excerpt',
        'allow_deposit',
        'allow_register',
        'deleted_at'
    ];

    protected $dates = ['deleted_at', 'start_date', 'end_date'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function messageGroups()
    {
        return $this->belongsToMany(MessageGroup::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teams()
    {
        return $this->hasMany(CoursePartTeam::class);
    }

    /**
     * @return string
     */
    public function getCostPerCourse()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return '£' . $this->cost_per_course;
    }

    /**
     * @return string
     */
    public function getRangeAge()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->min_age . ' - ' . $this->max_age . ' years';
    }

    /**
     * @return mixed
     */
    public function getActiveOrders()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->orders()->active()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithFreePlaces($query)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $query->where('free_places', '>', 0);
    }

    /**
     * @return bool
     */
    public function hasFreePlaces()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return (bool)$this->free_places;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActiveCourse($query)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $query->whereHas('course', function ($query) {
            /** @noinspection PhpUndefinedMethodInspection */
            return $query->active();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checkboxes()
    {
        return $this->hasMany(Checkbox::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(CoursePartQuestion::class);
    }

    /**
     * @return string
     */
    public function getCost()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return '£ ' . $this->cost_per_course;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryFaqs()
    {
        return $this->hasMany(CategoryFaq::class);
    }

    public function getFullNameAttribute()
    {
        return $this->course->name . ($this->name ? ' - ' . $this->name : '');
    }

    public function getChildren()
    {
        $children = new Collection();

        foreach ($this->orders()->with('child')->get() as $order) {
            $children->push($order->child);
        }

        return $children->unique();
    }
}
