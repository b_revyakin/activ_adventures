<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * Type email
     */
    const MODE_EMAIL = 1;

    /**
     * Type sms
     */
    const MODE_SMS = 2;

    /**
     * Boolean no completed
     */
    const NO_COMPLETED = 0;

    protected $fillable = ['contact'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function messageable()
    {
        return $this->morphTo();
    }

    public function statuses()
    {
        return $this->belongsTo(MessageStatus::class, 'service_id', 'service_id');
    }

    public function group()
    {
        return $this->belongsTo(MessageGroup::class);
    }

    /**
     * Get all message where status 0 (in treatment)
     *
     * @return mixed
     */
    public function scopeInTreatmentSms($query)
    {
        return $query->where('completed', '=', self::NO_COMPLETED);
    }

    /**
     * @param $contact
     * @return bool
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this->save();
    }

    /**
     * @return bool
     */
    public function setEmailContact()
    {
        return $this->setContact($this->messageable->email);
    }

    /**
     * @return bool
     */
    public function setPhoneContact()
    {
        return $this->setContact($this->messageable->mobile);
    }

}