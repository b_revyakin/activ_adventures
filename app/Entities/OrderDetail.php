<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = ['comment'];

    protected $dates = ['deleted_at'];

    public function canSwim()
    {
        return $this->belongsTo(SwimOption::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
