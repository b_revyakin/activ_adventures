<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class MessageGroup extends Model
{
    /**
     * Recently minutes
     */
    const RECENTLY_MINUTES = 5;

    /**
     * Recipient Mode.
     */
    const ONLY_CUSTOMER = 1,
        CUSTOMER_CHILDREN = 2,
        CUSTOMERS_AND_CHILDREN = 3;

    protected $fillable = ['mode', 'text', 'recipient_mode', 'subject'];

    protected $appends = ['course_names'];

    public function messages()
    {
        return $this->hasMany(Message::class, 'group_id')->with('statuses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courseParts()
    {
        return $this->belongsToMany(CoursePart::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Recently messages on 5 minutes
     *
     * @return mixed
     */
    public function scopeRecently($query)
    {
        return $query->whereDate('created_at', '>', Carbon::now()->subMinutes(self::RECENTLY_MINUTES));
    }

    /**
     * @return bool
     */
    public static function isRecently()
    {
        return (bool)MessageGroup::whereDate('created_at', '>', Carbon::now()->subMinutes(self::RECENTLY_MINUTES))->count();
    }

    public function files()
    {
        return $this->hasMany(MessageFile::class);
    }

    /**
     * @return mixed
     */
    public function getCourseNamesAttribute()
    {
        $names = new Collection();

        foreach ($this->courseParts as $coursePart) {
            $names->push(['name' => $coursePart->full_name]);
        }

        return $names->implode('name', ', ');
    }
}