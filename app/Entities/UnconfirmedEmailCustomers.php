<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UnconfirmedEmailCustomers extends Model
{
    protected $fillable = ['id', 'email_address', 'hash'];
}
