<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class QuestionOrder extends Model
{
    protected $fillable = ['answer', 'text'];

    public $timestamps = false;

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }


}