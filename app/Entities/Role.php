<?php

namespace App\Entities;

use Bican\Roles\Models\Role as BicanRole;
use Illuminate\Database\Eloquent\Model;

class Role extends BicanRole
{
    /**
     * Get admin role's model
     *
     * @return Role
     */
    public static function admin()
    {
        return self::whereSlug('admin')->first();
    }

    /**
     * Get staff role's model
     *
     * @return Role
     */
    public static function staff()
    {
        return self::whereSlug('staff')->first();
    }

    /**
     * Get staff role's model
     *
     * @return Role
     */
    public static function customer()
    {
        return self::whereSlug('customer')->first();
    }
    
    /**
     * Get staff role's model
     *
     * @return Role
     */
    public static function siteManager()
    {
        return self::whereSlug('site.manager')->first();
    }

    /**
     * Role belongs to many users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(config('auth.providers.users.model'))->withTimestamps();
    }

}
