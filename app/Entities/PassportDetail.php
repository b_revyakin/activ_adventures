<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class PassportDetail extends Entity
{
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = [];
    protected $guarded = ['id', 'order_id'];
    
    protected $dates = ['expiry_date', 'date_of_issue', 'date_of_expiry'];

    public function getExpiryDateAttribute($value)
    {
        return $value > Carbon::minValue() ? $value : null;
    }

    public function getDateOfIssueAttribute($value)
    {
        return $value > Carbon::minValue() ? $value : null;
    }

    public function getDateOfExpiryAttribute($value)
    {
        return $value > Carbon::minValue() ? $value : null;
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function valid()
    {
        $valid = true;

        foreach (array_except($this->attributesToArray(), ['id', 'order_id', 'deleted_at', 'expiry_date']) as $attribute) {
            if (!$attribute) {
                $valid = false;
                break;
            }
        }

        return $valid;
    }
}
