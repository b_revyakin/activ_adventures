<?php

namespace App\Entities;

use App\Traits\ActiveTrait;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use ActiveTrait;

    protected $fillable = ['phrase', 'percent'];

    public function scopeCoupon($query, $coupon)
    {
        return $query->where('phrase', $coupon);
    }


    /**
     * Check Coupon is Active. If active return count percent fractional
     *
     * @return bool|mixed
     */
    public function checkActive()
    {
        return $this->active ? $this->percent * 0.01 : false;
    }
}
