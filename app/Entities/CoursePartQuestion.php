<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoursePartQuestion extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = ['text'];

    protected $dates = ['deleted_at'];

    public function coursePart()
    {
        return $this->belongsTo(CoursePart::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_question', 'question_id', 'order_id')->withPivot(['answer']);
    }
}
