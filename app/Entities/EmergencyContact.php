<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmergencyContact extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = [
        'forename',
        'surname',
        'relationship',
        'mobile',
        'alternative_number',
        'type'
    ];

    protected $dates = ['deleted_at'];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->forename . ' ' . $this->surname;
    }
}
