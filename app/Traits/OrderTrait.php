<?php

namespace App\Traits;

trait OrderTrait
{
    public function setLevel($level)
    {
        $this->level = $level;
        $this->save();

        return $this;
    }
}
