<?php

namespace App\Traits;

trait ActiveTrait
{
    /**
     * Show is active or archive record
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Activate record
     *
     * @return $this
     */
    public function activate()
    {
        $this->active = true;
        $this->save();

        return $this;
    }

    /**
     * Deactivate record
     *
     * @return $this
     */
    public function deactivate()
    {
        $this->active = false;
        $this->save();

        return $this;
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('active', false);
    }

    public function isConfirmed()
    {
        return $this->is_confirmed;
    }
}
