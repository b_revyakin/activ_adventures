<?php

namespace App\Services;

use App\Entities\Customer;
use App\Entities\Role;
use App\Entities\Staff;
use App\Entities\User;
use Auth;
use DB;
use Exception;
use Log;
use Session;


class AdminService
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     * Create new Admin
     *
     * @param $data
     * @param bool $isStaff
     * @return User|false
     */
    public function create($data, $isStaff = false)
    {
        DB::beginTransaction();

        try {
            //1. Create user
            $user = $this->userService->create($data);

            //2. Create staff model with assign to user
            $staff = $user->staff()->save(new Staff(array_only($data, ['first_name', 'last_name', 'mobile', 'qualifications', 'about'])));

            //3. Assign to admin role
            $user->attachRole($isStaff ? Role::staff() : Role::admin());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());

            Log::error('Error by creating new admin', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $user;
    }

    /**
     * Update admin's info
     *
     * @param $userId
     * @param $data
     * @return User|false
     */
    public function update($userId, $data)
    {
        DB::beginTransaction();

        try {
            $user = User::findOrFail($userId);
            //1. Update user model
            $user->update($data);

            //2. Update staff model
            $user->staff->update($data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by updating new admin', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $user;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function delete($userId)
    {
        DB::beginTransaction();

        try {
            $user = User::findOrFail($userId);

            //1. Remove staff model
            $user->staff->delete();

            //2. Remove user model
            $user->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by removing new admin', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return true;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function loginAsCustomer(Customer $customer)
    {
        try {
            $admin = Auth::user();

            Auth::logout();

            if (!Auth::loginUsingId($customer->user->id)) {
                throw new Exception('Login as customer fail');
            }

            Session::set('adminIdForAuthBack', $admin->id);
        } catch (Exception $e) {
            Log::error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function backToAdmin()
    {
        try {
            /** @noinspection PhpUndefinedMethodInspection */
            $admin = User::findOrFail(Session::get('adminIdForAuthBack'));

            Auth::logout();

            if (!Auth::loginUsingId($admin->id)) {
                throw new Exception('Login as admin fail');
            }

            Session::forget('adminIdForAuthBack');
        } catch (Exception $e) {
            Log::error($e->getMessage());

            return false;
        }

        return true;
    }
}
