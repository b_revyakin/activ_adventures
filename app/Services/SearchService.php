<?php

namespace App\Services;


use App\Entities\Customer;
use DB;

class SearchService
{
    public function searchCustomers($q)
    {
        $results = DB::table('customers')
            ->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `full_name`, id, first_name, last_name'))
            ->having('full_name', 'LIKE', "%{$q}%")
            ->get();

        /** @noinspection PhpUndefinedMethodInspection */
        return Customer::whereIn('id', array_pluck($results, 'id'))->get();
    }
}
