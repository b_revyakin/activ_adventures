<?php

namespace App\Services;

use App\Entities\School;

class SchoolService
{

    /**
     * Create new school
     *
     * @param array $data
     * @return model
     */
    public function create(array $data)
    {
        try {
            $school = School::create($data);
        } catch (Exception $e) {
            Log::error('Error by creating new school', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $school;
    }

    /**
     * Update info for school
     *
     * @param array $data
     * @return bool
     */
    public function update(School $school, array $data)
    {
        try {
            $school->update($data);
        } catch (Exception $e) {
            Log::error('Error by updating school', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $school;
    }

    /**
     * Delete
     *
     * @param School $school
     * @return bool|null
     */
    public function destroy(School $school)
    {
        return $school->delete();
    }
}