<?php

namespace App\Services;

use App\Entities\MessageStatus;

class MessageStatusesService
{
    /**
     * Store message status
     *
     * @param $serviceId
     * @param $status
     * @return static
     */
    public function create($serviceId, $status)
    {
        return MessageStatus::create(['service_id' => self::trimServiceId($serviceId), 'status' => $status]);
    }

    public function updateServiceId(MessageStatus $messageStatus)
    {
        $messageStatus->service_id = self::trimServiceId($messageStatus->service_id);
        return $messageStatus->save();
    }

    /**
     * @param $serviceId
     * @return mixed
     */
    protected static function trimServiceId($serviceId)
    {
        $serviceId = preg_replace('/^</', '', $serviceId);
        return preg_replace('/>$/', '', $serviceId);
    }
}