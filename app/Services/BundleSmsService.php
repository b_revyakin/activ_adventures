<?php

namespace App\Services;

use App\ActivAdventures\Modules\Sms;

class BundleSmsService
{
    protected $bulkSms;

    public function __construct()
    {
        $this->bulkSms = \App::make(Sms::class);
    }

    /**
     * Send sms to gateway  across
     *
     * @param $text
     * @param $phone
     * @return mixed
     */
    public function send($text, $phone)
    {
        return $this->bulkSms->send($text, $phone);
    }


    /**
     * Get status for Batch ID
     *
     * @param $batchId
     * @return array
     */
    public function getStatus($batchId)
    {
        return $this->bulkSms->status($batchId);
    }
}