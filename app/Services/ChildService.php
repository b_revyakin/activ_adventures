<?php

namespace App\Services;

use App\Entities\Child;
use DB;
use Exception;
use Log;

class ChildService
{
    /**
     * @param array $schoolGroups
     * @return bool
     */
    public function saveSchoolGroups(array $schoolGroups)
    {
        $uniqueSchoolGroups = collect($schoolGroups)->unique()->groupBy('child_id');

        try {
            DB::beginTransaction();

            foreach ($uniqueSchoolGroups as $childId => $schoolGroups) {
                /** @noinspection PhpUndefinedMethodInspection */
                $child = Child::findOrFail($childId);

                /** @noinspection PhpUndefinedMethodInspection */
                $arraySchoolGroupIds = $schoolGroups->pluck('group_id')->filter(function ($value) {
                    return $value != '';
                })->toArray();

                /** @noinspection PhpUndefinedMethodInspection */
                $child->schoolGroups()->sync($arraySchoolGroupIds);

            }

            DB::commit();
        } catch (Exception $e) {
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return true;
    }
}