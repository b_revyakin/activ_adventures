<?php

namespace App\Services;


use App\Entities\Booking;
use App\Entities\Child;
use App\Entities\Coupon;
use App\Entities\CoursePart;
use App\Entities\Customer;
use App\Entities\Order;
use App\Entities\OrderDetail;
use App\Entities\PassportDetail;
use App\Entities\Payment;
use App\Entities\SwimOption;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Collection;
use Log;

class BookingService
{
    /**
     * @var StripeService
     */
    protected $stripeService;

    /**
     * @var QuestionService
     */
    protected $questionService;

    /**
     * BookingService constructor.
     * @param QuestionService $questionService
     * @param StripeService $stripeService
     */
    public function __construct(QuestionService $questionService, StripeService $stripeService)
    {
        $this->questionService = $questionService;
        $this->stripeService = $stripeService;
    }

    /**
     * @param Customer $customer
     * @param array $data
     * @return Booking|bool
     */
    public function create(Customer $customer, array $data)
    {
        DB::beginTransaction();

        try {
            $booking = new Booking($data);
            $booking->status = Booking::WAIT;
            $booking->customer()->associate($customer);
            $booking->save();

            $orders = new Collection();
            $priceProducts = 0;

            $ordersWithPrice = $this->calculatePrice($data['orders']);
            foreach ($ordersWithPrice as $orderData) {
                $order = new Order();
                $order->child()->associate(Child::findOrFail($orderData['child_id']));

                $coursePart = CoursePart::findOrFail($orderData['course_id']);
                $coursePart->decrement('free_places');

                $order->coursePart()->associate($coursePart);
                $order->booking()->associate($booking);
                $order->price = $orderData['amount']; //price calculate with discount!
                $order->save();

                $orderDetail = new OrderDetail(['comment' => array_key_exists('comment', $orderData) ? $orderData['comment'] : '']);
                $orderDetail->order()->associate($order);
                if (key_exists('can_swim_id', $orderData) && $orderData['can_swim_id']) {
                    $swimOption = SwimOption::findOrFail($orderData['can_swim_id']);
                    $orderDetail->canSwim()->associate($swimOption);
                }
                $orderDetail->save();

                $passportDetail = new PassportDetail($orderData['passport_detail']);
                $passportDetail->order()->associate($order);
                $passportDetail->save();

                $answers = [];
                foreach ($orderData['answers'] as $question => $answer) {
                    if ($answer) {
                        $answers[$question] = ['answer' => $answer['text']];
                    }
                }
                $order->answers()->sync($answers);
                /* 
                $products = new \Illuminate\Database\Eloquent\Collection();
                foreach ($orderData['products'] as $productId) {
                    $product = Product::findOrFail($productId);
                    $products->push($product);
                }
                $order->products()->saveMany($products);
                $priceProducts += $products->sum('price');
                $orders->push($order);*/
            }

            $coupon = isset($data['coupon']) && $data['coupon'] ? Coupon::coupon($data['coupon'])->first()->checkActive() : false;
            $priceList = $this->calculatePrice($data['orders'], true);

            if ($coupon) {
                $price = $priceList['totalAmount'] * (1 - $coupon);
            } else {
                $price = $priceList['totalAmount'] - $priceList['totalDiscount'];
            }
            $booking->price = $price + $priceProducts;
            $booking->discount = $priceList['totalDiscount'];
            $booking->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $booking;
    }

    /**
     * @param array $orders
     * @param bool|false $onlyPrice
     * @return array|float|int
     */
    public function calculatePrice(array $orders, $onlyPrice = false)
    {
        $totalAmount = 0;

        foreach ($orders as $i => $order) {
            $coursePart = CoursePart::findOrFail($order['course_id']);

            $orders[$i]['amount'] = $coursePart->cost_per_course;
            $totalAmount += $coursePart->cost_per_course;
        }
        $totalDiscount = 0;

        if ($onlyPrice) {
            return compact('totalAmount', 'totalDiscount');
        }

        return $orders;
    }

    /**
     * @param Booking $booking
     * @param $transaction
     * @param $amount
     * @param $response
     * @return Booking|bool
     */
    public function paidWithStripe(Booking $booking, $transaction, $amount, $response)
    {
        try {
            DB::beginTransaction();

            $payment = new Payment();
            $payment->booking()->associate($booking);
            $payment->amount = $amount / 100;
            $payment->setPaid($transaction);
            $payment->setMethodStripe();
            $payment->type = Payment::ONLINE;
            $payment->response = $response->__toJSON();
            $payment->save();

            $this->checkAndSetPaid($booking);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error in BookingService. Method paidWithStripe.', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return false;
        }

        return $booking;
    }

    /**
     * @param Booking $booking
     * @return Booking
     */
    protected function checkAndSetPaid(Booking $booking)
    {
        if ($booking->left_amount <= 0) {
            $booking->status = Booking::PAID;
            $booking->save();
        }

        return $booking;
    }

    public function confirmPayment(Booking $booking, Payment $payment)
    {
        try {
            DB::beginTransaction();

            $payment->setPaid();

            $this->checkAndSetPaid($booking);

            DB::commit();
        } catch (Exception $e) {
            Log::error('Error to BookingService method confirmPayment');
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return $payment;
    }

    /**
     * @param Order $order
     * @return Booking|bool
     */
    public function cancelOrder(Order $order)
    {
        $booking = $order->booking;

        if ($booking->orders()->count() === 1) {
            return $this->cancel($booking);
        }

        try {
            DB::beginTransaction();

            // 1. Delete order
            $order->delete();
            $order->coursePart->increment('free_places');

            // 2. Recalculate booking
            $orders = $booking->orders->load(['dates'])->toArray();
            foreach ($orders as $i => $order) {
                $orders[$i]['selectedDates'] = array_map(function ($array) {
                    return $array['date'];
                }, $order['dates']);
            }

            //ToDo: refactor this - remove twice calling same method
            $orders = $this->calculatePrice($orders);
            $totalPrice = $this->calculatePrice($orders, true);

            foreach ($orders as $orderData) {
                $order = Order::findOrFail($orderData['id']);
                $order->price = $orderData['amount'] - $orderData['discount'];
                $order->save();
            }

            $booking->price = $totalPrice['totalAmount'];
            $booking->discount = $totalPrice['totalDiscount'];
            $booking->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error in BookingService. Method cancelOrder.', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return false;
        }


        return $booking;
    }

    /**
     * @param Booking $booking
     * @return Booking|bool
     */
    public function cancel(Booking $booking)
    {
        try {
            DB::beginTransaction();

            $booking->status = Booking::CANCELLED;
            $booking->save();

            foreach ($booking->orders as $order) {
                $order->coursePart->increment('free_places');
            }

            foreach ($booking->payments as $payment) {
                if ($payment->isCompleted() && $payment->transaction_id) {
                    $this->stripeService->refund($payment);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error in BookingService. Method cancel.', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);

            return false;
        }

        return $booking;
    }

    /**
     * @param Booking $booking
     * @param array $data
     * @return Payment|bool
     */
    public function addAdminPayment(Booking $booking, array $data)
    {
        try {
            DB::beginTransaction();

            $payment = new Payment();
            $payment->booking()->associate($booking);
            $payment->amount = $data['amount'];
            $payment->setPaid();
            $payment->type = Payment::getTypeByTitle($data['type']);
            $payment->save();

            $this->checkAndSetPaid($booking);

            DB::commit();
        } catch (Exception $e) {
            Log::error('Error to BookingService method addAdminPayment');
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return $payment;
    }

    /**
     * @param array $dates
     * @return int
     */
    protected function hasWeeks(array $dates)
    {
        $count = 0;
        $weeks = 0;
        $carbonDates = new Collection();

        foreach ($dates as $date) {
            $carbonDates->push(Carbon::parse($date));
        }

        $carbonDates = $carbonDates->sortBy(function (Carbon $date) {
            return $date->toDateString();
        });

        $startDate = $carbonDates->first();

        foreach ($carbonDates as $carbonDate) {
            if ($startDate == $carbonDate) {
                $count++;
                $startDate->addDay();

                if ($count == 5) {
                    $count = 0;
                    $weeks++;
                }
            } else {
                $count = 1;
                $startDate = (new Carbon($carbonDate))->addDay();
            }
        }

        return $weeks;
    }
}
