<?php

namespace App\Services;

use App\Entities\Child;
use App\Entities\Country;
use App\Entities\Customer;
use App\Entities\EmergencyContact;
use App\Entities\Role;
use App\Entities\School;
use App\Entities\SecondParent;
use App\Entities\YearInSchool;
use DB;
use Exception;
use Log;

class ParentService
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * ParentService constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Create new parent
     *
     * @param array $data
     * @return Customer|bool
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {
            $user = $this->userService->create($data);

            $parent = new Customer($data);
            $parent->country()->associate(Country::findOrFail($data['country_id']));
            $user->customer()->save($parent);
            $user->attachRole(Role::customer());

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by creating parent', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $parent;
    }

    /**
     * Update a parent
     *
     * @param Customer $parent
     * @param $data
     * @return Customer|bool
     */
    public function update(Customer $parent, $data)
    {
        DB::beginTransaction();

        try {
            $parent->update($data);

            $parent->user->update($data);
            $parent->country()->associate(Country::findOrFail($data['country_id']));
            $parent->save();


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by updating parent', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $parent;
    }

    /**
     * Add child to parent
     *
     * @param Customer $parent
     * @param array $childData
     * @return Child|bool
     */
    public function addChild(Customer $parent, array $childData)
    {
        try {
            DB::beginTransaction();

            $child = new Child($childData);
            $secondParent = new SecondParent($childData['second_parent']);
            $mainContact = new EmergencyContact($childData['main_contact']);
            $secondContact = new EmergencyContact(array_merge($childData['second_contact'], ['type' => 'second']));

            $yearInSchool = YearInSchool::findOrFail($childData['year_in_school_id']);
            $school = School::where('code', $childData['school_code'])->first();
            $child->yearInSchool()->associate($yearInSchool);
            $child->school()->associate($school);

            $parent->children()->save($child);

            $secondParent->child()->associate($child);
            $secondParent->save();

            $mainContact->child()->associate($child);
            $mainContact->save();

            $secondContact->child()->associate($child);
            $secondContact->save();

            $child->questions()->sync(array_filter($childData['answers'], function ($answer) {
                return $answer !== null;
            }));

            DB::commit();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error('Error by creating a child', ['trace' => $e->getTraceAsString()]);

            DB::rollBack();

            return false;
        }

        return $child;
    }

    /**
     * Update child
     *
     * @param Child $child
     * @param array $data
     * @return Child|bool
     */
    public function updateChild(Child $child, array $data)
    {
        try {
            DB::beginTransaction();

            $child->update($data);

            $child->secondParent()->updateOrCreate(['child_id' => $child->id], $data['second_parent']);

            $yearInSchool = YearInSchool::findOrFail($data['year_in_school_id']);
            $school = School::where('code', $data['school_code'])->first();

            $child->yearInSchool()->associate($yearInSchool);
            $child->school()->associate($school);

            $dataContact = array_key_exists(0, $data['emergency_contacts']) && $data['emergency_contacts'][0] ? $data['emergency_contacts'][0] : [];

            if ($child->mainContact()) {
                $child->mainContact()->update($dataContact);
            } else {
                $mainContact = new EmergencyContact($dataContact);
                $mainContact->child()->associate($child);
                $mainContact->save();
            }

            $dataContact = array_key_exists(1, $data['emergency_contacts']) && $data['emergency_contacts'][1] ? $data['emergency_contacts'][1] : [];
            $dataContact = array_merge($dataContact, ['type' => 'second']);

            if ($child->secondContact()) {
                $child->secondContact()->update($dataContact);
            } else {
                $secondContact = new EmergencyContact($dataContact);
                $secondContact->child()->associate($child);
                $secondContact->save();
            }

            $child->save();

            $child->questions()->sync(array_filter($data['answers'], function ($answer) {
                return $answer !== null;
            }));

            DB::commit();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            DB::rollBack();

            return false;
        }

        return $child;
    }

    public function removeChild(Child $child)
    {
        return $child->delete();
    }
}
