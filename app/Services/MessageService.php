<?php

namespace App\Services;

use App\Entities\Child;
use App\Entities\Customer;
use App\Entities\Message;
use App\Entities\MessageGroup;
use App\Entities\MessageStatus;
use Mail;

class MessageService
{
    const STATUS_IN_PROGRESS = 0,
        LOG_STATUS_IN_PROGRESS = 200;

    protected $bundleSmsService;

    public function __construct()
    {
        $this->bundleSmsService = new BundleSmsService;
    }

    /**
     * @param MessageGroup $messageGroup
     * @param Customer $customer
     * @return Message
     */
    public function create(MessageGroup $messageGroup, Customer $customer)
    {
        $message = new Message;

        $message->messageable()->associate($customer);
        $message->group()->associate($messageGroup);

        $message->save();

        return $message;
    }

    /**
     * @param MessageGroup $messageGroup
     * @param Child $child
     * @return Message
     */
    public function createForChild(MessageGroup $messageGroup, Child $child)
    {
        $message = new Message;

        $message->messageable()->associate($child);
        $message->group()->associate($messageGroup);
        $message->save();

        return $message;
    }


    /**
     * Send email
     *
     * @param Message $message
     * @param MessageGroup $messageGroup
     */
    public function sendEmail(Message $message, MessageGroup $messageGroup)
    {
        $message->setEmailContact();

        $mail = Mail::send('emails.message', ['text' => $messageGroup->text, 'name' => $message->messageable->name],
            function ($send) use ($message, $messageGroup) {
                $send->to(trim($message->messageable->email), $message->messageable->name)->subject($messageGroup->subject);

                foreach ($messageGroup->files as $file) {
                    $send->attach(url('/') . $file->path);
                }
            });

        if (count(Mail::failures()) == 0 && $mail) {
            $response = json_decode($mail->getBody(), true);
            $serviceId = str_replace(['<', '>'], '', $response['id']);

            $this->updateServiceID($message, $serviceId);
            $this->updateStatus($serviceId, self::STATUS_IN_PROGRESS);
        } else {
            $serviceId = 'log_' . $message->id;
            $this->updateServiceID($message, $serviceId);
            $this->updateStatus($serviceId, self::LOG_STATUS_IN_PROGRESS);
        }
    }

    /**
     * Send SMS
     *
     * @param Message $message
     * @param MessageGroup $messageGroup
     */
    public function sendSms(Message $message, MessageGroup $messageGroup)
    {
        $message->setPhoneContact();

        $response = $this->bundleSmsService->send($messageGroup->text, $message->messageable->mobile);

        $this->updateServiceID($message, $response['id']);
        $this->updateStatus($response['id'], $response['status']);
    }

    /**
     * Update status message
     *
     * @param $serviceId
     * @param $status
     */
    public function updateStatus($serviceId, $status)
    {
        MessageStatus::create(['status' => $status, 'service_id' => $serviceId]);
    }

    /**
     * Update service ID
     *
     * @param Message $message
     * @param $id
     */
    public function updateServiceID(Message $message, $id)
    {
        $message->service_id = $id;
        $message->save();
    }

    /**
     * @param Message $message
     * @return bool
     */
    public function completed(Message $message)
    {
        $message->completed = true;
        return $message->save();
    }

}