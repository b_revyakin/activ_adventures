<?php
namespace App\Services;


use App\Entities\Category;
use DB;
use Exception;
use Log;

class CategoryService
{
    /**
     * @param array $data
     * @return bool
     */
    public function switchOrder(array $data)
    {
        try {
            DB::beginTransaction();

            foreach ($data as $level => $categoryId) {
                $category = Category::findOrFail($categoryId);
                $category->setLevel($level);
            }

            DB::commit();
        } catch (Exception $e) {
            Log::error('Error in CategoryService method switchOrder');
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return true;
    }
}
