<?php

namespace App\Services;


use App\Entities\Course;
use App\Entities\Document;
use Carbon\Carbon;
use Exception;
use Log;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentService
{
    public function upload(Course $course, UploadedFile $file)
    {
        $name = Carbon::now()->toDateTimeString() . ' - ' . $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
        $path = 'documents/' . $name;

        try {
            if ($course->document && Storage::exists($course->document->path)) {
                Storage::delete($course->document->path);
            }
            $result = Storage::put($path, file_get_contents($file->getRealPath()));

            if (!$result) {
                throw new Exception('Document was not saved!');
            }

            if ($course->document) {
                $document = $course->document->update(['path' => $path]);
            } else {
                $document = new Document(['path' => $path]);
                $document->course()->associate($course);
                $document->save();
            }
        } catch (Exception $e) {
            Log::error('Error to DocumentService method upload');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            return false;
        }

        return $document;
    }
}
