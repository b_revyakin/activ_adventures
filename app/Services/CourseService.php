<?php

namespace App\Services;

use App\Entities\Course;
use App\Entities\EmergencyContact;
use App\Entities\Season;
use App\Entities\Staff;
use App\Entities\Venue;
use Carbon\Carbon;
use DB;
use Excel;
use Exception;
use Log;

class CourseService
{

    /**
     * @var CoursePartService
     */
    protected $coursePartService;

    /**
     * CourseService constructor.
     * @param CoursePartService $coursePartService
     */
    public function __construct(CoursePartService $coursePartService)
    {
        $this->coursePartService = $coursePartService;
    }

    /**
     * Create new Course
     *
     * @param Staff $staff
     * @param array $data
     * @return Course
     */
    public function createSimple(Staff $staff, array $data)
    {

        try {
            DB::beginTransaction();

            $course = $this->create($staff, $data);

            $coursePart = $this->coursePartService->create($course, array_except($data, ['name']));

            if (!$coursePart) {
                throw new Exception('Course Part Not Created');
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $course;
    }

    /**
     * @param Staff $staff
     * @param array $data
     * @return Course|bool
     */
    public function create(Staff $staff, array $data)
    {
        try {
            $course = new Course($data);
            $course->season()->associate(Season::findOrFail($data['season_id']));
            $course->staff()->associate($staff);

            if ($data['venue_id']) {
                $course->venue()->associate(Venue::findOrFail($data['venue_id']));
            }
            $course->save();

            $course->schools()->sync($data['schools']);
            if (key_exists('school_groups', $data)) {
                $course->schoolGroups()->sync($data['school_groups']);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $course;
    }

    /**
     * Update course's info
     *
     * @param $id
     * @param array $data
     * @return Course
     */
    public function update($id, array $data)
    {
        DB::beginTransaction();

        try {
            $course = Course::findOrFail($id);

            if ($data['venue_id'] && $course->venue_id != $data['venue_id']) {
                $venue = Venue::findOrFail($data['venue_id']);
                $course->venue()->associate($venue);
            }

            if ($course->season_id != $data['season_id']) {
                $season = Season::findOrFail($data['season_id']);
                $course->season()->associate($season);
            }

            $course->schools()->sync($data['schools']);

            if (key_exists('school_groups', $data)) {
                $course->schoolGroups()->sync($data['school_groups']);
            }

            $course->update($data);

            if ($data['type'] !== $course->type) {
                $course->parts()->delete();

                if ($data['type'] === 'Single') {
                    $coursePart = $this->coursePartService->create($course, array_except($data, ['name']));

                    if (!$coursePart) {
                        throw new Exception('Course Part Not Created');
                    }
                }
            } else {
                if ($data['type'] === 'Single') {
                    $coursePart = $this->coursePartService->update($course->parts->first()->id, array_except($data, ['name']));

                    if (!$coursePart) {
                        throw new Exception('Course Part Not Updated');
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $course;
    }

    public function copy(Course $course, array $data)
    {
        try {
            DB::beginTransaction();

            $newCourse = $course->replicate();
            $newCourse->name = $data['name'];
            $newCourse->save();

            $newCourse->schools()->saveMany($course->schools);
            $newCourse->schoolGroups()->saveMany($course->schoolGroups);

            foreach ($course->parts as $part) {
                $newPart = $part->replicate();
                $newPart->course()->associate($newCourse);
                $newPart->save();

                foreach ($part->categoryFaqs as $faq) {
                    $newCategoryFaq = $faq->replicate();
                    $newCategoryFaq->coursePart()->associate($newPart);
                    $newCategoryFaq->save();

                    foreach ($faq->questions as $question) {
                        $newQuestion = $question->replicate();
                        $newQuestion->category()->associate($newCategoryFaq);
                        $newQuestion->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $newCourse;
    }

    /**
     * @param Course $course
     * @param $withEmergencyContacts
     * @return mixed
     */
    public function exportRegister(Course $course, $withEmergencyContacts)
    {
        $orders = $course->getOrders()->sortBy('child.last_name');

        $fileName = $this->getFileName('Medical' . ($withEmergencyContacts ? ' and Contacts ' : ' ') . 'Register');

        return Excel::create($fileName, function ($excel) use ($course, $withEmergencyContacts, $orders, $fileName) {
            /** @noinspection PhpUndefinedMethodInspection */
            $excel->sheet($fileName, function ($sheet) use ($course, $withEmergencyContacts, $orders) {
                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->setOrientation('landscape')->setAllBorders('thin');
                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->setWidth('G', 120);
                $row = 1;

                $headers = [
                    $course->name, null, null, null, null, null,
                    'Medical',
                ];

                if ($withEmergencyContacts) {
                    $headers = array_merge($headers, [
                        'Main Contact', null, null, null, null,
                        'Second Contact', null, null, null, null,
                    ]);
                }

                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->mergeCells('A1:F1');

                if ($withEmergencyContacts) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $sheet->mergeCells('H1:L1');
                    /** @noinspection PhpUndefinedMethodInspection */
                    $sheet->mergeCells('M1:Q1');
                }

                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->row($row++, $headers);

                $headers = [
                    'Surname',
                    'Forename',
                    'm/f',
                    'D.O.B.',
                    'Participant mobile',
                    'Participant email',
                    'Medical Details',
                ];

                if ($withEmergencyContacts) {
                    $headers = array_merge($headers, [
                        'Surname',
                        'Forename',
                        'Relationship',
                        'Mobile',
                        'Alt number',
                        'Surname',
                        'Forename',
                        'Relationship',
                        'Mobile',
                        'Alt number',
                    ]);
                }

                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->row($row++, $headers);

                foreach ($orders as $order) {
                    $values = [
                        $order->child->last_name,
                        $order->child->first_name,
                        $order->child->sex_string,
                        $order->child->birthday->format('d/m/Y'),
                        $order->child->mobile,
                        $order->child->email,
                        $order->child->getQuestionsForExport(),
                    ];

                    if ($withEmergencyContacts) {
                        $values = array_merge(
                            $values,
                            $this->prepareContact($order->child->mainContact()),
                            $this->prepareContact($order->child->secondContact())
                        );
                    }

                    /** @noinspection PhpUndefinedMethodInspection */
                    $sheet->row($row++, $values);
                }
                $range = $withEmergencyContacts ? 'A1:Q' . $sheet->getHighestRow() : 'A1:G' . $sheet->getHighestRow();
                $sheet->cells($range, function ($cells) {
                    $cells->setValignment('center');
                });
                $sheet->getStyle($range)->getAlignment()->setWrapText(true);
            });
        });
    }

    /**
     * @param Course $course
     * @param null $teamName
     * @return mixed
     */
    public function exportRegisterB(Course $course, $teamName = null)
    {
        $orders = $course->getOrders($teamName)->sortBy('child.last_name');
        $fileName = $this->getFileName('Event Register');

        return Excel::create($fileName, function ($excel) use ($course, $orders, $fileName) {
            /** @noinspection PhpUndefinedMethodInspection */
            $excel->sheet($fileName, function ($sheet) use ($course, $orders) {
                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->setOrientation('landscape')->setAllBorders('thin');
                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->setWidth('G', 20);
                $row = 1;
                $headers = [
                    'Surname',
                    'Forename',
                    'm/f',
                    'D.O.B.',
                    'School Name',
                    'Team',
                    '',
                ];

                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->row($row++, $headers);

                foreach ($orders as $order) {
                    $values = [
                        $order->child->last_name,
                        $order->child->first_name,
                        $order->child->sex_string,
                        $order->child->birthday->format('d/m/Y'),
                        $order->child->school->name,
                        ($order->coursePartTeam ? $order->coursePartTeam->name : ''),
                        '',
                    ];

                    /** @noinspection PhpUndefinedMethodInspection */
                    $sheet->row($row++, $values);
                }
            });
        });
    }

    /**
     * @param Course $course
     * @return mixed
     */
    public function exportPaymentRegister(Course $course)
    {
        $orders = $course->getOrders()->sortBy('child.last_name');
        $fileName = $this->getFileName('Payment Register');

        return Excel::create($fileName, function ($excel) use ($course, $orders, $fileName) {
            /** @noinspection PhpUndefinedMethodInspection */
            $excel->sheet($fileName, function ($sheet) use ($course, $orders) {
                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->setOrientation('landscape')->setAllBorders('thin');
                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->setWidth('J', 25);

                $row = 1;
                $headers = [
                    'Surname',
                    'Forename',
                    'm/f',
                    'D.O.B.',
                    'Booking Date',
                    'Price',
                    'Paid',
                    'Left to Pay',
                    'Payment Status',
                    '',
                ];

                /** @noinspection PhpUndefinedMethodInspection */
                $sheet->row($row++, $headers);

                foreach ($orders as $order) {
                    $values = [
                        $order->child->last_name,
                        $order->child->first_name,
                        $order->child->sex_string,
                        $order->child->birthday->format('d/m/Y'),
                        $order->booking->created_at->format('d/m/Y H:i:s'),
                        $order->booking->getPrice(),
                        $order->booking->getPaidSum(),
                        $order->booking->getLeftPay(),
                        $order->booking->showStatus(),
                        '',
                    ];

                    /** @noinspection PhpUndefinedMethodInspection */
                    $sheet->row($row++, $values);
                }
            });
        });
    }

    /**
     * @param Course $courseSource
     * @param Course $courseReceiver
     * @param bool $force
     * @return bool
     */
    public function copyFaqs(Course $courseSource, Course $courseReceiver)
    {
        try {
            DB::beginTransaction();

            $sourceFaqs = $courseSource->parts->first()->categoryFaqs;

            foreach ($courseReceiver->parts as $part) {
                foreach ($sourceFaqs as $faq) {
                    $newCategoryFaq = $faq->replicate();
                    $newCategoryFaq->coursePart()->associate($part);
                    $newCategoryFaq->save();

                    foreach ($faq->questions as $question) {
                        $newQuestion = $question->replicate();
                        $newQuestion->category()->associate($newCategoryFaq);
                        $newQuestion->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return true;
    }

    /**
     * @param $reportName
     * @return string
     */
    protected function getFileName($reportName)
    {
        return substr($reportName . ' ' . Carbon::now()->format('d.m.Y'), 0, 30);
    }

    /**
     * @param EmergencyContact $contact
     * @return array
     */
    protected function prepareContact(EmergencyContact $contact = null)
    {
        if (!$contact) {
            return ['', '', '', '', ''];
        }

        return [
            $contact->surname,
            $contact->forename,
            $contact->relationship,
            $contact->mobile,
            $contact->alternative_number,
        ];
    }
}
