<?php

namespace App\Services;

use App\Entities\Booking;
use App\Entities\Payment;
use Exception;
use Log;
use Stripe\Charge;
use Stripe\Stripe;

class StripeService
{
    /**
     * StripeService constructor.
     */
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    /**
     * @param Booking $booking
     * @param array $data
     * @return array|Charge
     */
    public function executeWithStripe(Booking $booking, array $data)
    {
        try {
            $data['amount'] = $booking->left_amount * ($data['amount'] ? 0.5 : 1);

            $charge = Charge::create([
                'amount' => $data['amount'] * 100,
                'currency' => 'gbp',
                'description' => $booking->customer->user->email,
                'card' => $data['stripe_token']
            ]);

        } catch (Exception $e) {
            Log::error("Error when customer {$booking->customer->id} try pay booking {$booking->id}.");
            Log::error($e->getMessage());

            return ['status' => 'fail', 'message' => $e->getMessage()];
        }

        return $charge;
    }

    /**
     * @param Payment $payment
     * @return bool|Charge
     */
    public function refund(Payment $payment)
    {
        try {
            $charge = Charge::retrieve($payment->transaction_id)->refund();

            if ($charge->status === 'succeeded') {
                $payment->setRefund();
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());

            return false;
        }

        return $charge;
    }
}
