<?php

namespace App\Services;

use App\Entities\User;
use DB;
use Exception;
use Intervention\Image\Image;
use Log;

class UserService
{
    /**
     * @var string Relation path to avatars folders
     */
    protected $avatarsPath;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->avatarsPath = '/avatars/';
    }

    /**
     * Update user's avatar
     *
     * @param User $user
     * @param Image $image
     * @return User
     */
    public function changeAvatar(User $user, Image $image)
    {
        try {
            $path = $this->avatarsPath . $user->id . '.jpg';

            $image->crop(500, 500)->save(public_path($path));

            $user->avatar = $path;
            $user->save();
        } catch (Exception $e) {
            Log::error('Error by changing avatar', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $user;
    }

    /**
     * Create a new user
     *
     * @param $data
     * @return User
     * @throws Exception
     */
    public function create($data)
    {
        DB::beginTransaction();

        try {
            $user = User::create(array_only($data, ['email', 'password']));

            $this->updatePassword($user, $data['password']);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error('Error by creating new user', ['trace' => $e->getTraceAsString()]);

            throw $e;//up to next level
        }

        return $user;
    }

    /**
     * Update user's password
     *
     * @param User $user
     * @param $password
     * @return User
     */
    public function updatePassword(User $user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();

        return $user;
    }
}