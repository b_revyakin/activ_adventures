<?php

namespace App\Services;


use App\Entities\Product;
use Exception;
use Log;

class ProductService
{
    /**
     * Create new product
     *
     * @param array $data
     * @return bool|Product
     */
    public function create(array $data)
    {
        try {
            $product = Product::create($data);
        } catch (Exception $e) {
            Log::error('Error by creating new product', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $product;
    }

    public function update(Product $product, array $data)
    {
        try {
            $product->update($data);
        } catch (Exception $e) {
            Log::error('Error by updating product', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $product;
    }
}
