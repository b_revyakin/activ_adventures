<?php

namespace App\Services;

use App\Entities\CategoryFaq;
use DB;
use Exception;
use Log;

class FaqService
{
    /**
     * @param array $data
     * @return bool
     */
    public function switchCategoryOrder(array $data)
    {
        try {
            DB::beginTransaction();

            foreach ($data as $level => $categoryId) {
                $category = CategoryFaq::findOrFail($categoryId);
                $category->setLevel($level);
            }

            DB::commit();
        } catch (Exception $e) {
            Log::error('Error in FaqService method switchCategory');
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return true;
    }

    /**
     * @param CategoryFaq $category
     * @param array $data
     * @return bool
     */
    public function switchQuestionOrder(CategoryFaq $category, array $data)
    {
        try {
            DB::beginTransaction();

            foreach ($data as $level => $questionId) {
                $question = $category->questions()->findOrFail($questionId);
                $question->setLevel($level);
            }

            DB::commit();
        } catch (Exception $e) {
            Log::error('Error in FaqService method switchQuestionOrder');
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return true;
    }
}
