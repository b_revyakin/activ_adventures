<?php

namespace App\Services;

use App\Entities\Category;
use App\Entities\Child;
use App\Entities\Order;
use App\Entities\Question;
use App\Entities\QuestionOrder;
use DB;
use Exception;
use Log;

class QuestionService
{
    /**
     * @param array $question
     * @param Order $order
     */
    public function save(array $data, Order $order)
    {
        $child = Child::with('questions')->findOrFail($order->child_id);

        $answers = $data['answers'];
        $texts = $data['texts'];

        if (empty($answers) || empty($texts)) {
            return false;
        }

        $questions = $child->questions;
        foreach ($questions as $question) {
            $orderQuestion = new QuestionOrder;

            if (in_array($question->id, $answers)) {
                $orderQuestion->answer = 1;
                $orderQuestion->text = $texts[$question->id];
            } else {
                $orderQuestion->answer = 0;
            }

            $orderQuestion->question()->associate(Question::findOrFail($question->id));
            $orderQuestion->order()->associate($order);

            $orderQuestion->save();
        }

        return true;
    }

    /**
     * @param Category $category
     * @param array $data
     * @return bool
     */
    public function switchOrder(Category $category, array $data)
    {
        try {
            DB::beginTransaction();

            foreach ($data as $level => $questionId) {
                $question = $category->questions()->findOrFail($questionId);
                $question->setLevel($level);
            }

            DB::commit();
        } catch (Exception $e) {
            Log::error('Error in QuestionService method switchOrder');
            Log::error($e->getMessage());

            DB::rollBack();

            return false;
        }

        return true;
    }
}
