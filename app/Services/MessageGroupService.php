<?php
/**
 * Created by PhpStorm.
 * User: Vitaly
 * Date: 13.08.2016
 * Time: 22:53
 */

namespace App\Services;

use App\Entities\Child;
use App\Entities\CoursePart;
use App\Entities\Customer;
use App\Entities\Message;
use App\Entities\MessageFile;
use App\Entities\MessageGroup;
use App\Entities\School;
use App\Jobs\SendEmail;
use App\Jobs\SendSms;
use DB;
use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Collection;
use Log;
use Storage;

class MessageGroupService
{
    use DispatchesJobs;

    protected $messageService;

    /**
     * @var Collection
     */
    protected $messages;

    /**
     * MessageGroupService constructor.
     * @param MessageService $messageService
     */
    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
        $this->messages = new Collection();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        try {

            $messageGroup = MessageGroup::create([
                'text' => $data['text'],
                'mode' => $data['mode'],
                'recipient_mode' => $data['recipient']
            ]);

            if (key_exists('file', $data) && ! empty($data['file'] && $data['file'][0])) {
                $this->saveFiles($messageGroup, $data['file']);
            }

            if ($data['mode'] == Message::MODE_EMAIL) {
                $messageGroup->subject = $data['subject'];
            }

            if (key_exists('course_part_ids', $data) && ! empty($data['course_part_ids'])) {
                foreach ($data['course_part_ids'] as $course_part_id) {
                    $coursePart = CoursePart::findOrFail($course_part_id);
                    $messageGroup->courseParts()->attach($coursePart);
                }
            } elseif (key_exists('school_id', $data) && $data['school_id']) {
                $messageGroup->school()->associate(School::findOrFail($data['school_id']));
            }

            $messageGroup->save();

            switch ($messageGroup->recipient_mode) {
                case MessageGroup::ONLY_CUSTOMER:
                    foreach ($data['customer_ids'] as $customerId) {
                        try {
                            if (key_exists('select_from', $data) && $data['select_from'] && $data['select_from'] === 'children') {
                                $customer = Child::findOrFail($customerId)->parent;
                            } else {
                                $customer = Customer::findOrFail($customerId);
                            }

                            $message = $this->messageService->create($messageGroup, $customer);

                            $this->message($data['mode'], $message, $messageGroup);
                        } catch (Exception $exception) {
                            Log::error("Error during sending email to customer {$customerId}");

                            Log::error($exception->getMessage());
                            Log::error($exception->getTraceAsString());
                        }
                    }
                    break;
                case MessageGroup::CUSTOMER_CHILDREN:
                    foreach ($data['customer_ids'] as $customerId) {
                        try {
                            if (key_exists('select_from', $data) && $data['select_from'] && $data['select_from'] === 'children') {
                                $child = Child::findOrFail($customerId);

                                $message = $this->messageService->createForChild($messageGroup, $child);

                                $this->message($data['mode'], $message, $messageGroup);
                            } else {
                                $customer = Customer::findOrFail($customerId);

                                $this->sendCustomerChildren($data, $customer, $messageGroup);
                            }
                        } catch (Exception $exception) {
                            Log::error("Error during sending email to customer's {$customerId} participants");

                            Log::error($exception->getMessage());
                            Log::error($exception->getTraceAsString());
                        }
                    }
                    break;
                case MessageGroup::CUSTOMERS_AND_CHILDREN:
                    foreach ($data['customer_ids'] as $customerId) {
                        try {
                            if (key_exists('select_from', $data) && $data['select_from'] && $data['select_from'] === 'children') {
                                $child = Child::findOrFail($customerId);
                                $customer = Child::findOrFail($customerId)->parent;

                                $message = $this->messageService->create($messageGroup, $customer);
                                $this->message($data['mode'], $message, $messageGroup);

                                $message = $this->messageService->createForChild($messageGroup, $child);
                                $this->message($data['mode'], $message, $messageGroup);
                            } else {
                                $customer = Customer::findOrFail($customerId);

                                $message = $this->messageService->create($messageGroup, $customer);

                                $this->message($data['mode'], $message, $messageGroup);

                                $this->sendCustomerChildren($data, $customer, $messageGroup);
                            }
                        } catch (Exception $exception) {
                            Log::error("Error during sending email to customer {$customerId} & his children");

                            Log::error($exception->getMessage());
                            Log::error($exception->getTraceAsString());
                        }
                    }
                    break;
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            $this->messages = new Collection();

            return false;
        }

        $this->sendMessages();

        return $messageGroup;
    }

    /**
     * @param array $data
     * @param Customer $customer
     * @param MessageGroup $messageGroup
     * @return bool
     */
    protected function sendCustomerChildren(array $data, Customer $customer, MessageGroup $messageGroup)
    {
        $customerChildrenIds = $customer->children->pluck('id')->toArray();

        if ($messageGroup->courseParts->count()) {
            foreach ($messageGroup->courseParts as $part) {
                $children = $part->getChildren()->whereIn('id', $customerChildrenIds);
                $this->sendChildren($data['mode'], $children, $messageGroup);
            }

            return true;
        }

        if ($messageGroup->school) {
            $children = $messageGroup->school->children->whereIn('id', $customerChildrenIds);
            $this->sendChildren($data['mode'], $children, $messageGroup);

            return true;
        }

        $children = $customer->children;
        $this->sendChildren($data['mode'], $children, $messageGroup);

        return true;
    }

    /**
     * @param $mode
     * @param $children
     * @param MessageGroup $messageGroup
     * @return bool
     */
    protected function sendChildren($mode, $children, MessageGroup $messageGroup)
    {
        foreach ($children as $child) {
            $message = $this->messageService->createForChild($messageGroup, $child);
            $this->message($mode, $message, $messageGroup);
        }

        return true;
    }

    /**
     * @param $mode
     * @param Message $message
     * @param MessageGroup $messageGroup
     */
    protected function message($mode, Message $message, MessageGroup $messageGroup)
    {
        $this->messages->push(compact('mode', 'message', 'messageGroup'));
    }

    /**
     * Send saved messages.
     */
    protected function sendMessages()
    {
        foreach ($this->messages as $message) {
            try {
                if ($message['mode'] == Message::MODE_EMAIL) {
                    $this->dispatch(new SendEmail($message['message'], $message['messageGroup']));
                } else {
                    $this->dispatch(new SendSms($message['message'], $message['messageGroup']));
                }
            } catch (Exception $e) {
                Log::error("Error during sending message {$message['message']->id}");

                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }

        $this->messages = new Collection();
    }

    /**
     * Save files for group
     *
     * @param MessageGroup $messageGroup
     * @param array $files
     * @return mixed
     */
    public function saveFiles(MessageGroup $messageGroup, array $files)
    {
        foreach ($files as $file) {
            $messageFile = new MessageFile(['original_name' => $file->getClientOriginalName()]);
            $messageFile->setName($file->getClientOriginalExtension());
            $messageFile->messageGroup()->associate($messageGroup);
            $messageFile->save();

            Storage::put($messageFile->path, file_get_contents($file->getRealPath()));
        }

        return $messageGroup;
    }
}