<?php

namespace App\Services;

use App\Entities\Course;
use App\Entities\CoursePart;
use DB;
use Exception;
use Log;

class CoursePartService
{
    /**
     * Create new course part
     *
     * @param Course $course
     * @param array $data
     * @return CoursePart|bool
     */
    public function create(Course $course, array $data)
    {

        try {
            $data['free_places'] = $data['places'];

            $coursePart = new CoursePart($data);
            $coursePart->course()->associate($course);
            $coursePart->save();

        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error('Error by creating new course part', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $coursePart;
    }

    /**
     * Update course part
     *
     * @param $id
     * @param array $data
     * @return bool|CoursePart
     */
    public function update($partId, array $data)
    {
        try {
            $coursePart = CoursePart::findOrFail($partId);

            $data['free_places'] = $coursePart->free_places + $data['places'] - $coursePart->places;
            $data['required_passport'] = key_exists('required_passport', $data) ? $data['required_passport'] : 0;
            $data['required_swim_option'] = key_exists('required_swim_option', $data) ? $data['required_swim_option'] : 0;
            $data['allow_register'] = key_exists('allow_register', $data) ? $data['allow_register'] : 0;

            $coursePart->update($data);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error('Error by update course part', ['trace' => $e->getTraceAsString()]);

            return false;
        }

        return $coursePart;
    }

    /**
     * @param CoursePart $coursePart
     * @param array $data
     * @return CoursePart|bool
     */
    public function addTeamToOrders(CoursePart $coursePart, array $data)
    {
        try {
            DB::beginTransaction();

            $team = $coursePart->teams()->find($data['team_id']);

            foreach ($data['order_ids'] as $orderId) {
                $order = $coursePart->orders()->find($orderId);

                $order->coursePartTeam()->associate($team);
                $order->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return $coursePart;
    }
}
