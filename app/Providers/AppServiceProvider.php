<?php

namespace App\Providers;

use App\Entities\Customer;
use App\Entities\Season;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        switch ($this->app->environment()) {
            case 'local':
                $this->app->register(
                    \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class
                );
        }

        view()->composer('admin.*', function ($view) {
            $seasons = Season::active()->orderBy('created_at', 'desc')->get();
            $view->with(compact('seasons'));
        });

        view()->composer('admin.*', function ($view) {
            $customersForSelect = Customer::active()->get();
            $view->with(compact('customersForSelect'));
        });

        Blade::directive('datetime', function ($expression) {
            return "<?php echo with{$expression}->format('d-m-Y H:i'); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\ActivAdventures\Modules\Sms', 'App\ActivAdventures\Modules\BulkSms');
    }
}
