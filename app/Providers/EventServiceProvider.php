<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\Bookings\Requested::class => [
            \App\Listeners\BookingListener::class,
        ],

        \App\Events\Bookings\Created::class => [
            \App\Listeners\BookingListener::class,
        ],

        \App\Events\Bookings\Paid::class => [
            \App\Listeners\BookingListener::class,
        ],

        \App\Events\Bookings\Cancel::class => [
            \App\Listeners\BookingListener::class,
        ],

        \App\Events\Bookings\ConfirmedRequestRegister::class => [
            \App\Listeners\BookingListener::class,
        ],

        \App\Events\Bookings\CancelledRequestRegister::class => [
            \App\Listeners\BookingListener::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
