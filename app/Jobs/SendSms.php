<?php

namespace App\Jobs;

use App\Entities\Message;
use App\Entities\MessageGroup;
use App\Services\MessageService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSms extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $message;
    protected $messageGroup;

    protected $messageService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Message $message, MessageGroup $messageGroup)
    {
        $this->message = $message;
        $this->messageGroup = $messageGroup;

        $this->messageService = new MessageService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->messageService->sendSms($this->message, $this->messageGroup);
    }
}
