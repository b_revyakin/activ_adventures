<?php

namespace App\Events\Bookings;

use App\Entities\Booking;

class Created extends Event
{
    /**
     * @var
     */
    public $paymentType;

    /**
     * Create a new event instance.
     *
     * @param Booking $booking
     * @param $paymentType
     */
    public function __construct(Booking $booking, $paymentType = 'online')
    {
        parent::__construct($booking);

        $this->booking = $booking;
        $this->paymentType = $paymentType;
    }
}
