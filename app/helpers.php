<?php

use App\Entities\Course;
use App\Entities\Season;
use App\Entities\Venue;
use Carbon\Carbon;

if (!function_exists('is_venue_and_season_date')) {
    /**
     * Check if the requested date include to events by the venue and the season
     *
     * @param Carbon $date
     * @param Venue $venue
     * @param Season $season
     * @return mixed
     */
    function is_venue_and_season_date(Carbon $date, Venue $venue, Season $season)
    {
        return Course::query()
            ->whereHas('venue', function ($query) use ($venue) {
                $query->where('id', $venue->id);
            })
            ->whereHas('season', function ($query) use ($season) {
                $query->where('id', $season->id);
            })
            ->exists();
    }

}

if (!function_exists('data_format')) {
    /**
     * Get string and return format data string.
     * @param $data
     * @return string
     */
    function data_format($data)
    {
        return Carbon::parse($data)->format('d-m-Y');
    }

}

if (!function_exists('price_currency')) {
    /**
     * Get value price with currency symbol.
     * @param $value
     * @return string
     */
    function price_currency($value)
    {
        return '£ ' . $value;
    }

}
