<?php

namespace App\Console;

use App\Console\Commands\CalculatePriceOrders;
use App\Console\Commands\CopyFaqsToCourses;
use App\Console\Commands\FirstAndLastNameToCapitalOnCustomersAndChildrens;
use App\Console\Commands\MessageUpdateStatus;
use App\Console\Commands\NecessaryCompletePaymentsReminder;
use App\Console\Commands\TrimMessageStatusServiceId;
use App\Entities\MessageGroup;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CalculatePriceOrders::class,
        FirstAndLastNameToCapitalOnCustomersAndChildrens::class,
        MessageUpdateStatus::class,
        NecessaryCompletePaymentsReminder::class,
        CopyFaqsToCourses::class,
        TrimMessageStatusServiceId::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:run --only-db')->daily();

        //$schedule->command('reminder:necessary-complete-payment')->daily();

        $schedule->command('message:update-status')->hourly();
        $schedule->command('message:update-status --recently')->when(function () {
            return MessageGroup::isRecently();
        });
    }
}
