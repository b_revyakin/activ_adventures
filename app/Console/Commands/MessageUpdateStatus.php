<?php

namespace App\Console\Commands;

use App\Entities\Message;
use App\Entities\MessageGroup;
use App\Services\BundleSmsService;
use App\Services\MessageService;
use App\Services\MessageStatusesService;
use DB;
use Illuminate\Console\Command;

class MessageUpdateStatus extends Command
{

    private $bundleSmsService;
    private $messageStatusesService;
    private $messageService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:update-status {--recently}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status for message';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BundleSmsService $bundleSmsService, MessageStatusesService $messageStatusesService, MessageService $messageService)
    {
        parent::__construct();

        $this->bundleSmsService = $bundleSmsService;
        $this->messageStatusesService = $messageStatusesService;
        $this->messageService = $messageService;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('recently') == 1) {
            $messagesGroup = MessageGroup::with(['messages' => function ($query) {
                $query->inTreatmentSms();
                $query->with('group');
            }])->recently()->get();

            foreach ($messagesGroup as $group) {
                $this->checkStatus($group->messages, $group->mode);
            }

        } else {
            $messages = Message::inTreatmentSms()->with('group')->get();
            $this->checkStatus($messages);
        }
    }

    public function checkStatus($messages, $mode = 0)
    {
        foreach ($messages as $message) {
            if ((isset($message->group->mode) && $message->group->mode == Message::MODE_SMS) || $mode == Message::MODE_SMS) {
                $status = $this->bundleSmsService->getStatus(intval($message->service_id));

                if ($status != 0) {
                    $this->messageStatusesService->create($message->service_id, $status);
                    $this->messageService->completed($message);
                }
            } else {
                $this->messageService->completed($message);
            }
        }
    }

}
