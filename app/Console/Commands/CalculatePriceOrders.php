<?php

namespace App\Console\Commands;

use App\Entities\Order;
use App\Services\BookingService;
use Illuminate\Console\Command;

class CalculatePriceOrders extends Command
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate-price-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command calculate price orders if price is zero';

    /**
     * Create a new command instance.
     *
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();
        $this->bookingService = $bookingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::with('dates')->where('price', 0)->get()->toArray();

        foreach ($orders as $i => $order) {
            $orders[$i]['selectedDates'] = array_map(function ($array) {
                return $array['date'];
            }, $order['dates']);
        }

        $orders = $this->bookingService->calculatePrice($orders);

        foreach ($orders as $orderData) {
            $order = Order::findOrFail($orderData['id']);
            $order->price = $orderData['amount'] - $orderData['discount'];
            $order->save();
        }
    }
}
