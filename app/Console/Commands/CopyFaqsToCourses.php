<?php

namespace App\Console\Commands;

use App\Entities\Course;
use App\Services\CourseService;
use Illuminate\Console\Command;

class CopyFaqsToCourses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy-faqs-to-courses 
                                    {id : The ID of the course for getting Faqs}
                                    {--force : Force copy}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command copy Faqs from course with typing id to all courses without faqs.';

    /**
     * @var CourseService
     */
    private $courseService;

    /**
     * Create a new command instance.
     *
     * @param CourseService $courseService
     */
    public function __construct(CourseService $courseService)
    {
        parent::__construct();

        $this->courseService = $courseService;
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $courseSource = Course::findOrFail($this->argument('id'));

        if (!$courseSource->parts->first()->categoryFaqs->count()) {
            $this->error("Course with id = {$courseSource->id} not have Faqs.");

            return false;
        }

        $courses = $this->option('force') ? Course::all() : Course::byNotExistsFaqs()->get();
        $courses->except(['id' => $courseSource->id]);

        foreach ($courses as $course) {
            if (!$this->courseService->copyFaqs($courseSource, $course)) {
                $this->error('Error copy Faqs for course with id = ' . $course->id);
            }
        }

        $this->info('Done');
    }
}
