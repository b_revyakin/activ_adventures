<?php

namespace App\Console\Commands;

use App\Entities\Customer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mail;

class NecessaryCompletePaymentsReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:necessary-complete-payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for remind customers about necessary complete a payment.';

    /**
     * NecessaryCompletePaymentsReminder constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customers = Customer::active()->get();
        foreach ($customers as $customer) {
            foreach ($customer->bookings()->processing()->get() as $booking) {
                foreach ($booking->orders as $order) {
                    if (Carbon::now()->addDays(config('reminder.before_count_days'))->diffInDays($order->coursePart->start_date) === 0) {
                        Mail::send('emails.bookings.remind-complete-payment', ['order' => $order, 'customer' => $customer], function ($m) use ($customer) {
                            $m->to($customer->user->email, $customer->user->name)->subject('Necessary Complete a Payment');
                        });
                    }
                }
            }
        }
    }
}
