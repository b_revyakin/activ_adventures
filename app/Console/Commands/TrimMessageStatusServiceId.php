<?php

namespace App\Console\Commands;

use App\Entities\MessageStatus;
use App\Services\MessageStatusesService;
use Illuminate\Console\Command;

class TrimMessageStatusServiceId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service-id:trimming';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trim all current service id of messages statuses';
    /**
     * @var MessageStatusesService
     */
    private $messageStatusesService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MessageStatusesService $messageStatusesService)
    {
        parent::__construct();

        $this->messageStatusesService = $messageStatusesService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $messageStatuses = MessageStatus::where('service_id', 'LIKE', '<%>')->get();

        foreach ($messageStatuses as $messageStatus) {
            $this->messageStatusesService->updateServiceId($messageStatus);
        }
    }
}
