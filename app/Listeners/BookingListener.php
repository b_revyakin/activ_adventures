<?php

namespace App\Listeners;

use App\Events\Bookings\CancelledRequestRegister;
use App\Events\Bookings\ConfirmedRequestRegister;
use App\Events\Bookings\Created;
use App\Events\Bookings\Event as BookingEvent;
use App\Events\Bookings\Paid;
use App\Events\Bookings\Requested;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class BookingListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingEvent $event
     * @return void
     */
    public function handle(BookingEvent $event)
    {
        $send = true;

        if ($event instanceof Created) {
            $subject = 'Activ Adventures: Booking Details';

            switch ($event->paymentType) {
                case 'online':
                    $view = 'emails.bookings.paing-sagepay';

                    if (!$event->booking->price) {
                        $view = 'emails.bookings.paid';
                    }
                    break;
                default:
                    $send = false;
                    break;
            }
        } elseif ($event instanceof Paid) {
            $view = 'emails.bookings.paid';
            $subject = 'Activ Adventures: Booking Confirmation';
        } elseif ($event instanceof Requested) {
            $view = 'emails.bookings.requested';
            $subject = 'Activ Adventures: Booking Details';
        } elseif ($event instanceof ConfirmedRequestRegister) {
            $view = 'emails.bookings.request-register.confirmed';
            $subject = 'Activ Adventures: Booking Details';
        } elseif ($event instanceof CancelledRequestRegister) {
            $view = 'emails.bookings.request-register.cancelled';
            $subject = 'Activ Adventures: Booking Details';
        } else {
            $send = false;
        }

        if (!$send) {
            return;
        }

        $customer = $event->booking->customer;
        $emails = [
            $customer->user->email,
            config('mail.duplicate-email')
        ];

        Mail::send($view, ['booking' => $event->booking, 'customer' => $customer], function ($m) use ($customer, $subject, $emails) {
            $m->to($emails, $customer->user->name)->subject($subject);
        });
    }
}
