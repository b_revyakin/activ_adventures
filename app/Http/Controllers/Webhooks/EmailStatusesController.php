<?php

namespace App\Http\Controllers\Webhooks;

use App\Entities\MessageStatus;
use App\Http\Controllers\Controller;
use App\Services\MessageStatusesService;
use Exception;
use Illuminate\Http\Request;
use Log;

class EmailStatusesController extends Controller
{
    protected $messageStatusesService;

    public function __construct(MessageStatusesService $messageStatusesService)
    {
        parent::__construct();

        $this->messageStatusesService = $messageStatusesService;
    }

    /**
     * Add status change (to Delivered) message type email
     *
     * @param Request $request
     * @return string
     */
    public function delivered(Request $request)
    {
        $this->messageStatusesService->create($this->fetchMessageId($request), MessageStatus::STATUS_DELIVERED);
    }

    /**
     * Add status change (to Dropped) message type email
     *
     * @param Request $request
     * @return string
     */
    public function dropped(Request $request)
    {
        $this->messageStatusesService->create($this->fetchMessageId($request), MessageStatus::STATUS_DROPPED);
    }

    /**
     * Add status change (to Dropped) message type email
     *
     * @param Request $request
     * @return string
     */
    public function spam(Request $request)
    {
        $this->messageStatusesService->create($this->fetchMessageId($request), MessageStatus::STATUS_SPAM);
    }

    /**
     * Add status change (to Dropped) message type email
     *
     * @param Request $request
     * @return string
     */
    public function clicks(Request $request)
    {
        $this->messageStatusesService->create($this->fetchMessageId($request), MessageStatus::STATUS_CLICKS);
    }

    /**
     * Add status change (to Dropped) message type email
     *
     * @param Request $request
     * @return string
     */
    public function opens(Request $request)
    {
        $this->messageStatusesService->create($this->fetchMessageId($request), MessageStatus::STATUS_OPENS);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    protected function fetchMessageId(Request $request)
    {
        $possibleKeysOfMessageId = ['Message-Id', 'message-id'];

        foreach ($possibleKeysOfMessageId as $messageIdKey) {
            if ($request->has($messageIdKey)) {
                $messageId = $request->get($messageIdKey);
                break;
            }
        }

        if (! isset($messageId)) {
            Log::debug($request->all());

            throw new Exception('Not found message id field of webhook');
        }

        return $messageId;
    }
}