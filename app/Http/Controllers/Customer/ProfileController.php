<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests;
use App\Http\Requests\Customer\Profile\ChangePassword;
use App\Services\UserService;
use Auth;

class ProfileController extends Controller
{
    public function show()
    {
        $customer = $this->customer;

        return view('customer.profile.show', compact('customer'));
    }

    public function getChangePassword()
    {
        return view('customer.profile.change-password');
    }

    public function putChangePassword(UserService $userService, ChangePassword $request)
    {
        $userService->updatePassword(Auth::user(), $request->password);

        $request->session()->flash('success', 'The Password was changed!');

        return redirect()->back();
    }
}
