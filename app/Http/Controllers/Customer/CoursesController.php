<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Course;
use App\Entities\Photo;
use App\Http\Requests\Customer\Course\CourseShow;
use Illuminate\Support\Collection;
use Storage;

class CoursesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $courses = $this->customer
            ->children()
            ->with('school.courses')
            ->get()
            ->pluck('school.courses')
            ->collapse()
            ->unique()->where('active', 1);

        return view('customer.courses.index', compact('courses'));
    }

    /**
     * @param CourseShow $request
     * @param Course $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(CourseShow $request, Course $course)
    {
        $faqsParts = new Collection();

        foreach ($course->parts as $part) {
            $categoriesWithQuestions = $part->categoryFaqs->sortBy('level')->each(function ($item) {
                $item->questions = $item->questions()->orderBy('level')->get();
            });
            $faqsParts->push($categoriesWithQuestions);
        }
        return view('customer.courses.show', compact('course', 'faqsParts'));
    }

    /**
     * @param Course $courses
     * @param Photo $photos
     * @return string
     */
    public function getPhoto(Course $courses, Photo $photos)
    {
        return Storage::get($courses->photos()->findOrFail($photos->id)->path);
    }

    /**
     * @param Course $courses
     * @return mixed
     */
    public function getDocument(Course $courses)
    {
        if ($courses->document && Storage::exists($courses->document->path)) {
            return response()->file($courses->document->path);
        }

        return redirect()->back()->withErrors('Server Error. Document invalid!');
    }
}


