<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Venue;
use App\Http\Requests;
use Storage;

class VenuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $venues = Venue::active()->orderBy('name')->get();

        return view('customer.venues.index', compact('venues'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venue = Venue::active()->findOrFail($id);
        $events = $venue->events()->active()->get();

        return view('customer.venues.show', compact('venue', 'events'));
    }

    /**
     * Get photo of project
     *
     * @param $venueId
     * @param $photoId
     * @return mixed
     */
    public function getPhoto($venueId, $photoId)
    {
        $venue = Venue::findOrFail($venueId);
        $photo = $venue->photos()->findOrFail($photoId);

        return Storage::get($photo->path);
    }
}
