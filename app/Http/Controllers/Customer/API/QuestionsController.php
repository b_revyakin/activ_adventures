<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class QuestionsController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $questionsByCategories = Category::get()->sortBy('level')->each(function ($item) {
            $item->questions = $item->questions()->orderBy('level')->get();
        });

        return $questionsByCategories;
    }
}
