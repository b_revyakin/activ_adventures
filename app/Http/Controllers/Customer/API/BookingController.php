<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Coupon;
use App\Entities\Product;
use App\Events\Bookings\Created;
use App\Events\Bookings\Requested;
use App\Http\Controllers\Customer\Controller;
use App\Http\Requests\Customer\API\Booking\Book;
use App\Http\Requests\Customer\API\Booking\Calculate;
use App\Services\BookingService;

class BookingController extends Controller
{
    /*
     * BookingService
     */
    protected $bookingService;

    protected $paymentService;

    /**
     * BookingController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();

        $this->bookingService = $bookingService;
    }


    /**
     * @param Book $request
     * @return array
     */
    public function book(Book $request)
    {
        $data = $request->all();
        $booking = $this->bookingService->create($this->customer, $data);

        if (!$booking) {
            return response(0, 500);
        }

        if ($booking->requested_register) {
            event(new Requested($booking));

            $response = [
                'redirect' => route('customer::bookings.index'),
                'method' => 'GET',
                'fields' => ''
            ];
        } elseif (!$booking->price) {
            event(new Created($booking, $data['payment_type']));

            $response = [
                'redirect' => route('customer::bookings.index'),
                'method' => 'GET',
                'fields' => ''
            ];

            return response($booking ? $response : 0, $booking ? 200 : 500);
        } elseif ($data['payment_type'] === 'online') {
            event(new Created($booking));

            $response = [
                'redirect' => route('customer::bookings.payment.stripe', [$booking->id, $data['pay_option']]),
                'method' => 'GET',
                'fields' => [
                    'pay_option' => $data['pay_option']
                ]
            ];
        } else {
            event(new Created($booking, $data['payment_type']));

            $response = [
                'redirect' => route('customer::bookings.index'),
                'method' => 'GET',
                'fields' => ''
            ];
        }

        return response($response, 200);
    }

    //Check exists and active requested coupon
    public function checkCoupon($coupon)
    {
        $coupon = Coupon::active()->coupon($coupon)->first();

        return response($coupon ? $coupon->percent : '', $coupon ? 200 : 404);
    }

    //Calculate full cost of orders
    public function calculate(Calculate $request)
    {
        return $this->bookingService->calculatePrice($request->orders);
    }

    public function products()
    {
        return Product::active()->get();
    }
}
