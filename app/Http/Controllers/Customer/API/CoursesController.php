<?php

namespace App\Http\Controllers\Customer\API;

use App\Entities\Category;
use App\Entities\Child;
use App\Entities\CoursePart;
use App\Http\Controllers\Controller;
use App\Services\CourseService;
use Illuminate\Database\Eloquent\Collection;

class CoursesController extends Controller
{
    public $courseService;

    public function __construct(CourseService $courseService)
    {
        parent::__construct();

        $this->courseService = $courseService;
    }

    public function index()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return CoursePart::with(['course', 'course.schools', 'course.schoolGroups', 'checkboxes', 'questions'])->withFreePlaces()->activeCourse()->get();
    }

    public function get(Child $child, CoursePart $coursePart)
    {
        $questionsByCategories = $child->getQuestionsByCategories();

        foreach (Category::all() as $category) {
            $categories[$category->level] = [
                'title' => $category->title,
                'description' => $category->description,
                'level' => $category->level
            ];
        }
        $questionInCategories = new Collection();

        foreach ($questionsByCategories as $category => $questions) {
            $level = Category::find($category)->level;

            $questionInCategories[$level] = [
                'category' => $categories[$level],
                'questions' => $questions->sortBy('level')->flatten(1),
            ];
        }

        $coursePart->course;
        $coursePart->checkboxes;
        $coursePart->questions;
        $coursePart->question_in_categories = $questionInCategories;

        return $coursePart;
    }
}

