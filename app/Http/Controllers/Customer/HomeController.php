<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    public function getDashboard()
    {
        return view('customer.dashboard');
    }
}
