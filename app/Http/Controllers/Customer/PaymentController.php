<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = $this->customer->payments()->orderBy('created_at', 'desc')->get();

        return view('customer.payments.index', compact('payments'));
    }
}
