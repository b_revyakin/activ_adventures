<?php

namespace App\Http\Controllers\Customer;

use App\Entities\Booking;
use App\Entities\PaymentSystem;
use App\Events\Bookings\Cancel as CancelEvent;
use App\Events\Bookings\Paid;
use App\Http\Requests\Customer\Booking\Cancel;
use App\Http\Requests\Customer\Payment\Stripe;
use App\Services\BookingService;
use App\Services\StripeService;

class BookingsController extends Controller
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    protected $paymentService;

    /**
     * BookingsController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();

        $this->bookingService = $bookingService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = $this->customer->bookings()->active()->orderBy('created_at', 'DESC')->get();
        $paymentSystems = PaymentSystem::active()->get();

        return view('customer.bookings.index', compact('bookings', 'paymentSystems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.bookings.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cancel $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cancel $request, $id)
    {
        $booking = Booking::findOrFail($id);

        if ($this->bookingService->cancel($booking)) {
            event(new CancelEvent($booking));

            $request->session()->flash('success', 'The booking was cancelled!');
        } else {
            $request->session()->flash('error', 'Server Error. The booking was not cancelled!');
        }

        return redirect()->route('customer::bookings.index');
    }

    public function getPayment(Booking $bookings, $payOption = null)
    {
        return view('customer.bookings.payment.stripe', compact('bookings', 'payOption'));
    }

    public function payment(StripeService $stripeService, Stripe $request, Booking $bookings)
    {
        $response = $stripeService->executeWithStripe($bookings, $request->all());

        if (!$response || $response['status'] != 'succeeded') {
            $request->session()->flash('error', $response['message'] ?: 'Server Error. The booking was not success executed!');
        } elseif ($response['status'] == 'succeeded') {
            $booking = $this->bookingService->paidWithStripe($bookings, $response->id, $response->amount, $response);

            if ($booking) {
                event(new Paid($booking));
            }

            $request->session()->flash('success', 'Thank you! Your payment has been made.
            You will receive an email confirmation with details of your booking and cancellation charges.
            Please contact us if the details on your confirmation email are not correct.
            For full booking terms and conditions please <a href="http://www.activadventures.com/btc/" target="_blank">click here</a>');
        }

        return redirect()->route('customer::bookings.index');
    }
}
