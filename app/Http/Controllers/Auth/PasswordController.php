<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\AuthRedirectTrait;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords, AuthRedirectTrait {
        AuthRedirectTrait::redirectPath insteadof ResetsPasswords;
        ResetsPasswords::redirectPath as defaultRedirectPath;
    }

    protected $redirectPath;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        $this->setRedirectPaths();
        $this->redirectPath = '/';
        $this->subject = 'Activ Adventures: Forgotten Password';
    }
}
