<?php

namespace App\Http\Controllers\Admin\Schools;

use App\Entities\School;
use App\Entities\SchoolGroup;
use App\Http\Controllers\Admin\Controller as AdminController;
use App\Http\Requests\Admin\School\Group\Request as GroupRequest;
use App\Http\Requests\Admin\School\Group\Save;

class GroupsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @param School $school
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $groups = $school->groups;

        return view('admin.schools.groups.index', compact('school', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param School $school
     * @return \Illuminate\Http\Response
     */
    public function create(School $school)
    {
        return view('admin.schools.groups.create', compact('school'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Save $request
     * @param School $school
     * @return \Illuminate\Http\Response
     */
    public function store(Save $request, School $school)
    {
        if ($school->groups()->save(new SchoolGroup($request->all()))) {
            $request->session()->flash('success', 'The group was created successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The group was not created!');
        }

        /** @noinspection PhpUndefinedFieldInspection */
        return redirect()->route('admin::schools.groups.index', [$school->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param GroupRequest $request
     * @param School $school
     * @param SchoolGroup $group
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupRequest $request, School $school, SchoolGroup $group)
    {
        return view('admin.schools.groups.edit', compact('school', 'group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Save $request
     * @param School $school
     * @param SchoolGroup $group
     * @return \Illuminate\Http\Response
     */
    public function update(Save $request, School $school, SchoolGroup $group)
    {
        if ($group->update($request->all())) {
            $request->session()->flash('success', 'The group was updated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The group was not updated!');
        }

        /** @noinspection PhpUndefinedFieldInspection */
        return redirect()->route('admin::schools.groups.index', [$school->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param GroupRequest $request
     * @param School $school
     * @param SchoolGroup $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupRequest $request, School $school, SchoolGroup $group)
    {
        if ($group->delete()) {
            session()->flash('success', 'The group was deleted successfully.');
        } else {
            session()->flash('error', 'Server Error. The group was not deleted!');
        }

        /** @noinspection PhpUndefinedFieldInspection */
        return redirect()->route('admin::schools.groups.index', [$school->id]);
    }
}
