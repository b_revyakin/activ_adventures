<?php

namespace App\Http\Controllers\Admin\Schools;

use App\Entities\School;
use App\Http\Controllers\Admin\Controller as AdminController;
use App\Http\Requests\Admin\School\Save;
use App\Services\SchoolService;
use Illuminate\Http\Request;

class Controller extends AdminController
{
    /**
     * @var SchoolService;
     */
    protected $schoolService;

    /**
     * SchoolController constructor.
     * @param SchoolService $schoolService
     */
    public function __construct(SchoolService $schoolService)
    {
        parent::__construct();

        $this->schoolService = $schoolService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::all();

        return view('admin.schools.index', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Save $request
     * @return \Illuminate\Http\Response
     */
    public function store(Save $request)
    {
        $school = $this->schoolService->create($request->all());

        if ($school) {
            $request->session()->flash('success', 'The school was created successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The school was not created!');
        }

        return redirect()->route('admin::schools.index');
    }

    /**
     * Display the specified resource.
     *
     * @param School $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        return view('admin.schools.show', compact('school'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param School $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        return view('admin.schools.edit', compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Save $request
     * @param School $school
     * @return \Illuminate\Http\Response
     */
    public function update(Save $request, School $school)
    {
        $school = $this->schoolService->update($school, $request->all());

        if ($school) {
            $request->session()->flash('success', 'The school was updated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The school was not updated!');
        }

        return redirect()->route('admin::schools.index');
    }

    /**
     * Deleted the specified resource in storage.
     *
     * @param Request $request
     * @param School $school
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, School $school)
    {
        $result = $this->schoolService->destroy($school);

        if ($result) {
            $request->session()->flash('success', 'The school was deleted successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The school was not deleted!');
        }

        return redirect()->route('admin::schools.index');
    }
}
