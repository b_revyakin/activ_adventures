<?php

namespace App\Http\Controllers\Admin\Schools;

use App\Entities\School;
use App\Http\Controllers\Admin\Controller as AdminController;
use App\Services\ChildService;
use Illuminate\Http\Request;

class ChildrenController extends AdminController
{
    /**
     * @param School $school
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(School $school)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $children = $school->children->sortByDesc(function ($child) {
            return $child->schoolGroups->count();
        });
        /** @noinspection PhpUndefinedFieldInspection */
        $groups = $school->groups;

        return view('admin.schools.children.index', compact('school', 'children', 'groups'));
    }

    /**
     * @param Request $request
     * @param ChildService $childService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, ChildService $childService)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $response = $childService->saveSchoolGroups($request->groups);

        return response('', $response ? 200 : 500);
    }
}
