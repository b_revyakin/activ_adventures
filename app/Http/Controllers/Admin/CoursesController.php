<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Child;
use App\Entities\Course;
use App\Entities\CoursePart;
use App\Entities\Photo;
use App\Entities\School;
use App\Entities\SchoolGroup;
use App\Entities\Season;
use App\Entities\Venue;
use App\Http\Requests\Admin\Courses\Activate;
use App\Http\Requests\Admin\Courses\Copy;
use App\Http\Requests\Admin\Courses\CourseParts\Request as CoursePartRequest;
use App\Http\Requests\Admin\Courses\CourseParts\Save;
use App\Http\Requests\Admin\Courses\CourseParts\Teams\AddOrders;
use App\Http\Requests\Admin\Courses\CourseParts\Update as UpdatePart;
use App\Http\Requests\Admin\Courses\Deactivate;
use App\Http\Requests\Admin\Courses\Report;
use App\Http\Requests\Admin\Courses\Store;
use App\Http\Requests\Admin\Courses\Update;
use App\Services\CoursePartService;
use App\Services\CourseService;
use App\Services\DocumentService;
use App\Services\PhotoService;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PDF;
use Storage;

class CoursesController extends Controller
{
    /**
     * @var PhotoService
     */
    protected $photoService;

    /**
     * @var CourseService
     */
    protected $courseService;

    /**
     * @var CoursePartService
     */
    protected $coursePartService;

    /**
     * @var DocumentService
     */
    protected $documentService;

    /**
     * CoursesController constructor.
     * @param CourseService $courseService
     * @param PhotoService $photoService
     * @param CoursePartService $coursePartService
     * @param DocumentService $documentService
     */
    public function __construct(CourseService $courseService,
                                PhotoService $photoService,
                                CoursePartService $coursePartService,
                                DocumentService $documentService)
    {
        parent::__construct();

        $this->courseService = $courseService;
        $this->photoService = $photoService;
        $this->coursePartService = $coursePartService;
        $this->documentService = $documentService;


        $this->middleware('permission:course.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:course.show', ['only' => ['show']]);
        $this->middleware('permission:course.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:course.activation', ['only' => ['activate', 'deactivate']]);

    }


    /**
     * Display a listing of the resourse.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $courses = Course::active();

        if ($request->get('only') === 'person') {
            $courses = $courses->where('staff_id', $this->staff->id);
        }

        $courses = $courses->get();

        return view('admin.courses.index', compact('courses'));
    }

    public function archive()
    {
        $courses = Course::notActive()->get();

        return view('admin.courses.archive', compact('courses'));
    }

    public function calendar()
    {
        return view('admin.courses.calendar');
    }

    /**
     * Show the form for creating a new resourse.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $venues = Venue::active()->get();
        $seasons = Season::active()->get();
        $schools = School::all();

        return view('admin.courses.create', compact('venues', 'seasons', 'schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        if ($request->type === 'Multiple') {
            $course = $this->courseService->create($this->staff, $request->all());
        } else {
            $course = $this->courseService->createSimple($this->staff, $request->all());
        }

        if (!$course) {
            return redirect()->back()->withErrors('Server Error. Please contact with administrator!');
        }

        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {
                if ($file) {
                    $this->photoService->upload($course, $file);
                }
            }
        }

        if ($request->file('document')) {
            $this->documentService->upload($course, $request->file('document'));
        }

        if ($request->type === 'Multiple') {
            return redirect()->route('admin::courses.parts.index', [$course->id]);
        }


        return redirect()->route('admin::courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);

        return view('admin.courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $venues = Venue::active()->get();
        $seasons = Season::active()->get();
        $schools = School::all();
        $schoolGroups = SchoolGroup::whereIn('school_id', $course->schools->pluck('id')->toArray())->get();

        return view('admin.courses.edit', compact('course', 'venues', 'seasons', 'schools', 'schoolGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $course = $this->courseService->update($id, $request->all());

        if (!$course) {
            return redirect()->back()
                ->withErrors('Server Error. Please contact with administrator!');
        }

        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {
                if ($file) {
                    $this->photoService->upload($course, $file);
                }
            }
        }

        if ($request->file('document')) {
            $this->documentService->upload($course, $request->file('document'));
        }

        if ($request->type === 'Multiple') {
            return redirect()->route('admin::courses.parts.index', $course->id);
        }

        return redirect()->route('admin::courses.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param Course $courses
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $id
     */
    public function activate(Activate $request, Course $courses)
    {
        $courses->activate();

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param Course $courses
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $id
     */
    public function deactivate(Deactivate $request, Course $courses)
    {
        $courses->deactivate();

        return redirect()->back();
    }

    public function getBookings($id)
    {
        $page = 'bookings';

        $course = Course::findOrFail($id);


        return view('admin.courses.details.bookings', compact('course', 'page'));
    }

    public function getParts($id)
    {
        $page = 'parts';

        $course = Course::findOrFail($id);
        $parts = $course->parts()->get();

        return view('admin.courses.parts.index', compact('course', 'parts', 'page'));
    }

    public function editPart($courseId, $partId, CoursePartRequest $request)
    {
        $course = Course::findOrFail($courseId);
        $part = CoursePart::find($partId);

        return view('admin.courses.parts.edit', compact('course', 'part'));
    }

    public function updatePart($courseId, $partId, UpdatePart $request)
    {
        if ($this->coursePartService->update($partId, $request->except('_token'))) {
            return redirect()->route('admin::courses.parts.index', [$courseId]);
        } else {
            return redirect()->route('admin::courses.index');
        }
    }

    public function createPart($courseId)
    {
        $course = Course::findOrFail($courseId);

        return view('admin.courses.parts.create', compact('course'));
    }

    public function copy(Course $course)
    {
        return view('admin.courses.copy', compact('course'));
    }

    public function copyStore(Course $course, Copy $request)
    {
        $newCourse = $this->courseService->copy($course, $request->all());

        if ($newCourse) {
            $request->session()->flash('success', 'The course' . $course->name . 'was copied!');

            return redirect()->route('admin::courses.show', [$newCourse->id]);
        } else {
            $request->session()->flash('error', 'Server Error. The course was not copied!');

            return redirect()->route('admin::courses.index');
        }
    }

    public function storePart($courseId, Save $request)
    {
        $course = Course::findOrFail($courseId);
        $part = $this->coursePartService->create($course, $request->except('_token'));

        if (!$part) {
            return redirect()->back()->withErrors('Server Error. Please contact with administrator!');
        }

        return redirect()->route('admin::courses.parts.index', [$courseId]);
    }

    public function destroyPart($courseId, $partId, CoursePartRequest $request)
    {
        if (CoursePart::findOrFail($partId)->delete()) {
            $request->session()->flash('success', 'The part was deleted successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The part was not deleted!');
        }

        return redirect()->route('admin::courses.parts.index', [$courseId]);
    }

    public function getRequestedRegisterBookings(Course $course)
    {
        $page = 'register-bookings';
        $bookings = $course->getRequestedRegisterBookings();

        return view('admin.courses.details.requested-register-bookings', compact('course', 'page', 'bookings'));
    }

    public function getRegister(Course $course, $withEmergencyContacts = false)
    {
        $page = $withEmergencyContacts ? 'register-with-emergency-contacts' : 'register';

        return view('admin.courses.details.course-register', compact('course', 'page', 'withEmergencyContacts'));
    }

    public function getRegisterB(Course $course)
    {
        $page = 'register-b';
        $orders = $course->getOrders();
        $teams = $course->parts->first()->teams;

        return view('admin.courses.details.register-b', compact('course', 'page', 'orders', 'teams'));
    }

    public function getPaymentRegister(Course $course)
    {
        $page = 'payment-register';
        $orders = $course->getOrders();

        return view('admin.courses.details.payment-register', compact('course', 'page', 'orders'));
    }

    public function exportRegisterB(Request $request, Course $course, $format)
    {
        $this->courseService->exportRegisterB($course, $request->team)->export($format);
    }

    public function exportRegister(Course $course, $format, $withEmergencyContacts = false)
    {
        $this->courseService->exportRegister($course, $withEmergencyContacts)->export($format);
    }

    public function exportPaymentRegister(Course $course, $format)
    {
        $this->courseService->exportPaymentRegister($course)->export($format);
    }

    public function getAgeBreaking($id)
    {
        $page = 'age-breaking';

        $course = Course::findOrFail($id);

        return view('admin.courses.details.course-age-breaking', compact('course', 'page'));
    }

    public function getReport($id)
    {
        $page = 'report';

        $course = Course::findOrFail($id);

        return view('admin.courses.details.course-report', compact('course', 'page'));
    }

    public function getAllReport(Report $request)
    {
        $courses = Course::active()
            ->with(['venue', 'season', 'schools'])
            ->where('season_id', $request->season_id)
            ->get();

        $schools = School::all();

        return view('admin.courses.report', compact('courses', 'schools'));
    }

    public function exportChildren(Course $course)
    {
        $orders = $course->getOrders();

        $pdf = PDF::loadView('admin.courses.details.children-export', compact('orders'));

        return $pdf->download($course->name . ' - Children Details.pdf');
    }

    /**
     * @param Course $course
     * @param Child $child
     * @return \Illuminate\Http\Response
     */
    public function exportChild(Course $course, Child $child)
    {
        $orders = $course->getOrders()->where('child.id', $child->id);

        $pdf = PDF::loadView('admin.courses.details.children-export', compact('orders'));

        return $pdf->download($course->name . ' - ' . $child->fullname . ' Details.pdf');
    }

    public function exportMailingLists($id)
    {
        $course = Course::findOrFail($id);
        $orders = $course->getActiveOrders();
        $children = new Collection();

        foreach ($orders as $order) {
            $children->push($order->child);
        }

        $children = $children->groupBy(function ($item, $key) {
            return $item->id;
        });


        $children = $children->sortBy(function ($child) {
            return $child[0]->last_name;
        });

        Excel::create($course->name . ' - Mailing Lists', function ($excel) use ($children) {
            $excel->sheet('Registers', function ($sheet) use ($children) {

                $row = 1;

                $sheet->row($row++, [
                    'Child Surname',
                    'Child Forename',
                    'Parent surname',
                    'Parent First name',
                    'Parent email address',
                    'Mobile telephone number',
                    'Address and post code',
                ]);

                foreach ($children as $child) {
                    $child = $child[0];

                    $sheet->row($row++, array(
                        $child->last_name,
                        $child->first_name,
                        $child->parent->last_name,
                        $child->parent->first_name,
                        $child->parent->user->email,
                        $child->parent->mobile,
                        $child->parent->full_address,
                    ));
                }

            });
        })->export('csv');
    }

    /**
     * @param Course $courses
     * @param Photo $photos
     * @return mixed
     */
    public function getPhoto(Course $courses, Photo $photos)
    {
        return Storage::get($courses->photos()->findOrFail($photos->id)->path);
    }

    /**
     * @param Course $courses
     * @param Photo $photos
     * @return mixed
     */
    public function deletePhoto(Course $courses, Photo $photos)
    {
        $courses->photos()->findOrFail($photos->id)->delete();

        return back();
    }

    public function getDocument(Course $courses)
    {
        if ($courses->document && Storage::exists($courses->document->path)) {
            return response()->file($courses->document->path);
        }

        return redirect()->back()->withErrors('Server Error. Document invalid!');
    }

    /**
     * @param AddOrders $request
     * @param CoursePart $coursePart
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addTeamToOrders(AddOrders $request, CoursePart $coursePart)
    {
        if ($this->coursePartService->addTeamToOrders($coursePart, $request->all())) {
            session()->flash('success', 'Team Was Added To Orders!');
        } else {
            session()->flash('error', 'Server Error. Team Was Not Added To Orders!');
        }

        return response('');
    }
}
