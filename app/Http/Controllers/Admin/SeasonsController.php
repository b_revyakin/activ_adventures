<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Season;
use App\Http\Requests;
use App\Http\Requests\Admin\Seasons\Activate;
use App\Http\Requests\Admin\Seasons\Deactivate;
use App\Http\Requests\Admin\Seasons\Store;
use App\Http\Requests\Admin\Seasons\Update;
use App\Services\SeasonService;

class SeasonsController extends Controller
{
    protected $seasonService;

    public function __construct(SeasonService $seasonService)
    {
        $this->seasonService = $seasonService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seasons = Season::all();

        return view('admin.seasons.index', compact('seasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.seasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $season = $this->seasonService->create($request->all());

        if ($season) {
            $request->session()->flash('success', 'The season was created successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The season was not created!');
        }

        return redirect()->route('admin::seasons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $season = Season::findOrFail($id);

        return view('admin.seasons.show', compact('season'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $season = Season::findOrFail($id);

        return view('admin.seasons.edit', compact('season'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $season = Season::findOrFail($id);

        $season = $this->seasonService->update($season, $request->all());

        if ($season) {
            $request->session()->flash('success', 'The season was updated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The season was not updated!');
        }

        return redirect()->route('admin::seasons.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $season = Season::findOrFail($id);

        if ($season->activate()) {
            $request->session()->flash('success', 'The season was activated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not activated!');
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $season = Season::findOrFail($id);

        if ($season->deactivate()) {
            $request->session()->flash('success', 'The season was deactivated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not deactivated!');
        }

        return redirect()->back();
    }
}
