<?php

namespace App\Http\Controllers\Admin;

class MessagesController extends Controller
{
    public function index()
    {
        return view('admin.messages.index');
    }
}
