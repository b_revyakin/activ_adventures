<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Checkbox;
use App\Entities\CoursePart;
use App\Http\Requests\Admin\Courses\CourseParts\Checkboxes\Request;
use App\Http\Requests\Admin\Courses\CourseParts\Checkboxes\Save;

class CheckboxesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function index(CoursePart $coursePart)
    {
        $checkboxes = $coursePart->checkboxes;

        return view('admin.courses.checkboxes.index', compact('checkboxes', 'coursePart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function create(CoursePart $coursePart)
    {
        return view('admin.courses.checkboxes.create', compact('coursePart'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Save|Request $request
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function store(Save $request, CoursePart $coursePart)
    {
        if ($coursePart->checkboxes()->save(new Checkbox($request->all()))) {
            $request->session()->flash('success', 'Checkbox Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Checkbox Was Not Created!');
        }

        return redirect()->route('admin::course-parts.checkboxes.index', [$coursePart->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param CoursePart $coursePart
     * @param Checkbox $checkbox
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Request $request, CoursePart $coursePart, Checkbox $checkbox)
    {
        return view('admin.courses.checkboxes.edit', compact('checkbox', 'coursePart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Save|Request $request
     * @param CoursePart $coursePart
     * @param Checkbox $checkbox
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Save $request, CoursePart $coursePart, Checkbox $checkbox)
    {
        if ($checkbox->update($request->all())) {
            $request->session()->flash('success', 'Checkbox Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Checkbox Was Not Updated!');
        }

        return redirect()->route('admin::course-parts.checkboxes.index', [$coursePart->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param CoursePart $coursePart
     * @param Checkbox $checkbox
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(Request $request, CoursePart $coursePart, Checkbox $checkbox)
    {
        if ($checkbox->delete()) {
            session()->flash('success', 'Checkbox Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Checkbox Was Not Deleted!');
        }

        return redirect()->route('admin::course-parts.checkboxes.index', [$coursePart->id]);
    }
}
