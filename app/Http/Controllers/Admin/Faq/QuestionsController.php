<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Entities\CategoryFaq;
use App\Entities\FaqQuestion;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\Faq\Question\Request;
use App\Http\Requests\Admin\Faq\Question\Save;
use App\Services\FaqService;
use Illuminate\Http\Request as BaseRequest;

class QuestionsController extends Controller
{
    public function index(CategoryFaq $category)
    {
        $questions = $category->questions()->orderBy('level')->get();

        return view('admin.faq.categories.questions.index', compact('questions', 'category'));
    }

    public function create(CategoryFaq $category)
    {
        return view('admin.faq.categories.questions.create', compact('category'));
    }

    public function store(Save $request, CategoryFaq $category)
    {
        if ($category->questions()->save(new FaqQuestion($request->all()))) {
            $request->session()->flash('success', 'Question Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Created!');
        }

        return redirect()->route('admin::faq.categories.questions.index', [$category->id]);
    }

    public function edit(Request $request, CategoryFaq $category, FaqQuestion $question)
    {
        return view('admin.faq.categories.questions.edit', compact('category', 'question'));
    }

    public function update(Save $request, CategoryFaq $category, FaqQuestion $question)
    {
        if ($question->update($request->all())) {
            $request->session()->flash('success', 'Question Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Updated!');
        }

        return redirect()->route('admin::faq.categories.questions.index', [$category->id]);
    }

    public function destroy(Request $request, CategoryFaq $category, FaqQuestion $question)
    {
        if ($question->delete()) {
            session()->flash('success', 'Question Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Question Was Not Deleted!');
        }

        return back();
    }

    public function switchOrder(FaqService $faqService, BaseRequest $request, CategoryFaq $category)
    {
        \Log::info($category);
        $response = $faqService->switchQuestionOrder($category, $request->items);

        return response('', $response ? 200 : 500);
    }
}
