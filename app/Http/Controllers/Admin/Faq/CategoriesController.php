<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Entities\CategoryFaq;
use App\Entities\CoursePart;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Faq\Category as CategoryRequest;
use App\Services\FaqService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(CoursePart $coursePart)
    {
        $categories = $coursePart->categoryFaqs()->orderBy('level')->get();

        return view('admin.faq.categories.index', compact('categories', 'coursePart'));
    }

    public function create(CoursePart $coursePart)
    {
        return view('admin.faq.categories.create', compact('coursePart'));
    }

    public function store(CategoryRequest $request, CoursePart $coursePart)
    {
        if ($coursePart->categoryFaqs()->save(new CategoryFaq($request->all()))) {
            $request->session()->flash('success', 'Category Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Category Was Not Created!');
        }

        return redirect()->route('admin::faq.course-parts.categories.index', [$coursePart->id]);
    }

    public function edit(CoursePart $coursePart, CategoryFaq $category)
    {
        return view('admin.faq.categories.edit', compact('category', 'coursePart'));
    }

    public function update(CategoryRequest $request, CoursePart $coursePart, CategoryFaq $category)
    {
        if ($category->update($request->all())) {
            $request->session()->flash('success', 'Category Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Category Was Not Updated!');
        }

        return redirect()->route('admin::faq.course-parts.categories.index', [$coursePart->id]);
    }

    public function destroy(CoursePart $coursePart, CategoryFaq $category)
    {
        if ($category->delete()) {
            session()->flash('success', 'Category Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Category Was Not Deleted!');
        }

        return back();
    }

    public function switchOrder(FaqService $faqService, Request $request)
    {
        $response = $faqService->switchCategoryOrder($request->items);

        return response('', $response ? 200 : 500);
    }
}
