<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    public function getDashboard()
    {
        return view('admin.dashboard');
    }
}
