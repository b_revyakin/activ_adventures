<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Product;
use App\Http\Requests;
use App\Http\Requests\Admin\Products\Activate;
use App\Http\Requests\Admin\Products\Deactivate;
use App\Http\Requests\Admin\Products\Store;
use App\Http\Requests\Admin\Products\Update;
use App\Services\ProductService;

class ProductsController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * ProductsController constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $product = $this->productService->create($request->all());

        if ($product) {
            $request->session()->flash('success', 'The product was created successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not created!');
        }

        return redirect()->route('admin::products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $product = Product::findOrFail($id);

        $product = $this->productService->update($product, $request->all());

        if ($product) {
            $request->session()->flash('success', 'The product was updated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not updated!');
        }

        return redirect()->route('admin::products.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $product = Product::findOrFail($id);

        if ($product->activate()) {
            $request->session()->flash('success', 'The product was activated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not activated!');
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $product = Product::findOrFail($id);

        if ($product->deactivate()) {
            $request->session()->flash('success', 'The product was deactivated successfully.');
        } else {
            $request->session()->flash('error', 'Server Error. The product was not deactivated!');
        }

        return redirect()->back();
    }
}
