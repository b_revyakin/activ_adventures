<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Entities\Question;
use App\Http\Requests;
use App\Http\Requests\Admin\Category\Question\Request;
use App\Http\Requests\Admin\Category\Question\Store;
use App\Http\Requests\Admin\Category\Question\Update;
use App\Services\QuestionService;
use Illuminate\Http\Request as BaseRequest;

class QuestionsController extends Controller
{
    public function index(Category $categories)
    {
        $questions = $categories->questions()->orderBy('level')->get();

        return view('admin.categories.questions.index', compact('questions', 'categories'));
    }

    public function create(Category $categories)
    {
        return view('admin.categories.questions.create', compact('categories'));
    }

    public function store(Store $request, Category $categories)
    {
        if ($categories->questions()->save(new Question($request->all()))) {
            $request->session()->flash('success', 'Question Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Created!');
        }

        return redirect()->route('admin::categories.questions.index', $categories->id);
    }

    public function edit(Request $request, Category $categories, Question $questions)
    {
        return view('admin.categories.questions.edit', compact('categories', 'questions'));
    }

    public function update(Update $request, Category $categories, Question $questions)
    {
        if ($questions->update($request->all())) {
            $request->session()->flash('success', 'Question Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Updated!');
        }

        return redirect()->route('admin::categories.questions.index', $categories->id);
    }

    public function destroy(Request $request, Category $categories, Question $questions)
    {
        if ($questions->delete()) {
            session()->flash('success', 'Question Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Question Was Not Deleted!');
        }

        return back();
    }

    public function switchOrder(QuestionService $questionService, BaseRequest $request, Category $categories)
    {
        $response = $questionService->switchOrder($categories, $request->items);

        return response('', $response ? 200 : 500);
    }
}
