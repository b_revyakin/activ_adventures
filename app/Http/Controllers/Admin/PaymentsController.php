<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Booking;
use App\Entities\Payment;
use App\Http\Requests\Admin\Payments\Refund;
use App\Http\Requests\Admin\Payments\Request as PaymentRequest;
use App\Http\Requests\Admin\Payments\Save;
use App\Services\BookingService;
use App\Services\StripeService;

class PaymentsController extends Controller
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    /**
     * PaymentsController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();

        $this->bookingService = $bookingService;
    }

    /**
     * @param Booking $bookings
     * @return \View
     */
    public function index(Booking $bookings)
    {
        $payments = $bookings->payments;

        return view('admin.bookings.payments.index', compact('bookings', 'payments'));
    }

    /**
     * @param Booking $bookings
     * @return \View
     */
    public function create(Booking $bookings)
    {
        return view('admin.bookings.payments.create', compact('bookings'));
    }

    /**
     * @param Save $request
     * @param Booking $bookings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Save $request, Booking $bookings)
    {
        $payment = $this->bookingService->addAdminPayment($bookings, $request->all());

        if ($payment) {
            $request->session()->flash('success', 'Payment Was Added!');
        } else {
            $request->session()->flash('error', 'Server Error. Payment Was Not Added!');
        }

        return redirect()->route('admin::bookings.payments.index', $bookings->id);
    }

    /**
     * @param PaymentRequest $request
     * @param Booking $bookings
     * @param Payment $payments
     * @return
     */
    public function cancel(PaymentRequest $request, Booking $bookings, Payment $payments)
    {
        if ($payments->setCancelled()) {
            session()->flash('success', 'Payment Was Cancelled!');
        } else {
            session()->flash('error', 'Server Error. Payment Was Not Cancelled!');
        }

        return redirect()->route('admin::bookings.payments.index', $bookings->id);
    }

    /**
     * @param PaymentRequest $request
     * @param Booking $bookings
     * @param Payment $payments
     * @return mixed
     */
    public function confirm(PaymentRequest $request, Booking $bookings, Payment $payments)
    {
        if ($this->bookingService->confirmPayment($bookings, $payments)) {
            session()->flash('success', 'Payment Was Confirmed!');
        } else {
            session()->flash('error', 'Server Error. Payment Was Not Confirmed!');
        }

        return redirect()->route('admin::bookings.payments.index', $bookings->id);
    }

    /**
     * @param Refund $request
     * @param StripeService $stripeService
     * @param Booking $bookings
     * @param Payment $payments
     * @return mixed
     */
    public function refund(Refund $request, StripeService $stripeService, Booking $bookings, Payment $payments)
    {
        if ($stripeService->refund($payments)) {
            session()->flash('success', 'Payment Was Refund!');
        } else {
            session()->flash('error', 'Server Error. Payment Was Not Refund!');
        }

        return redirect()->route('admin::bookings.payments.index', $bookings->id);
    }
}
