<?php

namespace App\Http\Controllers\Admin;

use App\Entities\CoursePart;
use App\Entities\CoursePartTeam;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Courses\CourseParts\Teams\Request;
use App\Http\Requests\Admin\Courses\CourseParts\Teams\Save;

class CoursePartTeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function index(CoursePart $coursePart)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $teams = $coursePart->teams;

        return view('admin.courses.parts.teams.index', compact('teams', 'coursePart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function create(CoursePart $coursePart)
    {
        return view('admin.courses.parts.teams.create', compact('coursePart'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Save $request
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function store(Save $request, CoursePart $coursePart)
    {
        if ($coursePart->questions()->save(new CoursePartTeam($request->all()))) {
            $request->session()->flash('success', 'Team Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Team Was Not Created!');
        }

        return redirect()->route('admin::course-parts.teams.index', [$coursePart->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param CoursePart $coursePart
     * @param CoursePartTeam $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, CoursePart $coursePart, CoursePartTeam $team)
    {
        return view('admin.courses.parts.teams.edit', compact('coursePart', 'team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Save $request
     * @param CoursePart $coursePart
     * @param CoursePartTeam $team
     * @return \Illuminate\Http\Response
     */
    public function update(Save $request, CoursePart $coursePart, CoursePartTeam $team)
    {
        if ($team->update($request->all())) {
            $request->session()->flash('success', 'Team Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Team Was Not Updated!');
        }

        return redirect()->route('admin::course-parts.teams.index', [$coursePart->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param CoursePart $coursePart
     * @param CoursePartTeam $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CoursePart $coursePart, CoursePartTeam $team)
    {
        if ($team->delete()) {
            session()->flash('success', 'Team Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Team Was Not Deleted!');
        }

        return redirect()->route('admin::course-parts.teams.index', [$coursePart->id]);
    }
}
