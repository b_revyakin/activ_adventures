<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Entities\Child;
use App\Entities\Question;
use Exception;
use Illuminate\Http\Request;

class QuestionnairesController extends Controller
{
    public function index(Child $child)
    {
        $questionsByCategories = $child->getQuestionsByCategories();

        foreach (Category::all() as $category) {
            $categories[$category->id] = [
                'title' => $category->title,
                'description' => $category->description
            ];
        }

        return view('admin.parents.children.questionnaires.index', compact('child', 'questionsByCategories', 'categories'));
    }

    public function create(Child $child)
    {
        $selectQuestionIds = [];
        foreach ($child->questions as $question) {
            $selectQuestionIds[] = $question->pivot->question_id;
        }

        $categories = Category::with('questions')->get();
        return view('admin.parents.children.questionnaires.create', compact('child', 'categories', 'selectQuestionIds'));
    }

    public function store(Request $request, Child $child)
    {
        try {
            $child->questions()->detach();

            foreach ($request->question_ids as $questionId) {
                $child->questions()->attach($questionId);
            }

            $request->session()->flash('success', 'Question Was Added To ' . $child->name);
        } catch (Exception $e) {
            $request->session()->flash('error', 'Server Error. Question Was Not Added!');
        }

        return redirect()->route('admin::children.questions.index', $child->id);
    }

    public function destroy(Request $request, Child $child, Question $questions)
    {
        if ($child->questions()->detach($questions)) {
            $request->session()->flash('success', 'Question Was Deleted From' . $child->name);
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Deleted!');
        }

        return redirect()->route('admin::children.questions.index', $child->id);
    }
}
