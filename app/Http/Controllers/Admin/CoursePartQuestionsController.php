<?php

namespace App\Http\Controllers\Admin;

use App\Entities\CoursePart;
use App\Entities\CoursePartQuestion;
use App\Http\Requests\Admin\Courses\CourseParts\Questions\Request;
use App\Http\Requests\Admin\Courses\CourseParts\Questions\Save;

class CoursePartQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function index(CoursePart $coursePart)
    {
        $questions = $coursePart->questions;

        return view('admin.courses.parts.questions.index', compact('coursePart', 'questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function create(CoursePart $coursePart)
    {
        return view('admin.courses.parts.questions.create', compact('coursePart'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Save|Request $request
     * @param CoursePart $coursePart
     * @return \Illuminate\Http\Response
     */
    public function store(Save $request, CoursePart $coursePart)
    {
        if ($coursePart->questions()->save(new CoursePartQuestion($request->all()))) {
            $request->session()->flash('success', 'Question Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Created!');
        }

        return redirect()->route('admin::course-parts.questions.index', [$coursePart->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param CoursePart $coursePart
     * @param CoursePartQuestion $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, CoursePart $coursePart, CoursePartQuestion $question)
    {
        return view('admin.courses.parts.questions.edit', compact('coursePart', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Save|Request $request
     * @param CoursePart $coursePart
     * @param CoursePartQuestion $question
     * @return \Illuminate\Http\Response
     */
    public function update(Save $request, CoursePart $coursePart, CoursePartQuestion $question)
    {
        if ($question->update($request->all())) {
            $request->session()->flash('success', 'Question Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Question Was Not Updated!');
        }

        return redirect()->route('admin::course-parts.questions.index', [$coursePart->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param CoursePart $coursePart
     * @param CoursePartQuestion $question
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, CoursePart $coursePart, CoursePartQuestion $question)
    {
        if ($question->delete()) {
            session()->flash('success', 'Question Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Question Was Not Deleted!');
        }

        return redirect()->route('admin::course-parts.questions.index', [$coursePart->id]);
    }
}
