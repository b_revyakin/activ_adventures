<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Http\Requests;
use App\Http\Requests\Admin\Category\Save;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('level')->get();

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Save $request)
    {
        if (Category::create($request->all())) {
            $request->session()->flash('success', 'Category Was Created!');
        } else {
            $request->session()->flash('error', 'Server Error. Category Was Not Created!');
        }

        return redirect()->route('admin::categories.index');
    }

    public function edit(Category $categories)
    {
        return view('admin.categories.edit', compact('categories'));
    }

    public function update(Save $request, Category $categories)
    {
        if ($categories->update($request->all())) {
            $request->session()->flash('success', 'Category Was Updated!');
        } else {
            $request->session()->flash('error', 'Server Error. Category Was Not Updated!');
        }

        return redirect()->route('admin::categories.index');
    }

    public function destroy(Category $categories)
    {
        if ($categories->delete()) {
            session()->flash('success', 'Category Was Deleted!');
        } else {
            session()->flash('error', 'Server Error. Category Was Not Deleted!');
        }

        return back();
    }

    public function switchOrder(CategoryService $categoryService, Request $request)
    {
        $response = $categoryService->switchOrder($request->items);

        return response('', $response ? 200 : 500);
    }
}
