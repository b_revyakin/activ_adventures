<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Customer;
use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\API\Profile;
use App\Http\Requests\Admin\API\Search;
use App\Services\ParentService;
use App\Services\SearchService;

class CustomersController extends Controller
{
    protected $searchService;

    protected $parentService;

    public function __construct(SearchService $searchService, ParentService $parentService)
    {
        parent::__construct();

        $this->searchService = $searchService;
        $this->parentService = $parentService;
    }

    public function searchCustomers(Search $request)
    {
        $customers = $this->searchService->searchCustomers($request->get('q'));

        return $customers;
    }

    public function index()
    {
        return Customer::all();
    }

    public function get()
    {
        $customers = Customer::take(3)->get();

        return $customers;
    }

    public function getChildren($customerId)
    {
        $customer = Customer::findOrFail($customerId);

        return $customer->children;
    }

    public function show($customerId)
    {
        return Customer::with(['user', 'country'])->findOrFail($customerId);
    }

    /**
     * Update customer's profile
     *
     * @param Profile $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Profile $request, $customerId)
    {
        return $this->parentService->update(
            Customer::with(['user', 'country'])->findOrFail($customerId),
            $request->all()
        ) ?: response('', 500);
    }
}
