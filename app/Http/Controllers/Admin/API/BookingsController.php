<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Booking;
use App\Entities\Coupon;
use App\Entities\Customer;
use App\Entities\Order;
use App\Entities\Payment;
use App\Events\Bookings\Cancel;
use App\Events\Bookings\CancelledRequestRegister;
use App\Events\Bookings\ConfirmedRequestRegister;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\API\Booking\Cancel as CancelRequest;
use App\Http\Requests\Admin\API\Booking\ConfirmRegister;
use App\Http\Requests\Customer\API\Booking\Book;
use App\Services\BookingService;

class BookingsController extends Controller
{
    /**
     * @var BookingService
     */
    protected $bookingService;

    /**
     * BookingsController constructor.
     * @param BookingService $bookingService
     */
    public function __construct(BookingService $bookingService)
    {
        parent::__construct();

        $this->bookingService = $bookingService;
        $this->middleware('permission:booking.cancel', ['only' => ['cancel', 'cancelOrder']]);
    }

    public function cancel(CancelRequest $request, Booking $booking)
    {
        $isOldStatusProcessing = $booking->isProcessing();
        $booking = $this->bookingService->cancel($booking);

        if ($booking) {
            if ($isOldStatusProcessing && $booking->requested_register) {
                event(new CancelledRequestRegister($booking));
            } else {
                event(new Cancel($booking));
            }
        }

        return response('', $booking ? 200 : 500);
    }

    public function confirmRegister(ConfirmRegister $request, Booking $booking)
    {
        if ($booking->setPaid()) {
            event(new ConfirmedRequestRegister($booking));
        }

        return response('', $booking ? 200 : 500);
    }

    public function cancelOrder($orderId)
    {
        $order = Order::findOrFail($orderId);

        $booking = $this->bookingService->cancelOrder($order);

        return response('', $booking ? 200 : 500);
    }

    public function book(Book $request, $customerId)
    {
        $data = $request->all();
        $data['payment_type'] = Payment::getTypeByTitle($data['payment_type']);
        $booking = $this->bookingService->create(Customer::findOrFail($customerId), $data);

        if ($booking) {
            // event(new Created($booking));

            $response = [
                'redirect' => route('admin::parents.bookings.index', [$customerId]),
                'method' => 'GET',
                'fields' => ''
            ];
        }

        return response($booking ? $response : 0, $booking ? 200 : 500);
    }

    //Check exists and active requested coupon
    public function checkCoupon($coupon)
    {
        $coupon = Coupon::active()->coupon($coupon)->first();

        return response($coupon ? $coupon->percent : '', $coupon ? 200 : 404);
    }
}
