<?php

namespace App\Http\Controllers\Admin\API;


use App\Entities\CoursePart;
use App\Http\Controllers\Controller;

class CoursePartsController extends Controller
{

    /**
     * Get all Courses
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return CoursePart::whereHas('course', function ($q) {
            return $q->active();
        })->with('course')->get();
    }

    /**
     * Get all customers
     *
     * @param CoursePart $coursePart
     * @return array
     */
    public function customers(CoursePart $coursePart)
    {
        return $coursePart->orders()->with('child.parent')->get()
            ->pluck('child.parent')
            ->unique()
            ->values()
            ->all();
    }

    public function calendar()
    {
        $courseParts = CoursePart::all();

        $partsToCalendar = [];
        foreach ($courseParts as $part) {
            $partsToCalendar[] = [
                'title' => $part->name,
                'start' => $part->start_date->toIso8601String(),
                'end' => $part->end_date->toIso8601String(),
                'url' => route('admin::courses.show', [$part->course->id]),
                'id' => $part->course->id
            ];
        }

        return $partsToCalendar;
    }

    public function teams(CoursePart $coursePart)
    {
        return $coursePart->teams()->get(['id', 'name']);
    }


}