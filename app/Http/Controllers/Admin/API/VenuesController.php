<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Venue;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class VenuesController extends Controller
{
    public function index()
    {
        return Venue::all('id', 'name');
    }
}
