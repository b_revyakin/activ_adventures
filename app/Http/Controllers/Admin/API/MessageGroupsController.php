<?php

namespace App\Http\Controllers\Admin\API;


use App\Entities\MessageGroup;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\API\MessageRequest;
use App\Services\MessageGroupService;


class MessageGroupsController extends Controller
{

    protected $messageGroupService;

    public function __construct(MessageGroupService $messageGroupService)
    {
        parent::__construct();

        $this->messageGroupService = $messageGroupService;
    }

    /**
     * Create Message Groups and messages
     *
     * @param MessageRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(MessageRequest $request)
    {
        if (!($messageGroup = $this->messageGroupService->create($request->all()))) {
            return response('Error', 500);
        }

        return $messageGroup;
    }

    /**
     * Get all message groups
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return MessageGroup::with('courseParts', 'school')->get();
    }

    /**
     * Get messages to group
     *
     * @param MessageGroup $group
     * @return mixed
     */
    public function messages(MessageGroup $group)
    {
        return $group->load('messages.messageable', 'school', 'courseParts.course', 'files');
    }


}