<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\School;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SchoolsController extends Controller
{

    /**
     * Get all schools
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return School::all();
    }


    /**
     * Get all customers
     *
     * @param $schoolId
     * @return array
     */
    public function customers(School $school)
    {
        return $school->childs()->with('parent')->get()
            ->pluck('parent')
            ->unique()->values()->all();
    }

    public function groupsBySchools(Request $request)
    {
        $arraySchoolGroups = new Collection();

        if ($request->school_ids) {
            /** @noinspection PhpUndefinedFieldInspection */
            foreach ($request->school_ids as $schoolId) {
                /** @noinspection PhpUndefinedMethodInspection */
                $school = School::findOrFail($schoolId);

                $arraySchoolGroups = $arraySchoolGroups->merge($school->groups);
            }
        }

        return $arraySchoolGroups;
    }
}