<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class QuestionsController extends Controller
{
    public function questions(Category $categories)
    {
        $item = [];
        $items = [];

        foreach ($categories->questions as $questions) {
            $item['id'] = $questions->id;
            $item['text'] = $questions->title;
            array_push($items, $item);
        }

        $response['items'] = $items;

        return $items;
    }
}
