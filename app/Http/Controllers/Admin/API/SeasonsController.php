<?php

namespace App\Http\Controllers\Admin\API;

use App\Entities\Season;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class SeasonsController extends Controller
{
    public function index()
    {
        $seasons = Season::active()->get(['id', 'name']);

        return response()->json($seasons);
    }
}
