<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Entities\Customer;
use App\Entities\PaymentSystem;
use App\Http\Controllers\Controller;

class BookingsController extends Controller
{
    public function index($customerId)
    {
        $customer = Customer::findOrFail($customerId);
        $bookings = $customer->bookings()->orderBy('created_at', 'DESC')->get();
        $paymentSystems = PaymentSystem::active();

        return view('admin.parents.bookings.index', compact('customer', 'bookings', 'paymentSystems'));
    }
}
