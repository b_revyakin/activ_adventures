<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Entities\Country;
use App\Entities\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Customer\Activate;
use App\Http\Requests\Admin\Customer\Deactivate;
use App\Http\Requests\Admin\Customer\SetPassword;
use App\Http\Requests\Admin\Customer\Store;
use App\Http\Requests\Admin\Customer\Update;
use App\Services\ParentService;

class ParentsController extends Controller
{
    /**
     * @var ParentService
     */
    protected $parentService;

    protected $object;

    /**
     * ParentsController constructor.
     * @param ParentService $parentService
     */
    public function __construct(ParentService $parentService)
    {
        parent::__construct();

        $this->parentService = $parentService;
        $this->object = 'Parent';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parents = Customer::all();

        return view('admin.parents.index', compact('parents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();

        return view('admin.parents.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $parent = $this->parentService->create($request->all());

        if ($parent) {
            $request->session()->flash('success', trans('manage.created_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('success', trans('manage.created_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::parents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parent = Customer::findOrFail($id);

        return view('admin.parents.show', compact('parent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent = Customer::findOrFail($id);
        $countries = Country::all();

        return view('admin.parents.edit', compact('parent', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        $parent = Customer::findOrFail($id);

        $parent = $this->parentService->update($parent, $request->all());

        if ($parent) {
            $request->session()->flash('success', trans('manage.updated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('success', trans('manage.updated_fail', ['object' => $this->object]));
        }

        return redirect()->route('admin::parents.index');
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param Activate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Activate $request, $id)
    {
        $product = Customer::findOrFail($id);

        if ($product->activate()) {
            $request->session()->flash('success', trans('manage.activated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('error', trans('manage.activated_fail', ['object' => $this->object]));
        }

        return redirect()->back();
    }

    /**
     * Deactivate the specified resource in storage.
     *
     * @param Deactivate $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate(Deactivate $request, $id)
    {
        $product = Customer::findOrFail($id);

        if ($product->deactivate()) {
            $request->session()->flash('success', trans('manage.deactivated_success', ['object' => $this->object]));
        } else {
            $request->session()->flash('error', trans('manage.deactivated_fail', ['object' => $this->object]));
        }

        return redirect()->back();
    }

    public function setPasswordPage(Customer $customer)
    {
        return view('admin.parents.set-password', compact('customer'));
    }

    /**
     * @param SetPassword $request
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPassword(SetPassword $request, Customer $customer)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        if ($customer->setPassword($request->password)) {
            $request->session()->flash('success', 'Password Has Been Set');
        } else {
            $request->session()->flash('error', 'Password Has Been Not Set');
        }

        return redirect()->route('admin::parents.index');
    }
}
