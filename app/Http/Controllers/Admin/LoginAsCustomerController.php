<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginAsCustomer;
use App\Services\AdminService;
use Illuminate\Http\Request;

class LoginAsCustomerController extends Controller
{
    /**
     * @var AdminService
     */
    private $adminService;

    public function __construct(AdminService $adminService)
    {
        parent::__construct();

        $this->adminService = $adminService;
        $this->middleware('role:admin', ['only' => ['switchUsers']]);
    }

    /**
     * @param LoginAsCustomer|Request $request
     * @param Customer $customer
     * @return LoginAsCustomerController|\Illuminate\Http\RedirectResponse
     */
    public function switchUsers(LoginAsCustomer $request, Customer $customer)
    {
        return $this->adminService->loginAsCustomer($customer) ? redirect()->route('customer::dashboard') : back()->withErrors('Error login as customer');
    }

    /**
     * @return LoginAsCustomerController|\Illuminate\Http\RedirectResponse
     */
    public function backToAdmin()
    {
        return $this->adminService->backToAdmin() ? redirect()->route('admin::dashboard') : back()->withErrors('Error login as admin');
    }
}
