<?php

Route::group([
    'middleware' => ['web'],
], function () {

    Route::get('/admin', ['as' => 'auth.admin_login', 'uses' => 'Auth\AuthController@getAdminLogin']);

    Route::group([
        'middleware' => 'auth',
        'namespace' => 'Admin',
        'as' => 'admin::',
        'prefix' => 'admin',
    ], function () {

        Route::post('login-as-customer/{customer}', ['as' => 'login-as-customer', 'uses' => 'LoginAsCustomerController@switchUsers']);
        Route::post('back-to-admin', ['as' => 'back-to-admin', 'uses' => 'LoginAsCustomerController@backToAdmin']);

        Route::group([
            'middleware' => ['role:admin'],
        ], function () {
            Route::resource('admins', 'AdminsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'admins.index',
                    'create' => 'admins.create',
                    'store' => 'admins.store',
                    'show' => 'admins.show',
                    'edit' => 'admins.edit',
                    'update' => 'admins.update'
                ]
            ]);
            Route::put('admins/{admins}/activate', ['as' => 'admins.activate',
                'uses' => 'AdminsController@activate']);
            Route::put('admins/{admins}/deactivate', ['as' => 'admins.deactivate',
                'uses' => 'AdminsController@deactivate']);


            Route::resource('staffs', 'StaffsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'staffs.index',
                    'create' => 'staffs.create',
                    'store' => 'staffs.store',
                    'show' => 'staffs.show',
                    'edit' => 'staffs.edit',
                    'update' => 'staffs.update'
                ]
            ]);

//            Route::get('payment-systems', ['as' => 'payment-systems.index', 'uses' => 'PaymentSystemsController@index']);
//            Route::post('payment-systems/save', ['as' => 'payment-systems.save', 'uses' => 'PaymentSystemsController@save']);

            Route::put('bookings/{bookings}/payments/{payments}/cancel', ['as' => 'bookings.payments.cancel', 'uses' => 'PaymentsController@cancel']);
            Route::put('bookings/{bookings}/payments/{payments}/confirm', ['as' => 'bookings.payments.confirm', 'uses' => 'PaymentsController@confirm']);
            Route::put('bookings/{bookings}/payments/{payments}/refund', ['as' => 'bookings.payments.refund', 'uses' => 'PaymentsController@refund']);
            Route::resource('bookings.payments', 'PaymentsController', [
                'only' => ['index', 'create', 'store'],
                'names' => [
                    'index' => 'bookings.payments.index',
                    'create' => 'bookings.payments.create',
                    'store' => 'bookings.payments.store'
                ]
            ]);

            Route::resource('site-managers', 'Staffs\SiteManagersController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'site-managers.index',
                    'create' => 'site-managers.create',
                    'store' => 'site-managers.store',
                    'show' => 'site-managers.show',
                    'edit' => 'site-managers.edit',
                    'update' => 'site-managers.update'
                ]
            ]);
            Route::put('staffs/{staffs}/activate', ['as' => 'staffs.activate',
                'uses' => 'Staffs\Controller@activate']);
            Route::put('staffs/{staffs}/deactivate', ['as' => 'staffs.deactivate',
                'uses' => 'Staffs\Controller@deactivate']);

            //Manage Products
            Route::resource('products', 'ProductsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'products.index',
                    'create' => 'products.create',
                    'store' => 'products.store',
                    'show' => 'products.show',
                    'edit' => 'products.edit',
                    'update' => 'products.update'
                ]
            ]);
            Route::put('products/{products}/activate', ['as' => 'products.activate',
                'uses' => 'ProductsController@activate']);
            Route::put('products/{products}/deactivate', ['as' => 'products.deactivate',
                'uses' => 'ProductsController@deactivate']);

            Route::resource('coupons', 'CouponsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'coupons.index',
                    'create' => 'coupons.create',
                    'store' => 'coupons.store',
                    'show' => 'coupons.show',
                    'edit' => 'coupons.edit',
                    'update' => 'coupons.update'
                ]
            ]);
            Route::put('coupons/{coupons}/activate', ['as' => 'coupons.activate',
                'uses' => 'CouponsController@activate']);


            Route::put('coupons/{coupons}/deactivate', ['as' => 'coupons.deactivate',
                'uses' => 'CouponsController@deactivate']);

            // Messages

            Route::get('message', ['as' => 'messages.index',
                'uses' => 'MessagesController@index']);


            Route::group([
                'namespace' => 'Schools',
            ], function () {
                Route::resource('schools', 'Controller', [
                    'parameters' => ['schools' => 'school'],
                    'names' => [
                        'index' => 'schools.index',
                        'create' => 'schools.create',
                        'store' => 'schools.store',
                        'show' => 'schools.show',
                        'edit' => 'schools.edit',
                        'update' => 'schools.update',
                        'destroy' => 'schools.destroy'
                    ]
                ]);

                Route::resource('schools.groups', 'GroupsController', [
                    'parameters' => [
                        'schools' => 'school',
                        'groups' => 'group'
                    ],
                    'except' => ['show'],
                    'names' => [
                        'index' => 'schools.groups.index',
                        'create' => 'schools.groups.create',
                        'store' => 'schools.groups.store',
                        'edit' => 'schools.groups.edit',
                        'update' => 'schools.groups.update',
                        'destroy' => 'schools.groups.destroy'
                    ]
                ]);

                Route::resource('schools.children', 'ChildrenController', [
                    'parameters' => [
                        'schools' => 'school',
                        'children' => 'child'
                    ],
                    'only' => ['index', 'store'],
                    'names' => [
                        'index' => 'schools.children.index',
                        'store' => 'schools.children.store',
                    ]
                ]);
            });


            Route::get('courses/calendar', ['as' => 'courses.calendar',
                'uses' => 'CoursesController@calendar']);

            Route::put('categories/switch-order', ['as' => 'categories.switch-order', 'uses' => 'CategoriesController@switchOrder']);
            Route::resource('categories', 'CategoriesController', [
                'except' => ['show'],
                'names' => [
                    'index' => 'categories.index',
                    'create' => 'categories.create',
                    'store' => 'categories.store',
                    'edit' => 'categories.edit',
                    'update' => 'categories.update',
                    'destroy' => 'categories.destroy'
                ]
            ]);

            Route::group([
                'namespace' => 'Faq',
                'as' => 'faq.',
                'prefix' => 'faq',
            ], function () {
                Route::put('categories/switch-order', ['as' => 'categories.switch-order', 'uses' => 'CategoriesController@switchOrder']);
                Route::resource('course-parts.categories', 'CategoriesController', [
                    'parameters' => [
                        'course-parts' => 'coursePart',
                        'categories' => 'category'
                    ],
                    'except' => ['show'],
                    'names' => [
                        'index' => 'course-parts.categories.index',
                        'create' => 'course-parts.categories.create',
                        'store' => 'course-parts.categories.store',
                        'edit' => 'course-parts.categories.edit',
                        'update' => 'course-parts.categories.update',
                        'destroy' => 'course-parts.categories.destroy'
                    ]
                ]);

                Route::put('categories/{category}/questions/switch-order', ['as' => 'categories.questions.switch-order', 'uses' => 'QuestionsController@switchOrder']);
                Route::resource('categories.questions', 'QuestionsController', [
                    'parameters' => [
                        'categories' => 'category',
                        'questions' => 'question'
                    ],
                    'except' => ['show'],
                    'names' => [
                        'index' => 'categories.questions.index',
                        'create' => 'categories.questions.create',
                        'store' => 'categories.questions.store',
                        'edit' => 'categories.questions.edit',
                        'update' => 'categories.questions.update',
                        'destroy' => 'categories.questions.destroy'
                    ]
                ]);
            });

            Route::put('categories/{categories}/questions/switch-order', ['as' => 'categories.questions.switch-order', 'uses' => 'QuestionsController@switchOrder']);
            Route::resource('categories.questions', 'QuestionsController', [
                'except' => ['show'],
                'names' => [
                    'index' => 'categories.questions.index',
                    'create' => 'categories.questions.create',
                    'store' => 'categories.questions.store',
                    'edit' => 'categories.questions.edit',
                    'update' => 'categories.questions.update',
                    'destroy' => 'categories.questions.destroy'
                ]
            ]);

            Route::resource('course-parts.checkboxes', 'CheckboxesController', [
                'parameters' => [
                    'course-parts' => 'coursePart',
                    'checkboxes' => 'checkbox'
                ],
                'except' => ['show'],
                'names' => [
                    'index' => 'course-parts.checkboxes.index',
                    'create' => 'course-parts.checkboxes.create',
                    'store' => 'course-parts.checkboxes.store',
                    'edit' => 'course-parts.checkboxes.edit',
                    'update' => 'course-parts.checkboxes.update',
                    'destroy' => 'course-parts.checkboxes.destroy'
                ]
            ]);

            Route::resource('course-parts.questions', 'CoursePartQuestionsController', [
                'parameters' => [
                    'course-parts' => 'coursePart',
                    'questions' => 'question'
                ],
                'except' => ['show'],
                'names' => [
                    'index' => 'course-parts.questions.index',
                    'create' => 'course-parts.questions.create',
                    'store' => 'course-parts.questions.store',
                    'edit' => 'course-parts.questions.edit',
                    'update' => 'course-parts.questions.update',
                    'destroy' => 'course-parts.questions.destroy'
                ]
            ]);

            Route::resource('course-parts.teams', 'CoursePartTeamsController', [
                'parameters' => [
                    'course-parts' => 'coursePart',
                    'teams' => 'team'
                ],
                'except' => ['show'],
                'names' => [
                    'index' => 'course-parts.teams.index',
                    'create' => 'course-parts.teams.create',
                    'store' => 'course-parts.teams.store',
                    'edit' => 'course-parts.teams.edit',
                    'update' => 'course-parts.teams.update',
                    'destroy' => 'course-parts.teams.destroy'
                ]
            ]);

        });

        Route::group([
            'middleware' => ['role:admin|staff|site.manager'],
        ], function () {

            Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@getDashboard']);

            Route::get('venues/{venues}/photos/{photos}', ['as' => 'venue.photos', 'uses' => 'VenuesController@getPhoto']);
            Route::delete('venues/{venues}/photos/{photos}', ['as' => 'venues.photos', 'uses' => 'VenuesController@deletePhoto']);
            Route::resource('venues', 'VenuesController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'venues.index',
                    'create' => 'venues.create',
                    'store' => 'venues.store',
                    'show' => 'venues.show',
                    'edit' => 'venues.edit',
                    'update' => 'venues.update'
                ]
            ]);
            Route::put('venues/{venues}/activate', ['as' => 'venues.activate',
                'uses' => 'VenuesController@activate']);
            Route::put('venues/{venues}/deactivate', ['as' => 'venues.deactivate',
                'uses' => 'VenuesController@deactivate']);
            Route::get('venues/{venues}/courses', ['as' => 'venues.courses', 'uses' => 'VenuesController@courses']);


            Route::resource('seasons', 'SeasonsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'seasons.index',
                    'create' => 'seasons.create',
                    'store' => 'seasons.store',
                    'show' => 'seasons.show',
                    'edit' => 'seasons.edit',
                    'update' => 'seasons.update'
                ]
            ]);
            Route::put('seasons/{seasons}/activate', ['as' => 'seasons.activate',
                'uses' => 'SeasonsController@activate']);
            Route::put('seasons/{seasons}/deactivate', ['as' => 'seasons.deactivate',
                'uses' => 'SeasonsController@deactivate']);

            Route::get('courses/report', ['as' => 'courses.report', 'uses' => 'CoursesController@getAllReport']);

            Route::get('courses/{course}/copy', ['as' => 'courses.copy', 'uses' => 'CoursesController@copy']);
            Route::post('courses/{course}/copy/store', ['as' => 'courses.copy.store', 'uses' => 'CoursesController@copyStore']);

            Route::get('courses/{courses}/parts', ['as' => 'courses.parts.index', 'uses' => 'CoursesController@getParts']);
            Route::get('courses/{courses}/parts/edit/{partId}', ['as' => 'courses.parts.edit', 'uses' => 'CoursesController@editPart']);
            Route::get('courses/{courses}/parts/create', ['as' => 'courses.parts.create', 'uses' => 'CoursesController@createPart']);
            Route::post('courses/{courses}/parts/update/{partId}', ['as' => 'courses.parts.update', 'uses' => 'CoursesController@updatePart']);
            Route::post('courses/{courses}/parts/store', ['as' => 'courses.parts.store', 'uses' => 'CoursesController@storePart']);
            Route::get('courses/{courses}/parts/destroy/{partId}', ['as' => 'courses.parts.destroy', 'uses' => 'CoursesController@destroyPart']);

            Route::get('courses/{courses}/details/bookings', ['as' => 'courses.details.bookings', 'uses' => 'CoursesController@getBookings']);
            Route::get('courses/{course}/details/register-bookings', ['as' => 'courses.details.register-bookings', 'uses' => 'CoursesController@getRequestedRegisterBookings']);
            Route::get('courses/{course}/details/register-b', ['as' => 'courses.details.register-b', 'uses' => 'CoursesController@getRegisterB']);
            Route::get('courses/{course}/details/payment-register', ['as' => 'courses.details.payment-register', 'uses' => 'CoursesController@getPaymentRegister']);
            Route::get('courses/{course}/details/register/export/{format}/{withEmergencyContacts?}', ['as' => 'courses.details.register.export', 'uses' => 'CoursesController@exportRegister']);
            Route::get('courses/{course}/details/register-b/export/{format}', ['as' => 'courses.details.register-b.export', 'uses' => 'CoursesController@exportRegisterB']);
            Route::get('courses/{course}/details/payment-register/export/{format}', ['as' => 'courses.details.payment-register.export', 'uses' => 'CoursesController@exportPaymentRegister']);
            Route::get('courses/{course}/details/register/{withEmergencyContacts?}', ['as' => 'courses.details.register', 'uses' => 'CoursesController@getRegister']);
            Route::get('courses/{courses}/details/age-breakdown', ['as' => 'courses.details.age-breaking', 'uses' => 'CoursesController@getAgeBreaking']);
            Route::get('courses/{courses}/details/report', ['as' => 'courses.details.report', 'uses' => 'CoursesController@getReport']);
            Route::get('courses/{course}/details/children/export', ['as' => 'courses.details.children.export', 'uses' => 'CoursesController@exportChildren']);
            Route::get('courses/{courses}/details/mailing-lists/export', ['as' => 'courses.details.mailing-lists.export', 'uses' => 'CoursesController@exportMailingLists']);
            Route::get('courses/{course}/children/{child}/export', ['as' => 'courses.details.child.export', 'uses' => 'CoursesController@exportChild']);

            Route::get('courses/archive', ['as' => 'courses.archive', 'uses' => 'CoursesController@archive']);
            Route::put('courses/{courses}/activate', ['as' => 'courses.activate', 'uses' => 'CoursesController@activate']);
            Route::put('courses/{courses}/deactivate', ['as' => 'courses.deactivate', 'uses' => 'CoursesController@deactivate']);
            Route::get('courses/{courses}/day-capacities', ['as' => 'courses.day-capacities', 'uses' => 'CoursesController@getDaysCapacity']);
            Route::put('courses/{courses}/day-capacities', ['as' => 'courses.day-capacities', 'uses' => 'CoursesController@updateDaysCapacity']);


            Route::get('courses/{courses}/photos/{photos}', ['as' => 'course.photos', 'uses' => 'CoursesController@getPhoto']);
            Route::delete('courses/{courses}/photos/{photos}', ['as' => 'course.photos', 'uses' => 'CoursesController@deletePhoto']);
            Route::get('courses/{courses}/document', ['as' => 'course.document', 'uses' => 'CoursesController@getDocument']);
            Route::post('course-part/{coursePart}/team.add-orders', ['as' => 'team.add-orders', 'uses' => 'CoursesController@addTeamToOrders']);
            Route::resource('courses', 'CoursesController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'courses.index',
                    'create' => 'courses.create',
                    'store' => 'courses.store',
                    'show' => 'courses.show',
                    'edit' => 'courses.edit',
                    'update' => 'courses.update'
                ]
            ]);

            Route::resource('children.questions', 'QuestionnairesController', [
                'parameters' => [
                    'children' => 'child',
                    'questions' => 'question'
                ],
                'only' => ['index', 'store', 'create'],
                'names' => [
                    'index' => 'children.questions.index',
                    'store' => 'children.questions.store',
                    'create' => 'children.questions.create'
                ]
            ]);

            //Parents and children
            Route::group([
                'namespace' => 'Customer',
            ], function () {

                Route::get('parents/{customer}/set-password', ['as' => 'parents.set-password', 'uses' => 'ParentsController@setPasswordPage'])->middleware(['role:admin']);
                Route::post('parents/{customer}/set-password', ['as' => 'parents.set-password', 'uses' => 'ParentsController@setPassword'])->middleware(['role:admin']);

                Route::resource('parents', 'ParentsController', [
                    'except' => ['destroy'],
                    'names' => [
                        'index' => 'parents.index',
                        'create' => 'parents.create',
                        'store' => 'parents.store',
                        'show' => 'parents.show',
                        'edit' => 'parents.edit',
                        'update' => 'parents.update'
                    ]
                ]);
                Route::resource('parents.bookings', 'BookingsController', [
                    'only' => ['index'],
                    'names' => [
                        'index' => 'parents.bookings.index',
                    ]
                ]);
                Route::put('parents/{parents}/activate', ['as' => 'parents.activate',
                    'uses' => 'ParentsController@activate']);
                Route::put('parents/{parents}/deactivate', ['as' => 'parents.deactivate',
                    'uses' => 'ParentsController@deactivate']);


                Route::resource('parents.children', 'ChildrenController', [
                    'names' => [
                        'index' => 'parents.children.index',
                        'create' => 'parents.children.create',
                        'store' => 'parents.children.store',
                        'show' => 'parents.children.show',
                        'edit' => 'parents.children.edit',
                        'update' => 'parents.children.update',
                        'destroy' => 'parents.children.destroy'
                    ]
                ]);

            });


            // Profile
            Route::group([
                'prefix' => 'profile',
                'as' => 'profile.',
            ], function () {

                Route::get('', ['as' => 'info', 'uses' => 'ProfileController@getProfile']);

                //update info
                Route::put('info', ['as' => 'update', 'uses' => 'ProfileController@updateProfile']);

                //update avatar
                Route::put('avatar', ['as' => 'avatar', 'uses' => 'ProfileController@updateAvatar']);

                //update password
                Route::put('profile/avatar', ['as' => 'password', 'uses' => 'ProfileController@updatePassword']);

            });

            Route::get('bookings/requested-registers', ['as' => 'bookings.requested-registers.index', 'uses' => 'BookingsController@getRequestedRegisters']);
            Route::get('bookings/{bookings}/orders', ['as' => 'bookings.orders', 'uses' => 'BookingsController@orders']);
            Route::delete('bookings/{bookings}/cancel', ['as' => 'bookings.cancel', 'uses' => 'BookingsController@cancel']);
            Route::resource('bookings', 'BookingsController', [
                'except' => ['destroy'],
                'names' => [
                    'index' => 'bookings.index',
                    'create' => 'bookings.create',
                    'store' => 'bookings.store',
                    'show' => 'bookings.show',
                    'edit' => 'bookings.edit',
                    'update' => 'bookings.update',
                ]
            ]);

            Route::resource('orders', 'OrdersController', [
                'only' => ['index', 'show'],
                'names' => [
                    'index' => 'orders.index',
                    'show' => 'orders.show'
                ]
            ]);

            Route::resource('children', 'ChildrenController', [
                'only' => ['show'],
                'names' => [
                    'show' => 'children.show'
                ]
            ]);

            //API
            Route::group([
                'namespace' => 'API',
                'prefix' => 'api',
                'as' => 'api::',
            ], function () {

                Route::post('{customerId}/booking', 'BookingsController@book');

                Route::resource('customers.children', 'ChildrenController', [
                    'except' => ['create', 'edit']
                ]);

                Route::get('venues', 'VenuesController@index');
                Route::get('customers', 'CustomersController@searchCustomers');
                Route::get('customers/all', 'CustomersController@index');
                Route::get('customers/get', 'CustomersController@get');
                Route::get('customers/{customerId}', 'CustomersController@show');
                Route::put('customers/{customerId}', 'CustomersController@update');
                Route::get('customers/{customerId}/children', 'CustomersController@getChildren');

                Route::post('bookings/{booking}/cancel', ['as' => 'bookings.cancel', 'uses' => 'BookingsController@cancel']);
                Route::post('bookings/{booking}/register/confirm', ['as' => 'bookings.register.confirm', 'uses' => 'BookingsController@confirmRegister']);

                Route::post('orders/{orderId}/cancel', ['as' => 'orders.cancel', 'uses' => 'BookingsController@cancelOrder']);

                Route::get('booking/coupons/{coupon}', 'BookingsController@checkCoupon');

                Route::get('seasons', 'SeasonsController@index');

                Route::post('messages/create', ['as' => 'messages.create', 'uses' => 'MessageGroupsController@store']);
                Route::get('messages/groups', 'MessageGroupsController@index');
                Route::get('messages/{group}', 'MessageGroupsController@messages');

                Route::get('schools', 'SchoolsController@index');
                Route::get('schools/{school}/customers', 'SchoolsController@customers');
                Route::get('schools/groups-by-schools', 'SchoolsController@groupsBySchools');

                Route::get('courses-parts', 'CoursePartsController@index');
                Route::get('courses-parts/calendar', 'CoursePartsController@calendar');
                Route::get('courses-parts/{coursePart}/customers', 'CoursePartsController@customers');
                Route::get('course-parts/{coursePart}/teams', 'CoursePartsController@teams');

                Route::get('categories/{categories}/questions', 'QuestionsController@questions');
            });

        });

    });

});
