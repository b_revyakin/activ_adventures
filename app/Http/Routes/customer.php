<?php

Route::group([
    'middleware' => ['web'],
    'namespace' => 'Customer',
    'as' => 'customer::',
], function () {

    Route::get('register/verify/{hash}', ['as' => 'confirm-email', 'uses' => 'ConfirmController@confirmEmail']);
    Route::get('register/confirm/{email}', ['as' => 'repeat-confirm-email', 'uses' => 'ConfirmController@repeatConfirmMail']);

    Route::group([
        'middleware' => ['auth'],
        'namespace' => 'API',
        'prefix' => 'api'
    ], function () {
        Route::get('courses', 'CoursesController@index');
        Route::get('course/{course}/questions', 'CoursesController@questions');
        Route::get('children/{child}/courses/{coursePart}/get', 'CoursesController@get');

        Route::get('products', 'BookingController@products');
        Route::get('countries', 'CountriesController@index');
        Route::get('swim-options', 'ChildrenController@swimOptions');
        Route::get('years-in-school', 'ChildrenController@yearsInSchool');
        Route::get('schools', 'ChildrenController@schools');

        Route::post('booking/calculate', 'BookingController@calculate');
        Route::get('questions', 'QuestionsController@index');
    });

    Route::group([
        'middleware' => ['auth', 'role:customer'],
    ], function () {

        Route::get('profile/show', ['as' => 'profile.show', 'uses' => 'ProfileController@show']);
        Route::get('profile/change-password', ['as' => 'profile.change-password', 'uses' => 'ProfileController@getChangePassword']);
        Route::put('profile/change-password', ['as' => 'profile.change-password', 'uses' => 'ProfileController@putChangePassword']);
        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@getDashboard']);

        Route::get('bookings/{bookings}/payment/stripe/{payOption?}', ['as' => 'bookings.payment.stripe', 'uses' => 'BookingsController@getPayment']);
        Route::post('bookings/{bookings}/payment/stripe', ['as' => 'bookings.payment.stripe.pay', 'uses' => 'BookingsController@payment']);
        Route::resource('bookings', 'BookingsController', [
            'only' => ['index', 'create', 'destroy'],
            'names' => [
                'index' => 'bookings.index',
                'create' => 'bookings.create',
                'destroy' => 'bookings.destroy'
            ]
        ]);

        Route::get('courses/{courses}/photos/{photos}', ['as' => 'courses.photos', 'uses' => 'CoursesController@getPhoto']);
        Route::get('courses/{courses}/document', ['as' => 'course.document', 'uses' => 'CoursesController@getDocument']);
        Route::resource('course', 'CoursesController', [
            'only' => ['index', 'show'],
            'names' => [
                'index' => 'courses.index',
                'show' => 'courses.show'
            ]
        ]);

        Route::group([
            'namespace' => 'API',
            'prefix' => 'api'
        ], function () {

            Route::get('profile', 'ProfileController@show');
            Route::put('profile', 'ProfileController@update');

            Route::resource('children', 'ChildrenController', [
                'except' => ['create', 'edit']
            ]);

            Route::post('booking', 'BookingController@book');
            //POST request because we can GET request is limited
            Route::get('booking/coupons/{coupon}', 'BookingController@checkCoupon');
            
            Route::get('payment-systems', 'PaymentSystemsController@index');
        });

        Route::resource('payments', 'PaymentController', [
            'only' => ['index'],
            'names' => [
                'index' => 'payments.index'
            ]
        ]);

    });

    Route::get('venues/{venues}/photos/{photos}', ['as' => 'venues.photos.show', 'uses' => 'VenuesController@getPhoto']);
    Route::resource('venues', 'VenuesController', [
        'only' => ['index', 'show'],
        'names' => [
            'index' => 'venues.index',
            'show' => 'venues.show'
        ]
    ]);

    Route::resource('seasons', 'SeasonsController', [
        'only' => ['index', 'show'],
        'names' => [
            'index' => 'seasons.index',
            'show' => 'seasons.show'
        ]
    ]);

});
