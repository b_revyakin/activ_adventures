<?php

namespace App\Http\Requests\Admin\Venues;

class Activate extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !$this->getVenue()->isActive();
    }
}
