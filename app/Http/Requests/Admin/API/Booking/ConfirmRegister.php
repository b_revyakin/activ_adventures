<?php

namespace App\Http\Requests\Admin\API\Booking;

use App\Http\Requests\Request;

class ConfirmRegister extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->booking->isProcessing() && $this->booking->isSimple() && $this->booking->requested_register;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
