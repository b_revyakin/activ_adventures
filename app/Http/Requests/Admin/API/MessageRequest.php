<?php

namespace App\Http\Requests\Admin\API;

use App\Entities\Child;
use App\Entities\Customer;
use App\Http\Requests\Request;

class MessageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required',
            'mode' => 'required|integer|min:1|max:2',
            'course_part_ids.*' => 'integer|exists:course_parts,id',
            'school_id' => 'integer|exists:schools,id',
            'customer_ids' => 'required|array',
            'customer_ids.*' => 'required',
            'subject' => 'required_if:mode,1',
        ];
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $data = $this->all();

        $validator->after(function () use ($validator, $data) {
            if(!($this->customer_ids && is_array($this->customer_ids))) {
                $validator->errors()->add('error', 'Selected items not valid.');

                return ;
            }

            foreach ($this->customer_ids as $id) {
                if (key_exists('select_from', $data) && $data['select_from'] && $data['select_from'] === 'children') {
                    if (!Child::find($id)) {
                        $validator->errors()->add($id, "Child with id - {$id} not exists.");
                    }

                    continue;
                }

                if (!Customer::find($id)) {
                    $validator->errors()->add($id, "Customer with id - {$id} not exists.");
                }
            }
        });


        return $validator;
    }
}
