<?php

namespace App\Http\Requests\Admin\Customer\Child;

class Update extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->getRules();
        $rules['email'] = 'required|email';

        return $rules;
    }
}
