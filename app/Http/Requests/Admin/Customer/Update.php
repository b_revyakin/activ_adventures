<?php

namespace App\Http\Requests\Admin\Customer;

class Update extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->getRules(), [
            'email' => 'required|email|unique:users,email,' . $this->getParent()->user->id
        ]);
    }
}
