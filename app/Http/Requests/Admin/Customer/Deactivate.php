<?php

namespace App\Http\Requests\Admin\Customer;

class Deactivate extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->getParent()->isActive();
    }
}
