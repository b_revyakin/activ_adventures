<?php

namespace App\Http\Requests\Admin\School\Group;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->group ? $this->checkGroup() : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }
}
