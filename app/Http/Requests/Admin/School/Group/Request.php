<?php

namespace App\Http\Requests\Admin\School\Group;

use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkGroup();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Check school have group.
     *
     * @return mixed
     */
    public function checkGroup()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->school->groups()->find($this->group->id);
    }

    /**
     * @return array
     */
    public function getRules()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return [
            'name' => 'required|unique:school_groups,name,' . ($this->group ? $this->group->id : '')
        ];
    }
}
