<?php

namespace App\Http\Requests\Admin\School;

use App\Http\Requests\Request;

class Save extends Request
{
    private function schoolId()
    {
        return ($this->school) ? $this->school->id : null;
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|unique:schools,name,' . $this->schoolId(),
            'code' => 'string|unique:schools,code,' . $this->schoolId()
        ];
    }
}
