<?php

namespace App\Http\Requests\Admin\Category\Question;

use App\Http\Requests\Request as BaseRequest;
use Exception;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkQuestion();
    }

    protected function checkQuestion()
    {
        try {
            $question = $this->categories->questions()->findOrFail($this->questions->id);
        } catch (Exception $e) {
            return false;
        }

        return $question;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Category Or Question Not Found!');

        return redirect()->route('admin::categories.questions.index', $this->categories->id);
    }

    protected function getRules()
    {
        return [
            'title' => 'required|unique:questions,title'
        ];
    }
}
