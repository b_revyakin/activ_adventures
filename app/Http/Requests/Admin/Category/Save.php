<?php

namespace App\Http\Requests\Admin\Category;

use App\Http\Requests\Request;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categoryId = $this->categories ? $this->categories->id : '';

        return [
            'title' => 'required|unique:categories,title,' . $categoryId
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Category Not Found!');

        return redirect()->route('admin::categories.index');
    }
}
