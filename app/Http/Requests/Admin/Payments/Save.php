<?php

namespace App\Http\Requests\Admin\Payments;

use App\Http\Requests\Request;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'min:0.01',
                'max:' . $this->bookings->left_amount,
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/'
            ]
        ];
    }
}
