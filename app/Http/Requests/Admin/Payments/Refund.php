<?php

namespace App\Http\Requests\Admin\Payments;

class Refund extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkPayment() && $this->payments->isCompleted();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'This Payment Not Found For This Booking Or Payment Status Is Not Completed!');

        return back();
    }
}
