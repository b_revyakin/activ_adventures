<?php

namespace App\Http\Requests\Admin\Payments;

use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkPayment() && $this->payments->isProgressing();
    }

    protected function checkPayment()
    {
        return $this->bookings->payments()->find($this->payments->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'This Payment Not Found For This Booking Or Payment Status Is Not Progressing!');

        return back();
    }
}
