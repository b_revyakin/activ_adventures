<?php

namespace App\Http\Requests\Admin\Admins;

class Store extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->getRules(), [
            'email' => 'required|email|unique:users,email,' . $this->getUserId(),
            'password' => 'required|between:6,40|confirmed',
        ]);
    }
}
