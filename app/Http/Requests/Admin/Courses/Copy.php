<?php

namespace App\Http\Requests\Admin\Courses;


class Copy extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:courses,name'
        ];
    }
}
