<?php

namespace App\Http\Requests\Admin\Courses;

use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{

    private function courseId()
    {
        return ($this->courses) ? $this->courses : null;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }



    protected function getRules()
    {
        return [
            'name' => 'required|string|unique:courses,name,' . $this->courseId(),
            'venue_id' => 'exists:venues,id',
            'season_id' => 'required|exists:seasons,id',
            'schools' => 'required',
            'schools.*' => 'required|exists:schools,id',
            'school_groups.*' => 'exists:school_groups,id',
            'type' => 'required|in:Single,Multiple',
            'places' => 'required_if:type,Single|integer|min:0',
            'cost_per_course' => 'required_if:type,Single|numeric|min:0',
            'min_age' => 'required_if:type,Single|integer|between:3,25',
            'max_age' => 'required_if:type,Single|integer|between:3,25',
            'description' => 'required_if:type,Single|string',
            'start_date' => 'required_if:type,Single|date_format:"Y-m-d H:i:s"',
            'end_date' => 'required_if:type,Single|date_format:"Y-m-d H:i:s"',
            'files[]' => 'mimes:jpeg,jpg,png,gif|max:10240',
            'document' => 'mimes:pdf|max:10240',
        ];
    }
}
