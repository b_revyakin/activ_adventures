<?php

namespace App\Http\Requests\Admin\Courses\CourseParts;

use App\Entities\Course;
use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkPart();
    }

    protected function checkPart()
    {
        try {
            $course = Course::findOrFail($this->courses);
            $part = $course->parts->find($this->partId);
        } catch (\Exception $e) {
            return false;
        }

        return $part;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        return back()->withErrors('Server Error. Course Or Course Part Not Exists!');
    }

    protected function getRules()
    {
        return [
            'name' => 'required|unique:course_parts,name,' . $this->partId,
            'start_date' => 'required|date_format:"Y-m-d H:i:s"',
            'end_date' => 'required|date_format:"Y-m-d H:i:s"',
            'places' => 'required|integer|min:0',
            'cost_per_course' => 'required|numeric|min:0',
            'min_age' => 'required|integer|between:3,25',
            'max_age' => 'required|integer|between:3,25',
        ];
    }
}
