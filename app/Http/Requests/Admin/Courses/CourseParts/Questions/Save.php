<?php

namespace App\Http\Requests\Admin\Courses\CourseParts\Questions;

class Save extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }
}
