<?php

namespace App\Http\Requests\Admin\Courses\CourseParts\Questions;

use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkExists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getRules()
    {
        return [
            'text' => 'required'
        ];
    }

    public function checkExists()
    {
        return $this->getMethod() === 'PUT' ? $this->coursePart->questions()->find($this->question->id) : true;
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Course Part Have Not This Question!');

        return back();
    }
}
