<?php

namespace App\Http\Requests\Admin\Courses\CourseParts\Teams;

class Save extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:course_part_teams,name,'
                . ($this->team ? $this->team->id : 'NULL') . ',id,course_part_id,' . $this->coursePart->id,
        ];
    }
}
