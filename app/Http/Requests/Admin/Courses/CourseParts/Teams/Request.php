<?php

namespace App\Http\Requests\Admin\Courses\CourseParts\Teams;

use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->team ? $this->coursePart->teams()->find($this->team->id) : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Course Part Have Not This Team!');

        return back();
    }
}
