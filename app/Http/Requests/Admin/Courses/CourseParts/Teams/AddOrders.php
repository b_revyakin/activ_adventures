<?php

namespace App\Http\Requests\Admin\Courses\CourseParts\Teams;

use App\Http\Requests\Request;

class AddOrders extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id' => 'required|exists:course_part_teams,id,course_part_id,' . $this->coursePart->id,
            'order_ids' => 'required|array',
            'order_ids.*' => 'exists:orders,id'
        ];
    }
}
