<?php

namespace App\Http\Requests\Admin\Courses\CourseParts\Checkboxes;

use App\Http\Requests\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkDateExists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getRules()
    {
        return [
            'text' => 'required'
        ];
    }

    public function checkDateExists()
    {
        return $this->getMethod() === 'PUT' ? $this->coursePart->checkboxes()->find($this->checkbox->id) : true;
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Course Part Have Not This Checkbox!');

        return back();
    }
}
