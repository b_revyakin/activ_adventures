<?php

namespace App\Http\Requests\Admin\Courses;

use Auth;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();

        return true;

        /*if ($user->is('admin')) {
            return true;
        }

        return $user->staff->events()->where('id', $this->events)->exists();*/
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules();
    }
}
