<?php

namespace App\Http\Requests\Admin\Faq\Question;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->getMethod() === 'PUT' ? $this->checkQuestion() : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:faq_questions,title,' . ($this->question ? $this->question->id : 'NULL') . ',id,category_faq_id,' . $this->category->id,
            'answer' => 'required'
        ];
    }
}
