<?php

namespace App\Http\Requests\Admin\Faq\Question;

use App\Http\Requests\Request as BaseRequest;
use Exception;

class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkQuestion();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Category Or Question Not Found!');

        return redirect()->route('admin::faq.categories.questions.index', $this->category->id);
    }

    protected function checkQuestion()
    {
        try {
            $question = $this->category->questions()->findOrFail($this->question->id);
        } catch (Exception $e) {
            return false;
        }

        return $question;
    }
}
