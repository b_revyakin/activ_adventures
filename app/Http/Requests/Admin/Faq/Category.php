<?php

namespace App\Http\Requests\Admin\Faq;

use App\Http\Requests\Request;

class Category extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categoryId = $this->category ? $this->category->id : 'NULL';

        return [
            'title' => 'required|unique:category_faqs,title,' . $categoryId . ',id,course_part_id,' . $this->coursePart->id
        ];
    }

    public function forbiddenResponse()
    {
        session()->flash('error', 'Server Error. Category Not Found!');

        return redirect()->route('admin::faq.categories.index');
    }
}
