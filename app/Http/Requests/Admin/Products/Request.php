<?php

namespace App\Http\Requests\Admin\Products;

use App\Entities\Product;
use App\Http\Requests\Request as BaseRequest;

abstract class Request extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get requested product
     *
     * @return Product
     */
    protected function getProduct()
    {
        return Product::findOrFail($this->products);
    }

    protected function getRules()
    {
        return [
            'name' => 'required|string',
            'price' => 'required|numeric'
        ];
    }
}
