<?php

namespace App\Http\Requests\Customer\API\Booking;

use App\Entities\Child;
use App\Entities\CoursePart;
use App\Entities\Order;
use App\Http\Requests\Request;
use App\Services\CourseService;
use Auth;
use Exception;
use Log;

class Book extends Request
{
    /**
     * @var CourseService;
     */
    protected $courseService;

    /**
     * Book constructor.
     * @param CourseService $courseService
     */
    public function __construct(CourseService $courseService)
    {
        parent::__construct();

        $this->courseService = $courseService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(config('validation-rules.orders'), [
            'payment_type' => 'required|in:online,cheque', // also have cheque
            'coupon' => 'exists:coupons,phrase,active,1',
            'requested_register' => 'boolean'
        ]);
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        //Not need check available capacity if admin booking!
        if (Auth::user()->is('admin|staff')) {
            return $validator;
        }

        try {
            $data = $this->all();
            $validator->after(function () use ($validator, $data) {
                foreach ($this->orders as $orderData) {

                    $course = CoursePart::findOrFail($orderData['course_id']);
                    $courseName = $course->name ?: $course->course->name;

                    if (!$course->hasFreePlaces()) {
                        $validator->errors()->add('Not Places', 'Course ' . $courseName . ' have not free places');
                        break;
                    }

                    if ($course->required_passport && !$this->validPassportDetail($orderData['passport_detail'])) {
                        $validator->errors()->add('Passport Detail Fail', 'Passport Detail Is Required For Course' . $courseName);
                        break;
                    }

                    $child = Child::findOrFail($orderData['child_id']);

                    $childOrders = Order::where('child_id', $child->id)
                        ->whereHas('booking', function ($query) {
                            $query->active();
                        })->get();

                    foreach ($childOrders as $childOrder) {
                        if ($childOrder->coursePart->id === $course->id) {
                            $validator->errors()->add('Already a child booked', 'Already you booked your child ' . $child->fullname);
                            break;
                        }
                    }
                }

                if (array_key_exists('requested_register', $data) && $data['requested_register']) {
                    // Need check that it allow request register
                    if (count($this->orders) !== 1) {
                        $validator->errors()->add('Request Register Error', 'You can\'t request register for multiple event at same time.');
                        return;
                    }

                    $coursePart = CoursePart::findOrFail($data['orders'][0]['course_id']);
                    if (!$coursePart->allow_register) {
                        $validator->errors()->add('Request Register Error', 'For the event that you requesting register not allow this option.');
                        return;
                    }
                }
            });
        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            $validator->errors()->add('Server Error by validation your request. Please contact with administration.');

            return $validator;
        }

        return $validator;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validPassportDetail(array $data)
    {
        $valid = true;

        foreach ($data as $param) {
            if (!$param) {
                $valid = false;
                break;
            }
        }
        return $valid;
    }
}
