<?php

namespace App\Http\Requests\Customer\API\Children;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->isParentOfRequestedChild();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(config('validation-rules.child'), [
            'emergency_contacts.0.forename' => 'required',
            'emergency_contacts.0.surname' => 'required',
            'emergency_contacts.0.relationship' => 'required',
            'emergency_contacts.0.mobile' => 'required',
            'emergency_contacts.0.alternative_number' => '',

            'emergency_contacts.1.forename' => 'required',
            'emergency_contacts.1.surname' => 'required',
            'emergency_contacts.1.relationship' => 'required',
            'emergency_contacts.1.mobile' => 'required',
            'emergency_contacts.1.alternative_number' => '',
        ]);
        $rules['email'] = 'required|email';

        return $rules;
    }

}
