<?php

namespace App\Http\Requests\Customer\API\Children;

class Store extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(config('validation-rules.child'), [
            'second_contact.forename' => 'required',
            'second_contact.surname' => 'required',
            'second_contact.relationship' => 'required',
            'second_contact.mobile' => 'required',
            'second_contact.alternative_number' => '',

            'main_contact.forename' => 'required',
            'main_contact.surname' => 'required',
            'main_contact.relationship' => 'required',
            'main_contact.mobile' => 'required',
            'main_contact.alternative_number' => '',
        ]);
    }
}
