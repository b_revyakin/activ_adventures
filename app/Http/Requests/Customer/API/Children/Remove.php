<?php

namespace App\Http\Requests\Customer\API\Children;

class Remove extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->isParentOfRequestedChild();
    }
}
