<?php

namespace App\Http\Requests\Customer\Course;

use App\Http\Requests\Request;
use Auth;

class CourseShow extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $courseIds = Auth::user()->customer
            ->children()
            ->with('school.courses')
            ->get()
            ->pluck('school.courses.*.id')
            ->collapse()
            ->unique()
            ->toArray();

        return $this->course->isActive() && in_array($this->course->id, $courseIds);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function forbiddenResponse()
    {
        return redirect()->route('customer::courses.index')->withErrors(['This Course Not Found.']);
    }
}
