<?php

namespace App\Http\Requests\Customer\Payment;

use App\Http\Requests\Request;

class Stripe extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->checkAmount();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|boolean'
        ];
    }

    public function checkAmount()
    {
        if ($this->amount && $this->bookings->payments()->paid()->count()) {
            return false;
        }
        return true;
    }

    public function forbiddenResponse()
    {
        $this->session()->flash('error', 'You Can Only Make Full Payment!');

        return back();
    }
}
