<?php

namespace App\ActivAdventures\Modules;

use GuzzleHttp\Client;

class BulkSms implements Sms
{
    private $credentials;

    private $isLiveMode;

    /**
     * Meaning of response status codes.
     *
     * @var array
     */
    protected static $statusMessages = [
        0 => 'In progress',
        1 => 'Scheduled',
        10 => 'Delivered upstream',
        11 => 'Delivered mobile',
        12 => 'Delivered upstream unacknowledged (presume in progress)',
        22 => 'Internal fatal error',
        23 => 'Authentication error',
        24 => 'Data validation failed',
        25 => 'You do not have sufficient credits',
        26 => 'Upstream credits not available',
        27 => 'You have exceeded your daily quota',
        28 => 'Upstream quota exceeded',
        40 => 'Temporarily unavailable',
        201 => 'Maximum batch size exceeded',
    ];

    public function __construct()
    {
        $this->isLiveMode = strtolower(config('sms.api_mode')) === 'live';
        $this->credentials = [
            'username' => config('sms.username'),
            'password' => config('sms.password'),
        ];
    }

    /**
     * Send message to gateway
     *
     * @param $text
     * @param $phone
     * @return array
     */
    public function send($text, $phone)
    {
        $data = array_merge($this->credentials, [
            'message' => $text,
            'msisdn' => $phone
        ]);

        $client = new Client(['base_uri' => config('sms.base_url')]);

        $response = $client->request(
            $this->isLiveMode ? 'POST' : 'GET',
            'eapi/submission/send_sms/2/2.0',
            [
                ($this->isLiveMode ? 'form_params' : 'query') => $data
            ]
        );

        $data = explode("|", $response->getBody());
        $status = $data[0];
        $id = (isset($data[2])) ? $data[2] : 0;

        return ['status' => $status, 'id' => $id];
    }

    /**
     * Get status for batch id
     *
     * @param $batchId
     * @return mixed
     */
    public function status($batchId)
    {
        $data = array_merge($this->credentials, [
            'batch_id' => $batchId
        ]);

        $client = new Client(['base_uri' => config('sms.base_url')]);

        $response = $client->get('eapi/status_reports/get_report/2/2.0', ['query' => $data]);

        $status = explode("|", $response->getBody());

        return $status[0];
    }
}