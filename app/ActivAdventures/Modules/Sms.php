<?php

namespace App\ActivAdventures\Modules;

interface Sms
{
    /**
     * Send message, get response batch id
     *
     * @param $text
     * @param $phone
     * @return mixed
     */
    public function send($text, $phone);

    /**
     * Get status phone for batch id
     *
     * @param $batchId
     * @return mixed
     */
    public function status($batchId);
}
